/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef StepperGauge_h
#define StepperGauge_h

#include <Arduino.h>
#include "AccelStepper.h"
#include "Pins.h"
#include "StepperSettings.h"
#include "DisplayableItems.h"

// The interval rate of the stepper gauge to move one step
#define STEPPER_GAUGE_INTERVAL_REFRESH 50 // in microseconds

class StepperGauge {
  public:
    void begin();
    void update();
    void refreshSettings();

  private:
    void homing();

    uint16_t totalNumberOfSteps;
    volatile bool isFirstUpdate;
    uint8_t channel;
};

#endif