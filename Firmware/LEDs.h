/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef LEDS_H
#define LEDS_H

#include <Arduino.h>
#include "Pins.h"

class LEDs {
  private:
    bool leftLEDLit;
    bool rightLEDLit;

  public:
    void begin();

    void turnOnLeftLED();
    void turnOffLeftLED();
    void toggleLeftLED();
    bool isLeftLEDLit();

    void turnOnRightLED();
    void turnOffRightLED();
    void toggleRightLED();
    bool isRightLEDLit();
};

#endif