﻿/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef Page_t_h
#define Page_t_h

enum PageType {
  UNUSED, BASIC_READOUT_INDICATORS, ANALOG_GAUGES, HISTOGRAM, BAR_GRAPH, ZERO_SIXTY_WIDGET, DATE_TIME_WIDGET
};

// Used to save into EEPROM
struct PageSettings_t {
  PageType type;

  int16_t indicator1;
  int16_t indicator2;
  int16_t indicator3;
  int16_t indicator4;
  int16_t indicator5;
  int16_t indicator6;
  int16_t indicator7;
};

#endif
