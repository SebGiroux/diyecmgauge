#include "VehicleSettings.h"

VehicleSettings_t VehicleSettings::vehicleSettings;

/**
 * Set the vehicle settings object
 *
 * @param pVehicleSettings The new vehicle settings object to be used by this class
 */
void VehicleSettings::set(VehicleSettings_t pVehicleSettings) {
  vehicleSettings = pVehicleSettings;
}

/*
 * @return Get the current vehicle settings object
 */
VehicleSettings_t VehicleSettings::get() {
  return vehicleSettings;
}