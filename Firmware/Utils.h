/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef Utils_h
#define Utils_h

#include <WString.h>
#include "u8g_teensy.h"

class Utils {
  public:
    static String ftoa(float number, uint8_t precision);
    static bool endsWith(const char *str, const char *suffix);
    static uint16_t getXPositionToCenterInScreen(U8GLIB& display, String text);
};

#endif