#ifndef _U8G_TEENSY_H
#define _U8G_TEENSY_H

#include "libs/u8glib/u8glib.h"
#include "Pins.h"
 
//main com function. read on...
uint8_t u8g_com_hw_spi_fn(u8g_t *u8g, uint8_t msg, uint8_t arg_val, void *arg_ptr);
 
#endif