/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

/**
 * When writing to the serial port, it's hard to calculate a checksum because we're passing all sort of variable types (char, char array, string, float, etc) to
 * the Serial.print() command and those values get formatted by the Serial class, in a way that we're not aware of. That mean that we have no idea on the exact
 * bytes that get send to the serial port, therefore no way to calculate a checksum. This class extends the Print class (that the Serial class also extends) keep
 * track of the exact bytes that are sent to the serial port and calculate a checksum.
 *
 * The checksum is just the sum of all the bytes. It will most likely roll over, that's fine.
 */

#include "SerialWriterChecksum.h"

SerialWriterChecksum::SerialWriterChecksum(HardwareSerial *pSerial) {
  hwSerial = pSerial;
}

SerialWriterChecksum::SerialWriterChecksum(usb_serial_class *pSerial) {
  usbSerial = pSerial;
}

/**
 * Reset the checksum to 0. To be used before starting to send a response
 */
void SerialWriterChecksum::resetChecksum() {
  checksum = 0;
}

/**
 * @return The calculated checksum since the last reset
 */
uint8_t SerialWriterChecksum::getChecksum() {
  return checksum;
}

/**
 * Method that receive all the data from the inherited print() and println() methods
 *
 * @param data The written data
 * @return
 */
size_t SerialWriterChecksum::write(uint8_t data) {
  if (hwSerial != NULL) {
    hwSerial->write(data);
  }
  else if (usbSerial != NULL) {
    usbSerial->write(data);
  } 

  checksum += data;
  return 1;
}