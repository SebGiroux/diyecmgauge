#include "HIH6130.h"

uint8_t HIH6130::fetchHumidityTemperature(unsigned int *pHumidityData, unsigned int *pTemperatureData) {
  byte address, humidityHigh, humidityLow, temperatureHigh, temperatureLow, _status;
  unsigned int humidityData, temperatureData;
  address = 0x27;
  Wire.beginTransmission(address); 
  byte statusCode = Wire.endTransmission(I2C_NOSTOP);
  
  if (statusCode == 0) {
    Wire.requestFrom((int) address, (int) 4, I2C_STOP);
    humidityHigh = Wire.receive();
    humidityLow = Wire.receive();
    temperatureHigh = Wire.receive();
    temperatureLow = Wire.receive();
//    Wire.endTransmission(I2C_STOP);
    
    _status = (humidityHigh >> 6) & 0x03;
    humidityHigh = humidityHigh & 0x3f;
    humidityData = (((uint16_t) humidityHigh) << 8) | humidityLow;
    temperatureData = (((uint16_t) temperatureHigh) << 8) | temperatureLow;
    temperatureData = temperatureData / 4;
    *pHumidityData = humidityData;
    *pTemperatureData = temperatureData;
    
    return _status;
  }
  else {
    return 3;
  }
}

void HIH6130::read() {
  unsigned int humidityData, temperatureData;
  uint8_t _status = fetchHumidityTemperature(&humidityData, &temperatureData);
  if ((_status == 0) || (_status == 1)) {
    relativeHumidity = (float) humidityData * 6.10e-3;
    temperatureCelsius = (float) temperatureData * 1.007e-2 - 40.0;
  }
}

float HIH6130::getRelativeHumidity() {
  return relativeHumidity;
}

float HIH6130::getTemperatureCelsius() {
  return temperatureCelsius;
}
