// Core
#include <SPI.h>
#include "libs/SdFat/SdFat.h"
#include <EEPROM.h>
#include "I2C.h"
#include "RTC.h"

// Sensors
#include "Sensors.h"
#include "LSM303.h"
#include "HIH6130.h"
#include "TinyGPSPP.h"
#include "MPU9150.h"

// Display
#include "u8g_teensy.h"
#include "Histogram.h"
#include "BarGraph.h"
#include "AnalogGauge.h"
#include "PageItems.h"

// ECU
#include "ECU.h"
#include "MegasquirtCAN.h"

// Misc
#include "Alarms.h"
#include "Settings.h"
#include "DataLogging.h"
#include "SerialHandler.h"
#include "Dynamometer.h"
#include "LEDs.h"
#include "Buzzer.h"

// Stepper
#include "StepperGauge.h"

// Widgets
#include "WidgetZeroSixty.h"
#include "WidgetDateTime.h"

#define BUZZER_TONE_HZ 440
#define MAX_BASIC_READ_OUT_INDICATORS_PER_PAGE 7
#define MAX_BAR_GRAPHS_PER_PAGE 5
#define MAX_ANALOG_GAUGES_PER_PAGE 4
#define TIME_BEFORE_SHOWING_ECU_COMMS_ERROR 2000 // in ms
#define DELAY_BEFORE_INITIALIZING_DATA_LOGGING 1000 // in ms

ECU* ecu;
DataLogging dataLogging;
LEDs leds;
Buzzer buzzer;
Alarms alarms;
Sensors sensors;
U8GLIB display(&u8g_dev_ssd1351_128x128_16x_332_hw_spi, u8g_com_hw_spi_fn);
RTC rtc;
Dynamometer dynamometer;
StepperGauge stepperGauge;

// Widgets
WidgetZeroSixty widgetZeroSixty(&display, &sensors);
WidgetDateTime widgetDateTime(&display, &rtc);

// Paging
volatile uint8_t currentPageNumber = 0;
volatile uint32_t lastButtonsInterruptTime = 0;
const uint8_t DEBOUNCE_TIME = 80;

bool eepromEmpty = false;
bool sdInit = false;

/**
 * Entry point where we start the communication with all the hardware
 */
void setup() {
  // Fetch pages, alarms and settings data from EEPROM
  Settings::fetchAll();

  // Initialize the ECU
  ecu = new MegasquirtCAN();
  ecu->begin();

  DisplayableItems::initializeItems();
  
  // Initialize the alarms
  alarms.begin(&dataLogging, &leds, &buzzer);

  // Initialize the RTC - Set the Time library to use Teensy 3.0's RTC to keep time
  setSyncProvider(getTeensy3Time);
  rtc.begin();

  GeneralSettings_t generalSettings = GeneralSettings::get();

  Serial.begin(USB_SERIAL_BPS);
  if (generalSettings.configurationSerialPortOnMainConnector) {
    Serial1.begin(SERIAL_MAIN_CONNECTOR_BPS);
  }

  // Initialize the LEDs
  leds.begin();

  // Initialize the buzzer
  buzzer.begin();
  
  // Initialize the LSM303 and HIH6130
  sensors.begin();
  
  // Changing pages
  pinMode(DISPLAY_NEXT_PAGE_PIN, INPUT);
  pinMode(DISPLAY_PREVIOUS_PAGE_PIN, INPUT);
  
  // Attach the interrupt to the previous / next page buttons pins
  attachInterrupt(DISPLAY_NEXT_PAGE_PIN, changePageButton, FALLING);
  attachInterrupt(DISPLAY_PREVIOUS_PAGE_PIN, changePageButton, FALLING);
  
  // Set the right date / time when manipulating files on the SD card
  SdFile::dateTimeCallback(dateTime);

  eepromEmpty = Settings::isEEPROMEmpty();
}

/**
 * Main page display function that display the right page based on the current page number variable
 */
Histogram histogram(&display);
BarGraph barGraph(&display);
AnalogGauge analogGauge(&display);
void displayPage() {
  PageSettings_t page = PageItems::getPageAtIndex(currentPageNumber);
  int16_t indicators[] = { page.indicator1, page.indicator2, page.indicator3, page.indicator4, page.indicator5, page.indicator6, page.indicator7 };

  switch (page.type) {
    case BASIC_READOUT_INDICATORS:
      displayBasicReadOutIndicatorsPage(indicators);
      break;

    case ANALOG_GAUGES:
      displayAnalogGaugesIndicatorsPage(indicators);
      break;

    case HISTOGRAM:
      displayHistogramPage(page.indicator1);
      break;

    case BAR_GRAPH:
      displayBarGraphPage(indicators);
      break;

    case ZERO_SIXTY_WIDGET:
      displayZeroSixtyWidgetPage();
      break;

    case DATE_TIME_WIDGET:
      displayDateTimeWidgetPage();
      break;

    case UNUSED:
      break;
  }
}

/**
 * Main loop
 */
void loop() {
  if (eepromEmpty) {
    // No settings loaded yet, show a message on the screen
    displayNoConfigLoaded();

    // Check if EEPROM is still empty, a new configuration could have been uploaded over serial
    eepromEmpty = Settings::isEEPROMEmpty();
  }
  else if (ecu->isConfigError()) {
    // ECU reporting a config error, show a message on the screen
    displayECUConfigError();

    // Request data from ECU
    ecu->requestData();
  }
  else {
    // Request data from ECU
    bool success = ecu->requestData();
    if (success) {
      // Refresh LSM303/HIH6130/GPS data
      sensors.refreshSensorsData();
      
      // Check alarm, display either alarm page or regular page as required
      alarms.check();
      if (alarms.getNbTriggeredAlarms(true) > 0) {
        alarms.displayAlarmPage(display);
      }
      else {
        displayPage();
      }

      // Feed VSS data to dynanometer
      dynamometer.feedVSSData();
      
      // If data logging is enabled, write a wrote in the data log
      dataLogging.writeRow();
    }
    else {
      displayECUCommunicationError();
    }
  }
  
  // Check if we've received anything over the serial line(s)
  SerialHandler::checkSerial();
  
  // Wait a little before initializing the data logging object. When it is done at the beginning, the display shows all grey for a second for some reasons... They share the same SPI bus so maybe there is some sort of conflicts ?
  if (!sdInit && millis() > DELAY_BEFORE_INITIALIZING_DATA_LOGGING) {
    // Initialize stepper gauge
    stepperGauge.begin();
    
    // Initialize the data logging
    dataLogging.begin(ecu);
    sdInit = true;
  }
}

/**
 * Should be called when there was no settings loaded into the EEPROM so that a message can be shown to the user
 */
void displayNoConfigLoaded() {
  display.firstPage();
  do {
    display.setFontPosTop();
    display.setFont(u8g_font_helvB08);
    display.setRGB(255, 255, 255);

    display.setPrintPos(0, 0);
    display.print(F("Please upload your"));
    display.setPrintPos(0, 13);
    display.print(F("settings to the gauge"));
    display.setPrintPos(0, 26);
    display.print(F("using the Android or"));
    display.setPrintPos(0, 39);
    display.print(F("desktop application."));
  } while (display.nextPage());
}

/**
 * Should be called when the ECU is reporting a config error so that a message can be shown to the user
 */
void displayECUConfigError() {
  display.firstPage();
  do {
    display.setFontPosTop();
    display.setFont(u8g_font_helvB08);
    display.setRGB(255, 255, 255);

    display.setPrintPos(0, 0);
    display.print(F("Your ECU is reporting"));
    display.setPrintPos(0, 13);
    display.print(F("a config error. Please"));
    display.setPrintPos(0, 26);
    display.print(F("fix  it and power cycle"));
    display.setPrintPos(0, 39);
    display.print(F("your ECU."));
  } while (display.nextPage());
}

/**
 * Should be called when the communication with the ECU cannot be completed
 */
 void displayECUCommunicationError() {
  // Don't display this message for the first few seconds as it
  // could be just because we're booting faster than the ECU
  if (millis() > TIME_BEFORE_SHOWING_ECU_COMMS_ERROR) {
    display.firstPage();
    do {
      display.setFontPosTop();
      display.setFont(u8g_font_helvB08);
      display.setRGB(255, 255, 255);

      display.setPrintPos(0, 0);
      display.print(F("There is an issue"));
      display.setPrintPos(0, 13);
      display.print(F("communicating with your"));
      display.setPrintPos(0, 26);
      display.print(F("ECU. Please fix the"));
      display.setPrintPos(0, 39);
      display.print(F("connection between your"));
      display.setPrintPos(0, 52);
      display.print(F("ECU and the gauge and"));
      display.setPrintPos(0, 65);
      display.print(F("try again."));
    } while (display.nextPage());
  }
 }

/**
 * Get the time from the Teensy RTC
 */
time_t getTeensy3Time() {
  return Teensy3Clock.get();
}

/**
 * Display a basic read out indicator
 *
 * @param index The index in the displayable items structure
 * @param y The y position of the indicator on the screen
 */
void displayBasicReadOutIndicator(int16_t index, uint8_t y) {
  DisplayableItem_t item = DisplayableItems::getItemAtIndex(index);

  display.setFontPosTop();
  display.setPrintPos(0, y);
  display.setFont(u8g_font_helvB10);
  display.setRGB(255, 255, 255);

  display.print(item.title);
  display.print(F(":"));

  String value = DisplayableItems::getItemValueByItem(item);
  // Align the value to the right of the screen
  char valueArray[value.length() + 1];
  value.toCharArray(valueArray, value.length() + 1);
  char unitsArray[item.units.length() + 1];
  item.units.toCharArray(unitsArray, item.units.length() + 1);

  display.setPrintPos(display.getWidth() - display.getStrWidth(valueArray) - display.getStrWidth(unitsArray), y);
  display.print(value);

  if (item.units != "") {
    display.print(item.units);
  }
}

/**
 * Display a page with one to MAX_BASIC_READ_OUT_INDICATORS_PER_PAGE read out indicators
 *
 * @param index The inxdexes of the DisplayableItem to display
 */
void displayBasicReadOutIndicatorsPage(int16_t index[]) {
  display.firstPage();
  do {
    uint8_t y = 0;

    for (uint8_t i = 0; i < MAX_BASIC_READ_OUT_INDICATORS_PER_PAGE; i++) {
      if (index[i] > -1) {
        displayBasicReadOutIndicator(index[i], y);
      }

      y += 19;
    }
  } while (display.nextPage());
}

/**
 * Display a page with one to MAX_ANALOG_GAUGES_PER_PAGE analog gauges
 *
 * @param index The inxdexes of the DisplayableItem to display
 */
void displayAnalogGaugesIndicatorsPage(int16_t index[]) {
  uint8_t pageSize = 0;

  for (uint8_t i = 0; i < MAX_ANALOG_GAUGES_PER_PAGE; i++) {
    if (index[i] > -1) {
      pageSize++;
    }
  }

  display.firstPage();
  do {
    for (uint8_t i = 0; i < MAX_ANALOG_GAUGES_PER_PAGE; i++) {
      if (index[i] > -1) {
          DisplayableItem_t item = DisplayableItems::getItemAtIndex(index[i]);
          analogGauge.drawAnalogGauge(item, i, pageSize);
      }
    }
  } while (display.nextPage());
}

/**
 * Display a page with the zero sixty widget
 */
void displayZeroSixtyWidgetPage() {
  widgetZeroSixty.displayWidget();
}

/**
 * Display a page with the date / time widget
 */
void displayDateTimeWidgetPage() {
  widgetDateTime.displayWidget();
}

/**
 * Display an histogram page
 *
 * @param index The index of the displayable item to display for this histogram
 */
void displayHistogramPage(int16_t index) {
  display.firstPage();
  do {
    if (index > -1) {
      DisplayableItem_t item = DisplayableItems::getItemAtIndex(index);

      histogram.addValue(DisplayableItems::getItemFloatValueByIndex(index));
      histogram.drawHistogram(item);
    }
  } while (display.nextPage());
}

/**
 * Display a bar graph page
 *
 * @param index The indexes of the displayable item to display on this page
 */
void displayBarGraphPage(int16_t index[]) {
  uint8_t pageSize = 0;

  for (uint8_t i = 0; i < MAX_BAR_GRAPHS_PER_PAGE; i++) {
    if (index[i] > -1) {
      pageSize++;
    }
  }

  display.firstPage();
  do {
    for (uint8_t i = 0; i < MAX_BAR_GRAPHS_PER_PAGE; i++) {
      if (index[i] > -1) {
          DisplayableItem_t item = DisplayableItems::getItemAtIndex(index[i]);
          barGraph.drawBarGraph(item, i, pageSize);
      }
    }
  } while (display.nextPage());
}

/**
 * Function triggered by the interrupt that read the digital pin of the push button and check if it's pressed or not.
 * Implements software debouncing to avoid multiple triggers on a noisy edge transition.
 */
void changePageButton() {
  uint32_t interruptTime = millis();

  if ((interruptTime - lastButtonsInterruptTime) > DEBOUNCE_TIME) {
    uint8_t nextPage = digitalRead(DISPLAY_NEXT_PAGE_PIN);
    uint8_t previousPage = digitalRead(DISPLAY_PREVIOUS_PAGE_PIN);

    bool pageChanged = false;

    // If next page button is pressed
    if (nextPage == LOW) {
      resetWidgetZeroSixtyTime();
      showNextPage();
      pageChanged = true;
    }

    // If previous page button is pressed
    if (previousPage == LOW) {
      resetWidgetZeroSixtyTime();
      showPreviousPage();
      pageChanged = true;
    }

    if (pageChanged) {
      SerialHandler::sendDebug(F("Displaying page "));
      SerialHandler::sendDebug(currentPageNumber);
    }
  }

  lastButtonsInterruptTime = interruptTime;
}

/**
 * Reset the start time of the 0-60 widget
 */
void resetWidgetZeroSixtyTime() {
  widgetZeroSixty.setWidgetStartTime(millis());
}

/**
 * Change the display to the previous page
 */
void showPreviousPage() {
  // We roll over to the last page when we've reached the first one
  if (currentPageNumber == 0) {
    currentPageNumber = PageItems::getNbUsedPages() - 1;
  }
  else {
    currentPageNumber--;
  }
}

/**
 * Change the display to the next page
 */
void showNextPage() {
  // Once we reach the limit of pages, we go back to the first one
  if (currentPageNumber == PageItems::getNbUsedPages() - 1) {
    currentPageNumber = 0;
  }
  else {
    currentPageNumber++;
  }
}

/**
 * Callback function called when the SD library need a timestamp (Example: file creation timestamp).
 */
void dateTime(uint16_t* date, uint16_t* time) {
  tmElements_t tm = rtc.getTime();
  
  // Return date using FAT_DATE macro to format fields
  *date = FAT_DATE(tmYearToCalendar(tm.Year), tm.Month, tm.Day);

  // Return time using FAT_TIME macro to format fields
  *time = FAT_TIME(tm.Hour, tm.Minute, tm.Second);
}