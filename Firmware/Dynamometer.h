/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef Dynamometer_h
#define Dynamometer_h

#include <Arduino.h>
#include "UnitsConversion.h"
#include "DisplayableItems.h"
#include "GeneralSettings.h"
#include "VehicleSettings.h"

#define GRAVITATIONAL_ACCELERATION 9.80665 // meters / second²

// One horse power is the equivalent of lifting 550 pounds one foot in one second or 33000 lb-ft/min
#define ONE_HORSEPOWER_LIFT_POUNDS 550

#define ACCELERATION_IN_G_AT_SEA_LEVEL 32.174 // ft/sec^2

#define DRY_AIR_GAS_CONSTANT 287.05

#define AVERAGE_SEA_LEVEL_PRESSURE 101.325

#define NB_DATA_SAMPLES 14 // this should always be an even number

class Dynamometer {
  public:
    Dynamometer();
    double getNetHorsePower();
    double getNetTorque();
    double getAerodynamicDrag(uint16_t speedMph);
    double getRollingResistance(uint16_t speedMph);
    double getAirDensity();
    void feedVSSData();
    void setVehicleWeight(uint16_t pVehicleWeight);
    void setCoefficientOfDrag(double pCoefficientOfDrag);
    void setCoefficientOfRollingResistance(double pCoefficientOfRollingResistance);
    void setFrontalArea(double pFrontalArea);
    void refreshVehicleSettings();

  private:
    float speedValues[NB_DATA_SAMPLES];
    uint8_t nbSpeedSamples;

    uint32_t timeValues[NB_DATA_SAMPLES];

    uint16_t rpmValues[NB_DATA_SAMPLES];

    uint16_t vehicleWeight;
    double frontalArea;

    double coefficientOfDrag;
    double coefficientOfRollingResistance;

    double getHorsePower();
    double getTorque();
    double getCurrentMph();
};

#endif