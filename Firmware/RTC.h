/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef RTC_h
#define RTC_h

#include <Arduino.h>
#include "Time.h"

#define NB_MONTHS_IN_YEAR 12

class RTC {
  public:
    void begin();
    tmElements_t getTime();
    
  private:
    void setRTCTime();
    boolean getTime(const char *str);
    boolean getDate(const char *str);
};

#endif
