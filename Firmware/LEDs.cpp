/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "LEDs.h"

/**
 * Prepare the LEDs outputs
 */
void LEDs::begin() {
  pinMode(LED_LEFT_PIN, OUTPUT);
  pinMode(LED_RIGHT_PIN, OUTPUT);
}

/**
 * Turn on the left LED
 */
void LEDs::turnOnLeftLED() {
  digitalWrite(LED_LEFT_PIN, HIGH);
  leftLEDLit = true;
}

/**
 * Turn off the left LED
 */
void LEDs::turnOffLeftLED() {
  digitalWrite(LED_LEFT_PIN, LOW);
  leftLEDLit = false;
}

/**
 * Toggle the left LED (turn it off if it's on, turn if on if it's off)
 */
void LEDs::toggleLeftLED() {
  if (leftLEDLit) {
    turnOffLeftLED();
  }
  else {
    turnOnLeftLED();
  }
}

/**
 * @return true if the left LED is currently lit, false otherwise
 */
bool LEDs::isLeftLEDLit() {
  return leftLEDLit;
}

/**
 * Turn on the right LED
 */
void LEDs::turnOnRightLED() {
  digitalWrite(LED_RIGHT_PIN, HIGH);
  rightLEDLit = true;
}

/**
 * Turn off the right LED
 */
void LEDs::turnOffRightLED() {
  digitalWrite(LED_RIGHT_PIN, LOW);
  rightLEDLit = false;
}

/**
 * Toggle the right LED (turn it off if it's on, turn if on if it's off)
 */
void LEDs::toggleRightLED() {
  if (rightLEDLit) {
    turnOffRightLED();
  }
  else {
    turnOnRightLED();
  }
}

/**
 * @return true if the right LED is currently lit, false otherwise
 */
bool LEDs::isRightLEDLit() {
  return rightLEDLit;
}