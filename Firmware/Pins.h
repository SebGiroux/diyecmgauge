/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef PINS_H
#define PINS_H

  // Buzzer
  #define BUZZER_PIN 23

  // SD Card
  #define SD_CD_PIN 9
  #define SD_CS_PIN 10

  // LEDs (above screen)
  #define LED_LEFT_PIN 15
  #define LED_RIGHT_PIN 14

  // Change page buttons
  #define DISPLAY_NEXT_PAGE_PIN 16
  #define DISPLAY_PREVIOUS_PAGE_PIN 17

  // Screen
  #define SCREEN_DC 20
  #define SCREEN_CS 2
  #define SCREEN_RESET 21

  // Stepper
  #define STEPPER_1 5
  #define STEPPER_2 6

  // Output
  #define PWM_OUPUT 22

#endif
