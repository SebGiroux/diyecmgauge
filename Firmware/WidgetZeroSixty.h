/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef WidgetZeroSixty_h
#define WidgetZeroSixty_h

#include <Arduino.h>
#include "u8g_teensy.h"
#include "Sensors.h"
#include "SerialHandler.h"

#define DELAY_BETWEEN_TIME_FLASHES 400 // ms

class WidgetZeroSixty {
  public:
    WidgetZeroSixty(U8GLIB *display, Sensors *sensors);
    void setWidgetStartTime(unsigned long startTime);
    void displayWidget();

  private:
    U8GLIB *display;
    Sensors *sensors;

    unsigned long zeroSixtyStartTime = 0;
    double elapsedTime = 0;
    unsigned long flashTimeStart = 0;
    unsigned long widgetStartTime;
    bool shouldDisplayTime = true;

    unsigned long getElapsedTime();
};

#endif