/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef UnitsConversion_h
#define UnitsConversion_h

#include <Arduino.h>

#define POUND_PER_NEWTON 0.22481
#define POUND_FORCE_MPH_PER_HP 375
#define KG_PER_LB 0.45359237
#define MILLISECOND_PER_SECOND 1000
#define KILOMETER_PER_MILE 1.60935
#define FEET_PER_SECOND_IN_ONE_MILE_PER_HOUR 1.46666667

// At speeds below 5252 RPM, the torque is always greater than horsepower; above 5252; torque is always less than horse power.
// On a dyno graph, HP and torque will always cross at 5252 RPM
#define RPM_TORQUE_CROSS 5252


class UnitsConversion {
  public:
    static double getPoundsFromNewtons(double newtons);
    static double getSquareMetersFromSquareFeet(double squareMeters);
    static double getMeterSecondFromMilePerHour(double mph);
    static double getHorsePowerFromTorque(double torque, uint16_t rpm);
    static double getHorsePowerFromPounds(double pounds, uint16_t speedMph);
    static double getTorqueFromHorsePower(double horsePower, uint16_t rpm);
    static double getKgInLbs(double lbs);
    static double getSecondsFromMilliseconds(uint32_t milliseconds);
    static double getMphFromKmh(double kmh);
    static double getFpsFromMph(double mph);
    static double getKelvinFromCelsius(double celsius);
    static double getPaFromKpa(double kpa);
};

#endif