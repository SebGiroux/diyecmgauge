/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "UnitsConversion.h"

/**
 * Convert Newtons into pounds
 *
 * @param newtons
 *
 * @return
 */
double UnitsConversion::getPoundsFromNewtons(double newtons) {
  return newtons * POUND_PER_NEWTON;
}

/**
 * Convert square feet in square meters
 *
 * @param squareMeters The number in square meters to be converted
 *
 * @return
 */
double UnitsConversion::getSquareMetersFromSquareFeet(double squareMeters) {
  return squareMeters * 0.09290304;
}

/**
 * Convert mile per hour in meter per second
 *
 * @param mph The number in miles per hour to be converted
 *
 * @return
 */
double UnitsConversion::getMeterSecondFromMilePerHour(double mph) {
  return mph * 0.44704;
}

/**
 * Get the horse power from the torque at the specified Engine RPM
 *
 * @param torque The torque value (in ft / lbs) to be converted
 * @param rpm The engine RPM
 *
 * @return The calculated horse power
 */
double UnitsConversion::getHorsePowerFromTorque(double torque, uint16_t rpm) {
  return torque * rpm / RPM_TORQUE_CROSS;
}

/**
 * Get the torque from the horse power at the specified Engine RPM
 *
 * @param horsePower The horsepower value to be converted
 * @param rpm The engine RPM
 *
 * @return The calculated torque
 */
double UnitsConversion::getTorqueFromHorsePower(double horsePower, uint16_t rpm) {
  return RPM_TORQUE_CROSS * horsePower / rpm;
}

/**
 * Get the horse power from the pounds at the specified vehicule speed
 *
 * @param pounds The number of pounds to be converted
 * @param speedMph The vehicule speed (in miles per hour)
 *
 * @return The horse power from the pounds and speed
 */
double UnitsConversion::getHorsePowerFromPounds(double pounds, uint16_t speedMph) {
  return pounds * speedMph / POUND_FORCE_MPH_PER_HP;
}

/**
 * Get the kilograms from the lbs
 *
 * @param lbs The number of lbs to be converted
 *
 * @return The equivalent number of kilograms
 */
double UnitsConversion::getKgInLbs(double lbs) {
  return lbs * KG_PER_LB;
}

/**
 * Get the number of seconds from a number of milliseconds
 *
 * @param milliseconds The number of milliseconds to be converted
 *
 * @return The equivalent number of seconds
 */
double UnitsConversion::getSecondsFromMilliseconds(uint32_t milliseconds) {
  return milliseconds / (double) MILLISECOND_PER_SECOND;
}

/**
 * Get number of miles per hour from a number of kilometers per hour
 *
 * @param kmh The number of kilometers per hour to be converted
 *
 * @return The equivalent number of miles per hour
 */
double UnitsConversion::getMphFromKmh(double kmh) {
  return kmh / KILOMETER_PER_MILE;
}

/**
 * Get number of feet per seconds from a number of miles per hour
 *
 * @param mph The number of miles per hour to be converted
 *
 * @return The equivalent number of feet per second
 */
double UnitsConversion::getFpsFromMph(double mph) {
  return mph * FEET_PER_SECOND_IN_ONE_MILE_PER_HOUR;
}

/**
 * Get number of Kelvins from a number of celsius
 *
 * @param celsius The number of celsius to be converted
 *
 * @return The equivalent number of Kelvins
 */
double UnitsConversion::getKelvinFromCelsius(double celsius) {
  return celsius + 273.15;
}

/**
 * Get number of pascal from a number of kilopascal
 *
 * @param kpa The number of kilopascal to be converted
 *
 * @return The equivalent number of pascal
 */
double UnitsConversion::getPaFromKpa(double kpa) {
  return kpa * 1000;
}