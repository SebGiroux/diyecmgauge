/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "Dynamometer.h"

Dynamometer::Dynamometer() {
  refreshVehicleSettings();
}

/**
 * Get the areodynamic drag HP for the specified parameters
 * The formula is: frontalArea / 2 * Cd * densityOfair * speed^2 and the result will be in Newtons
 *
 * @param speedMph The speed of the vehicle (in miles per hour)
 *
 * @return The HP used by aerodynamic drag
 */
double Dynamometer::getAerodynamicDrag(uint16_t speedMph) {
  // Calculate newtons for the specified parameters
  double newtons = UnitsConversion::getSquareMetersFromSquareFeet(frontalArea) / 2 * coefficientOfDrag * getAirDensity() * pow(UnitsConversion::getMeterSecondFromMilePerHour(speedMph), 2);

  // Convert newtons to pounds
  double pounds = UnitsConversion::getPoundsFromNewtons(newtons);

  // Convert pounds to horse power
  return UnitsConversion::getHorsePowerFromPounds(pounds, speedMph);
}

/**
 * Get the rolling resistance HP for the specified parameters
 * The formula is: coefficientOfRollingResistance * weightKg * Gravitational acceleration and the result will be in Newtons
 *
 * @param speedMph The speed of the vehicle (in miles per hour)
 *
 * @return The HP used by rolling resistance
 */
double Dynamometer::getRollingResistance(uint16_t speedMph) {
  uint16_t weightKg = UnitsConversion::getKgInLbs(vehicleWeight);

  double newtons = coefficientOfRollingResistance * weightKg * GRAVITATIONAL_ACCELERATION;

   // Convert newtons to pounds
  double pounds = UnitsConversion::getPoundsFromNewtons(newtons);

  // Convert pounds to horse power
  return UnitsConversion::getHorsePowerFromPounds(pounds, speedMph);
}

/**
 * Function that calculated horse power based on VSS data, velocity and vehicle speed
 *
 * @return The horse power
 */
double Dynamometer::getHorsePower() {
  double feetPerSecond = UnitsConversion::getFpsFromMph((speedValues[NB_DATA_SAMPLES - 1] + speedValues[NB_DATA_SAMPLES - 2]) / 2);
  double feetPerSecond2 = UnitsConversion::getFpsFromMph((speedValues[0] + speedValues[1]) / 2);
  double timeDelta = UnitsConversion::getSecondsFromMilliseconds(timeValues[NB_DATA_SAMPLES - 1] - timeValues[0]);
  double hp = (vehicleWeight / ACCELERATION_IN_G_AT_SEA_LEVEL) * (feetPerSecond - feetPerSecond2) / timeDelta * feetPerSecond / ONE_HORSEPOWER_LIFT_POUNDS;

  return hp;
}

/**
 * @return The torque calculated from the HP based on the average RPM of the samples
 */
double Dynamometer::getTorque() {
  uint16_t averageRpm = (rpmValues[NB_DATA_SAMPLES - 1] + rpmValues[0]) / 2;

  return UnitsConversion::getTorqueFromHorsePower(getHorsePower(), averageRpm);
}

/**
 * @return The net horse power, including aerodynamic drag and rolling resistance
 */
double Dynamometer::getNetHorsePower() {
  uint16_t averageSpeed = (speedValues[NB_DATA_SAMPLES - 1] + speedValues[0]) / 2;

  return max(0, getHorsePower() + getAerodynamicDrag(averageSpeed) + getRollingResistance(averageSpeed));
}

/**
 * @return The net torque calculated from the horse power, including aerodynamic drag and rolling resistance
 */
double Dynamometer::getNetTorque() {
  uint16_t averageRpm = (rpmValues[NB_DATA_SAMPLES - 1] + rpmValues[0]) / 2;

  return max(0, UnitsConversion::getTorqueFromHorsePower(getNetHorsePower(), averageRpm));
}

/**
 * @return The last VSS value we got from the ECU (in mile per hour)
 */
double Dynamometer::getCurrentMph() {
  return speedValues[nbSpeedSamples];
}

/**
 * The density of dry air can be calculated using the ideal gas law, expressed as a function of temperature and pressure:
 * Pdry air = p / (R*T) where:
 *
 *   Pdray air = Density of dry air (kg/m3)
 *   p = Air pressure (Pa)
 *   R = Specific gas constant for dry air, 287.05J/(kg.K)
 *   T = Temperature (K)
 */
double Dynamometer::getAirDensity() {
  extern Sensors sensors;

  return UnitsConversion::getPaFromKpa(AVERAGE_SEA_LEVEL_PRESSURE) / (DRY_AIR_GAS_CONSTANT * (UnitsConversion::getKelvinFromCelsius(sensors.getHIH().getTemperatureCelsius())));
}

/**
 * Feed the VSS data structures with the relevant value from the VSS
 */
void Dynamometer::feedVSSData() {
  GeneralSettings_t generalSettings = GeneralSettings::get();
  
  String itemTitle = "SPD";
  if (generalSettings.speedSourceDynamometer == ECU_VSS) {
    itemTitle = "VSS";
  }
  DisplayableItem_t item = DisplayableItems::getItemByTitle(itemTitle);

  float speedValue = DisplayableItems::getItemFloatValueByItem(item);

  // We need to feed this class with MPH data, so convert if required
  if (generalSettings.speedUnit == KMH) {
    speedValue = UnitsConversion::getMphFromKmh(speedValue);
  }

  item = DisplayableItems::getItemByTitle("RPM");
  uint16_t rpm = (uint16_t) DisplayableItems::getItemFloatValueByItem(item);

  // If we haven't reached the limit of the array
  if (nbSpeedSamples < NB_DATA_SAMPLES) {
    speedValues[nbSpeedSamples++] = speedValue;
    timeValues[nbSpeedSamples] = millis();
    rpmValues[nbSpeedSamples] = rpm;
  }
  // Otherwise we shift all values and replace the last one with the new value
  else {
    uint8_t i;
    for (i = 0; i < NB_DATA_SAMPLES - 1; i++) {
      speedValues[i] = speedValues[i + 1];
      timeValues[i] = timeValues[i + 1];
    }
    
    speedValues[i] = speedValue;
    timeValues[i] = millis();
    rpmValues[i] = rpm;
  }
}

/**
 * Set the vehicle weight to be used in a lot of computations
 *
 * @param pVehicleWeight The vehicle weight (in lbs - Don't forget to include the weight of the driver / passengers, cargo & fuel)
 */
void Dynamometer::setVehicleWeight(uint16_t pVehicleWeight) {
  vehicleWeight = pVehicleWeight;
}

/**
 * Set the coefficient of drag of the vehicle
 *
 * @param pCoefficientOfDrag Coefficient of drag. It can be determined through coastdown testing.
 */
void Dynamometer::setCoefficientOfDrag(double pCoefficientOfDrag) {
  coefficientOfDrag = pCoefficientOfDrag;
}

/**
 * Set the coefficient of rolling resistance of the vehicle
 *
 * @param pCoefficientOfRollingResistance Also called rolling drag. A value of 0.006 to 0.010 represents low rolling resistance tires on a smooth surface; 0.010 to 0.015 represents ordinary car tires on concrete (see Wikipedia for other sample values). You can also calculate your CRR experimentally.
 */
void Dynamometer::setCoefficientOfRollingResistance(double pCoefficientOfRollingResistance) {
  coefficientOfRollingResistance = pCoefficientOfRollingResistance;
}

/**
 * Set the frontal area of the vehicle
 *
 * @param pFrontalArea Projected vehicle area as seen from either the front or rear (in square feet).
 */
void Dynamometer::setFrontalArea(double pFrontalArea) {
  frontalArea = pFrontalArea;
}

/**
 * Refresh the vehicle settings used in this class by the latest version from the vehicle settings class
 */
void Dynamometer::refreshVehicleSettings() {
  VehicleSettings_t vehicleSettings = VehicleSettings::get();

  setVehicleWeight(vehicleSettings.vehicleWeight);
  setCoefficientOfDrag(vehicleSettings.coefficientOfDrag);
  setCoefficientOfRollingResistance(vehicleSettings.coefficientOfRollingResistance);
  setFrontalArea(vehicleSettings.frontalArea);
}