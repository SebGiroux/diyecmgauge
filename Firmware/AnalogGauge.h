/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef ANALOGGAUGE_H
#define ANALOGGAUGE_H

#include "u8g_teensy.h"

#include "DisplayableItems.h"

#define TICKS_START_ANGLE -225
#define TICKS_END_ANGLE 70

class AnalogGauge {
  public:
    AnalogGauge(U8GLIB *pDisplay);

    void drawAnalogGauge(DisplayableItem_t displayableItem, uint8_t gaugeIndex, uint8_t pageSize);

  private:
    U8GLIB *display;

    void drawTicks(uint16_t x, uint16_t y, uint16_t radius);
    void drawValue(DisplayableItem_t displayableItem, uint16_t x, uint16_t y, uint16_t radius);
};

#endif

