/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "Buzzer.h"

/**
 * Prepare the buzzer output
 */
void Buzzer::begin() {
  pinMode(BUZZER_PIN, OUTPUT);
}

/**
 * Start the buzzer
 */
void Buzzer::start() {
  tone(BUZZER_PIN, BUZZER_TONE_HZ);
  triggered = true;
}

/**
 * Stop the buzzer
 */
void Buzzer::stop() {
  noTone(BUZZER_PIN);
  triggered = false;
}

bool Buzzer::isTriggered() {
  return triggered;
}