/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef WidgetDateTime_h
#define WidgetDateTime_h

#include <Arduino.h>
#include "u8g_teensy.h"
#include "RTC.h"

class WidgetDateTime {
  public:
    WidgetDateTime(U8GLIB *display, RTC *pRtc);
    void displayWidget();

  private:
    U8GLIB *display;
    RTC *rtc;

    void printTwoDigits(uint8_t number);
};

#endif