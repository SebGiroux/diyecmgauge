﻿/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "Sensors.h"

LSM303 lsm;
HIH6130 hih;
TinyGPSPlus gps;

bool rtcSyncWithGps = false;

/**
 * Function that will start all the sensors monitored by this class
 */
void Sensors::begin() {
  //startAccelerometer();
  startGPS();
}

/**
 * Start the accelerometer (LSM303)
 */
void Sensors::startAccelerometer() {
  // Enable I2C
  Wire.begin();
  
  if (lsm.init()) {
    lsm.enableDefault();
  }
}

/**
 * Start the GPS
 */
void Sensors::startGPS() {
  // GPS baud rate is 9600 to start
  Serial3.begin(9600);

  // So we switch that over to 38400bps
  Serial3.write("$PMTK251,38400*27\r\n");
  Serial3.flush();
  delay(10);
  Serial3.end();

  Serial3.begin(38400);
  
  // Turns off all NMEA messages except VTG, GGA and GSA. GSA is only sent once every five transmissions.
  Serial3.write("$PMTK314,0,1,1,1,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0*2C\r\n");

  // After sending those commands to the GPS, it is possible to turn on 10Hz update rate.
  Serial3.write("$PMTK220,100*2F\r\n");
}

/**
 * Refresh the data with fresh data fetched from the sensor
 */
void Sensors::refreshSensorsData() {
  // LSM303
  //lsm.read();
  
  // HIH6130
  // hih.read();
  
  bool nmeaReceived = false;
  while (!nmeaReceived && Serial3.available() > 0) {
    nmeaReceived = gps.encode(Serial3.read());
  }

  // If a sentence is received, we can check the checksum, parse it...
  if (gps.location.isUpdated() && gps.time.age() < 500) {
    DisplayableItem_t item = DisplayableItems::getItemByTitle("LAT");
    item.value = gps.location.lat();
    item.value = DisplayableItems::getUserValueFromRawValue(item);
    DisplayableItems::setItem(item);

    item = DisplayableItems::getItemByTitle("LONG");
    item.value = gps.location.lng();
    item.value = DisplayableItems::getUserValueFromRawValue(item);
    DisplayableItems::setItem(item);

    item = DisplayableItems::getItemByTitle("ALT");
    item.value = gps.altitude.kilometers();
    item.value = DisplayableItems::getUserValueFromRawValue(item);
    DisplayableItems::setItem(item);

    item = DisplayableItems::getItemByTitle("SPD");
    item.value = gps.speed.mps();
    item.value = DisplayableItems::getUserValueFromRawValue(item);
    DisplayableItems::setItem(item);

    item = DisplayableItems::getItemByTitle("SAT");
    item.value = gps.satellites.value();
    item.value = DisplayableItems::getUserValueFromRawValue(item);
    DisplayableItems::setItem(item);

    syncRtcWithGps();
  }
}

/**
 * If enabled in general settings, the RTC will be synced with the GPS date / time the first time the GPS has a fix
 */
void Sensors::syncRtcWithGps() {
  GeneralSettings_t generalSettings = GeneralSettings::get();

  if (!rtcSyncWithGps && generalSettings.rtcSyncWithGps) {
    rtcSyncWithGps = true;

    // Set time using GPS
    setTime(gps.time.hour(), gps.time.minute(), gps.time.second(), gps.date.day(), gps.date.month(), gps.date.year());

    int32_t secondsToAdjust = generalSettings.timeZone * SECONDS_IN_ONE_HOUR;
    if (isDaylightSavingTime()) {
      secondsToAdjust += SECONDS_IN_ONE_HOUR;
    }

    // Adjust time based on time zone and day light saving
    adjustTime(secondsToAdjust);

    // Set the new date
    Teensy3Clock.set(now());
  }
}

/**
 * Figure out if the current date is day light saving or not
 *
 * @return true if currently in day light saving time, false otherwise
 */
bool Sensors::isDaylightSavingTime() {
  tmElements_t te;

  // Date for March 1st
  te.Year = year() - 1970;
  te.Month = 3;
  te.Day = 1;
  te.Hour = 0;
  te.Minute = 0;
  te.Second = 0;
  
  time_t dstStart, dstEnd;

  // Date for second Sunday of March at 2AM
  dstStart = makeTime(te);
  dstStart = nextSunday(nextSunday(dstStart));
  dstStart += 2 * SECS_PER_HOUR;
  
  // Date for first Sunday of November at 1AM
  te.Month = 11;
  dstEnd = makeTime(te);
  dstEnd = nextSunday(dstEnd);
  dstEnd += SECS_PER_HOUR;

  return (now() >= dstStart && now() < dstEnd);
}

/**
 * @return The instance of the HIH6130 (temperature + humidity sensor)
 */
HIH6130 Sensors::getHIH() {
  return hih;
}

/**
 * @return The instance of the LSM303 (accelerometer + magnetometer)
 */
LSM303 Sensors::getLSM() {
  return lsm;
}

/**
 * @return The instance of the GPS
 */
TinyGPSPlus Sensors::getGPS() {
  return gps;
}

/**
 * @return The last speed value we got from the GPS
 */
float Sensors::getGPSSpeed() {
  return DisplayableItems::getItemByTitle("SPD").value;
}
