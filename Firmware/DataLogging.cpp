/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "DataLogging.h"

const char* DataLogging::FILE_NAME_EXTENSION = "msl";

/*
 * Should be called at power on. This function will initialize all the parameter and get ready for data logging when requested
 *
 * @param pEcu            The ECU instance object that contains all the ECU data to be logged
 */
void DataLogging::begin(ECU* pEcu) {
  ecu = pEcu;

  if (sd.begin(SD_CS_PIN)) {
    status = NOT_DATA_LOGGING;
    SerialHandler::sendDebug(F("SD card initialization OK"));

    // Create the datalogs directory if it doesn't already exists
    if (!sd.exists(const_cast<char *>(DATALOGS_DIRECTORY))) {
      sd.mkdir(const_cast<char *>(DATALOGS_DIRECTORY));
    }
  }
  else {
    status = NO_SD_CARD;
    SerialHandler::sendDebug(F("SD card initialization failed"));
  }
}

/**
 * Start the data logging
 */
void DataLogging::startLogging() {
  if (status == NOT_DATA_LOGGING) {
    if (!(dataLogFile = sd.open(getFileName(), FILE_WRITE))) {
      SerialHandler::sendDebug(F("Error opening data log file"));
    }
    else {
      writeHeader();

      status = DATA_LOGGING;

      SerialHandler::dataLoggingSendStatus();
    }
  }
}

/*
 * Stop the data logging
 */
void DataLogging::stopLogging() {
  if (status == DATA_LOGGING) {
    status = NOT_DATA_LOGGING;
    dataLogFile.close();

    SerialHandler::dataLoggingSendStatus();
  }
}

/**
 * @return The current data log file name
 */
char* DataLogging::getFileName() {
  extern RTC rtc;
  tmElements_t tm = rtc.getTime();
  
  sprintf(dataLoggingFileName, "%s/%04d-%02d-%02d_%02d.%02d.%02d.%s", DATALOGS_DIRECTORY, tmYearToCalendar(tm.Year), tm.Month, tm.Day, tm.Hour, tm.Minute, tm.Second, FILE_NAME_EXTENSION);

  return dataLoggingFileName;
}

/**
 * Write the ECU revision number and signature in the data log file
 */
void DataLogging::writeRevNumAndSignature() {
  dataLogFile.println(ecu->getDataLogHeader());
}

/**
 * Write the capture date of the data log in the data log file
 */
void DataLogging::writeCaptureDate() {
  char buffer[3];
  extern RTC rtc;
  tmElements_t tm = rtc.getTime();

  dataLogFile.print(F("\"Capture date: "));

  dataLogFile.print(tmYearToCalendar(tm.Year));
  dataLogFile.print(F("-"));

  sprintf(buffer, "%02d", tm.Month);
  dataLogFile.print(buffer);
  dataLogFile.print(F("-"));

  sprintf(buffer, "%02d", tm.Day);
  dataLogFile.print(buffer);
  dataLogFile.print(F(" "));

  sprintf(buffer, "%02d", tm.Hour);
  dataLogFile.print(buffer);
  dataLogFile.print(F(":"));

  sprintf(buffer, "%02d", tm.Minute);
  dataLogFile.print(buffer);
  dataLogFile.print(F(":"));

  sprintf(buffer, "%02d", tm.Second);
  dataLogFile.print(buffer);
  dataLogFile.println(F("\""));
}

/**
 * Write the header of the data log file
 */
void DataLogging::writeHeader() {
  writeRevNumAndSignature();
  writeCaptureDate();

  // First column is always the elapsed time
  dataLogFile.print(F("TIME\t"));

  for (uint8_t x = 0; x < DisplayableItems::getNbItemsMonitored(); x++) {
    dataLogFile.print(DisplayableItems::getItemAtIndex(x).title);

    // Add tab character between header titles (but not the last one)
    if (x < (DisplayableItems::getNbItemsMonitored() - 1)) {
      dataLogFile.print("\t");
    }
    // Add an end of line character after the last header title
    else {
      dataLogFile.println();
    }
  }

  dataLogFile.flush();
}

/**
 * Write a row in the data log file. Should be called every time in the main loop. This function
 * will take care of checking if the SD card is ready and if logging is enabled.
 */
void DataLogging::writeRow() {
  if (status != DATA_LOGGING) {
    return;
  }

  // First column is always the elapsed time
  dataLogFile.print(millis() / 1000.0, 3);
  dataLogFile.print(F("\t"));

  for (uint8_t x = 0; x < DisplayableItems::getNbItemsMonitored(); x++) {
    dataLogFile.print(DisplayableItems::getItemValueByTitle(DisplayableItems::getItemAtIndex(x).title));

    // Add tab character between row values (but not the last one)
    if (x < (DisplayableItems::getNbItemsMonitored() - 1)) {
      dataLogFile.print(F("\t"));
    }
    // Add an end of line character after the last value
    else {
      dataLogFile.println();
    }
  }

  dataLogFile.flush();
}

/**
 * @return The number of blocks per cluster on the SD card
 */
uint8_t DataLogging::getSdCardBlocksPerCluster() {
  return sd.vol()->blocksPerCluster();
}

/**
 * @return The number of clusters on the SD card
 */
uint32_t DataLogging::getClustersCount() {
  return sd.vol()->clusterCount();
}

/**
 * @return
 */
SdFat& DataLogging::getSd() {
	return sd;
}

/**
 * @return The current data logging status
 */
DataLoggingStatus DataLogging::getStatus() {
  return status;
}
