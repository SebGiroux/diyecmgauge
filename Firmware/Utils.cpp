/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "Utils.h"

/*
 * Function that take a float and return a String
 *
 * @param number   The number to convert
 * @param digits   The digits needed for the output
 *
 */
String Utils::ftoa(float number, uint8_t digits) {
  String integerPart = "";

  // Handle negative numbers
  if (number < 0.0) {
    integerPart += '-';
    number = -number;
  }

  // Round correctly so that print(1.999, 2) prints as "2.00"
  double rounding = 0.5;
  for (uint8_t i = 0; i < digits; i++) {
    rounding *= 0.1;
  }

  number += rounding;

  // Extract the integer part of the number and print it
  unsigned long withoutDecimals = (unsigned long)number;
  double remainder = number - (double)withoutDecimals;

  integerPart += withoutDecimals;

  String decimalPart = "";

  if (digits > 0) {
    integerPart += '.';

    // Extract digits from the remainder one at a time
    while (digits-- > 0) {
      remainder *= 10.0;
      uint16_t toPrint = uint16_t(remainder);
      decimalPart += toPrint;
      remainder -= toPrint;
    }
  }
  
  return integerPart + decimalPart;
}

/**
 * Function that checks if a string ends with the specified suffix
 *
 * @param str The string to test against
 * @param suffix The suffix to look for
 *
 * @return true if the string ends with the suffix, false otherwise
 */
bool Utils::endsWith(const char *str, const char *suffix) {
  if (!str || !suffix) {
    return 0;
  }

  uint16_t lenStr = strlen(str);
  uint16_t lenSuffix = strlen(suffix);
  if (lenSuffix >  lenStr) {
    return 0;
  }

  return (strncmp(str + lenStr - lenSuffix, suffix, lenSuffix) == 0);
}

/**
 * Get the X position to place the specified text so that it appears vertically centered
 *
 * @param display The display object to display the text on
 * @param text The text to be centered in the screen
 *
 * @return The X position to have the text centered on the display
 */
uint16_t Utils::getXPositionToCenterInScreen(U8GLIB& display, String text) {
  char textArray[text.length() + 1]; 
  text.toCharArray(textArray, text.length() + 1);

  return (display.getWidth() / 2) - (display.getStrWidth(textArray) / 2);
}