﻿/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "MegasquirtCAN.h"
#include "DisplayableItems.h"

FlexCAN CANbus(500000);

char revNum[MS_REV_NUM_LENGTH + 1];
bool revNumFetched = false;
bool revNumCompleted = false;

char signature[MS_SIGNATURE_LENGTH + 1];
bool signatureFetched = false;
bool signatureCompleted = false;

static CAN_message_t rxmsg;
static CAN_message_t txmsg;

/**
 * Start the CAN bus and prepare the CAN filters
 */
void MegasquirtCAN::begin() {
  initializeDisplayableItems();

  CAN_filter_t mask;

  // Ignore if RTR
  mask.rtr = 0;

  // Check if extended message
  mask.ext = 1;

  // Check the message destination
  mask.id = 0x000F << 7;

  CANbus.begin(mask);
  
  // Set first filter - Check that the message is addressed to local_ID
  mask.id = (uint32_t)(GAUGE_CANID & 0x0F) << 7;
  CANbus.setFilter(mask, 0);
  
  // Set additional filters for broadcast messages (CANID == 15)
  // All 8 available filters have to be initialized otherwise random filters are there
  CANbus.setFilter(mask, 1);
  CANbus.setFilter(mask, 2);
  CANbus.setFilter(mask, 3);
  CANbus.setFilter(mask, 4);
  CANbus.setFilter(mask, 5);
  CANbus.setFilter(mask, 6);
  CANbus.setFilter(mask, 7);
}

/**
 * Initialize all the ECU specific displayable items
 */
void MegasquirtCAN::initializeDisplayableItems() {
  DisplayableItems::addItem({"SECL", "s", MS_OUTPC_BLOCK, 0 , 2, 1, 0, 0, 3, 0, 1000, true, NULL});
  DisplayableItems::addItem({"RPM", "", MS_OUTPC_BLOCK, 6, 2, 1, 0, 0, 0, 0, 8000, true, NULL});
  DisplayableItems::addItem({"AFR", "", MS_OUTPC_BLOCK, 28, 2, 0.1, 0, 0, 2, 10, 20, true, NULL});
  DisplayableItems::addItem({"CLT", "F", MS_OUTPC_BLOCK, 22, 2, 0.1, 0, 0, 1, -40, 240, true, NULL});
  DisplayableItems::addItem({"MAP", "", MS_OUTPC_BLOCK, 18, 2, 0.1, 0, 0, 1, 0, 240, true, NULL});
  DisplayableItems::addItem({"IAT", "F", MS_OUTPC_BLOCK, 20, 2, 0.1, 0, 0, 1, -40, 240, true, NULL});
  DisplayableItems::addItem({"SPKA", "", MS_OUTPC_BLOCK,  8, 2, 0.1, 0, 0, 2, 0, 50, true, NULL});
  DisplayableItems::addItem({"BATTV", "", MS_OUTPC_BLOCK, 26, 2, 0.1, 0, 0, 1, 7, 21, true, NULL});
  DisplayableItems::addItem({"TPS", "%", MS_OUTPC_BLOCK, 24, 2, 0.1, 0, 0, 0, 0, 100, true, NULL});
  DisplayableItems::addItem({"BARO", "", MS_OUTPC_BLOCK, 16, 2, 0.1, 0, 0, 2, 50, 150, true, NULL});
  DisplayableItems::addItem({"EGOC", "", MS_OUTPC_BLOCK, 34, 2, 0.1, 0, 0, 0, 50, 150, true, NULL});
  DisplayableItems::addItem({"IAC", "", MS_OUTPC_BLOCK, 54, 2, 0.392, 0, 0, 0, 0, 240, true, NULL});
  DisplayableItems::addItem({"IDLT", "", MS_OUTPC_BLOCK, 380, 2, 1, 0, 0, 2, 0, 8000, true, NULL});
  DisplayableItems::addItem({"AFRT", "", MS_OUTPC_BLOCK, 12, 1, 0.1, 0, 0, 2, 10, 20, true, NULL});
  DisplayableItems::addItem({"MPG", "", MS_OUTPC_BLOCK, 222, 2, 2.352, 0, 0, 2, 0, 100, true, NULL});
  DisplayableItems::addItem({"WUE", "", MS_OUTPC_BLOCK, 40, 2, 1, 0, 0, 2, 50, 150, true, NULL});
  DisplayableItems::addItem({"PW", "", MS_OUTPC_BLOCK, 2, 2, 0.001, 0, 0, 2, 0, 24, true, NULL});
  DisplayableItems::addItem({"VSS", "", MS_OUTPC_BLOCK, 336, 2, 0.36, 0, 0, 0, 0, 250, true, NULL});
  DisplayableItems::addItem({"AFRE", "", 0, 0, 0, 0, 0, 0, 2, 10, 20, false, &DisplayableItems::getAFRError});
  DisplayableItems::addItem({"BST", "", 0, 0, 0, 0, 0, 0, 2, 0, 15, false, &DisplayableItems::getBoost});
  DisplayableItems::addItem({"ENG", "", MS_OUTPC_BLOCK, 11, 1, 1, 0, 0, 0, 0, 0, true, NULL});
  DisplayableItems::addItem({"STS1", "", MS_OUTPC_BLOCK, 78, 1, 1, 0, 0, 0, 0, 0, true, NULL});

  refreshGeneralSettings();
}

/**
 * Interrupt handler function called when we receive data on the CAN bus
 */
void MegasquirtCAN::checkCANBusMessages() {
  while (CANbus.available() > 0) {
    if (CANbus.read(rxmsg)) {
      uint16_t localOffset = (uint16_t)(rxmsg.id >> 18);
      uint8_t msgType = (uint8_t)((rxmsg.id & 0x00038000) >> 15);
      uint8_t remoteID = (uint8_t)((rxmsg.id & 0x00007800) >> 11);
      uint8_t localID = (uint8_t)((rxmsg.id & 0x00000780) >> 7);
      uint8_t localBlock = ((uint8_t)((rxmsg.id & 0x00000078) >> 3)) + ((uint8_t)((rxmsg.id & 0x00000004) << 2));

      if (remoteID == MS_CANID && localID == GAUGE_CANID && rxmsg.ext) {
        // MegaSquirt is answering our data request
        if (msgType == MSG_RSP) {
          pendingCANMessages--;

          switch (localBlock) {
            case MS_OUTPC_BLOCK:
              handleOutputChannelsResponse(localOffset);
              break;

            case MS_REV_NUM_BLOCK:
              handleRevNumResponse(localOffset);
              break;

            case MS_SIGNATURE_BLOCK:
              handleSignatureResponse(localOffset);
              break;

            default:
              SerialHandler::sendDebug(F("Got response for unknown block "));
              SerialHandler::sendDebug(localBlock);
          }
        }
        // MegaSquirt is requesting data
        else if (msgType == MSG_REQ) {
          switch (localBlock) {
            case SENSORS_BLOCK:
              handleMegaSquirtRequest(localOffset);
              break;

            default:
              SerialHandler::sendDebug(F("Got request for unknown block "));
              SerialHandler::sendDebug(localBlock);
          }
        }
        else {
          SerialHandler::sendDebug(F("Got invalid message type "));
          SerialHandler::sendDebug(msgType);
        }
      }
    }
    else {
      SerialHandler::sendDebug(F("Something terrible happened while fetching CAN message")); 
    }
  }
}

/**
 * Handle an output channels (real-time data) response from the ECU
 *
 * @param localOffset The offset of the data we received in the displayable items structure
 */
void MegasquirtCAN::handleOutputChannelsResponse(uint16_t localOffset) {
  DisplayableItem_t item = DisplayableItems::getItemAtIndex(localOffset);

  switch (item.reqbytes) {
    case 1:
      item.value = rxmsg.buf[0];
      break;

    case 2:
      item.value = (int16_t) word(rxmsg.buf[0], rxmsg.buf[1]);
      break;

    default:
      SerialHandler::sendDebug(F("Invalid number of bytes"));
      break;
  }

  DisplayableItems::setItemValueAtIndex(localOffset, DisplayableItems::getUserValueFromRawValue(item));
}

/**
 * Handle a signature response frm the ECU
 *
 * @param offset The offset of the bytes we received
 */
void MegasquirtCAN::handleSignatureResponse(uint16_t offset) {
  uint8_t len = getRequestBytesForOffsetAndLength(offset, MS_SIGNATURE_LENGTH);
      
  for (uint8_t i = 0; i < len; i++) {
    signature[offset + i] = rxmsg.buf[i];
  }
  
  // If we've reached the end, we're done. We got the whole signature.
  if ((offset + len) == (MS_SIGNATURE_LENGTH - 1)) {
    signatureCompleted = true;
  }
}

/**
 * Handle a revision number response from the ECU
 *
 * @param offset The offset of the bytes we received
 */
void MegasquirtCAN::handleRevNumResponse(uint16_t offset) {
  uint8_t len = getRequestBytesForOffsetAndLength(offset, MS_REV_NUM_LENGTH);
      
  for (uint8_t i = 0; i < len; i++) {
    revNum[offset + i] = rxmsg.buf[i];
  }

  // If we've reached the end, we're done. We got the whole revision number.
  if ((offset + len) == (MS_REV_NUM_LENGTH - 1)) {
    revNumCompleted = true;
  }
}

/**
 * Handle a request received from MegaSquirt to fetch data from us
 * Usually a request for RTC, GPS or Accelerometer
 *
 * @param offset The offset of the data requested
 */
void MegasquirtCAN::handleMegaSquirtRequest(uint16_t offset) {
  // Figure out the block / offset of the response based on the three bytes of data we got
  uint8_t remoteBlock = rxmsg.buf[0];
  uint16_t remoteOffset = ((uint16_t) rxmsg.buf[1] << 3) | ((rxmsg.buf[2] & 0xE0) >> 5);
  uint8_t nbBytes = rxmsg.buf[2] & 0x1F;

  txmsg.len = nbBytes;
  txmsg.ext = 1;
  txmsg.id = getMessageId(remoteBlock, remoteOffset, MSG_RSP);

  switch (offset) {
    case SENSORS_RTC_OFFSET:
      {
        extern RTC rtc;
        tmElements_t tm = rtc.getTime();
        uint16_t year = tmYearToCalendar(tm.Year);

        txmsg.buf[0] = tm.Second;
        txmsg.buf[1] = tm.Minute;
        txmsg.buf[2] = tm.Hour;
        txmsg.buf[3] = tm.Wday;
        txmsg.buf[4] = tm.Day;
        txmsg.buf[5] = tm.Month;
        txmsg.buf[6] = year / 256;
        txmsg.buf[7] = year % 256;

        CANbus.write(txmsg);
        break;
      }

    case SENSORS_GPS_PART_1_OFFSET:
      {
        extern Sensors sensors;
        TinyGPSPlus gps = sensors.getGPS();

        double intpart;
        unsigned char latmin = (gps.location.lat() - gps.location.rawLat().deg) * 60;
        unsigned char lonmin = modf(abs(gps.location.lng()), &intpart) * 60;
        uint16_t latmmin = ((gps.location.rawLat().billionths * 3 / 50000) - latmin * 1000) * 10;
        uint16_t lonmmin = ((gps.location.rawLng().billionths * 3 / 50000) - lonmin * 1000) * 10;

        txmsg.buf[0] = gps.location.rawLat().deg;
        txmsg.buf[1] = (gps.location.lat() - gps.location.rawLat().deg) * 60;
        txmsg.buf[2] = latmmin / 256;
        txmsg.buf[3] = latmmin % 256;
        txmsg.buf[4] = gps.location.rawLng().deg;
        txmsg.buf[5] = lonmin;
        txmsg.buf[6] = lonmmin / 256;
        txmsg.buf[7] = lonmmin % 256;

        CANbus.write(txmsg);
        break;
      }

    case SENSORS_GPS_PART_2_OFFSET:
      {
        extern Sensors sensors;
        TinyGPSPlus gps = sensors.getGPS();

        txmsg.buf[0] = gps.location.rawLng().negative ? 1 : 0;
        txmsg.buf[1] = gps.altitude.kilometers();
        txmsg.buf[2] = gps.altitude.meters() * 10 / 256;
        txmsg.buf[3] = (int8_t) gps.altitude.meters() * 10 % 256;
        txmsg.buf[4] = gps.speed.kmph() / 256;
        txmsg.buf[5] = (int8_t) gps.speed.kmph() % 256;
        txmsg.buf[6] = gps.course.deg() / 256;
        txmsg.buf[7] = (int8_t) gps.course.deg() % 256;

        CANbus.write(txmsg);
        break;
      }

    case SENSORS_ACCEL_OFFSET:
      {
        extern Sensors sensors;

        int16_t accelX = map(sensors.getLSM().a.x, -32760, 32760, 0, 4095);
        int16_t accelY = map(sensors.getLSM().a.y, -32760, 32760, 0, 4095);
        int16_t accelZ = map(sensors.getLSM().a.z, -32760, 32760, 0, 4095);

        txmsg.buf[0] = accelX / 256;
        txmsg.buf[1] = accelX % 256;
        txmsg.buf[2] = accelY / 256;
        txmsg.buf[3] = accelY % 256;
        txmsg.buf[4] = accelZ / 256;
        txmsg.buf[5] = accelZ % 256;
        txmsg.buf[6] = 0;
        txmsg.buf[7] = 0;

        CANbus.write(txmsg);
        break;
      }

    default:
      SerialHandler::sendDebug(F("Got request for unknown offset "));
      SerialHandler::sendDebug(offset);
  }
}

/**
 * Send a request to the MegaSquirt over the CAN bus
 *
 * @param block The block the item is in
 * @param localOffset The offset of the requested data in the displayable items structure
 * @param remoteOffset The remote offset from the beginning of the block for the item
 * @param length The number of bytes the item is
 *
 * @return true if the request was sent successfully, false otherwise
 */
bool MegasquirtCAN::sendRequest(uint8_t block, uint8_t localOffset, uint16_t remoteOffset, uint8_t length) {
  CAN_message_t txmsg;
  
  // Table where the data should go
  uint8_t localBlock = block;

  // Create the message to read specified block and offset
  txmsg.id = getMessageId(block, remoteOffset, MSG_REQ);
  // Extended ID (29-bit header)
  txmsg.ext = 1;
  // Paylod of request message is 3 bytes
  txmsg.len = 3;
  // No timeout. If no buffer available, fails right away
  txmsg.timeout = 0;
  txmsg.buf[0] = localBlock;
  txmsg.buf[1] = (uint8_t)(localOffset >> 3);
  txmsg.buf[2] = (uint8_t)((localOffset & 0x0007) << 5) | length;

  // If we don't have any buffer available, we wait until we do
  unsigned long startTime = millis();
  while (pendingCANMessages >= NB_RX_BUFFERS && (millis() - startTime) <= SEND_REQUEST_TIMEOUT) {
    checkCANBusMessages();
    delayMicroseconds(100);
  }

  // Check if timeout was reached
  if ((millis() - startTime) > SEND_REQUEST_TIMEOUT) {
    return false;
  }

  // Write the CAN message on the bus and increase the number of pending messages to be answered
  if (CANbus.write(txmsg) == 1) {
    pendingCANMessages++;

    return true;
  }

  return false;
}

/**
 * Get the CAN message id for the specified parameter
 *
 * @param block The block the item is in
 * @param offset The offset from the beginning of the block for the item
 * @param msgType The type of message (MSG_CMD, MSG_REQ, MSG_RSP)
 */
uint32_t MegasquirtCAN::getMessageId(uint8_t block, uint16_t offset, uint8_t msgType) {
  // The CAN ID of this gauge
  uint8_t localID = GAUGE_CANID;
  // CAN ID of MS2 or MS3
  uint8_t remoteID = MS_CANID;

  return (((uint32_t)offset)<<18) + (((uint32_t)msgType)<<15) + (((uint32_t)localID)<<11) + (((uint32_t)remoteID)<<7) + (((uint32_t)block)<<3);
}

/**
 * Send a bunch of requests over the CAN bus for the MS for all the items we're monitoring
 */
bool MegasquirtCAN::requestData() {
  checkCANBusMessages();

  // Fetch the revision number from the MegaSquirt if not already done
  if (!revNumFetched) {
    fetchRevNum();
  }

  // Fetch the signature from the MegaSquirt if not already done
  if (!signatureFetched) {
    fetchSignature();
  }

  // Fetch all the monitored items data
  for (uint8_t x = 0; x < DisplayableItems::getNbItemsMonitored(); x++) {
    DisplayableItem_t item = DisplayableItems::getItemAtIndex(x);

    // Make sure it is an item we need to fetch from the ECU
    if (item.fetchFromECU) {
      bool success = sendRequest(item.block, x, item.offset, item.reqbytes);
      if (!success) {
        return false;
      }
    }
  }

  return true;
}

/**
 * @return true if the ECU is currently reporting a config error, false otherwise
 */
bool MegasquirtCAN::isConfigError() {
  DisplayableItem_t item = DisplayableItems::getItemByTitle("STS1");
  uint8_t configError = (uint8_t) item.value & 4;

  return (configError == 4);
}

/**
 * Returns the number of bytes to request based on the current offset to be fetched and overall length
 *
 * @param offset The current offset of the data to be fetched
 * @param length The total length of the data to be fetched
 *
 * @return The number of bytes to request
 */
uint8_t MegasquirtCAN::getRequestBytesForOffsetAndLength(uint8_t offset, uint8_t length) {
  uint8_t requestBytes = MS_REQUEST_BLOCK_LENGTH;

  // If we're at the end, adjust the number of requested bytes
  if ((length - 1 - offset) < requestBytes) {
    requestBytes = length - 1 - offset;
  }

  return requestBytes;
}

/**
 * Send all the requests required to fetch the revision number from the ECU
 */
void MegasquirtCAN::fetchRevNum() {
  // Initialize to all spaces
  memset(revNum, 0, sizeof(revNum));

  bool success = false;

  // Send requests for each part of the revision number
  for (uint8_t offset = 0; offset < MS_REV_NUM_LENGTH; offset += MS_REQUEST_BLOCK_LENGTH) {
    success = sendRequest(MS_REV_NUM_BLOCK, offset, offset, getRequestBytesForOffsetAndLength(offset, MS_REV_NUM_LENGTH));
    if (!success) {
      break;
    }
  }

  if (success) {
    revNumFetched = true;
  }
}

/**
 * Send all the requests required to fetch the signature from the ECU
 */
void MegasquirtCAN::fetchSignature() {
  // Initialize to all spaces
  memset(signature, 0, sizeof(signature));

  bool success = false;

  // Send requests for each part of the signature
  for (uint8_t offset = 0; offset < MS_SIGNATURE_LENGTH; offset += MS_REQUEST_BLOCK_LENGTH) {
    success = sendRequest(MS_SIGNATURE_BLOCK, offset, offset, getRequestBytesForOffsetAndLength(offset, MS_SIGNATURE_LENGTH));
    if (!success) {
      break;
    }
  }

  if (success) {
    signatureFetched = true;
  }
}

/**
 * @return The current revision number of the ECU
 */
String MegasquirtCAN::getRevNum() {
  if (revNumCompleted) {
    return revNum;
  }

  return "";
}

/**
 * @return The current signature of the ECU
 */
String MegasquirtCAN::getSignature() {
  if (signatureCompleted) {
    return signature;
  }

  return "";
}

/**
 * @return The header to be used in a data log file
 */
String MegasquirtCAN::getDataLogHeader() {
  String header;

  String revNum = getRevNum();
  revNum.trim();

  header += F("\"");
  header += revNum;
  header += F(": ");
  header += getSignature();
  header += F("\"");

  return header;
}

/**
 * Refresh general settings, for example speed unit (either km/h or mp/h) based on the general settings
 */
void MegasquirtCAN::refreshGeneralSettings() {
  GeneralSettings_t generalSettings = GeneralSettings::get();

  // VSS - speed unit
  DisplayableItem_t item = DisplayableItems::getItemByTitle("VSS");
  item.scale = (generalSettings.speedUnit == KMH) ? 0.36 : 0.22369;
  DisplayableItems::setItem(item);

  // CLT and IAT - temperature unit
  double tempScale;
  int16_t tempTranslate;
  String tempUnits;

  if (generalSettings.temperatureUnit == CELSIUS) {
    tempScale = 0.05555;
    tempTranslate = -320;
    tempUnits = "C";
  }
  else {
    tempScale = 0.1;
    tempTranslate = 0;
    tempUnits = "F";
  }

  item = DisplayableItems::getItemByTitle("CLT");
  item.scale = tempScale;
  item.translate = tempTranslate;
  item.units = tempUnits;
  DisplayableItems::setItem(item);

  item = DisplayableItems::getItemByTitle("IAT");
  item.scale = tempScale;
  item.translate = tempTranslate;
  item.units = tempUnits;
  DisplayableItems::setItem(item);
}