﻿/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef SerialHandler_h
#define SerialHandler_h

#include <Arduino.h>
#include "libs/SdFat/SdFat.h"
#include "DataLogging.h"
#include "Alarms.h"
#include "PageItems.h"
#include "SerialWriterChecksum.h"
#include "Settings.h"
#include "StepperGauge.h"

#define VERSION "DIYECMGauge v1.0"

#define SERIAL_OK "OK"
#define SERIAL_ERR "ERR"
#define SERIAL_CHECKSUM_MISMATCH "CHECKSUM_MISMATCH"

#define SERIAL_EDIT_PAGE_ARGUMENTS_COUNT 9
#define SERIAL_EDIT_ALARM_ARGUMENTS_COUNT 17

#define SERIAL_SET_RTC_DATE_TIME_ARGUMENTS_COUNT 6
#define SERIAL_GET_DATA_LOG_CHUNK_ARGUMENTS_COUNT 3
#define SERIAL_EDIT_GENERAL_SETTINGS_ARGUMENTS_COUNT 7
#define SERIAL_EDIT_VEHICLE_SETTINGS_ARGUMENTS_COUNT 4
#define SERIAL_EDIT_STEPPER_SETTINGS_ARGUMENTS_COUNT 5
#define SERIAL_CHANGE_DISPLAY_PAGE_ARGUMENTS_COUNT 1

#define SERIAL_COMMAND_LINES_SEPARATOR "|"
#define SERIAL_COMMAND_VALUES_SEPARATOR "~"
#define SERIAL_COMMAND_SUB_VALUES_SEPARATOR "^"
#define SERIAL_COMMAND_TERMINATION_CHARACTER "#"

#define SERIAL_COMMANDS_BUFFER_SIZE 96

#define MAXIMUM_LONG_FILE_NAME_LENGTH 255

#define USB_SERIAL_BPS 115200
#define SERIAL_MAIN_CONNECTOR_BPS 9600

class SerialHandler {
  public:
    static void checkSerial();
    static void dataLoggingSendStatus();
    static void sendDebug(String message);

  private:
    static uint8_t getArgumentsCount(char *args);

    static bool isBufferOverflow(uint8_t bufferPosition);
    static bool validateChecksum(char* buffer, char *checksum);
    static void validateReceivedCommand(SerialWriterChecksum serialWriter, char data, char (&buffer)[SERIAL_COMMANDS_BUFFER_SIZE], char (&bufferTokenized)[SERIAL_COMMANDS_BUFFER_SIZE], uint8_t &bufferPosition);
    static void showVersion(SerialWriterChecksum serialWriter);
    static void sdCardGetStats(SerialWriterChecksum serialWriter);
    static void dataLoggingGetStatus(SerialWriterChecksum serialWriter);
    static void dataLoggingStart(SerialWriterChecksum serialWriter);
    static void dataLoggingStop(SerialWriterChecksum serialWriter);
    static void pagesList(SerialWriterChecksum serialWriter);
    static void editPage(SerialWriterChecksum serialWriter, char *args);
    static void indicatorsList(SerialWriterChecksum serialWriter);
    static void alarmsList(SerialWriterChecksum serialWriter);
    static void editAlarm(SerialWriterChecksum serialWriter, char *args);
    static void dataLogsDirectoryListing(SerialWriterChecksum serialWriter);
    static void dataLogsDeleteFile(SerialWriterChecksum serialWriter, char* filename);
    static void dataLogsDownloadFile(SerialWriterChecksum serialWriter, char* filename);
    static void rtcGetCurrentDateTime(SerialWriterChecksum serialWriter);
    static void rtcSetCurrentDateTime(SerialWriterChecksum serialWriter, char *args);
    static void sendGeneralSettings(SerialWriterChecksum serialWriter);
    static void editGeneralSettings(SerialWriterChecksum serialWriter, char *args);
    static void sendVehicleSettings(SerialWriterChecksum serialWriter);
    static void editVehicleSettings(SerialWriterChecksum serialWriter, char *args);
    static void sendStepperSettings(SerialWriterChecksum serialWriter);
    static void editStepperSettings(SerialWriterChecksum serialWriter, char *args);
    static void changeDisplayPage(SerialWriterChecksum serialWriter, char *args);
};

#endif