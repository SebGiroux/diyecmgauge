/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "WidgetZeroSixty.h"

/**
 * Constructor that take the display object in parameter
 *
 * @param pDisplay The display object that will be used to draw the widget
 * @param pSensors The sensors object to fetch GPS data from
 */
WidgetZeroSixty::WidgetZeroSixty(U8GLIB *pDisplay, Sensors *pSensors) {
  display = pDisplay;
  sensors = pSensors;
}


/**
 * Set the time at which the widget was started
 *
 * @param startTime The current time (millis())
 */
void WidgetZeroSixty::setWidgetStartTime(unsigned long startTime) {
  widgetStartTime = startTime;
}

/**
 * @return The elapsed time since the widget started
 */
unsigned long WidgetZeroSixty::getElapsedTime() {
  return millis() - widgetStartTime;
}

/**
 * Display the 0-60 widget
 */
void WidgetZeroSixty::displayWidget() {
  GeneralSettings_t generalSettings = GeneralSettings::get();
  
  // This widget is either 0-60mp/h or 0-100km/h based on the speed unit selected in the general settings
  String max = F("60");
  String units = F("mp/h");
  int maxSpeed = 60;

  if (generalSettings.speedUnit == KMH) {
    max = F("100");
    units = F("km/h");
    maxSpeed = 100;
  }

  if (getElapsedTime() < 1500) {
    display->firstPage();
    do {
      display->setFontPosTop();
      display->setFont(u8g_font_helvB18);
      display->setRGB(255, 255, 255);

      display->setPrintPos(0, 35);
      display->print(F("0-"));
      display->print(max);
      display->print(units);
      display->setPrintPos(40, 60);
      display->print(F("time"));
    } while (display->nextPage());
  }
  else if (getElapsedTime() < 3000) {
    display->firstPage();
    do {
      display->setPrintPos(25, 35);
      display->println(F("Are you"));
      display->setPrintPos(25, 60);
      display->println(F("ready ?"));
    } while (display->nextPage());
  }
  else if (getElapsedTime() < 4500) {
    display->firstPage();
    do {
      display->setPrintPos(20, 45);
      display->println(F("Get set!"));
    } while (display->nextPage());
  }
  else if (getElapsedTime() < 6000) {
    display->firstPage();
    do {
      display->setPrintPos(15, 45);
      display->println(F("Let's go!"));
    } while (display->nextPage());
  }
  else {
    zeroSixtyStartTime = millis();

    display->setFont(u8g_font_helvB10);

    if (sensors->getGPSSpeed() < maxSpeed) {
      elapsedTime = (millis() - zeroSixtyStartTime) / (double) 1000;
      float gpsSpeed = sensors->getGPSSpeed();

      display->firstPage();
      do {
        display->setPrintPos(10, 40);
        display->print(F("Speed: "));
        display->print(gpsSpeed);
        display->print(F(" "));
        display->println(units);

        display->setPrintPos(10, 70);
        display->print(F("Time: "));
        display->print(elapsedTime);
        display->println(F(" sec"));
      } while (display->nextPage());
    }
    else {
      display->setFont(u8g_font_helvB18);

      // If we've reached the flash time interval
      if (millis() - flashTimeStart > DELAY_BETWEEN_TIME_FLASHES) {
        display->firstPage();
        do {
          // If we should display the elapsed time
          if (shouldDisplayTime) {
            display->setPrintPos(40, 75);
            display->println(elapsedTime);
          }
        } while (display->nextPage());

        // Set flash time to current timestamp
        flashTimeStart = millis();

        // Reverse the value of shouldDisplayTime
        shouldDisplayTime = !shouldDisplayTime;
      }
    }
  }
}