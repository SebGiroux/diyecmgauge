/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "AnalogGauge.h"

/*
 * Constructor that take the display object in parameter
 *
 * @param pDisplay The display object that will be used to draw the analog gauge
 */
AnalogGauge::AnalogGauge(U8GLIB *pDisplay) {
  display = pDisplay;
}

/*
 * Main public function to draw a gauge
 *
 * @param DisplayableItem_t displayableItem The item to be displayed by this analog gauge
 * @param gaugeIndex The index of the analog gauge on the page
 * @param pageSize The total number of analog gauges on the page
 */
void AnalogGauge::drawAnalogGauge(DisplayableItem_t displayableItem, uint8_t gaugeIndex, uint8_t pageSize) {
  uint16_t x = 0;
  uint16_t y = 0;
  uint16_t radius = 0;

  switch (pageSize) {
    case 1:
      x = 63;
      y = 63;
      radius = 63;
      break;

    case 2:
      radius = 35;

      switch (gaugeIndex) {
        case 0:
          x = 35;
          y = 35;
          break;

        case 1:
          x = 90;
          y = 90;
          break;
      }
      break;

    case 3:
      radius = 30;

      switch (gaugeIndex) {
        case 0:
          x = 30;
          y = 36;
          break;

        case 1:
          x = 93;
          y = 36;
          break;

        case 2:
          x = 60;
          y = 93;
          break;
      }
      break;

    case 4:
      radius = 30;

      switch (gaugeIndex) {
        case 0:
          x = 30;
          y = 33;
          break;

        case 1:
          x = 93;
          y = 33;
          break;

        case 2:
          x = 30;
          y = 96;
          break;

        case 3:
          x = 93;
          y = 96;
          break;
      }
      break;
  }
  
  display->setFontPosTop();
  display->setFont(u8g_font_helvB08);
  display->setRGB(255, 255, 255);

  // Draw gauge circle
  display->drawCircle(x, y, radius);
  
  // Draw text
  char titleArray[displayableItem.title.length() + 1];
  displayableItem.title.toCharArray(titleArray, displayableItem.title.length() + 1);
  display->setPrintPos(x - (display->getStrWidth(titleArray) / 2), y - (radius / 2));
  display->println(displayableItem.title);
  
  drawTicks(x, y, radius);

  drawValue(displayableItem, x, y, radius);
}

/*
 * Draw all the marks that goes around the gauge
 * 
 * @param x The x position of the analog gauge
 * @param y the y position of the analog gauge
 * @param radius The radius of the analog gauge
 */
void AnalogGauge::drawTicks(uint16_t x, uint16_t y, uint16_t radius) {
  float currentAngle = TICKS_START_ANGLE * 3.1416 / 180;
  const uint16_t size = radius * 2 / 20;
  const uint8_t nbSteps = 10;
  const float angleIncr = ((TICKS_END_ANGLE - TICKS_START_ANGLE) / nbSteps) * 3.1416 / 180;
 
  for (uint8_t i = 1; i <= nbSteps; i++) {
    uint16_t x0 = (x + radius * cos(currentAngle));
    uint16_t y0 = (y + radius * sin(currentAngle));
    uint16_t x1 = (x + (radius - size) * cos(currentAngle));
    uint16_t y1 = (y + (radius - size) * sin(currentAngle));
    
    display->drawLine(x0, y0, x1, y1);
    
    currentAngle += angleIncr;
  }
}

/**
 * Draw the current value on the gauge with the title and the needle
 *
 * @param displayableItem The displayable item that we're displaying the value for
 * @param x The x position of the analog gauge
 * @param y the y position of the analog gauge
 * @param radius The radius of the analog gauge
 */
void AnalogGauge::drawValue(DisplayableItem_t displayableItem, uint16_t x, uint16_t y, uint16_t radius) {
  /*
    Draw center dot
  */
  float shift = 0.2 * (radius / 2);
  uint16_t rX = x - (shift / 2);
  uint16_t rY = y - (shift / 2);
  
  display->drawDisc(rX + 3, rY + 3, shift);
      
  /*
    Draw needle
  */
  float _value = (TICKS_START_ANGLE + ((TICKS_END_ANGLE - TICKS_START_ANGLE) * (displayableItem.max * (displayableItem.value - displayableItem.min)) / (displayableItem.max - displayableItem.min)) / displayableItem.max) * 3.1416 / 180;

  rX = (x + (radius - 10) * cos(_value));
  rY = (y + (radius - 10) * sin(_value));
  
  display->drawLine(x, y, rX, rY);
  
  /*
    Draw value
  */
  String value = DisplayableItems::getItemValueByItem(displayableItem);
  String valueWithUnits = value + displayableItem.units;

  char valueWithUnitsArray[valueWithUnits.length() + 1]; 
  valueWithUnits.toCharArray(valueWithUnitsArray, valueWithUnits.length() + 1);

  display->setPrintPos(x - (display->getStrWidth(valueWithUnitsArray) / 2), y + radius - 20);
  display->println(valueWithUnits);
}