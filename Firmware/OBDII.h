/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef OBDII_h
#define OBDII_h

#include "ECU.h"

class OBDII : public ECU {
  public:
    void begin();
    bool requestData();
    void refreshGeneralSettings();
};

#endif