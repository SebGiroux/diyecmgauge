/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef ExpressionParser_h
#define ExpressionParser_h

#include <Arduino.h>
#include "SerialHandler.h"

#define MAX_OP_STACK 64
#define MAX_NUM_STACK 64
#define MAX_VAR_STACK 3

enum Association {ASSOC_NONE, ASSOC_LEFT, ASSOC_RIGHT};

struct Operator {
  // The character representing the operator
  char op;  

  // The order that the operator should be performed in
  uint8_t precedence;

  // Association
  Association assoc;

  // If it is an unary operator
  bool unary;

  // An evaluation function 
  double (*eval)(double firstOperand, double secondOperand);
};

class ExpressionParser {
  public:
    bool addVariable(char variable, double value);
    void clearVariables();
    double solve(char *expression);

  private:
    static double evalUminus(double firstOperand, double secondOperand);
    static double evalExp(double firstOperand, double secondOperand);
    static double evalMul(double firstOperand, double secondOperand);
    static double evalDiv(double firstOperand, double secondOperand);
    static double evalMod(double firstOperand, double secondOperand);
    static double evalAdd(double firstOperand, double secondOperand);
    static double evalSub(double firstOperand, double secondOperand);

    Operator *getOperator(char operatorCharacter);
    void shuntOp(struct Operator *op);
    void pushOperatorStack(struct Operator *op);
    struct Operator *popOperatorStack();
    void pushNumberStack(String num);
    void pushNumberStack(double num);
    double popNumberStack();
    void pushResultToNumStack();

    bool isVariable(char variable);
    double getValueForVariable(char variable);

    static Operator ops[];
    struct Operator* opstack[MAX_OP_STACK];
    uint8_t nopstack;

    double numstack[MAX_NUM_STACK];
    uint8_t nnumstack;

    char variables[MAX_VAR_STACK];
    double variablesValues[MAX_VAR_STACK];
    uint8_t nvarstack;
};

#endif