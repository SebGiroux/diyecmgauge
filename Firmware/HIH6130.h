/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef HIH6130_h
#define HIH6130_h

#include "I2C.h"
#include <Arduino.h>

class HIH6130 {
  public:
    void read();
    float getRelativeHumidity();
    float getTemperatureCelsius();
    
  private:
    uint8_t fetchHumidityTemperature(unsigned int *pHumidityData, unsigned int *pTemperatureData);
    
    float relativeHumidity;
    float temperatureCelsius;
};

#endif
