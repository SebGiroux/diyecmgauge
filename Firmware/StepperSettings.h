/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef StepperSettings_t_h
#define StepperSettings_t_h

#include <Arduino.h>

enum HomingDirection {
  NORMAL, INVERTED
};

struct StepperSettings_t {
  uint16_t totalNumberOfSteps;
  uint32_t maximumSpeed;
  uint32_t acceleration;
  HomingDirection homingDirection;
  uint8_t channel;
};

class StepperSettings {
  public:
    static void set(StepperSettings_t stepperSettings);
    static StepperSettings_t get();

  private:
    static StepperSettings_t stepperSettings;
};

#endif
