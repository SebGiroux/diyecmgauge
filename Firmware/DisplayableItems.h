/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef DisplayableItems_h
#define DisplayableItems_h

#include <Arduino.h>
#include "Utils.h"
#include "Pins.h"
#include "DisplayableItem.h"
#include "Dynamometer.h"
#include "Sensors.h"

#define NB_DISPLAYABLE_ITEMS 80

#define EARTH_GRAVITY 9.80665 // Earth's gravity in SI units (m/s^2)

class DisplayableItems {
  public:
    static void initializeItems();
    static size_t getNbItemsMonitored();
    static uint8_t getItemIndexByBlockAndOffset(uint8_t block, uint16_t offset);
    static uint8_t getItemIndexByTitle(String title);
    static float getUserValueFromRawValue(DisplayableItem_t item);
    static void addItem(DisplayableItem_t item);

    static String getItemValueByTitle(String title);
    static float getItemFloatValueByIndex(uint8_t index);
    static float getItemFloatValueByItem(DisplayableItem_t item);
    static String getItemValueByItem(DisplayableItem_t item);
    static DisplayableItem_t getItemByTitle(String title);
    static DisplayableItem_t getItemAtIndex(uint8_t index);
    static double getValueAtIndex(uint8_t index);
    static int16_t getMinAtIndex(uint8_t index);
    static int16_t getMaxAtIndex(uint8_t index);
    static void setItemValueAtIndex(uint8_t index, double value);
    static void setItem(DisplayableItem_t item);

    // Callbacks
    static String getAFRError();
    static String getBoost();
    static String getGForce();
    static String getTemperature();
    static String getHumidity();
    static String getCompassHeading();
    static String getAccelX();
    static String getAccelY();
    static String getAccelZ();
    static String getHorsePower();
    static String getTorque();

    // Bits
    static uint8_t getBitMask(uint8_t bitLow, uint8_t bitHigh);
    static uint8_t getBitValue(uint8_t value, uint8_t bitLow, uint8_t bitHigh);

    static void refreshGeneralSettings();

  private:
    static DisplayableItem_t items[NB_DISPLAYABLE_ITEMS];
    static uint16_t nbItemsMonitored;
};

#endif
