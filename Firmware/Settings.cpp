/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "Settings.h"

const uint8_t Settings::PAGES_OFFSET = 0;
const uint16_t Settings::ALARMS_OFFSET = sizeof(PageSettings_t) * NB_DISPLAY_PAGES;
const uint16_t Settings::GENERAL_SETTINGS_OFFSET = ALARMS_OFFSET + sizeof(Alarm_t) * NB_ALARMS;
const uint16_t Settings::VEHICLE_SETTINGS_OFFSET = GENERAL_SETTINGS_OFFSET + sizeof(GeneralSettings_t);
const uint16_t Settings::STEPPER_SETTINGS_OFFSET = VEHICLE_SETTINGS_OFFSET + sizeof(VehicleSettings_t);
const uint16_t Settings::LAST_USED_OFFSET = STEPPER_SETTINGS_OFFSET + sizeof(StepperSettings_t);

/**
 * Fetch the pages from the EEPROM and copy them in RAM
 */
void Settings::fetchPagesFromEEPROM() {
  for (uint8_t i = 0; i < NB_DISPLAY_PAGES; i++) {
    PageSettings_t pageSettings;

    // Read settings from EEPROM into pageSettings
    readPage(i, pageSettings);

    // Set the page in RAM
    PageItems::setPageAtIndex(i, pageSettings);
  }
}

/**
 * Fetch the alarms from the EEPROM and copy them in RAM
 */
void Settings::fetchAlarmsFromEEPROM() {
  for (uint8_t i = 0; i < NB_ALARMS; i++) {
    Alarm_t alarm;

    // Read settings from EEPROM into alarm
    readAlarm(i, alarm);

    // Set the alarm in RAM
    Alarms::setAlarmAtIndex(i, alarm);
  }
}

/**
 * Fetch the general settings from the EEPROM and copy them in RAM
 */
void Settings::fetchGeneralSettingsFromEEPROM() {
  GeneralSettings_t generalSettings;
  readGeneralSettings(generalSettings);

  GeneralSettings::set(generalSettings);
}

/**
 * Fetch the vehicle settings from the EEPROM and copy them in RAM
 */
void Settings::fetchVehicleSettingsFromEEPROM() {
  VehicleSettings_t vehicleSettings;
  readVehicleSettings(vehicleSettings);

  VehicleSettings::set(vehicleSettings);
}

/**
 * Fetch the stepper settings from the EEPROM and copy them in RAM
 */
void Settings::fetchStepperSettingsFromEEPROM() {
  StepperSettings_t stepperSettings;
  readStepperSettings(stepperSettings);

  StepperSettings::set(stepperSettings);
}

/**
 * Fetch all the data from the EEPROM and copy them in RAM
 */
void Settings::fetchAll() {
  fetchPagesFromEEPROM();
  fetchAlarmsFromEEPROM();
  fetchGeneralSettingsFromEEPROM();
  fetchVehicleSettingsFromEEPROM();
  fetchStepperSettingsFromEEPROM();
}

/**
 * Write the page of the specified index with the specified page structure
 *
 * @param index The index of the page to be written
 * @param pageSettings The page structure to be written
 */
void Settings::writePage(uint8_t index, PageSettings_t pageSettings) {
  writeData(getPageIndexOffset(index), pageSettings);
}

/**
 * Read a page structure from the EEPROM and affect it to the page settings structure passed in parameter
 *
 * @param index The index of the page to be read
 * @param pageSettings The page structure (passed by reference) that will be filled up
 */
void Settings::readPage(uint8_t index, PageSettings_t& pageSettings) {
  readData(getPageIndexOffset(index), pageSettings);
}

/**
 * Write the alarm of the specified index with the specified alarm structure
 *
 * @param index The index of the alarm to be written
 * @param alarm The alarm structure to be written
 */
void Settings::writeAlarm(uint8_t index, Alarm_t alarm) {
  writeData(getAlarmIndexOffset(index), alarm);
}

/**
 * Read an alarm structure from the EEPROM and affect it to the alarm structure passed in parameter
 *
 * @param index The index of the alarm to be read
 * @param alarm The alarm structure (passed by reference) that will be filled up
 */
void Settings::readAlarm(byte index, Alarm_t& alarm) {
  readData(getAlarmIndexOffset(index), alarm);
}

/**
 * Write the general settings with the specified general settings structure
 *
 * @param generalSettings The general settings structure to be written
 */
void Settings::writeGeneralSettings(GeneralSettings_t generalSettings) {
  GeneralSettings::set(generalSettings);
  writeData(getGeneralSettingsOffset(), generalSettings);
}

/**
 * Read the general settings structure from the EEPROM and affect it to the general settings structure passed in parameter
 *
 * @param generalSettings The general settings structure (passed by reference) that will be filled up
 */
void Settings::readGeneralSettings(GeneralSettings_t& generalSettings) {
  readData(getGeneralSettingsOffset(), generalSettings);
}

/**
 * Write the vehicle settings with the specified vehicle settings structure
 *
 * @param vehicleSettings The vehicle settings structure to be written
 */
void Settings::writeVehicleSettings(VehicleSettings_t vehicleSettings) {
  VehicleSettings::set(vehicleSettings);
  writeData(getVehicleSettingsOffset(), vehicleSettings);
}

/**
 * Read the vehicle settings structure from the EEPROM and affect it to the vehicle settings structure passed in parameter
 *
 * @param vehicleSettings The vehicle settings structure (passed by reference) that will be filled up
 */
void Settings::readVehicleSettings(VehicleSettings_t& vehicleSettings) {
  readData(getVehicleSettingsOffset(), vehicleSettings);
}

/**
 * Write the stepper settings with the specified stepper settings structure
 *
 * @param stepperSettings The stepper settings structure to be written
 */
void Settings::writeStepperSettings(StepperSettings_t stepperSettings) {
  StepperSettings::set(stepperSettings);
  writeData(getStepperSettingsOffset(), stepperSettings);
}

/**
 * Read the stepper settings structure from the EEPROM and affect it to the stepper settings structure passed in parameter
 *
 * @param stepperSettings The stepper settings structure (passed by reference) that will be filled up
 */
void Settings::readStepperSettings(StepperSettings_t& stepperSettings) {
  readData(getStepperSettingsOffset(), stepperSettings);
}

/**
 * Get the index of a page in the EEPROM
 *
 * @param index The index of the page we're interested in
 */
uint16_t Settings::getPageIndexOffset(uint8_t index) {
  return PAGES_OFFSET + (sizeof(PageSettings_t) * index);
}

/**
 * Get the index of an alarm in the EEPROM
 *
 * @param index The index of the alarm we're interested in
 */
uint16_t Settings::getAlarmIndexOffset(uint8_t index) {
  return ALARMS_OFFSET + (sizeof(Alarm_t) * index);
}

/**
 * @return The index of the general settings in the EEPROM
 */
uint16_t Settings::getGeneralSettingsOffset() {
  return GENERAL_SETTINGS_OFFSET;
}

/**
 * @return The index of the vehicle settings in the EEPROM
 */
uint16_t Settings::getVehicleSettingsOffset() {
  return VEHICLE_SETTINGS_OFFSET;
}

/**
 * @return The index of the stepper settings in the EEPROM
 */
uint16_t Settings::getStepperSettingsOffset() {
  return STEPPER_SETTINGS_OFFSET;
}

/**
 * Write the data at the specified address with the value passed
 *
 * @param addr The address where we will start writing
 * @param value The value to be written
 *
 * @return The number of bytes written
 */
template <class T>
uint16_t Settings::writeData(uint16_t addr, const T& value) {
  const uint8_t* p = (const uint8_t*)(const void*)&value;
  uint16_t i;

  for (i = 0; i < sizeof(value); i++) {
    // Write byte only if it's different to minise write cycles to EEPROM
    if (EEPROM.read(addr) == *p) {
      addr++;
      p++;
    }
    else {
      EEPROM.write(addr++, *p++);
    }
  }

  return i;
}

/**
 * Read the data at the specified address and populate the value with it
 *
 * @param addr The address where we will start reading
 * @param value The value (passed by reference) that will be modified with read values
 *
 * @return The number of bytes read
 */
template <class T>
uint16_t Settings::readData(uint16_t addr, T& value) {
  uint8_t* p = (uint8_t*)(void*)&value;
  unsigned int i;

  for (i = 0; i < sizeof(value); i++) {
    *p++ = EEPROM.read(addr++);
  }

  return i;
}

/**
 * @return true if the EEPROM is currently empty, false otherwise
 */
bool Settings::isEEPROMEmpty() {
  bool empty = true;

  for (uint16_t i = 0; i < LAST_USED_OFFSET; i++) {
    if (EEPROM.read(i) != 0) {
      empty = false;
      break;
    }
  }

  return empty;
}

/**
 * Clear the whole EEPROM by reinitializing every bytes to zero
 */
void Settings::clear() {
  for (uint16_t i = 0; i < EEPROM_SIZE; i++) {
    if (EEPROM.read(i) != 0) {
      EEPROM.write(i, 0);
    }
  }
}
