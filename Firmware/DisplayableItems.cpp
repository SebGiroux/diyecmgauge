﻿/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "DisplayableItems.h"

DisplayableItem_t DisplayableItems::items[NB_DISPLAYABLE_ITEMS];
uint16_t DisplayableItems::nbItemsMonitored = 0;

/**
 * Initialize the displayable items related to sensors.
 *
 * Note that no ECU specific items should be defined here, they should go in their respective ECU class instead.
 */
void DisplayableItems::initializeItems() {
  //
  // GPS
  //
  addItem({"LAT", "", 0, 0, 0, 0.01, 0, 0, 2, 0, 90, false, NULL});
  addItem({"LONG", "", 0, 0, 0, 0.01, 0, 0, 2, -180, 180, false, NULL});
  addItem({"ALT", "", 0, 0, 0, 1, 0, 0, 2, 0, 500, false, NULL});
  addItem({"SPD", "", 0, 0, 0, 1.852, 0 ,0, 2, 0, 250, false, NULL});
  addItem({"SAT", "", 0, 0, 0, 1, 0, 0, 0, 0, 20, false, NULL});
  addItem({"GFC", "", 0, 0, 0, 0, 0, 0, 2, -2, 2, false, &DisplayableItems::getGForce});

  //
  // Temperature + Humidity
  //
  addItem({"HUMD", "", 0, 0, 0, 1, 0, 0, 0, 0, 100, false, &DisplayableItems::getHumidity});
  addItem({"TEMP", "", 0, 0, 0, 1, 0, 0, 0, -40, 240, false, &DisplayableItems::getTemperature});
  
  //
  // Compass
  //
  addItem({"HEADING", "", 0, 0, 0, 1, 0, 0, 0, 0, 0, false, &DisplayableItems::getCompassHeading});

  //
  // Triple axis accelerometer
  //
  addItem({"X", "", 0, 0, 0, 1, 0, 0, 0, -2, 2, false, &DisplayableItems::getAccelX});
  addItem({"Y", "", 0, 0, 0, 1, 0, 0, 0, -2, 2, false, &DisplayableItems::getAccelY});
  addItem({"Z", "", 0, 0, 0, 1, 0, 0, 0, -2, 2, false, &DisplayableItems::getAccelZ});

  //
  // HP & Torque
  //
  addItem({"HP", "", 0, 0, 0, 1, 0, 0, 2, 0, 1000, false, &DisplayableItems::getHorsePower});
  addItem({"TQ", "", 0, 0, 0, 1, 0, 0, 2, 0, 1000, false, &DisplayableItems::getTorque});

  refreshGeneralSettings();
}

/**
 * Return the number of items we're currently monitoring in the big array above.
 */
size_t DisplayableItems::getNbItemsMonitored() {
  return nbItemsMonitored;
}

/**
 * Return the item at the specified index
 * 
 * @param index The index of the item we're interested in
 * @return The item at the specified index
 */
DisplayableItem_t DisplayableItems::getItemAtIndex(uint8_t index) {
  return items[index];
}

/**
 * Return the item's value at the specified index
 *
 * @param index The index of the item we're interested in
 * @return The item's value at the specified index
 */
double DisplayableItems::getValueAtIndex(uint8_t index) {
  return items[index].value;
}

/**
 * Return the item's minimum at the specified index
 *
 * @param index The index of the item we're interested in
 * @return The item's minimum at the specified index
 */
int16_t DisplayableItems::getMinAtIndex(uint8_t index) {
  return items[index].min;
}

/**
 * Return the item's maximum at the specified index
 *
 * @param index The index of the item we're interested in
 * @return The item's maximum at the specified index
 */
int16_t DisplayableItems::getMaxAtIndex(uint8_t index) {
  return items[index].max;
}

/**
 * Set the item value at the specified index with the newly specified value
 *
 * @param index The index of the item to change the value
 * @param item The new value of the item
 */
void DisplayableItems::setItemValueAtIndex(uint8_t index, double value) {
  items[index].value = value;
}

/**
 * Replace the item by the specified one. The lookup is made with the title
 * of the item.
 *
 * @param item The new item replace the old item with
 */
void DisplayableItems::setItem(DisplayableItem_t item) {
  for (uint8_t i = 0; i < getNbItemsMonitored(); i++) {
    if (item.title == items[i].title) {
      items[i] = item;
      break;
    }
  }
}

/**
 * Get a displayable item index by its block and offset value
 *
 * @param block   The block we're looking for
 * @param offset   The offset we're looking for
 *
 * @return The index of the item in the DisplayableItems array
 */
uint8_t DisplayableItems::getItemIndexByBlockAndOffset(uint8_t block, uint16_t offset) {
  uint8_t index = 0;

  for (uint8_t i = 0; i < getNbItemsMonitored(); i++) {
    DisplayableItem_t item = items[i];

    if (item.block == block && item.offset == offset) {
      index = i;
      break;
    }
  }

  return index;
}

/**
 * Get a displayable item index by its title
 *
 * @param title The title of the item to retreive
 *
 * @return The index of the item in the DisplayableItems array
 */
uint8_t DisplayableItems::getItemIndexByTitle(String title) {
  uint8_t index = 0;

  for (uint8_t i = 0; i < getNbItemsMonitored(); i++) {
    DisplayableItem_t item = items[i];

    if (item.title == title) {
      index = i;
      break;
    }
  }

  return index;
}

/**
 * Transform the raw value of an item to its user readable value.
 *
 * @param item The item to calculate the user value for
 */
float DisplayableItems::getUserValueFromRawValue(DisplayableItem_t item) {
  return (item.value + item.translate) * item.scale;
}

/**
 * Get a displayable item value by its title 
 *
 * @param title The title we're looking for
 * @return     The current value of the item
 */
String DisplayableItems::getItemValueByTitle(String title) {
  // Apply number of digits to the value
  DisplayableItem_t item = getItemByTitle(title);
  return getItemValueByItem(item);
}

/**
 * Get a displayable item value by its index 
 *
 * @param index The index of the item we're looking for
 * @return     The current value of the item
 */
float DisplayableItems::getItemFloatValueByIndex(uint8_t index) {
  DisplayableItem_t item = getItemAtIndex(index);
  
  return getItemFloatValueByItem(item);
}

/**
 * Get a displayable item value by its item 
 *
 * @param index The item we're looking for its value
 * @return     The current value of the item
 */
float DisplayableItems::getItemFloatValueByItem(DisplayableItem_t item) {
  String value = getItemValueByTitle(item.title);

  char buffer[value.length() + 1];
  value.toCharArray(buffer, value.length() + 1);

  return atof(buffer);
}

/**
 * Get the item value of the specified item. The value will be rounded according to
 * the number of digits specified in the item definition.
 *
 * @param item   The item we want the value for
 */
String DisplayableItems::getItemValueByItem(DisplayableItem_t item) {
  if (item.callback == NULL) {
    return Utils::ftoa(item.value, item.digits);
  }
  else {
    return item.callback();
  }
}

/**
 * Get an item by its title
 * 
 * @param title The title of the item we're looking for
 */
DisplayableItem_t DisplayableItems::getItemByTitle(String title) {
  DisplayableItem_t itemFound;

  for (byte i = 0; i < getNbItemsMonitored(); i++) {
    DisplayableItem_t item = items[i];

    if (item.title == title) {
      itemFound = item;
      break;
    }
  }

  return itemFound;
}

/**
 * @return 
 */
String DisplayableItems::getAFRError() {
  float afrValue = DisplayableItems::getItemByTitle("AFR").value;
  float afrTargetValue = DisplayableItems::getItemByTitle("AFRT").value;

  return Utils::ftoa(afrValue - afrTargetValue, 2);
}

/**
 * @return 
 */
String DisplayableItems::getBoost() {
  // Convert MAP kPa to PSI
  float boostPsi = (DisplayableItems::getItemByTitle("MAP").value - 100) * 0.14503773773020923;
  if (boostPsi < 0) {
    boostPsi = 0;
  }

  return Utils::ftoa(boostPsi, 2);
}

/**
 * @return 
 */
String DisplayableItems::getGForce() {
  extern Sensors sensors;
  
  // Display the results (acceleration is measured in m/s^2)
  float x = (sensors.getLSM().a.x >> 4) / (float) 10000;
  float y = (sensors.getLSM().a.y >> 4) / (float) 10000;
  float z = (sensors.getLSM().a.z >> 4) / (float) 10000;

  x /= EARTH_GRAVITY;
  y /= EARTH_GRAVITY;
  z /= EARTH_GRAVITY;

  return Utils::ftoa(sqrt(sq(x) + sq(y) + sq(z)), 2);
}

/**
 * @return
 */
String DisplayableItems::getTemperature() {
  extern Sensors sensors;
  
  return Utils::ftoa(sensors.getHIH().getTemperatureCelsius(), 2);
}

/**
 * @return
 */
String DisplayableItems::getHumidity() {
  extern Sensors sensors;
  
  return Utils::ftoa(sensors.getHIH().getRelativeHumidity(), 2);
}

/**
 * Take the heading from the LSM303 and return its associated cardinal direction.
 * 
 * @return The corresponding cardinal direction of the compass heading
 */
String DisplayableItems::getCompassHeading() {
  extern Sensors sensors;
  
  float heading = sensors.getLSM().heading();
  String cardinalDirection = "";

  if (heading >= 337.5 || heading <= 22.5) {
    cardinalDirection = "N";
  }
  else if (heading >= 22.5 && heading <= 67.5) {
    cardinalDirection = "NE";
  }
  else if (heading >= 67.5 && heading <= 112.5) {
    cardinalDirection = "E";
  }
  else if (heading >= 112.5 && heading <= 157.5) {
    cardinalDirection = "SE";
  }
  else if (heading >= 157.5 && heading <= 202.5) {
    cardinalDirection = "S";
  }
  else if (heading >= 202.5 && heading <= 247.5) {
    cardinalDirection = "SW";
  }
  else if (heading >= 247.5 && heading <= 292.5) {
    cardinalDirection = "W";
  }
  else if (heading >= 247.5 && heading <= 337.5) {
    cardinalDirection = "NW";
  }

  return cardinalDirection;
}

/**
 * @return
 */
String DisplayableItems::getAccelX() {
  extern Sensors sensors;

  return Utils::ftoa((sensors.getLSM().a.x >> 4) / (float) 1000, 4);
}

/**
 * @return
 */
String DisplayableItems::getAccelY() {
  extern Sensors sensors;
  
  return Utils::ftoa((sensors.getLSM().a.y >> 4) / (float) 1000, 4);
}

/**
 * @return
 */
String DisplayableItems::getAccelZ() {
  extern Sensors sensors;
  
  return Utils::ftoa((sensors.getLSM().a.z >> 4) / (float) 1000, 4);
}

/**
 * @return The horse power calculated in real time
 */
String DisplayableItems::getHorsePower() {
  extern Dynamometer dynamometer;

  return Utils::ftoa(dynamometer.getNetHorsePower(), 2);
}

/**
 * @return The torque calculated in real time
 */
String DisplayableItems::getTorque() {
  extern Dynamometer dynamometer;

  return Utils::ftoa(dynamometer.getNetTorque(), 2);
}

/**
 * Get a bit mask for the specified low bit and high bit
 *
 * @param bitLow  The low bit to get a mask for
 * @param bitHigh  The high bit to get a mask for
 *
 * @return
 */
uint8_t DisplayableItems::getBitMask(uint8_t bitLow, uint8_t bitHigh) {
  return (2 << bitHigh) - (1 << bitLow);
}

/**
 * Get the value of a bit in a byte
 *
 * @param value    The  byte value
 * @param bitLow  The low bit to get a mask for
 * @param bitHigh  The high bit to get a mask for
 *
 * @return 
 */
uint8_t DisplayableItems::getBitValue(uint8_t value, uint8_t bitLow, uint8_t bitHigh) {
  return (value & getBitMask(bitLow, bitHigh)) >> bitLow;
}

/**
 * Add an item to the list of displayable items
 *
 * @param item The displayable item to be added
 */
void DisplayableItems::addItem(DisplayableItem_t item) {
  items[nbItemsMonitored++] = item;
}

/**
 *
 */
void DisplayableItems::refreshGeneralSettings() {
  DisplayableItem_t item = DisplayableItems::getItemByTitle("SPD");

  GeneralSettings_t generalSettings = GeneralSettings::get();
  item.scale = (generalSettings.speedUnit == KMH) ? 1.852 : 1.15077945;

  DisplayableItems::setItem(item);
}