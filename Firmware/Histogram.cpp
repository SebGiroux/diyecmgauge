/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "Histogram.h"

/**
 * Constructor that take the display object in parameter
 *
 * @param pDisplay The display object that will be used to draw the histogram
 */
Histogram::Histogram(U8GLIB *pDisplay) {
  display = pDisplay;

  nbValuesInArray = 0;
}

/**
 * Draw the two axis, the title and all the values of the histogram
 *
 * @param displayableItem The item to be displayed by this histogram
 */
void Histogram::drawHistogram(DisplayableItem_t displayableItem) {
  x = 0;
  y = 0;
  w = display->getWidth();
  h = display->getHeight();
  
  minYValue = displayableItem.min;
  maxYValue = displayableItem.max;
  
  display->setRGB(255, 255, 255);

  // Draw X axis
  display->drawLine(x, y + h - 1, x + w, y + h - 1);
  
  // Draw Y axis
  display->drawLine(x, y, x, y + h);

  // Draw max value
  display->setFont(u8g_font_helvB08);
  display->setPrintPos(x + 2, y);
  display->println(Utils::ftoa(maxYValue, 2));

  // Draw middle value
  display->setPrintPos(x + 2, y + (h / 2) - 4);
  display->println(Utils::ftoa((maxYValue - minYValue) / 2 + minYValue, 2));

  // Draw min value
  display->setPrintPos(x + 2, y + h - 10);
  display->println(Utils::ftoa(minYValue, 2));

  // Draw title
  char titleArray[displayableItem.title.length() + 1];
  displayableItem.title.toCharArray(titleArray, displayableItem.title.length() + 1);
  display->setPrintPos(w - display->getStrWidth(titleArray), 0);
  display->println(displayableItem.title);

  // Draw value under title
  String value = DisplayableItems::getItemValueByItem(displayableItem);
  char valueArray[value.length() + 1];
  value.toCharArray(valueArray, value.length() + 1);
  display->setPrintPos(w - display->getStrWidth(valueArray), 10);
  display->println(value);

  // We need at least two pairs of coordinates to draw a line
  if (nbValuesInArray > 1) {
    uint16_t pixelsBetweenValue = w / NB_VALUES_TO_DISPLAY;

    float currentValue;
    uint16_t currentY, oldX, oldY;
    
    float oldValue = values[nbValuesInArray - 1];
    // Draw the values
    for (int16_t i = nbValuesInArray - 1; i >= 0; i--) {
      currentValue = values[i];

      currentY = y + h * (1 - (currentValue - minYValue) / (maxYValue - minYValue));
      
      oldX = w - (x + (pixelsBetweenValue * (nbValuesInArray - 1 - i)) + 1);
      oldY = y + h * (1 - (oldValue - minYValue) / (maxYValue - minYValue));

      // Draw new value
      display->drawLine(oldX, oldY, max(0, oldX - pixelsBetweenValue), currentY);
      
      oldValue = currentValue;
    }
  }
}

/**
 * Add a new value to the values array
 *
 * @param value The new value to be added and displayed
 */
void Histogram::addValue(float value) {
  // Make sure we are not going over the limit
  if (value > maxYValue) {
    value = maxYValue;
  }
  
  // Make sure we are not going under the limit
  if (value < minYValue) {
    value = minYValue;
  }
  
  // If we haven't reached the limit of the array
  if (nbValuesInArray < NB_VALUES_TO_DISPLAY) {
    values[nbValuesInArray++] = value;
  }
  // Otherwise we shift all values and replace the last one with the new value
  else {
    uint16_t i;
    for (i = 0; i < NB_VALUES_TO_DISPLAY - 1; i++) {
      values[i] = values[i + 1];
    }
    
    values[i] = value;
  }
}