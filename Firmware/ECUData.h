/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef ECUData_t_h
#define ECUData_t_h

typedef struct ECUData_t {
  String title;
  String units;
  byte block; 
  unsigned int offset; 
  byte reqbytes;
  float scale;
  float translate;
  float value;
  unsigned int digits;
};

#endif;