#include "StepperSettings.h"

StepperSettings_t StepperSettings::stepperSettings;

/**
 * Set the stepper settings object
 *
 * @param pStepperSettings The new stepper settings object to be used by this class
 */
void StepperSettings::set(StepperSettings_t pStepperSettings) {
  stepperSettings = pStepperSettings;
}

/*
 * @return Get the current stepper settings object
 */
StepperSettings_t StepperSettings::get() {
  return stepperSettings;
}