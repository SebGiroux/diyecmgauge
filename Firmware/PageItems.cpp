/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "PageItems.h"

static PageSettings_t pages[NB_DISPLAY_PAGES] = {
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},  
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1},
  {UNUSED, -1, -1, -1, -1, -1, -1}
};

PageSettings_t PageItems::getPageAtIndex(byte index) {
  return pages[index];
}

void PageItems::setPageAtIndex(byte index, PageSettings_t page) {
  pages[index] = page;
}

/**
 * @return The number of currently used pages
 */
byte PageItems::getNbUsedPages() {
  byte nbUsedPages = 0;

  for (byte i = 0; i < NB_DISPLAY_PAGES; i++) {
    if (pages[i].type != UNUSED) {
      nbUsedPages++;
    }
  }
  
  return nbUsedPages;
}
