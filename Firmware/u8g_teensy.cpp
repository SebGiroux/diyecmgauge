#include "u8g_teensy.h"
#include <SPI.h>

uint8_t u8g_com_hw_spi_fn(u8g_t *u8g, uint8_t msg, uint8_t arg_val, void *arg_ptr)
{
  switch(msg)
  {
    case U8G_COM_MSG_STOP:
      // Stop the device
      break;

    case U8G_COM_MSG_INIT:
      // Init hardware interfaces, timers, GPIOs...
      pinMode(SCREEN_DC, OUTPUT);
      // For hardware SPI
      pinMode(SCREEN_CS, OUTPUT);
      SPI.begin();
      
      pinMode(SCREEN_RESET, OUTPUT);
      digitalWrite(SCREEN_RESET, HIGH);
      // VDD (3.3V) goes high at start, lets just chill for a ms
      delay(1);
      // Bring reset low
      digitalWrite(SCREEN_RESET, LOW);
      // Wait 10ms
      delay(10);
      // Bring out of reset
      digitalWrite(SCREEN_RESET, HIGH);

      break;

    case U8G_COM_MSG_ADDRESS:  
      // Switch from data to command mode (arg_val == 0 for command mode)
      if (arg_val != 0)
      {
          digitalWrite(SCREEN_DC, HIGH);
      }
      else
      {
          digitalWrite(SCREEN_DC, LOW);
      }

      break;
  
    case U8G_COM_MSG_CHIP_SELECT:
      if (arg_val == 0)
      {
        digitalWrite(SCREEN_CS, HIGH);
      }
      else {
        digitalWrite(SCREEN_CS, LOW);
      }
      break;

    case U8G_COM_MSG_RESET:
      // Toggle the reset pin on the display by the value in arg_val
      digitalWrite(SCREEN_RESET, arg_val);
      break;

    case U8G_COM_MSG_WRITE_BYTE:
      // Write byte to device
      SPI.beginTransaction(SPISettings());
      SPI.transfer(arg_val);
      SPI.endTransaction();

      break;

    case U8G_COM_MSG_WRITE_SEQ:
    case U8G_COM_MSG_WRITE_SEQ_P:
    {
      // Write a sequence of bytes to the device
      SPI.beginTransaction(SPISettings());
      register uint8_t *ptr = static_cast<uint8_t *>(arg_ptr);
      while (arg_val > 0)
      {
        SPI.transfer(*ptr++);
        arg_val--;
      }
      SPI.endTransaction();
    }
    break;

  }

  return 1;
}