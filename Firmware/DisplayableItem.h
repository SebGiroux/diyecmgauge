/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef DisplayableItem_t_h
#define DisplayableItem_t_h

struct DisplayableItem_t {
  String title;
  String units;
  uint8_t block; 
  uint16_t offset; 
  uint8_t reqbytes;
  double scale;
  double translate;
  double value;
  uint8_t digits;
  int16_t min;
  int16_t max;
  bool fetchFromECU;
  String (*callback)(void);
};

#endif
