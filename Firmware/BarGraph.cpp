/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "BarGraph.h"

/**
 * Constructor that take the display object in parameter
 *
 * @param pDisplay The display object that will be used to draw the histogram
 */
BarGraph::BarGraph(U8GLIB *pDisplay) {
  display = pDisplay;
}

/**
 * Draw the bar graph
 *
 * @param displayableItem The item to be displayed by this bar graph
 * @param index The index of the bar graph on the page (0 based)
 * @param pageSize The total number of bar graphs on the page
 */
void BarGraph::drawBarGraph(DisplayableItem_t displayableItem, uint8_t index, uint8_t pageSize) {
  float percent = (DisplayableItems::getItemFloatValueByItem(displayableItem) - displayableItem.min) / (displayableItem.max - displayableItem.min);
  percent = max(0, min(percent, 100)); // Make sure percent is a 0-100 value

  uint16_t width = percent * (display->getWidth() - (BAR_GRAPH_SIDE_MARGIN * 2) + 4);

  String value = DisplayableItems::getItemValueByItem(displayableItem);

  uint8_t textSize = 1;
  uint8_t textYOffset = 0;
  uint8_t barYOffset = 0;
  uint8_t barHeight = 0;

  switch (pageSize) {
    case 1:
      textSize = 2;
      textYOffset = BAR_GRAPH_ONE_ITEM_TOP_OFFSET;
      barYOffset = 20 + textYOffset;
      barHeight = BAR_GRAPH_ONE_ITEM_BAR_HEIGHT;
      break;

    case 2:
      textSize = 2;
      textYOffset = index * BAR_GRAPH_TWO_ITEMS_HEIGHT + BAR_GRAPH_TWO_ITEMS_TOP_OFFSET;
      barYOffset = 20 + textYOffset;
      barHeight = BAR_GRAPH_TWO_ITEMS_BAR_HEIGHT;
      break;

    case 3:
      textYOffset = index * BAR_GRAPH_THREE_ITEMS_HEIGHT + BAR_GRAPH_THREE_ITEMS_TOP_OFFSET;
      barYOffset = 12 + textYOffset;
      barHeight = BAR_GRAPH_THREE_ITEMS_BAR_HEIGHT;
      break;

    case 4:
      textYOffset = index * BAR_GRAPH_FOUR_ITEMS_HEIGHT + BAR_GRAPH_FOUR_ITEMS_TOP_OFFSET;
      barYOffset = 12 + textYOffset;
      barHeight = BAR_GRAPH_FOUR_ITEMS_BAR_HEIGHT;
      break;

    case 5:
      textYOffset = index * BAR_GRAPH_FIVE_ITEMS_HEIGHT + BAR_GRAPH_FIVE_ITEMS_TOP_OFFSET;
      barYOffset = 12 + textYOffset;
      barHeight = BAR_GRAPH_FIVE_ITEMS_BAR_HEIGHT;
      break;
  }

  display->setFontPosTop();
  display->setRGB(255, 255, 255);

  // Draw bar
  display->drawFrame(BAR_GRAPH_SIDE_MARGIN, barYOffset, display->getWidth() - BAR_GRAPH_SIDE_MARGIN, barHeight);
  display->drawBox(BAR_GRAPH_SIDE_MARGIN + 1, barYOffset, width, barHeight);

  // Draw title on the left
  if (textSize == 2) {
    display->setFont(u8g_font_helvB10);
  }
  else {
    display->setFont(u8g_font_helvB08);
  }
  display->setPrintPos(BAR_GRAPH_SIDE_MARGIN, textYOffset);
  display->println(displayableItem.title);

  // Align text to the right of the screen
  char valueArray[value.length() + 1];
  value.toCharArray(valueArray, value.length() + 1);
  display->setPrintPos(display->getWidth() - display->getStrWidth(valueArray), textYOffset); // each character is 6 pixels when text size is 1
  display->println(value);
}
