/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef Alarm_t_h
#define Alarm_t_h

enum AlarmOperator {
  GREATHER_THAN, EQUAL_TO, LESS_THAN
};

enum AlarmCondition {
  NONE, AND, OR
};

enum AlarmConditionIndex {
  FIRST, SECOND
};

struct Alarm_t {
  bool used;
  char name[16];

  // First condition
  uint8_t channel1;
  AlarmOperator operator1;
  float threshold1;
  float hysteresis1;

  AlarmCondition condition;

  // Second condition
  uint8_t channel2;
  AlarmOperator operator2;
  float threshold2;
  float hysteresis2;

  bool triggered;

  bool showWarningOnScreen;
  bool triggerBuzzer;
  bool startDataLogging;
  bool turnOnLeftLED;
  bool turnOnRightLED;
};

#endif
