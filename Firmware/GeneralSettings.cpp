#include "GeneralSettings.h"

GeneralSettings_t GeneralSettings::generalSettings;

/**
 * Set the general settings object
 *
 * @param pGeneralSettings The new general settings object to be used by this class
 */
void GeneralSettings::set(GeneralSettings_t pGeneralSettings) {
  generalSettings = pGeneralSettings;
}

/*
 * @return Get the current general settings object
 */
GeneralSettings_t GeneralSettings::get() {
  return generalSettings;
}