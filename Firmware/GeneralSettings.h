/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef GeneralSettings_t_h
#define GeneralSettings_t_h

#include <Arduino.h>

enum SpeedUnit {
  KMH, MPH
};

enum TemperatureUnit {
  CELSIUS, FARENHEIT
};

enum SpeedSourceDynamometer {
  ECU_VSS, INTERNAL_GPS
};

struct GeneralSettings_t {
  SpeedUnit speedUnit;
  TemperatureUnit temperatureUnit;
  SpeedSourceDynamometer speedSourceDynamometer;
  int8_t timeZone;
  bool rtcSyncWithGps;
  int16_t rtcCompensate;
  bool configurationSerialPortOnMainConnector;
};

class GeneralSettings {
  public:
    static void set(GeneralSettings_t generalSettings);
    static GeneralSettings_t get();

  private:
    static GeneralSettings_t generalSettings;
};

#endif
