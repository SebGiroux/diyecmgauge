/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef VehicleSettings_t_h
#define VehicleSettings_t_h

#include <Arduino.h>

struct VehicleSettings_t {
  uint16_t vehicleWeight;
  double coefficientOfDrag;
  double coefficientOfRollingResistance;
  double frontalArea;
};

class VehicleSettings {
  public:
    static void set(VehicleSettings_t vehicleSettings);
    static VehicleSettings_t get();

  private:
    static VehicleSettings_t vehicleSettings;
};

#endif
