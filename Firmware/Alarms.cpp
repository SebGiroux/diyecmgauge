﻿/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "Alarms.h"

static Alarm_t alarms[NB_ALARMS] = {
  {false, "", 0, GREATHER_THAN, 0, 0, NONE, 0, GREATHER_THAN, 0, 0, false, false, false, false},
  {false, "", 0, GREATHER_THAN, 0, 0, NONE, 0, GREATHER_THAN, 0, 0, false, false, false, false},
  {false, "", 0, GREATHER_THAN, 0, 0, NONE, 0, GREATHER_THAN, 0, 0, false, false, false, false}
};

/**
 * Constructor that receive various objects that can be triggered by alarms
 *
 * @param pDataLogging The data logging object (to be used if an alarm needs to trigger data logging)
 * @param pLEDs The LEDs object (to be used if an alarm needs to trigger the LEDs)
 * @param pBuzzer The Buzzer object (to be used if an alarm needs to trigger the buzzer)
 */
void Alarms::begin(DataLogging* pDataLogging, LEDs* pLEDs, Buzzer* pBuzzer) {
  dataLogging = pDataLogging;
  leds = pLEDs;
  buzzer = pBuzzer;

  currentScreenAlarmIndex = 0;
  currentScreenAlarmFlashes = 0;
}

/**
 * Check if the condition of an alarm is met
 *
 * @param alarm The alarm object to check the condition
 * @param conditionIndex The index of the alarm condition to check
 */
bool Alarms::isAlarmConditionMet(Alarm_t alarm, AlarmConditionIndex conditionIndex) {
  AlarmOperator alarmOperator = GREATHER_THAN;

  float value = 0;
  float threshold = 0;
  float hysteresis = 0;

  switch (conditionIndex) {
    case FIRST:
      alarmOperator = alarm.operator1;

      value = DisplayableItems::getItemAtIndex(alarm.channel1).value;
      threshold = alarm.threshold1;
      hysteresis = alarm.hysteresis1;
      break;

    case SECOND:
      alarmOperator = alarm.operator2;

      value = DisplayableItems::getItemAtIndex(alarm.channel2).value;
      threshold = alarm.threshold2;
      hysteresis = alarm.hysteresis2;
      break;
  }

  bool conditionMet = false;

  switch (alarmOperator) {
    case GREATHER_THAN:
      if (value > threshold) {
        conditionMet = true;
      }

      break;

    case EQUAL_TO:
      if (value == threshold) {
        conditionMet = true;
      }
      break;

    case LESS_THAN:
      if (value < threshold) {
        conditionMet = true;
      }
      break;
  }

  // If the alarm is currently triggered, it will stay trigger if its within the hysteresis
  if (alarm.triggered) {
    if (value < (threshold + (hysteresis * 0.5)) && value > (threshold - (hysteresis * 0.5))) {
      conditionMet = true;
    }
  }

  return conditionMet;
}

/**
 * Figure out if the alarm is triggered based on the conditions
 *
 * @param alarm The alarm to check
 *
 * @return true if the alarm should be triggered, false otherwise
 */
bool Alarms::isAlarmTriggered(Alarm_t alarm) {
  bool condition1met = isAlarmConditionMet(alarm, FIRST);

  bool condition2met = false;
  if (alarm.condition != NONE) {
    condition2met = isAlarmConditionMet(alarm, SECOND);
  }

  bool alarmTriggered = false;

  switch (alarm.condition) {
    case AND:
      if (condition1met && condition2met) {
        alarmTriggered = true;
      }
      break;

    case OR:
      if (condition1met || condition2met) {
        alarmTriggered = true;
      }
      break;

    case NONE:
      if (condition1met) {
        alarmTriggered = true;
      }
      break;
  }

  return alarmTriggered;
}

/**
 * Returns the number of alarm that are currently triggered
 *
 * @param onlyShowOnScreenAlarm true if it should only count the alarms that are displayed on screen
 * @return The number of alarms that are currently triggered
 */
uint8_t Alarms::getNbTriggeredAlarms(bool onlyShowOnScreenAlarm) {
  uint8_t nbTriggeredAlarms = 0;

  for (uint8_t i = 0; i < NB_ALARMS; i++) {
    if (alarms[i].used && alarms[i].triggered && (alarms[i].showWarningOnScreen || !onlyShowOnScreenAlarm)) {
      nbTriggeredAlarms++;
    }
  }

  return nbTriggeredAlarms;
}

/**
 * Scan the triggered alarms that we should display a warning on the screen and
 * return the alarm object at the specified index.
 *
 * @param index The index of the alarm to get (0 based)
 * @return The alarm object
 */
Alarm_t* Alarms::getTriggeredShowWarningOnScreenAlarmAtIndex(uint8_t index) {
  uint8_t nbTriggeredAlarms = 0;

  for (uint8_t i = 0; i < NB_ALARMS; i++) {
    if (alarms[i].used && alarms[i].triggered && alarms[i].showWarningOnScreen) {
      nbTriggeredAlarms++;
    }

    // Check if we found the one we're interested in
    if ((nbTriggeredAlarms - 1) == index) {
      return &alarms[i];
    }
  }

  return NULL;
}

/**
 * Check all the alarms to see if one should be triggered
 */
void Alarms::check() {
  for (uint8_t i = 0; i < NB_ALARMS; i++) {
    // Skip unused alarms
    if (!alarms[i].used) {
      continue;
    }

    Alarm_t alarm = alarms[i];

    // If the alarm is currently triggered
    if (isAlarmTriggered(alarm)) {
      if (getNbTriggeredAlarms(false) == 0) {
        // If the alarm should trigger the buzzer and the buzzer isn't already triggered
        if (alarm.triggerBuzzer && !buzzer->isTriggered()) {
          buzzer->start();
        }

        // If the alarm should start data logging and that we're not currently data logging
        if (alarm.startDataLogging && dataLogging->getStatus() == NOT_DATA_LOGGING) {
          dataLoggingStartTimeStamp = millis();
          dataLogging->startLogging();
        }

        if (alarm.turnOnLeftLED) {
          leds->turnOnLeftLED();
        }
        if (alarm.turnOnRightLED) {
          leds->turnOnRightLED();
        }
      }

      alarms[i].triggered = true;
    }
    else {
      if (getNbTriggeredAlarms(false) > 0) {
        if (alarm.triggerBuzzer) {
          buzzer->stop();
        }

        if (alarm.turnOnLeftLED) {
          leds->turnOffLeftLED();
        }
        if (alarm.turnOnRightLED) {
          leds->turnOffRightLED();
        }
      }

      alarms[i].triggered = false;
    }
  }

  // No more alarms that show warning on screen, reset the counters
  if (getNbTriggeredAlarms(true) == 0) {
    currentScreenAlarmIndex = 0;
    currentScreenAlarmFlashes = 0;
  }

  // If the number of seconds we need to data log after an alarm has been triggered is elapsed
  if (getNbTriggeredAlarms(false) && (millis() - dataLoggingStartTimeStamp) > (DATALOGGING_SECONDS * 1000)) {
    dataLogging->stopLogging();
  }
}

/**
 * Returns the char value of an alarm condition operator
 *
 * @param alarmOperator The operator to get the char representation of
 *
 * @return The char representation of the specified operator
 */
char Alarms::getOperatorChar(AlarmOperator alarmOperator) {
  char operatorChar = ' ';

  switch (alarmOperator) {
    case GREATHER_THAN:
      operatorChar = '>';
      break;
    case EQUAL_TO:
      operatorChar = '=';
      break;
    case LESS_THAN:
      operatorChar = '<';
      break;
  }

  return operatorChar;
}

/**
 * Display an alarm condition on the screen in the following format (channel operator threshold, ex: TPS > 90)
 *
 * @param alarm The alarm object to display the condition
 * @param conditionIndex The index of the alarm condition to display
 * @param y The y position of where to display the text on the screen
 */
void Alarms::displayAlarmCondition(U8GLIB& display, Alarm_t alarm, AlarmConditionIndex conditionIndex, uint8_t y) {
  AlarmOperator alarmOperator = GREATHER_THAN;
  uint8_t channel = 0;
  float threshold = 0;

  switch (conditionIndex) {
    case FIRST:
      alarmOperator = alarm.operator1;
      channel = alarm.channel1;
      threshold = alarm.threshold1;
      break;

    case SECOND:
      alarmOperator = alarm.operator2;
      channel = alarm.channel2;
      threshold = alarm.threshold2;
      break;
  }

  DisplayableItem_t displayableItem = DisplayableItems::getItemAtIndex(channel);

  char alarmOperatorChar = getOperatorChar(alarmOperator);
  String thresholdText = Utils::ftoa(threshold, displayableItem.digits);

  String conditionText = displayableItem.title;
  conditionText += " ";
  conditionText += alarmOperatorChar;
  conditionText += " ";
  conditionText += thresholdText;

  display.setPrintPos(Utils::getXPositionToCenterInScreen(display, conditionText), y);
  display.print(conditionText);
}

/**
 * Display all the triggered alarms (that have the showOnScreen flag set) on the screen
 * by flashing each alarm NB_FLASHES_PER_ALARM times and starting over 
 *
 * @param display The display object where the alarms should be displayed
 */
void Alarms::displayAlarmPage(U8GLIB& display) {
  // Wait DELAY_BETWEEN_ALARM_FLASHES milliseconds before changing current alarm flashing state
  if ((millis() - lastFlashMillis) > DELAY_BETWEEN_ALARM_FLASHES) {
    if ((currentScreenAlarmFlashes % 2) == 0) {
      display.firstPage();
      do {
        Alarm_t* alarm = getTriggeredShowWarningOnScreenAlarmAtIndex(currentScreenAlarmIndex);
        if (alarm != NULL) {
          display.setFontPosTop();
          display.setFont(u8g_font_helvB10);
          display.setRGB(255, 255, 255);

          // Center alarm name in screen
          display.setPrintPos(Utils::getXPositionToCenterInScreen(display, alarm->name), 15);
          display.print(alarm->name);

          bool firstConditionMet = isAlarmConditionMet(*alarm, FIRST);
          bool secondConditionMet = isAlarmConditionMet(*alarm, SECOND);

          // If first condition is met, display it on the screen
          if (firstConditionMet) {
            displayAlarmCondition(display, *alarm, FIRST, 55);
          }

          // If second condition is met, display it on screen
          if (secondConditionMet) {
            uint8_t y = 55;
            // Display "And" on screen if there is more than one condition met
            if (firstConditionMet) {
              y = 75;

              String andCondition = "And";

              display.setPrintPos(Utils::getXPositionToCenterInScreen(display, andCondition), y);
              display.print(andCondition);

              y += 20;
            }

            displayAlarmCondition(display, *alarm, SECOND, y);
          }
        }
      } while (display.nextPage());
    }
    else {
      // Clear screen (empty picture loop)
      display.firstPage();
      while (display.nextPage());
    }

    lastFlashMillis = millis();
    currentScreenAlarmFlashes++;
  }

  if (currentScreenAlarmFlashes == NB_FLASHES_PER_ALARM) {
    currentScreenAlarmFlashes = 0;
    currentScreenAlarmIndex++;
  }

  // If we've rotated all the alarms, start over
  if (currentScreenAlarmIndex >= getNbTriggeredAlarms(true)) {
    currentScreenAlarmIndex = 0;
  }
}

/**
 * Return the alarm object at the specified index
 *
 * @param index The index of the alarm to get
 * @return The alarm object at the specified index
 */
Alarm_t Alarms::getAlarmAtIndex(uint8_t index) {
  return alarms[index];
}

/**
 * Set the alarm object at the specified index with the one passed in parameter
 *
 * @param index The index of the alarm to set
 * @param alarm The alarm object to be set
 */
void Alarms::setAlarmAtIndex(uint8_t index, Alarm_t alarm) {
  alarms[index] = alarm;
}