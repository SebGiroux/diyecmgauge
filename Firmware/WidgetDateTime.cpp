/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "WidgetDateTime.h"

/**
 * Constructor that take the display object in parameter as well as the real-time clock
 *
 * @param pDisplay The display object that will be used to draw the widget
 * @param pRtc The real-time clock object to get the date / time from
 */
WidgetDateTime::WidgetDateTime(U8GLIB *pDisplay, RTC *pRtc) {
  display = pDisplay;
  rtc = pRtc;
}

void WidgetDateTime::printTwoDigits(uint8_t number) {
  if (number >= 0 && number < 10) {
    display->write('0');
  }

  display->print(number);
}

/**
 * Display the date and time on the screen
 */
void WidgetDateTime::displayWidget() {
  tmElements_t tm = rtc->getTime();

  display->firstPage();

  do {
    display->setFontPosTop();
    display->setFont(u8g_font_helvB18);
    display->setRGB(255, 255, 255);
    
    // Time
    display->setPrintPos(25, 50);
    printTwoDigits(tm.Hour);
    display->write(':');
    printTwoDigits(tm.Minute);
    display->setFont(u8g_font_helvB14);
    display->write(':');
    printTwoDigits(tm.Second);
    display->println();

    // Date
    display->setPrintPos(20, 90);
    printTwoDigits(tm.Day);
    display->write('/');
    printTwoDigits(tm.Month);
    display->write('/');
    display->print(tmYearToCalendar(tm.Year));
    display->println();
  } while (display->nextPage());
}
