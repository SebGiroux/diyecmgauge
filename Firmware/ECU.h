/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef ECU_h
#define ECU_h

#include <Arduino.h>

// This is an abstract class that every ECU implementation class (MegaSquirt, OBD-II, GM OBD-I ALDL, etc.) should extends
class ECU {
  public:
    virtual void begin() = 0;
    virtual bool requestData() = 0;
    virtual String getDataLogHeader() = 0;
    virtual void refreshGeneralSettings() = 0;
    virtual bool isConfigError() = 0;
};
#endif