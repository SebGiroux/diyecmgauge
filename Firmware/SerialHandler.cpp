﻿/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "SerialHandler.h"

static char usbBuffer[SERIAL_COMMANDS_BUFFER_SIZE];
// We use a second buffer because strtok modify the original string
static char usbBufferTokenized[SERIAL_COMMANDS_BUFFER_SIZE];

static char mainConnectorSerialPortBuffer[SERIAL_COMMANDS_BUFFER_SIZE];
// We use a second buffer because strtok modify the original string
static char mainConnectorSerialPortBufferTokenized[SERIAL_COMMANDS_BUFFER_SIZE];

static SerialWriterChecksum usbWriter(&Serial);
static SerialWriterChecksum mainConnectorSerialPortWriter(&Serial1);

static uint8_t usbBufferPosition = 0;
static uint8_t mainConnectorSerialPortBufferPosition = 0;

/**
 * Check if any data is avaiable on the serial lines, and process it
 */
void SerialHandler::checkSerial() {
  GeneralSettings_t generalSettings = GeneralSettings::get();
  if (generalSettings.configurationSerialPortOnMainConnector) {
    while (Serial1.available() > 0) {
      if (isBufferOverflow(mainConnectorSerialPortBufferPosition)) {
        mainConnectorSerialPortBufferPosition = 0;
      }

      char data = Serial1.read();
      validateReceivedCommand(mainConnectorSerialPortWriter, data, mainConnectorSerialPortBuffer, mainConnectorSerialPortBufferTokenized, mainConnectorSerialPortBufferPosition);
    }
  }

  while (Serial.available() > 0) {
    if (isBufferOverflow(usbBufferPosition)) {
      usbBufferPosition = 0;
    }

    char data = Serial.read();
    validateReceivedCommand(usbWriter, data, usbBuffer, usbBufferTokenized, usbBufferPosition);
  }
}

/**
 * Check if a serial buffer overflowed based on it's position
 *
 * @param bufferPosition The current position of the buffer
 *
 * @return true if buffer has overflowed, false otherwise
 */
bool SerialHandler::isBufferOverflow(uint8_t bufferPosition) {
  if ((bufferPosition + 1) == SERIAL_COMMANDS_BUFFER_SIZE) {
    sendDebug(F("USB serial buffer overflow"));
    return true;
  }

  return false;
}

/**
 * Validate (and handle) a serial command that was received
 *
 * @param serialWriter The serial writer to use to send the response
 * @param data The char that was received
 * @param buffer The whole buffer for the current transaction
 * @param bufferTokenized A buffer that we can use for tokenization
 * @param bufferPosition The current position of the buffer
 */
void SerialHandler::validateReceivedCommand(SerialWriterChecksum serialWriter, char data, char (&buffer)[SERIAL_COMMANDS_BUFFER_SIZE], char (&bufferTokenized)[SERIAL_COMMANDS_BUFFER_SIZE], uint8_t &bufferPosition) {
  if (data == SERIAL_COMMAND_TERMINATION_CHARACTER[0]) {
    // We just received a new command and the response will begin right after this, so reset the checksum
    serialWriter.resetChecksum();

    // Print the command that we received as the beginning of the response so the client knows which command we're replying to
    serialWriter.print(buffer[0]);
    serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);

    // Checksum validation
    strcpy(bufferTokenized, buffer);
    strtok(bufferTokenized, SERIAL_COMMAND_VALUES_SEPARATOR);
    
    char *args = strtok(NULL, SERIAL_COMMAND_VALUES_SEPARATOR);
    char *checksum = strtok(NULL, SERIAL_COMMAND_VALUES_SEPARATOR);

    if (checksum == NULL) {
      checksum = args;
      args = NULL;
    }

    // Make sure the checksum is valid, otherwise reply with a checksum mistmatch response
    if (validateChecksum(buffer, checksum)) {
      switch (buffer[0]) {
        // Show firmware version
        case 'V':
          showVersion(serialWriter);
          break;

        // SD card - Get stats (ex: S)
        case 'S':
          sdCardGetStats(serialWriter);
          break;

        // Data logging - Get status
        case 'T':
          dataLoggingGetStatus(serialWriter);
          break;

        // Data logging - start
        case 'U':
          dataLoggingStart(serialWriter);
          break;

        // Data logging - stop
        case 'W':
          dataLoggingStop(serialWriter);
          break;

        // Pages - Get list
        case 'P':
          pagesList(serialWriter);
          break;

        // Pages - Edit
        case 'Q':
          editPage(serialWriter, args);
          break;

        // Indicators - Get list
        case 'I':
          indicatorsList(serialWriter);
          break;

        // Alarms - Get list
        case 'A':
          alarmsList(serialWriter);
          break;

        // Alarms - Edit
        case 'B':
          editAlarm(serialWriter, args);
          break;

        // Data logs - Directory listing (ex: L)
        case 'L':
          dataLogsDirectoryListing(serialWriter);
          break;

        // Data logs - Delete file (ex: D LOG001.MSL)
        case 'D':
          dataLogsDeleteFile(serialWriter, args);
          break;

        // Data logs - Get (download) file (ex: G LOG001.MSL offset size)
        case 'G':
          dataLogsDownloadFile(serialWriter, args);
          break;

        // RTC - Get current date / time
        case 'R':
          rtcGetCurrentDateTime(serialWriter);
          break;

        // RTC - Set current date / time
        case 'C':
          rtcSetCurrentDateTime(serialWriter, args);
          break;

        // Get general settings
        case 'M':
          sendGeneralSettings(serialWriter);
          break;

        // Edit general settings
        case 'N':
          editGeneralSettings(serialWriter, args);
          break;

        // Get vehicle settings
        case 'X':
          sendVehicleSettings(serialWriter);
          break;

        // Edit vehicle settings
        case 'Y':
          editVehicleSettings(serialWriter, args);
          break;

        // Get stepper settings
        case 'J':
          sendStepperSettings(serialWriter);
          break;

        // Edit stepper settings
        case 'K':
          editStepperSettings(serialWriter, args);
          break;

        // Change display page
        case 'Z':
          changeDisplayPage(serialWriter, args);
          break;
      }
    }
    else {
      serialWriter.print(SERIAL_CHECKSUM_MISMATCH);
      serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
      serialWriter.print(serialWriter.getChecksum());
      serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
    }

    // Reset the buffer to be ready for the next command
    memset(buffer, 0, sizeof(buffer));
    bufferPosition = 0;
  }
  else {
    buffer[bufferPosition++] = data;
  }
}

/**
 * Validate the check sum of serial communication
 *
 * @param buffer The buffer to validate the checksum for
 * @param checksum The checksum we are validating against
 *
 * @return true if the validation was succesful, false otherwise
 */
bool SerialHandler::validateChecksum(char *buffer, char *checksum) {
  uint8_t i = 0;
  uint8_t sum = 0;

  // Find position of the last occurence of SERIAL_COMMAND_VALUES_SEPARATOR
  uint8_t checksumPosition = strrchr(buffer, SERIAL_COMMAND_VALUES_SEPARATOR[0]) - buffer + 1;

  while (i < checksumPosition) {
    sum += buffer[i++];
  }

  return (sum == atoi(checksum));
}

/**
 * Count the number of arguments in the specified string
 *
 * @param args The arguments string to count the number of arguments
 */
uint8_t SerialHandler::getArgumentsCount(char *args) {
  uint8_t argsCount = 1;

  for (int i = 0; args[i] != '\0'; i++) {
    if (args[i] == SERIAL_COMMAND_SUB_VALUES_SEPARATOR[0]) {
      argsCount++;
    }
  }

  return argsCount;
}

/**
 * Show the current firmware version string
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::showVersion(SerialWriterChecksum serialWriter) {
  serialWriter.print(VERSION);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Get stats from the SD card
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::sdCardGetStats(SerialWriterChecksum serialWriter) {
  uint8_t blocksPerCluster = 0;
  uint32_t clusterCount = 0;

  // If there is an SD card installed in the gauge, get the number of
  // blocks per cluster and the number of cluster of it
  extern DataLogging dataLogging;
  if (dataLogging.getStatus() != NO_SD_CARD) {
    blocksPerCluster = dataLogging.getSdCardBlocksPerCluster();
    clusterCount = dataLogging.getClustersCount();
  }

  /*
   * Let's do the calculation on the Java side instead.
   * uint32_t could support up to 4GB card before it overflows.
   *
   * Volume size in bytes = blocksPerCluster * clusterCount * 512
   */
  serialWriter.print(blocksPerCluster);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(clusterCount);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Send the current data logging status
 */
void SerialHandler::dataLoggingSendStatus() {
  extern DataLogging dataLogging;

  usbWriter.resetChecksum();
  usbWriter.print('T');
  usbWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  usbWriter.print(dataLogging.getStatus());
  usbWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  usbWriter.print(usbWriter.getChecksum());
  usbWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);

  mainConnectorSerialPortWriter.resetChecksum();
  mainConnectorSerialPortWriter.print('T');
  mainConnectorSerialPortWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  mainConnectorSerialPortWriter.print(dataLogging.getStatus());
  mainConnectorSerialPortWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  mainConnectorSerialPortWriter.print(mainConnectorSerialPortWriter.getChecksum());
  mainConnectorSerialPortWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Get the status of the data logging (no SD card, not data logging, data logging)
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::dataLoggingGetStatus(SerialWriterChecksum serialWriter) {
  extern DataLogging dataLogging;

  serialWriter.print(dataLogging.getStatus());
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Start the data logging
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::dataLoggingStart(SerialWriterChecksum serialWriter) {
  extern DataLogging dataLogging;

  serialWriter.print(SERIAL_OK);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);

  serialWriter.resetChecksum();

  // Make sure SD card is there and we're not already data logging
  if (dataLogging.getStatus() == NOT_DATA_LOGGING) {
    dataLogging.startLogging();
  }
}

/**
 * Stop the data logging
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::dataLoggingStop(SerialWriterChecksum serialWriter) {
  extern DataLogging dataLogging;

  serialWriter.print(SERIAL_OK);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);

  serialWriter.resetChecksum();

  // Make sure we're currently data logging
  if (dataLogging.getStatus() == DATA_LOGGING) {
    dataLogging.stopLogging();
  }
}

/**
 * Send the list of configured pages
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::pagesList(SerialWriterChecksum serialWriter) {
  for (uint8_t i = 0; i < NB_DISPLAY_PAGES; i++) {
    PageSettings_t page = PageItems::getPageAtIndex(i);

    serialWriter.print(page.type);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(page.indicator1);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(page.indicator2);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(page.indicator3);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(page.indicator4);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(page.indicator5);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(page.indicator6);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(page.indicator7);

    // If it's not the last page, we add a separator
    if (i < (NB_DISPLAY_PAGES - 1)) {
      serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
    }
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Edit a page of the gauge and save it to the EEPROM and RAM
 *
 * @param serialWriter The serial writer to use to send the response
 * @param args <page index><page type><indicator 1><indicator 2><indicator 3>
 */
void SerialHandler::editPage(SerialWriterChecksum serialWriter, char *args) {
  if (getArgumentsCount(args) == SERIAL_EDIT_PAGE_ARGUMENTS_COUNT) {
    unsigned int pageIndex = atoi(strtok(args, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    unsigned int pageType = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));

    PageSettings_t page = PageItems::getPageAtIndex(pageIndex);

    page.type = static_cast<PageType>(pageType);
    page.indicator1 = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    page.indicator2 = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    page.indicator3 = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    page.indicator4 = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    page.indicator5 = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    page.indicator6 = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    page.indicator7 = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));

    // Write page to RAM
    PageItems::setPageAtIndex(pageIndex, page);

    // Write page to EEPROM
    Settings::writePage(pageIndex, page);

    serialWriter.print(SERIAL_OK);
  }
  else {
    serialWriter.print(SERIAL_ERR);
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Send the list of indicators available
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::indicatorsList(SerialWriterChecksum serialWriter) {
  for (byte i = 0; i < NB_DISPLAYABLE_ITEMS; i++) {
    DisplayableItem_t item = DisplayableItems::getItemAtIndex(i);

    serialWriter.print(item.title);

    if (i < (NB_DISPLAYABLE_ITEMS - 1)) {
      serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
    }
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Send the list of alarms currently configured
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::alarmsList(SerialWriterChecksum serialWriter) {
  for (byte i = 0; i < NB_ALARMS; i++) {
    Alarm_t alarm = Alarms::getAlarmAtIndex(i);

    serialWriter.print(alarm.used);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.name);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);

    // First condition
    serialWriter.print(alarm.channel1);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.operator1);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.threshold1);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.hysteresis1);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.condition);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);

    // Second condition
    serialWriter.print(alarm.channel2);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.operator2);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.threshold2);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.hysteresis2);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.triggered);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);

    // Reactions
    serialWriter.print(alarm.showWarningOnScreen);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.triggerBuzzer);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.startDataLogging);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.turnOnLeftLED);
    serialWriter.print(SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    serialWriter.print(alarm.turnOnRightLED);

    if (i < (NB_ALARMS - 1)) {
      serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
    }
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Edit an alarm of the gauge and send it to the EEPROM and RAM
 *
 * @param serialWriter The serial writer to use to send the response
 * @param args
 */
void SerialHandler::editAlarm(SerialWriterChecksum serialWriter, char *args) {
  if (getArgumentsCount(args) == SERIAL_EDIT_ALARM_ARGUMENTS_COUNT) {
    unsigned int alarmIndex = atoi(strtok(args, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));

    Alarm_t alarm = Alarms::getAlarmAtIndex(alarmIndex);

    alarm.used = strcmp(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR), "1") == 0 ? true : false;

    strcpy(alarm.name, strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    alarm.name[sizeof(alarm.name) - 1] = '\0';

    alarm.channel1 = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    alarm.operator1 = static_cast<AlarmOperator>(atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR)));
    alarm.threshold1 = atof(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    alarm.hysteresis1 = atof(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));

    alarm.condition = static_cast<AlarmCondition>(atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR)));

    alarm.channel2 = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    alarm.operator2 = static_cast<AlarmOperator>(atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR)));
    alarm.threshold2 = atof(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    alarm.hysteresis2 = atof(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));

    alarm.showWarningOnScreen = strcmp(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR), "1") == 0 ? true : false;
    alarm.triggerBuzzer = strcmp(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR), "1") == 0 ? true : false;
    alarm.startDataLogging = strcmp(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR), "1") == 0 ? true : false;
    alarm.turnOnLeftLED = strcmp(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR), "1") == 0 ? true : false;
    alarm.turnOnRightLED = strcmp(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR), "1") == 0 ? true : false;

    // Write alarm to RAM
    Alarms::setAlarmAtIndex(alarmIndex, alarm);

    // Write alarm to EEPROM
    Settings::writeAlarm(alarmIndex, alarm);

    serialWriter.print(SERIAL_OK);
  }
  else {
    serialWriter.print(SERIAL_ERR);
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * List all the data logs file in the data logs directory
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::dataLogsDirectoryListing(SerialWriterChecksum serialWriter) {
  extern DataLogging dataLogging;
  SdFat sd = dataLogging.getSd();
  
  sd.chdir(DATALOGS_DIRECTORY, true);

  SdFile file;
  dir_t dirEntry;
  char fileName[MAXIMUM_LONG_FILE_NAME_LENGTH];

  while (file.openNext(sd.vwd(), O_READ)) {
    file.getName(fileName, MAXIMUM_LONG_FILE_NAME_LENGTH);

    // Make sure the file has the data log file extension
    if (!Utils::endsWith(fileName, DataLogging::FILE_NAME_EXTENSION)) {
      continue;
    }
    
    if (file.fileSize() > 0) {
      // File name
      serialWriter.print(fileName);

      // Creation date (yyyy-mm-dd)
      file.dirEntry(&dirEntry);
      serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
      serialWriter.print(FAT_YEAR(dirEntry.creationDate));
      serialWriter.print('-');
      serialWriter.print(FAT_MONTH(dirEntry.creationDate));
      serialWriter.print('-');
      serialWriter.print(FAT_DAY(dirEntry.creationDate));

      // Creation time (hh:mm:ss)
      serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
      serialWriter.print(FAT_HOUR(dirEntry.creationTime));
      serialWriter.print(':');
      serialWriter.print(FAT_MINUTE(dirEntry.creationTime));
      serialWriter.print(':');
      serialWriter.print(FAT_SECOND(dirEntry.creationTime));

      // File size (in bytes)
      serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
      serialWriter.print(file.fileSize());

      serialWriter.print(SERIAL_COMMAND_LINES_SEPARATOR);
    }
    file.close();

  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Delete one file in the data logs directory
 *
 * @param serialWriter The serial writer to use to send the response
 * @param fileName The file name to be deleted
 */
void SerialHandler::dataLogsDeleteFile(SerialWriterChecksum serialWriter, char* fileName) {
  char dataLoggingFilePath[DATA_LOG_FILE_PATH_MAX_LENGTH];
  sprintf(dataLoggingFilePath, "%s/%s", DATALOGS_DIRECTORY, fileName);

  extern DataLogging dataLogging;
  SdFat sd = dataLogging.getSd();
  
  if (sd.exists(dataLoggingFilePath)) {
    if (sd.remove(dataLoggingFilePath)) {
      serialWriter.print(SERIAL_OK);
    }
    else {
      serialWriter.print(SERIAL_ERR);
    }
  }
  else {
    serialWriter.print(SERIAL_ERR);
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Download one file in the data logs directory
 *
 * @param serialWriter The serial writer to use to send the response
 * @param args The arguments (file name, offset and size)
 */
void SerialHandler::dataLogsDownloadFile(SerialWriterChecksum serialWriter, char* args) {
  if (getArgumentsCount(args) == SERIAL_GET_DATA_LOG_CHUNK_ARGUMENTS_COUNT) {
    char* fileName = strtok(args, SERIAL_COMMAND_SUB_VALUES_SEPARATOR);
    uint32_t offset = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    uint32_t size = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));

    char dataLoggingFilePath[DATA_LOG_FILE_PATH_MAX_LENGTH];
    sprintf(dataLoggingFilePath, "%s/%s", DATALOGS_DIRECTORY, fileName);
  
    extern DataLogging dataLogging;
    SdFat sd = dataLogging.getSd();
  
    if (sd.exists(dataLoggingFilePath)) {
      File dataLog = sd.open(dataLoggingFilePath, FILE_READ);

      if (dataLog) {
        dataLog.seek(offset);

        for (uint32_t i = 0; i < size; i++) {
          serialWriter.print((char) dataLog.read());
        }

        dataLog.close();
      }
      else {
        serialWriter.print(SERIAL_ERR);
      }
    }
    else {
      serialWriter.print(SERIAL_ERR);
    }
  }
  else {
    serialWriter.print(SERIAL_ERR);
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Send the current date / time for the real-time clock
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::rtcGetCurrentDateTime(SerialWriterChecksum serialWriter) {
  extern RTC rtc;
  tmElements_t tm = rtc.getTime();

  serialWriter.print(tm.Hour);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(tm.Minute);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(tm.Second);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(tm.Day);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(tm.Month);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(tmYearToCalendar(tm.Year));
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Set the real-time clock to the received date / time
 *
 * @param serialWriter The serial writer to use to send the response
 * @param args The date / time in the following format: yy|mm|dd|hh|mm|ss
 */
void SerialHandler::rtcSetCurrentDateTime(SerialWriterChecksum serialWriter, char *args) {
  if (getArgumentsCount(args) == SERIAL_SET_RTC_DATE_TIME_ARGUMENTS_COUNT) {
    uint16_t year = atoi(strtok(args, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    uint8_t month = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    uint8_t day = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    uint8_t hour = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    uint8_t minute = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    uint8_t second = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));

    setTime(hour, minute, second, day, month, year);
    Teensy3Clock.set(now());

    serialWriter.print(SERIAL_OK);
  }
  else {
    serialWriter.print(SERIAL_ERR);
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Send a debugging string to the console
 *
 * @param message
 */
void SerialHandler::sendDebug(String message) {
  usbWriter.resetChecksum();

  usbWriter.print('E');
  usbWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  usbWriter.print(message);
  usbWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  usbWriter.print(usbWriter.getChecksum());
  usbWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);

  mainConnectorSerialPortWriter.resetChecksum();

  mainConnectorSerialPortWriter.print('E');
  mainConnectorSerialPortWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  mainConnectorSerialPortWriter.print(message);
  mainConnectorSerialPortWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  mainConnectorSerialPortWriter.print(mainConnectorSerialPortWriter.getChecksum());
  mainConnectorSerialPortWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Send the general settings structure to the client
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::sendGeneralSettings(SerialWriterChecksum serialWriter) {
  GeneralSettings_t generalSettings;
  Settings::readGeneralSettings(generalSettings);

  serialWriter.print(generalSettings.speedUnit);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(generalSettings.temperatureUnit);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(generalSettings.speedSourceDynamometer);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(generalSettings.timeZone);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(generalSettings.rtcSyncWithGps);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(generalSettings.rtcCompensate);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(generalSettings.configurationSerialPortOnMainConnector);

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Edit the general settings by applying the values we got from the client
 *
 * @param serialWriter The serial writer to use to send the response
 * @param args The arguments (speed unit, temperature unit, speed source dynamometer, RTC sync with GPS)
 */
void SerialHandler::editGeneralSettings(SerialWriterChecksum serialWriter, char *args) {
  if (getArgumentsCount(args) == SERIAL_EDIT_GENERAL_SETTINGS_ARGUMENTS_COUNT) {
    GeneralSettings_t generalSettings;

    generalSettings.speedUnit = static_cast<SpeedUnit>(atoi(strtok(args, SERIAL_COMMAND_SUB_VALUES_SEPARATOR)));
    generalSettings.temperatureUnit = static_cast<TemperatureUnit>(atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR)));
    generalSettings.speedSourceDynamometer= static_cast<SpeedSourceDynamometer>(atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR)));
    generalSettings.timeZone = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    generalSettings.rtcSyncWithGps = strcmp(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR), "1") == 0 ? true : false;
    generalSettings.rtcCompensate = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    generalSettings.configurationSerialPortOnMainConnector = strcmp(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR), "1") == 0 ? true : false;

    Settings::writeGeneralSettings(generalSettings);

    // Refresh ECU instance with new general settings
    extern ECU* ecu;
    ecu->refreshGeneralSettings();

    // Refresh displayable items with new general settings
    DisplayableItems::refreshGeneralSettings();

    // Change serial port on main connector configuration
    if (generalSettings.configurationSerialPortOnMainConnector) {
      Serial1.begin(SERIAL_MAIN_CONNECTOR_BPS);
    }
    else {
      Serial1.end();
    }

    // Tell the RTC about the new compensate value
    Teensy3Clock.compensate(generalSettings.rtcCompensate);

    serialWriter.print(SERIAL_OK);
  }
  else {
    serialWriter.print(SERIAL_ERR);
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Send the vehicle settings structure to the client
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::sendVehicleSettings(SerialWriterChecksum serialWriter) {
  VehicleSettings_t vehicleSettings;
  Settings::readVehicleSettings(vehicleSettings);

  serialWriter.print(vehicleSettings.vehicleWeight);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(vehicleSettings.coefficientOfDrag, 4);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(vehicleSettings.coefficientOfRollingResistance, 4);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(vehicleSettings.frontalArea, 4);

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Edit the vehicle settings by applying the values we got from the client
 *
 * @param serialWriter The serial writer to use to send the response
 * @param args The arguments (vehicle weight, coefficient of drag, coefficient of rolling resistance, frontal area)
 */
void SerialHandler::editVehicleSettings(SerialWriterChecksum serialWriter, char *args) {
  if (getArgumentsCount(args) == SERIAL_EDIT_VEHICLE_SETTINGS_ARGUMENTS_COUNT) {
    VehicleSettings_t vehicleSettings;

    vehicleSettings.vehicleWeight = atoi(strtok(args, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    vehicleSettings.coefficientOfDrag = atof(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    vehicleSettings.coefficientOfRollingResistance = atof(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    vehicleSettings.frontalArea = atof(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));

    Settings::writeVehicleSettings(vehicleSettings);

    // Refresh dynamometer instance with new vehicle settings
    extern Dynamometer dynamometer;
    dynamometer.refreshVehicleSettings();

    serialWriter.print(SERIAL_OK);
  }
  else {
    serialWriter.print(SERIAL_ERR);
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Send the stepper settings structure to the client
 *
 * @param serialWriter The serial writer to use to send the response
 */
void SerialHandler::sendStepperSettings(SerialWriterChecksum serialWriter) {
  StepperSettings_t stepperSettings;
  Settings::readStepperSettings(stepperSettings);

  serialWriter.print(stepperSettings.totalNumberOfSteps);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(stepperSettings.maximumSpeed);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(stepperSettings.acceleration);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(stepperSettings.homingDirection);
  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(stepperSettings.channel);

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 * Edit the stepper settings by applying the values we got from the client
 *
 * @param serialWriter The serial writer to use to send the response
 * @param args The arguments (total number of steps, maximum speed, acceleration, homing direction and channel)
 */
void SerialHandler::editStepperSettings(SerialWriterChecksum serialWriter, char *args) {
  if (getArgumentsCount(args) == SERIAL_EDIT_STEPPER_SETTINGS_ARGUMENTS_COUNT) {
    StepperSettings_t stepperSettings;

    stepperSettings.totalNumberOfSteps = atoi(strtok(args, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    stepperSettings.maximumSpeed = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    stepperSettings.acceleration = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));
    stepperSettings.homingDirection = static_cast<HomingDirection>(atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR)));
    stepperSettings.channel = atoi(strtok(NULL, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));

    Settings::writeStepperSettings(stepperSettings);

    // Refresh stepper gauge instance with new stepper settings
    extern StepperGauge stepperGauge;
    stepperGauge.refreshSettings();

    serialWriter.print(SERIAL_OK);
  }
  else {
    serialWriter.print(SERIAL_ERR);
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}

/**
 *
 * @param serialWriter The serial writer to use to send the response
 * @param args The arguments
 */
void SerialHandler::changeDisplayPage(SerialWriterChecksum serialWriter, char *args) {
  if (getArgumentsCount(args) == SERIAL_CHANGE_DISPLAY_PAGE_ARGUMENTS_COUNT) {
    uint8_t prevOrNext = atoi(strtok(args, SERIAL_COMMAND_SUB_VALUES_SEPARATOR));

    switch (prevOrNext) {
      case 0:
        extern void showPreviousPage();
        showPreviousPage();
        break;
        
      case 1:
        extern void showNextPage();
        showNextPage();
        break;
    }

    serialWriter.print(SERIAL_OK);
  }
  else {
    serialWriter.print(SERIAL_ERR);
  }

  serialWriter.print(SERIAL_COMMAND_VALUES_SEPARATOR);
  serialWriter.print(serialWriter.getChecksum());
  serialWriter.print(SERIAL_COMMAND_TERMINATION_CHARACTER);
}