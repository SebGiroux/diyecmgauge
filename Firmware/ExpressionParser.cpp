/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

/*
 * Basic implementation of the shunting yard algorithm (http://en.wikipedia.org/wiki/Shunting-yard_algorithm)
 *
 * Currently supported:
 *   - Operators: +, -, *, /, %, ^
 *   - Parenthesis
 *   - Decimal numbers
 *   - Variables
 */

#include "ExpressionParser.h"

Operator ExpressionParser::ops[] = {
  {'_', 10, ASSOC_RIGHT, true, ExpressionParser::evalUminus},
  {'^', 9, ASSOC_RIGHT, false, ExpressionParser::evalExp},
  {'*', 8, ASSOC_LEFT, false, ExpressionParser::evalMul},
  {'/', 8, ASSOC_LEFT, false, ExpressionParser::evalDiv},
  {'%', 8, ASSOC_LEFT, false, ExpressionParser::evalMod},
  {'+', 5, ASSOC_LEFT, false, ExpressionParser::evalAdd},
  {'-', 5, ASSOC_LEFT, false, ExpressionParser::evalSub},
  {'(', 0, ASSOC_NONE, false, NULL},
  {')', 0, ASSOC_NONE, false, NULL}
};

uint8_t nopstack = 0;
uint8_t nnumstack = 0;
uint8_t nvarstack = 0;

/**
 * Add a variable to the list of variables to replace in the expression
 *
 * @param variable The variable to be added to the list
 * @param value The value of the variable
 *
 * @return true on success, false on failure (stack overflow)
 */
bool ExpressionParser::addVariable(char variable, double value) {
  if (nvarstack > MAX_VAR_STACK - 1) {
    SerialHandler::sendDebug("ERROR: Variables stack overflow\n");

    return false;
  }
  else {
    variables[nvarstack] = variable;
    variablesValues[nvarstack++] = value;

    return true;
  }
}

/**
 * Clear the variables stack by resetting the stack counter to zero
 */
void ExpressionParser::clearVariables() {
  nvarstack = 0;
}

/**
 * Look for the operator matching the specified character in the operators list
 *
 * @param operatorCharacter The character of the operator we're looking for
 * @return The operator if found, NULL otherwise
 */
Operator *ExpressionParser::getOperator(char operatorCharacter) {
  for (uint8_t i = 0; i < sizeof(ops) / sizeof(ops[0]); i++) {
    if (ops[i].op == operatorCharacter) {
      return ops + i;
    }
  }

  return NULL;
}

/**
 *
 * @param op
 */
void ExpressionParser::shuntOp(struct Operator *op) {
  if (op->op == '(') {
    pushOperatorStack(op);
  }
  else if (op->op == ')') {
    while (nopstack > 0 && opstack[nopstack - 1]->op != '(') {
      pushResultToNumStack();
    }

    Operator *pop;
    if (!(pop = popOperatorStack()) || pop->op != '(') {
      SerialHandler::sendDebug("ERROR: Stack error. No matching \'(\'\n");
    }
  }
  else {
    if (op->assoc == ASSOC_RIGHT) {
      while (nopstack && op->precedence < opstack[nopstack - 1]->precedence) {
        pushResultToNumStack();
      }
    }
    else {
      while (nopstack && op->precedence <= opstack[nopstack - 1]->precedence) {
        pushResultToNumStack();
      }
    }

    pushOperatorStack(op);
  }
}

/**
 *
 */
void ExpressionParser::pushResultToNumStack() {
  Operator *pop;
  double firstOperand, secondOperand;

  pop = popOperatorStack();
  firstOperand = popNumberStack();

  if (pop->unary) {
    pushNumberStack(pop->eval(firstOperand, 0));
  }
  else {
    secondOperand = popNumberStack();
    pushNumberStack(pop->eval(secondOperand, firstOperand));
  }
}

/**
 *
 * @param firstOperand The first operand
 * @param secondOperand Unused
 *
 * @return The negative version of the first operand
 */
double ExpressionParser::evalUminus(double firstOperand, double secondOperand) {
  return -firstOperand;
}

/**
 *
 * @param firstOperand The first operand
 * @param secondOperand The second operand
 *
 * @return The value of the first operand raised to the power of the second operand
 */
double ExpressionParser::evalExp(double firstOperand, double secondOperand) {
  return secondOperand < 0 ? 0 : (secondOperand == 0 ? 1 : firstOperand * evalExp(firstOperand, secondOperand - 1));
}

/**
 *
 * @param firstOperand The first operand
 * @param secondOperand The second operand
 *
 * @return The value of the first operand multiplied by the second operand
 */
double ExpressionParser::evalMul(double firstOperand, double secondOperand) {
  return firstOperand * secondOperand;
}

/**
 *
 * @param firstOperand The first operand
 * @param secondOperand The second operand
 *
 * @return The value of the first operand divided by the second operand
 */
double ExpressionParser::evalDiv(double firstOperand, double secondOperand) {
  if (secondOperand == 0) {
    SerialHandler::sendDebug("ERROR: Division by zero\n");
    return 0;
  }

  return firstOperand / secondOperand;
}

/**
 *
 * @param firstOperand The first operand
 * @param secondOperand The second operand
 *
 * @return The value of the remainder of the first operand divided by the second operand
 */
double ExpressionParser::evalMod(double firstOperand, double secondOperand) {
  if (secondOperand == 0) {
    SerialHandler::sendDebug("ERROR: Division by zero\n");
    return 0;
  }

  return (uint32_t) firstOperand % (uint32_t) secondOperand;
}

/**
 *
 * @param firstOperand The first operand
 * @param secondOperand The second operand
 *
 * @return The value of the first operand added to the second operand
 */
double ExpressionParser::evalAdd(double firstOperand, double secondOperand) {
  return firstOperand + secondOperand;
}

/**
 *
 * @param firstOperand The first operand
 * @param secondOperand The second operand
 *
 * @return The value of the first operand substracted by the second operand
 */
double ExpressionParser::evalSub(double firstOperand, double secondOperand) {
  return firstOperand - secondOperand;
}

/**
 * Push an operator to the operators stack
 *
 * @param op The operator to be pushed to the stack
 */
void ExpressionParser::pushOperatorStack(struct Operator *op) {
  if (nopstack > MAX_OP_STACK - 1) {
    SerialHandler::sendDebug("ERROR: Operator stack overflow\n");
  }
  else {
    opstack[nopstack++] = op;
  }
}

/**
 * Pop an operator from the operators stack
 *
 * @return Pop the last operator from the stack
 */
struct Operator *ExpressionParser::popOperatorStack() {
  if (nopstack == 0) {
    SerialHandler::sendDebug("ERROR: Operator stack empty\n");

    return NULL;
  }
  else {
    return opstack[--nopstack];
  }
}

/**
 * Push a number to the numbers stack
 *
 * @param op The number to be pushed to the stack
 */
void ExpressionParser::pushNumberStack(String num) {
  char buffer[num.length() + 1];
  num.toCharArray(buffer, sizeof(buffer));
  pushNumberStack(atof(buffer));
}

/**
 * Push a number to the numbers stack
 *
 * @param op The number to be pushed to the stack
 */
void ExpressionParser::pushNumberStack(double num) {
  if (nnumstack > MAX_NUM_STACK - 1) {
    SerialHandler::sendDebug("ERROR: Number stack overflow\n");
  }
  else {
    numstack[nnumstack++] = num;
  }
}

/**
 * Pop a number from the numbers stack
 *
 * @return Pop the last number from the stack
 */
double ExpressionParser::popNumberStack() {
  if (nnumstack == 0) {
    SerialHandler::sendDebug("ERROR: Number stack empty\n");

    return 0;
  }
  else {
    return numstack[--nnumstack];
  }
}

/**
 * Check if the specified character is in the variables list
 *
 * @param variable The character to be checked
 *
 * @return true if the character is a variable, false otherwise
 */
bool ExpressionParser::isVariable(char variable) {
  for (uint8_t i = 0; i < nvarstack; i++) {
    if (variables[i] == variable) {
      return true;
    }
  }

  return false;
}

/**
 * Search for the specified variable in the variables list and returns its value
 *
 * @param variable The character we're looking for
 *
 * @return The value of the variable if it exists, 0 otherwise
 */
double ExpressionParser::getValueForVariable(char variable) {
  for (uint8_t i = 0; i < nvarstack; i++) {
    if (variables[i] == variable) {
      return variablesValues[i];
    }
  }

  return 0;
}

/**
 * Try to solve the expression passed in parameter
 *
 * @param expression The expression to be solved
 *
 * @return A double value representing the result of the expression
 */
double ExpressionParser::solve(char *expression) {
  char *expr;
  String tstart = "";
  struct Operator startop = {'X', 0, ASSOC_NONE, 0, NULL};  /* Dummy operator to mark start */
  struct Operator *op = NULL;
  double firstOperand, secondOperand;
  struct Operator *lastop = &startop;

  // Reset stacks before we start
  nopstack = 0;
  nnumstack = 0;

  for (expr = expression; *expr; expr++) {
    if (tstart == "") {
      if ((op = getOperator(*expr))) {
        // The '-' character can actually mean either the binary subtract operator or the unary negate operator. To find out which it is, we use the fact that a binary operator can not show up right after another operator or a left parenthesis.

        // To separate them, we use the '_' character for the unary variant.
        if (lastop && (lastop == &startop || lastop->op != ')')) {
          if (op->op == '-') {
            op = getOperator('_');
          }
          else if (op->op!='(') {
            SerialHandler::sendDebug("ERROR: Illegal use of binary operator (");
            SerialHandler::sendDebug(op->op);
            SerialHandler::sendDebug(")\n");
            return 0;
          }
        }
        shuntOp(op);
        lastop = op;
      }
      else if (isdigit(*expr) || *expr == '.') {
        tstart += expr;
      }
      else if (isVariable(*expr)) {
        tstart += getValueForVariable(*expr);
      }
      else if (!isspace(*expr)) {
        SerialHandler::sendDebug("ERROR: Syntax error\n");
        return 0;
      }
    }
    else {
      if (isspace(*expr)) {
        pushNumberStack(tstart);
        tstart = "";
        lastop = NULL;
      }
      else if ((op = getOperator(*expr))) {
        pushNumberStack(tstart);
        tstart = "";
        shuntOp(op);
        lastop = op;
      }
      else if (isVariable(*expr)) {
        tstart += getValueForVariable(*expr);
      }
      else if (!isdigit(*expr) && *expr != '.') {
        SerialHandler::sendDebug(*expr);
        SerialHandler::sendDebug("ERROR: Syntax error\n");
        return 0;
      }
    }
  }
  if (tstart != "") {
    pushNumberStack(tstart);
  }

  // After all tokens are handled, we use a simple loop to evaluate all remaining tokens on top of the operator stack. 
  while (nopstack) {
    op = popOperatorStack();
    firstOperand = popNumberStack();
    if (op->unary) {
      pushNumberStack(op->eval(firstOperand, 0));
    }
    else {
      secondOperand = popNumberStack();
      pushNumberStack(op->eval(secondOperand, firstOperand));
    }
  }

  // When we are done, there better be exactly one value left on the number stack. This is the result of our expression, and is printed.
  if (nnumstack != 1) {
    SerialHandler::sendDebug("ERROR: Number stack has ");
    SerialHandler::sendDebug(nnumstack);
    SerialHandler::sendDebug(" elements after evaluation. Should be 1.\n");
    return 0;
  }

  return numstack[0];
}