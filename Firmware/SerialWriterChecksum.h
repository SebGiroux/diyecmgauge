/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef SerialWriterChecksum_h
#define SerialWriterChecksum_h

#include <Arduino.h>
#include <Print.h>

class SerialWriterChecksum : public Print {
  public:
    SerialWriterChecksum(HardwareSerial* pSerial);
    SerialWriterChecksum(usb_serial_class* pSerial);
    size_t write(uint8_t data);
    uint8_t getChecksum();
    void resetChecksum();

  private:
    HardwareSerial* hwSerial;
    usb_serial_class* usbSerial;
    uint8_t checksum;
};

#endif