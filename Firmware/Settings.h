/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef Settings_h
#define Settings_h

#include <Arduino.h>
#include "EEPROM.h"
#include "Alarms.h"
#include "Alarm.h"
#include "Page.h"
#include "EEPROM.h"
#include "PageItems.h"
#include "GeneralSettings.h"
#include "VehicleSettings.h"
#include "StepperSettings.h"

#define EEPROM_SIZE 2048

class Settings {
  public:
    static void fetchPagesFromEEPROM();
    static void fetchAlarmsFromEEPROM();
    static void fetchGeneralSettingsFromEEPROM();
    static void fetchVehicleSettingsFromEEPROM();
    static void fetchStepperSettingsFromEEPROM();
    static void fetchAll();

    static void writePage(uint8_t index, PageSettings_t page);
    static void readPage(uint8_t index, PageSettings_t& page);

    static void writeAlarm(uint8_t index, Alarm_t alarm);
    static void readAlarm(uint8_t index, Alarm_t& alarm);

    static void writeGeneralSettings(GeneralSettings_t generalSettings);
    static void readGeneralSettings(GeneralSettings_t& generalSettings);

    static void writeVehicleSettings(VehicleSettings_t vehicleSettings);
    static void readVehicleSettings(VehicleSettings_t& vehicleSettings);

    static void writeStepperSettings(StepperSettings_t stepperSettings);
    static void readStepperSettings(StepperSettings_t& stepperSettings);

    static bool isEEPROMEmpty();

    static void clear();

  private:
    const static uint8_t PAGES_OFFSET;
    const static uint16_t ALARMS_OFFSET;
    const static uint16_t GENERAL_SETTINGS_OFFSET;
    const static uint16_t VEHICLE_SETTINGS_OFFSET;
    const static uint16_t STEPPER_SETTINGS_OFFSET;
    const static uint16_t LAST_USED_OFFSET;

    static uint16_t getPageIndexOffset(uint8_t index);
    static uint16_t getAlarmIndexOffset(uint8_t index);
    static uint16_t getGeneralSettingsOffset();
    static uint16_t getVehicleSettingsOffset();
    static uint16_t getStepperSettingsOffset();

    template <class T> static uint16_t writeData(uint16_t addr, const T& value);
    template <class T> static uint16_t readData(uint16_t addr, T& value);
};

#endif
