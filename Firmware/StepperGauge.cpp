﻿/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "StepperGauge.h"

// Timer interrupt to refresh the stepper gauge to avoid doing it in the main loop
IntervalTimer stepperTimer;

AccelStepper stepperMotor(AccelStepper::FULL2WIRE, STEPPER_1, STEPPER_2);

/**
 * Function called by the timer interrupt to refresh the stepper gauge
 */
void stepperGaugeUpdate() {
  extern StepperGauge stepperGauge;
  stepperGauge.update();
}

/**
 * Start of the stepper gauge, refresh the settings and start the timer interrupt
 */
void StepperGauge::begin() {
  refreshSettings();

  isFirstUpdate = true;

  stepperTimer.begin(stepperGaugeUpdate, STEPPER_GAUGE_INTERVAL_REFRESH);
}

/**
 * Bring the stepper motor to its home position
 */
void StepperGauge::homing() {
  stepperMotor.runToNewPosition(0);
}

/**
 * Called in the main loop to update the stepper motor to be closer to the value to be displayed
 */
void StepperGauge::update() {
  if (isFirstUpdate) {
    homing();
    isFirstUpdate = false;
  }

  if (totalNumberOfSteps > 0) {
    double value = DisplayableItems::getValueAtIndex(channel);
    int16_t min = DisplayableItems::getMinAtIndex(channel);
    int16_t max = DisplayableItems::getMaxAtIndex(channel);

    // Make sure we don't divide by zero
    if ((max - min) != 0) {
      float percent = (value - min) / (max - min);
      percent = max(0, min(percent, 1));

      stepperMotor.moveTo(percent * totalNumberOfSteps);
      stepperMotor.run();
    }
  }
}

/**
 * Refresh the settings of the stepper motor (to be called when they change or on initialization)
 */
void StepperGauge::refreshSettings() {
  StepperSettings_t stepperSettings = StepperSettings::get();

  stepperMotor.setMaxSpeed(stepperSettings.maximumSpeed);
  stepperMotor.setAcceleration(stepperSettings.acceleration);
  stepperMotor.setCurrentPosition(stepperSettings.totalNumberOfSteps);

  totalNumberOfSteps = stepperSettings.totalNumberOfSteps;
  channel = stepperSettings.channel;

  if (stepperSettings.homingDirection == NORMAL) {
    stepperMotor.setPinsInverted(false, false, false);
  }
  else {
    stepperMotor.setPinsInverted(true, false, false);
  }
}