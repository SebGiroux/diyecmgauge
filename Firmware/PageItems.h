/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef PageItems_h
#define PageItems_h

#include <Arduino.h>
#include "Page.h"

#define NB_DISPLAY_PAGES 15

class PageItems {
  public:
    static PageSettings_t getPageAtIndex(byte index);
    static void setPageAtIndex(byte index, PageSettings_t page);
    static byte getNbUsedPages();
};

#endif