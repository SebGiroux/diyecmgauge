/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef Histogram_h
#define Histogram_h

#include "u8g_teensy.h"

#include "Utils.h"
#include "DisplayableItem.h"
#include "DisplayableItems.h"

#define NB_VALUES_TO_DISPLAY 128

class Histogram {
  public:
    Histogram(U8GLIB *display);

    void drawHistogram(DisplayableItem_t displayableItem);
    void addValue(float value);

  private:
    U8GLIB *display;

    uint16_t x;
    uint16_t y;
    uint16_t w;
    uint16_t h;
    
    float minYValue;
    float maxYValue;
    
    float values[NB_VALUES_TO_DISPLAY];
    uint8_t nbValuesInArray;
};

#endif
