/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#include "RTC.h"

tmElements_t tm;
const char *monthName[12] = {
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

void RTC::begin() {
  timeStatus_t tstatus;
  
  tstatus = timeStatus();
  if (tstatus == timeNotSet) {
    setRTCTime();
  }
}

void RTC::setRTCTime() {
  boolean parse = false;
  boolean config = false;
  
  // Get the date and time the compiler was run
  if (getDate(__DATE__) && getTime(__TIME__)) {
    parse = true;
    // And configure the RTC with this info
    setTime(tm.Hour, tm.Minute, tm.Second, tm.Day, tm.Month, 1970 + (int)tm.Year);
    Teensy3Clock.set(now()); // set the RTC
    config = true;
  }
  
  if (parse && config) {
    Serial.print("DS1307 configured Time=");
    Serial.print(__TIME__);
    Serial.print(", Date=");
    Serial.println(__DATE__);
  } else if (parse) {
    Serial.println("DS1307 Communication Error :-{");
    Serial.println("Please check your circuitry");
  } else {
    Serial.print("Could not parse info from the compiler, Time=\"");
    Serial.print(__TIME__);
    Serial.print("\", Date=\"");
    Serial.print(__DATE__);
    Serial.println("\"");
  }
}

boolean RTC::getTime(const char *str) {
  int Hour, Min, Sec;

  if (sscanf(str, "%d:%d:%d", &Hour, &Min, &Sec) != 3) {
    return false;
  }
  
  tm.Hour = Hour;
  tm.Minute = Min;
  tm.Second = Sec;
  return true;
}

boolean RTC::getDate(const char *str) {
  char Month[NB_MONTHS_IN_YEAR];
  int Day, Year;
  uint8_t monthIndex;

  if (sscanf(str, "%s %d %d", Month, &Day, &Year) != 3) {
    return false;
  }
  
  for (monthIndex = 0; monthIndex < NB_MONTHS_IN_YEAR; monthIndex++) {
    if (strcmp(Month, monthName[monthIndex]) == 0) {
      break;
    }
  }
  
  if (monthIndex >= NB_MONTHS_IN_YEAR) {
    return false;
  }
  
  tm.Day = Day;
  tm.Month = monthIndex + 1;
  tm.Year = CalendarYrToTm(Year);
  return true;
}

tmElements_t RTC::getTime() {
  breakTime(now(), tm);
  return tm;
}
