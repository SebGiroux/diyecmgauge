/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef Sensors_h
#define Sensors_h

#include "Pins.h"
#include "DisplayableItems.h"
#include "TinyGPSPP.h"
#include "Time.h"

// Triple axis accelerometer + Magnetometer
#include "LSM303.h"

// Temperature + Humidity sensor
#include "HIH6130.h"

#define SECONDS_IN_ONE_HOUR 3600

class Sensors {
  public:
    void begin();
    void startAccelerometer();
    void startGPS();
    
    void refreshSensorsData();
    void syncRtcWithGps();
    float getGPSSpeed();
    bool isDaylightSavingTime();

    HIH6130 getHIH();
    LSM303 getLSM();
    TinyGPSPlus getGPS();

  private:
    bool rtcSyncWithGps;
};

#endif
