﻿/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef DataLogging_h
#define DataLogging_h

#include "Pins.h"
#include "ECU.h"
#include "RTC.h"
#include "SerialHandler.h"
#include "DisplayableItems.h"

// SD card
#include "libs/SdFat/SdFat.h"

// Long file name supports up to 255 characters
// Also add the 8 characters datalogs directory name and  2 slashes
#define DATA_LOG_FILE_PATH_MAX_LENGTH 265
#define DATALOGS_DIRECTORY "/Datalogs"

enum DataLoggingStatus {
  NO_SD_CARD, NOT_DATA_LOGGING, DATA_LOGGING
};

class SerialHandler;

class DataLogging {
  public:
    void begin(ECU* ecu);

    void startLogging();
    void stopLogging();
    void writeRow();
    DataLoggingStatus getStatus();

    SdFat& getSd();
    uint8_t getSdCardBlocksPerCluster();
    uint32_t getClustersCount();

    const static char* FILE_NAME_PREFIX;
    const static char* FILE_NAME_EXTENSION;
    const static byte MAX_FILE_NAME_LENGTH = 8; // Does not include extension

  private:
    char dataLoggingFileName[DATA_LOG_FILE_PATH_MAX_LENGTH];
    DataLoggingStatus status;
    ECU* ecu;
    File dataLogFile;
    SdFat sd;

    char* getFileName();

    void writeCaptureDate();
    void writeRevNumAndSignature();
    void writeHeader();
};

#endif
