/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef BarGraph_h
#define BarGraph_h

#include "u8g_teensy.h"

#include "DisplayableItems.h"

#define BAR_GRAPH_SIDE_MARGIN 5 // in pixels

// One item
#define BAR_GRAPH_ONE_ITEM_TOP_OFFSET 35
#define BAR_GRAPH_ONE_ITEM_BAR_HEIGHT 25

// Two items
#define BAR_GRAPH_TWO_ITEMS_TOP_OFFSET 15
#define BAR_GRAPH_TWO_ITEMS_HEIGHT 55
#define BAR_GRAPH_TWO_ITEMS_BAR_HEIGHT 18

// Three items
#define BAR_GRAPH_THREE_ITEMS_TOP_OFFSET 20
#define BAR_GRAPH_THREE_ITEMS_HEIGHT 32
#define BAR_GRAPH_THREE_ITEMS_BAR_HEIGHT 10

// Four items
#define BAR_GRAPH_FOUR_ITEMS_TOP_OFFSET 15
#define BAR_GRAPH_FOUR_ITEMS_HEIGHT 25
#define BAR_GRAPH_FOUR_ITEMS_BAR_HEIGHT 10

 // Five items
#define BAR_GRAPH_FIVE_ITEMS_TOP_OFFSET 0
#define BAR_GRAPH_FIVE_ITEMS_HEIGHT 25
#define BAR_GRAPH_FIVE_ITEMS_BAR_HEIGHT 10

class BarGraph {
  public:
    BarGraph(U8GLIB *display);
    void drawBarGraph(DisplayableItem_t displayableItem, uint8_t index, uint8_t pageSize);

  private:
    U8GLIB *display;
};

#endif
