/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef MegasquirtCAN_h
#define MegasquirtCAN_h

#include "ECU.h"
#include "Pins.h"
#include "FlexCAN.h"
#include "RTC.h"
#include "SerialHandler.h"
#include <Arduino.h>

// In MS3 (+ MS2 + MS2/Extra), block 7 is "outpc" (outputs-to-PC), the collection of realtime data that is displayed on the gauges in the tuning software.
#define MS_OUTPC_BLOCK 7

// Signature
#define MS_SIGNATURE_BLOCK 14
#define MS_SIGNATURE_LENGTH 60
#define MS_REQUEST_BLOCK_LENGTH 8 // Buffer size in the CAN class is 8 bytes so don't go over that

// Revision number
#define MS_REV_NUM_BLOCK 15
#define MS_REV_NUM_LENGTH 20

// Sensors offsets
// These are the offsets requested by the MegaSquirt that are configured in the tuning software
#define SENSORS_BLOCK 7
#define SENSORS_RTC_OFFSET 110
#define SENSORS_GPS_PART_1_OFFSET 128
#define SENSORS_GPS_PART_2_OFFSET 136
#define SENSORS_ACCEL_OFFSET 17

#define SEND_REQUEST_TIMEOUT 1500 // in MS

// CAN message types (only basic ones)
#define MSG_CMD 0
#define MSG_REQ 1
#define MSG_RSP 2

#define MS_CANID 0
#define GAUGE_CANID 10

#define NB_RX_BUFFERS 6

class MegasquirtCAN : public ECU {
  public:
    void begin();
    void checkAlarms();
    bool requestData();
    bool isConfigError();
    void fetchRevNum();
    void handleOutputChannelsResponse(uint16_t localOffset);
    void handleSignatureResponse(uint16_t offset);
    void handleRevNumResponse(uint16_t offset);
    void handleMegaSquirtRequest(uint16_t offset);
    void fetchSignature();
    String getRevNum();
    String getSignature();
    void checkCANBusMessages();
    String getDataLogHeader();
    void refreshGeneralSettings();

  private:
    static uint8_t getRequestBytesForOffsetAndLength(uint8_t offset, uint8_t length);
    bool sendRequest(uint8_t block, uint8_t localOffset, uint16_t remoteOffset, uint8_t length);
    uint32_t getMessageId(uint8_t block, uint16_t offset, uint8_t msgType);
    void initializeDisplayableItems();

    uint8_t pendingCANMessages;
};

#endif
