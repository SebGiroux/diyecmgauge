/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef Alarms_h
#define Alarms_h

#include <Arduino.h>
#include "Alarm.h"

#include "ECU.h"
#include "DataLogging.h"
#include "u8g_teensy.h"
#include "DisplayableItems.h"
#include "LEDs.h"
#include "Buzzer.h"

// Number of seconds to data log after an alarm that request data logging has been triggered
#define DATALOGGING_SECONDS 10

#define NB_ALARMS 3

#define DELAY_BETWEEN_ALARM_FLASHES 400
#define NB_FLASHES_PER_ALARM 4

class DataLogging;

class Alarms {
  public:
    void begin(DataLogging* pDataLogging, LEDs* pLeds, Buzzer* pBuzzer);
    void check();
    uint8_t getNbTriggeredAlarms(bool onlyShowOnScreenAlarm);
    Alarm_t* getTriggeredShowWarningOnScreenAlarmAtIndex(uint8_t index);
    void displayAlarmPage(U8GLIB &display);
    static Alarm_t getAlarmAtIndex(uint8_t index);
    static void setAlarmAtIndex(uint8_t index, Alarm_t alarm);

  private:
    uint8_t currentScreenAlarmIndex;
    int currentScreenAlarmFlashes;
    long dataLoggingStartTimeStamp;
    unsigned long lastFlashMillis;

    void displayAlarmCondition(U8GLIB& display, Alarm_t alarm, AlarmConditionIndex conditionIndex, uint8_t y);
    bool isAlarmConditionMet(Alarm_t alarm, AlarmConditionIndex conditionIndex);
    bool isAlarmTriggered(Alarm_t alarm);
    char getOperatorChar(AlarmOperator alarmOperator);

    DataLogging* dataLogging;
    LEDs* leds;
    Buzzer* buzzer;
};

#endif
