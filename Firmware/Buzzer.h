/**
 * DIYECMGauge
 * By Sébastien Giroux
 *
 * Copyright - (c) 2013-2014
 */

#ifndef BUZZER_H
#define BUZZER_H

#include <Arduino.h>
#include "Pins.h"

#define BUZZER_TONE_HZ 440

class Buzzer {
  private:
    bool triggered;

  public:
    void begin();

    void start();
    void stop();

    bool isTriggered();
};

#endif