package com.sgiroux.diyecmgauge.serial;

/**
 * Interface that the class that want to listens to serial response should implements
 * 
 * @author Seb
 */
public interface SerialResponseReceivedListener {
	public void onResponseReceived(SerialCommand serialCommand, byte[] response);
}