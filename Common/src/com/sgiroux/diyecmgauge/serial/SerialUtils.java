package com.sgiroux.diyecmgauge.serial;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Utility functions around serial communications
 * 
 * @author Seb
 */
public class SerialUtils {
	public static final char SERIAL_RESPONSE_LINES_SEPARATOR = '|';
	public static final char SERIAL_RESPONSE_VALUES_SEPARATOR = '~';
	public static final char SERIAL_RESPONSE_SUB_VALUES_SEPARATOR = '^';
	public static final char SERIAL_RESPONSE_TERMINATION_CHARACTER = '#';

	public static final String SERIAL_RESPONSE_OK = "OK";
	public static final String SERIAL_RESPONSE_ERR = "ERR";
	public static final String SERIAL_RESPONSE_CHECKSUM_MISMATCH = "CHECKSUM_MISMATCH";

	/**
	 * Convert an array list of byte to a byte array
	 * 
	 * @param data The array list to convert
	 * @return The converted byte array
	 */
	public static byte[] convertArrayListToByteArray(ArrayList<Byte> data) {
		byte[] convertedData = new byte[data.size()];

		for (int i = 0; i < data.size(); i++) {
			convertedData[i] = data.get(i);
		}

		return convertedData;
	}

	/**
	 * Convert a byte array response to its string representation
	 * 
	 * @param response The array of byte representing the response
	 * @return The string of the response
	 */
	public static String getTextResponse(final byte[] response) {
		String textResponse = null;

		try {
			textResponse = new String(response, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// The gauge answer with the command as first parameter so we need to remove it
		textResponse = textResponse.substring(textResponse.indexOf(SERIAL_RESPONSE_VALUES_SEPARATOR) + 1);

		// Remove the checksum, if any
		if (textResponse.lastIndexOf(SERIAL_RESPONSE_VALUES_SEPARATOR) > -1) {
			textResponse = textResponse.substring(0, textResponse.lastIndexOf(SERIAL_RESPONSE_VALUES_SEPARATOR));
		}

		return textResponse;
	}

	/**
	 * Take a byte array response and returns just the relevant data (remove command, checksum, etc)
	 * 
	 * @param response The array of byte representing the response
	 * 
	 * @return The relevant data from the response
	 */
	public static byte[] getBytesResponse(byte[] response) {
		int beginDataPosition = SerialUtils.findFirstValueSeparator(response) + 1;
		int endDataPosition = SerialUtils.findLastValueSeparator(response) - 1;

		// Remove the command from the beginning of the response
		int length = endDataPosition - beginDataPosition + 1;
		byte[] data = new byte[length];
		System.arraycopy(response, beginDataPosition, data, 0, length);

		return data;
	}

	/**
	 * Find the position of the first value separator in the response
	 * 
	 * @param response The response that we will try to find the value separator in
	 * 
	 * @return The position of the first value separator in the response, -1 if not found
	 */
	private static int findFirstValueSeparator(byte[] response) {
		for (int i = 0; i < response.length; i++) {
			if (response[i] == SERIAL_RESPONSE_VALUES_SEPARATOR) {
				return i;
			}
		}

		return -1;
	}

	/**
	 * Find the position of the last value separator in the response
	 * 
	 * @param response The response that we will try to find the value separator in
	 * 
	 * @return The position of the last value separator in the response, -1 if not found
	 */
	private static int findLastValueSeparator(byte[] response) {
		for (int i = response.length - 1; i >= 0; i--) {
			if (response[i] == SERIAL_RESPONSE_VALUES_SEPARATOR) {
				return i;
			}
		}

		return -1;
	}

	// @formatter:off
	/**
	 * Prepare a serial command to be sent to the gauge
	 * 
	 * Commands have the following format:
	 * 
	 *   With arguments:
	 *     <command>SERIAL_RESPONSE_VALUES_SEPARATOR<arguments>SERIAL_RESPONSE_VALUES_SEPARATOR<checksum>SERIAL_RESPONSE_TERMINATION_CHARACTER
	 * 
	 *   Without arguments:
	 *     <command>SERIAL_RESPONSE_VALUES_SEPARATOR<checksum>SERIAL_RESPONSE_TERMINATION_CHARACTER
	 * 
	 *   The checksum is the sum of all the bytes
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param arguments The arguments of the serial command if any, null otherwise
	 * @return A byte array to send to the gauge
	 */
	// @formatter:on
	public static byte[] prepareCommand(SerialCommand serialCommand, String arguments) {
		byte command[] = serialCommand.getCommand();

		ArrayList<Byte> data = new ArrayList<Byte>();

		// Add command
		for (int i = 0; i < command.length; i++) {
			data.add(command[i]);
		}

		// Add separator
		data.add((byte) SERIAL_RESPONSE_VALUES_SEPARATOR);

		// Copy arguments, if any
		if (arguments != null) {
			for (int i = 0; i < arguments.length(); i++) {
				data.add((byte) arguments.charAt(i));
			}

			// Add separator
			data.add((byte) SERIAL_RESPONSE_VALUES_SEPARATOR);
		}

		String checksum = String.valueOf(calculateCheckSum(data));

		// Add checksum
		for (int i = 0; i < checksum.length(); i++) {
			data.add((byte) checksum.charAt(i));
		}

		// Add termination character
		data.add((byte) SERIAL_RESPONSE_TERMINATION_CHARACTER);

		return convertArrayListToByteArray(data);
	}

	/**
	 * Calculate the checksum of the response or of a command we need to send
	 * 
	 * @param data The data to calculate the checksum on
	 * @return The calculated checksum
	 */
	public static int calculateCheckSum(ArrayList<Byte> data) {
		int checksumPosition = 0;

		// Find last SERIAL_RESPONSE_VALUES_SEPARATOR in array
		for (int i = data.size() - 1; i >= 0; i--) {
			if (data.get(i) == SERIAL_RESPONSE_VALUES_SEPARATOR) {
				checksumPosition = i + 1;
				break;
			}
		}

		// Compute sum of the bytes
		byte sum = 0;
		for (int i = 0; i < checksumPosition; i++) {
			sum += data.get(i);
		}

		// Return an unsigned byte
		return (sum & 0xFF);
	}

	/**
	 * Look at the response we got from the gauge and extract the checksum from it.
	 * 
	 * @param data The array list of bytes we got from the gauge
	 * @return The checksum extracted from the response we got from the gauge
	 */
	public static int extractChecksumFromResponse(ArrayList<Byte> data) {
		int checksumPosition = 0;

		// Find last SERIAL_RESPONSE_VALUES_SEPARATOR in array
		for (int i = data.size() - 1; i >= 0; i--) {
			if (data.get(i) == SERIAL_RESPONSE_VALUES_SEPARATOR) {
				checksumPosition = i + 1;
				break;
			}
		}

		// Create a string with all the characters of the checksum
		byte current;
		StringBuffer checksum = new StringBuffer();

		for (int i = checksumPosition; i < data.size(); i++) {
			current = data.get(i);
			checksum.append((char) current);
		}

		return Integer.parseInt(checksum.toString());
	}

	/**
	 * Split a string (usually one returned by getTextResponse()) by lines
	 * 
	 * @param text The text to split into lines
	 * @return An array representing all the lines
	 */
	public static String[] splitByLines(String text) {
		return text.split(Pattern.quote(Character.toString(SerialUtils.SERIAL_RESPONSE_LINES_SEPARATOR)));
	}

	/**
	 * Split a string (usually one returned by getTextResponse()) by values
	 * 
	 * @param text The text to split into values
	 * @return An array representing all the values
	 */
	public static String[] splitByValues(String text) {
		return text.split(Pattern.quote(Character.toString(SerialUtils.SERIAL_RESPONSE_VALUES_SEPARATOR)));
	}

	/**
	 * Split a string (usually an index of the array returned by splitByValues()) by it's sub values
	 * 
	 * @param text The text to split into sub values
	 * @return An array representing all the sub values
	 */
	public static String[] splitBySubValues(String text) {
		return text.split(Pattern.quote(Character.toString(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR)));
	}

	/**
	 * Check the response and see if the response was OK
	 * 
	 * @param response The response to check
	 * @return true if response is OK, false otherwise
	 */
	public static boolean isOKResponse(String response) {
		return response.equals(SERIAL_RESPONSE_OK);
	}
}
