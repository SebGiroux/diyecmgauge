package com.sgiroux.diyecmgauge.serial;

/**
 * Enumeration of all the available serial commands that can be send to the gauge
 * 
 * @author Seb
 */
public enum SerialCommand {
	// @formatter:off
	SD_CARD_GET_STATS(new byte[] { 'S' }),
	DATA_LOGS_DIRECTORY_LISTING(new byte[] { 'L' }),
	DATA_LOGS_DELETE_FILE(new byte[] { 'D' }),
	DATA_LOGS_GET_FILE(new byte[] { 'G' }),
	DATA_LOGGING_STATUS(new byte[] { 'T' }),
	DATA_LOGGING_START(new byte[] { 'U' }),
	DATA_LOGGING_STOP(new byte[] { 'W' }),
	GET_VERSION(new byte[] { 'V' }),
	GET_INDICATORS_LIST(new byte[] { 'I' }),
	GET_ALARMS_LIST(new byte[] { 'A' }),
	SEND_ALARM(new byte[] { 'B' }),
	GET_PAGES_LIST(new byte[] { 'P' }),
	SEND_PAGE(new byte[] { 'Q' }),
	GET_DEBUG(new byte[] { 'E' }),
	GET_RTC_DATE_TIME(new byte[] { 'R' }),
	SEND_RTC_DATE_TIME(new byte[] { 'C' }),
	GET_GENERAL_SETTINGS(new byte[] { 'M' }),
	SEND_GENERAL_SETTINGS(new byte[] { 'N' }),
	GET_VEHICLE_SETTINGS(new byte[] { 'X' }),
	SEND_VEHICLE_SETTINGS(new byte[] { 'Y' }),
	GET_STEPPER_SETTINGS(new byte[] {'J'}),
	SEND_STEPPER_SETTINGS(new byte[] {'K'}),
	CHANGE_DISPLAY_PAGE(new byte[] { 'Z' });
	// @formatter:on

	private byte[] mCommand;

	private SerialCommand(byte[] command) {
		mCommand = command;
	}

	public byte[] getCommand() {
		return mCommand;
	}

	/**
	 * Get an instance of SerialCommand by its byte representation
	 * 
	 * @param cmd The byte representation of the serial command
	 * @return An instance of SerialCommand
	 */
	public static SerialCommand getByCommand(byte cmd) {
		for (SerialCommand sm : SerialCommand.values()) {
			if (sm.getCommand()[0] == cmd) {
				return sm;
			}
		}

		return null;
	}
}
