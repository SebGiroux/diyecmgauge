package com.sgiroux.diyecmgauge.serial;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.sgiroux.diyecmgauge.DIYECMGauge;

/**
 * Thread to write on the serial communication port. Implements a queueing mechanism. Both the Android and desktop application have a class extends
 * from this one.
 * 
 * @author Seb
 * 
 */
public abstract class SerialCommThreadBase extends Thread {
	private static final byte MAX_SERIAL_COMMAND_RETRY = 8;
	protected static final int SERIAL_COMMAND_REGULAR_TIME_OUT = 800; // in ms
	protected static final int SERIAL_COMMAND_SLOW_COMMAND_TIME_OUT = 10000; // in ms

	private static final SimpleDateFormat mTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

	private boolean mStopWriter = false;
	private final Object mWriterThreadLock = new Object();
	private byte mNbRetries;
	private boolean mSerialCommandResponseReceived;
	private ConcurrentLinkedQueue<SerialCommandQueue> mSerialCommandsQueue = new ConcurrentLinkedQueue<SerialCommandQueue>();
	private SerialCommandQueue mLastSerialCommandQueue;
	private boolean mLastSerialCommandFailed;
	private long mSerialCommandSentTimestamp;

	protected ArrayList<Byte> mSerialBuffer = new ArrayList<Byte>();

	@Override
	public void run() {
		// Check to see if the run loop should stop
		while (!mStopWriter) {
			while (mSerialCommandsQueue.isEmpty()) {
				// The thread could be unblocked to signal the close of the processor
				if (mStopWriter) {
					mSerialCommandsQueue.clear();
					break;
				}
				else {
					try {
						synchronized (mWriterThreadLock) {
							mWriterThreadLock.wait();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

			SerialCommandQueue serialCommandQueue = mSerialCommandsQueue.remove();

			// Remember last command
			mLastSerialCommandQueue = serialCommandQueue;

			mNbRetries = 0;

			do {
				mLastSerialCommandFailed = false;

				// Send the message
				mSerialCommandResponseReceived = false;
				mSerialCommandSentTimestamp = System.currentTimeMillis();

				SerialCommand serialCommand = serialCommandQueue.getSerialCommand();
				String arguments = serialCommandQueue.getArguments();

				debugSendCommand(serialCommand, arguments);
				write(serialCommand, arguments);

				waitForResponseComplete();

				// If the command failed, kick in retry mechanism
				if (mLastSerialCommandFailed) {
					if (mNbRetries <= MAX_SERIAL_COMMAND_RETRY) {
						System.out.println("Command " + mLastSerialCommandQueue.getSerialCommand() + " failed, retrying (" + mNbRetries + "/" + MAX_SERIAL_COMMAND_RETRY + ")");
					}
					else {
						break;
					}

					mNbRetries++;
				}
			} while (mLastSerialCommandFailed);

			// All tries failed, trigger all serial response time out listeners
			if (mNbRetries == MAX_SERIAL_COMMAND_RETRY && mLastSerialCommandFailed) {
				triggerAllSerialResponseTimeoutListeners(mLastSerialCommandQueue.getSerialCommand());
			}
		}
	}

	public abstract void write(SerialCommand serialCommand, String arguments);

	public void addCommandToQueue(SerialCommandQueue command) {
		mSerialCommandsQueue.add(command);
	}

	public SerialCommandQueue getLastSerialCommandQueue() {
		return mLastSerialCommandQueue;
	}

	public boolean isSerialCommandResponseReceived() {
		return mSerialCommandResponseReceived;
	}

	public void setSerialCommandResponseReceived() {
		mSerialCommandResponseReceived = true;
	}

	public Object getThreadLock() {
		return mWriterThreadLock;
	}

	public void stopWriter() {
		mStopWriter = true;
	}

	public void setLastSerialCommandFailed() {
		mLastSerialCommandFailed = true;
	}

	/**
	 * Function that wait until a serial response is fully received or time out is reached
	 */
	private void waitForResponseComplete() {
		while (!isSerialCommandResponseReceived() && !isTimeOutReached()) {
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Calculate the time between the serial command was sent and the current time to know if the time out is reached. Some commands (such as fetching
	 * a data log file) bypass the time out.
	 * 
	 * @return true if timeout is reached, false otherwise
	 */
	private boolean isTimeOutReached() {
		boolean timeOutReached = ((System.currentTimeMillis() - mSerialCommandSentTimestamp) > SERIAL_COMMAND_REGULAR_TIME_OUT);

		if (timeOutReached && !isSerialCommandRegularTimeout(getLastSerialCommandQueue().getSerialCommand())) {
			timeOutReached = false;
		}

		if (timeOutReached) {
			System.out.println("Time out reached for command " + getLastSerialCommandQueue().getSerialCommand());
			setLastSerialCommandFailed();
		}

		return timeOutReached;
	}

	/**
	 * Check if the specified serial command need to implements a regular time out time or a long time out time for slower commands
	 * 
	 * @param serialCommand The serial command we're interested in
	 * 
	 * @return true if the serial command should implements a short time out, false otherwise
	 */
	protected boolean isSerialCommandRegularTimeout(SerialCommand serialCommand) {
		// No timeout for fetching data logging file (it can take a while)
		if (serialCommand == SerialCommand.DATA_LOGS_GET_FILE) {
			return false;
		}

		return true;
	}

	/**
	 * Print the serial command into the console
	 * 
	 * @param serialCommand Serial command to be sent
	 * @param arguments The arguments of the serial command
	 */
	private void debugSendCommand(SerialCommand serialCommand, String arguments) {
		byte output[] = SerialUtils.prepareCommand(serialCommand, arguments);

		System.out.print(mTimestamp.format(new Date()));
		System.out.print(" -> ");
		for (int i = 0; i < output.length; i++) {
			System.out.print((char) output[i]);
		}
		System.out.println("");
	}

	/**
	 * Print the current input serial buffer into the console
	 */
	protected void debugReceiveResponse() {
		System.out.print(mTimestamp.format(new Date()));
		System.out.print(" <- ");
		for (byte b : mSerialBuffer) {
			System.out.print((char) b);
		}
		System.out.println("");
	}

	/**
	 * @return Return the current serial command we received from the gauge that is currently in the buffer
	 */
	protected SerialCommand getSerialCommandInBuffer() {
		return (mSerialBuffer.size() > 0) ? SerialCommand.getByCommand(mSerialBuffer.get(0)) : null;
	}

	/**
	 * Validate the serial response we got from the gauge
	 * 
	 * Note: Main serial buffer will be cleared by calling this method
	 */
	protected void validateSerialResponse() {
		validateSerialResponse(mSerialBuffer);
		mSerialBuffer.clear();
	}

	/**
	 * Validate the serial response we got from the gauge
	 * 
	 * @param buffer The buffer of the response that will be sent to the listeners
	 */
	public void validateSerialResponse(ArrayList<Byte> buffer) {
		String response = SerialUtils.getTextResponse(SerialUtils.convertArrayListToByteArray(buffer));
		int calculatedChecksum = SerialUtils.calculateCheckSum(buffer);
		int gaugeChecksum = SerialUtils.extractChecksumFromResponse(buffer);

		if (response.equals(SerialUtils.SERIAL_RESPONSE_CHECKSUM_MISMATCH) || calculatedChecksum != gaugeChecksum) {
			if (calculatedChecksum != gaugeChecksum) {
				System.out.println("Checksum mismatch (from gauge to app - (" + calculatedChecksum + " != " + gaugeChecksum + "))");
			}
			else {
				System.out.println("Checksum mismatch (from app to gauge)");
			}
			DIYECMGauge.getInstance().getWriterThread().setLastSerialCommandFailed();
		}
		else {
			triggerAllSerialResponseReceivedListeners(buffer);
		}

		DIYECMGauge.getInstance().getWriterThread().setSerialCommandResponseReceived();
	}

	/**
	 * Trigger all the listeners to tell them we got the response for a serial command that we sent
	 * 
	 * @param buffer The buffer of the response that will be sent to the listeners
	 */
	protected abstract void triggerAllSerialResponseReceivedListeners(ArrayList<Byte> buffer);

	/**
	 * Trigger all the listeners to tell them we got a time out for a serial command that we sent
	 * 
	 * @param serialCommand The serial command that timed out
	 */
	protected abstract void triggerAllSerialResponseTimeoutListeners(SerialCommand serialCommand);

	/**
	 * Add a byte received from the gauge to the serial buffer
	 * 
	 * @param data The byte to be added to the serial buffer
	 */
	protected void addByteToBuffer(byte data) {
		if (data == SerialUtils.SERIAL_RESPONSE_TERMINATION_CHARACTER) {
			debugReceiveResponse();
			validateSerialResponse();
		}
		else {
			mSerialBuffer.add(data);
		}
	}
}