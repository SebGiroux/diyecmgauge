package com.sgiroux.diyecmgauge.serial;

public class SerialCommandQueue {
	private SerialCommand mSerialCommand;
	private String mArguments;

	public SerialCommandQueue(SerialCommand serialCommand) {
		mSerialCommand = serialCommand;
	}

	public SerialCommandQueue(SerialCommand serialCommand, String arguments) {
		mSerialCommand = serialCommand;
		mArguments = arguments;
	}

	public SerialCommand getSerialCommand() {
		return mSerialCommand;
	}

	public void setSerialCommand(SerialCommand serialCommand) {
		mSerialCommand = serialCommand;
	}

	public String getArguments() {
		return mArguments;
	}

	public void setArguments(String arguments) {
		mArguments = arguments;
	}
}
