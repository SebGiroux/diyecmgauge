package com.sgiroux.diyecmgauge.data;

import java.util.ArrayList;
import java.util.List;

public enum TemperatureUnit {
	CELSIUS("Celsius"), FARENHEIT("Farenheit");

	private String mTitle;

	private TemperatureUnit(String title) {
		mTitle = title;
	}

	public String getTitle() {
		return mTitle;
	}

	public static List<String> getTitleList() {
		List<String> temperatureUnits = new ArrayList<String>();

		for (TemperatureUnit temperatureUnit : TemperatureUnit.values()) {
			temperatureUnits.add(temperatureUnit.getTitle());
		}

		return temperatureUnits;
	}
}