package com.sgiroux.diyecmgauge.data;

public enum PageContent {
	UNUSED("Unused page", 0), BASIC_READOUT_INDICATORS("Basic readout indicators", 7), ANALOG_GAUGES("Analog gauges", 4), HISTOGRAM("Histogram", 1), BAR_GRAPH("Bar graph", 5), ZERO_SIXTY_WIDGET("0-60 widget", 0), DATE_TIME_WIDGET("Date/time widget", 0);

	private String mTitle;
	private int mNbIndicators;

	private PageContent(String title, int nbIndicators) {
		mTitle = title;
		mNbIndicators = nbIndicators;
	}

	public String getTitle() {
		return mTitle;
	}

	public int getNbIndicators() {
		return mNbIndicators;
	}
}