package com.sgiroux.diyecmgauge.data;

import java.util.ArrayList;

import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Page object representing one page from the gauge
 * 
 * @author Seb
 * 
 */
public class Page implements Cloneable {
	private PageContent mContent;
	private ArrayList<Integer> mIndicatorsChannels;

	private static final int NO_INDICATOR = -1;
	private static final int MAX_INDICATORS = 7;

	public Page() {
		mIndicatorsChannels = new ArrayList<Integer>();
	}

	public PageContent getContent() {
		return mContent;
	}

	public void setContent(PageContent content) {
		mContent = content;
	}

	public ArrayList<Integer> getIndicatorsChannels() {
		return mIndicatorsChannels;
	}

	public void setIndicatorsChannels(ArrayList<Integer> indicatorsChannels) {
		mIndicatorsChannels = indicatorsChannels;
	}

	public void addIndicatorChannel(int indicatorChannel) {
		mIndicatorsChannels.add(indicatorChannel);
	}

	public String getSerialCommand(int pageIndex) {
		StringBuffer command = new StringBuffer();

		command.append(pageIndex);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mContent.ordinal());
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);

		for (int i = 0; i < MAX_INDICATORS; i++) {
			command.append(i < mIndicatorsChannels.size() ? mIndicatorsChannels.get(i) : NO_INDICATOR);

			// If it's not the last indicators, we add a separator
			if (i < MAX_INDICATORS - 1) {
				command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
			}
		}

		return command.toString();
	}

	public Page clone() {
		try {
			return (Page) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Page)) {
			return false;
		}

		Page page = (Page) other;

		int indicatorsChannelsSize = page.getIndicatorsChannels().size();
		boolean equals = page.getContent() == getContent();

		if (equals && indicatorsChannelsSize == getIndicatorsChannels().size()) {
			if (indicatorsChannelsSize > 0) {
				equals &= page.getIndicatorsChannels().get(0) == getIndicatorsChannels().get(0);
			}
			if (equals && indicatorsChannelsSize > 1) {
				equals &= page.getIndicatorsChannels().get(1) == getIndicatorsChannels().get(1);
			}
			if (equals && indicatorsChannelsSize > 2) {
				equals &= page.getIndicatorsChannels().get(2) == getIndicatorsChannels().get(2);
			}
			if (equals && indicatorsChannelsSize > 3) {
				equals &= page.getIndicatorsChannels().get(3) == getIndicatorsChannels().get(3);
			}
			if (equals && indicatorsChannelsSize > 4) {
				equals &= page.getIndicatorsChannels().get(4) == getIndicatorsChannels().get(4);
			}
			if (equals && indicatorsChannelsSize > 5) {
				equals &= page.getIndicatorsChannels().get(5) == getIndicatorsChannels().get(5);
			}
			if (equals && indicatorsChannelsSize > 6) {
				equals &= page.getIndicatorsChannels().get(6) == getIndicatorsChannels().get(6);
			}
		}
		else {
			return false;
		}

		return equals;
	}

	@Override
	public String toString() {
		return "Page [mContent=" + mContent + ", mIndicatorsChannels=" + mIndicatorsChannels + "]";
	}
}
