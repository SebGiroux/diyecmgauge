package com.sgiroux.diyecmgauge.data;

public enum AlarmConditionOperator {
	GREATHER_THAN(">"), EQUAL_TO("="), LESS_THAN("<");

	private String mTitle;

	private AlarmConditionOperator(String title) {
		mTitle = title;
	}

	public String getTitle() {
		return mTitle;
	}

	public static AlarmConditionOperator fromTitle(String title) {
		for (AlarmConditionOperator aco : AlarmConditionOperator.values()) {
			if (aco.getTitle().equals(title)) {
				return aco;
			}
		}

		return null;
	}
}