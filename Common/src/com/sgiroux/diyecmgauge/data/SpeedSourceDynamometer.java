package com.sgiroux.diyecmgauge.data;

import java.util.ArrayList;
import java.util.List;

public enum SpeedSourceDynamometer {
	ECU_VSS("ECU Vehicle speed sensor"), GPS("GPS");

	private String mTitle;

	private SpeedSourceDynamometer(String title) {
		mTitle = title;
	}

	public String getTitle() {
		return mTitle;
	}

	public static List<String> getTitleList() {
		List<String> speedSourceDynamometer = new ArrayList<String>();

		for (SpeedSourceDynamometer speedSource : SpeedSourceDynamometer.values()) {
			speedSourceDynamometer.add(speedSource.getTitle());
		}
		return speedSourceDynamometer;
	}
}