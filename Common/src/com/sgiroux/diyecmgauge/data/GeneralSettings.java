package com.sgiroux.diyecmgauge.data;

import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Class representing all the possible general settings of the gauge
 * 
 * @author Seb
 * 
 */
public class GeneralSettings {
	private SpeedUnit mSpeedUnit = SpeedUnit.KMH;
	private TemperatureUnit mTemperatureUnit = TemperatureUnit.CELSIUS;
	private SpeedSourceDynamometer mSpeedSourceDynamometer = SpeedSourceDynamometer.ECU_VSS;
	private int mTimeZone;
	private boolean mRtcSyncWithGps;
	private int mRtcCompensate;
	private boolean mConfigurationSerialPortOnMainConnector;

	public SpeedUnit getSpeedUnit() {
		return mSpeedUnit;
	}

	public void setSpeedUnit(SpeedUnit speedUnit) {
		mSpeedUnit = speedUnit;
	}

	public TemperatureUnit getTemperatureUnit() {
		return mTemperatureUnit;
	}

	public void setTemperatureUnit(TemperatureUnit temperatureUnit) {
		mTemperatureUnit = temperatureUnit;
	}

	public SpeedSourceDynamometer getSpeedSourceDynamometer() {
		return mSpeedSourceDynamometer;
	}

	public void setSpeedSourceDynamometer(SpeedSourceDynamometer speedSourceDynamometer) {
		mSpeedSourceDynamometer = speedSourceDynamometer;
	}

	public int getTimeZone() {
		return mTimeZone;
	}

	public void setTimeZone(int timeZone) {
		mTimeZone = timeZone;
	}

	public boolean isRtcSyncWithGps() {
		return mRtcSyncWithGps;
	}

	public void setRtcSyncWithGps(boolean rtcSyncWithGps) {
		mRtcSyncWithGps = rtcSyncWithGps;
	}

	public int getRtcCompensate() {
		return mRtcCompensate;
	}

	public void setRtcCompensate(int rtcCompensate) {
		mRtcCompensate = rtcCompensate;
	}

	public boolean isConfigurationSerialPortOnMainConnector() {
		return mConfigurationSerialPortOnMainConnector;
	}

	public void setConfigurationSerialPortOnMainConnector(boolean configurationSerialPortOnMainConnector) {
		mConfigurationSerialPortOnMainConnector = configurationSerialPortOnMainConnector;
	}

	public String getSerialCommand() {
		StringBuffer command = new StringBuffer();

		command.append(mSpeedUnit.ordinal());
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mTemperatureUnit.ordinal());
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mSpeedSourceDynamometer.ordinal());
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mTimeZone);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mRtcSyncWithGps ? 1 : 0);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mRtcCompensate);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mConfigurationSerialPortOnMainConnector ? 1 : 0);

		return command.toString();
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof GeneralSettings)) {
			return false;
		}

		GeneralSettings generalSettings = (GeneralSettings) other;
		return generalSettings.getSpeedUnit() == getSpeedUnit() && generalSettings.getTemperatureUnit() == getTemperatureUnit() && generalSettings.getSpeedSourceDynamometer() == getSpeedSourceDynamometer() && generalSettings.getTimeZone() == getTimeZone()
				&& generalSettings.isRtcSyncWithGps() == isRtcSyncWithGps() && generalSettings.getRtcCompensate() == getRtcCompensate() && generalSettings.isConfigurationSerialPortOnMainConnector() == isConfigurationSerialPortOnMainConnector();
	}

	@Override
	public String toString() {
		return "GeneralSettings [mSpeedUnit=" + mSpeedUnit + ", mTemperatureUnit=" + mTemperatureUnit + ", mSpeedSourceDynamometer=" + mSpeedSourceDynamometer + ", mTimeZone=" + mTimeZone + ", mRtcSyncWithGps=" + mRtcSyncWithGps + ", mRtcCompensate=" + mRtcCompensate
				+ ", mConfigurationSerialPortOnMainConnector=" + mConfigurationSerialPortOnMainConnector + "]";
	}
}
