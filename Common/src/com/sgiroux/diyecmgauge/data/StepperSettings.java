package com.sgiroux.diyecmgauge.data;

import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Class representing all the possible stepper settings of the gauge
 * 
 * @author Seb
 * 
 */
public class StepperSettings {
	private int mTotalNumberOfSteps;
	private int mMaximumSpeed;
	private int mAcceleration;
	private HomingDirection mHomingDirection = HomingDirection.NORMAL;
	private int mChannel;

	public int getTotalNumberOfSteps() {
		return mTotalNumberOfSteps;
	}

	public void setTotalNumberOfSteps(int totalNumberOfSteps) {
		mTotalNumberOfSteps = totalNumberOfSteps;
	}

	public int getMaximumSpeed() {
		return mMaximumSpeed;
	}

	public void setMaximumSpeed(int maximumSpeed) {
		mMaximumSpeed = maximumSpeed;
	}

	public int getAcceleration() {
		return mAcceleration;
	}

	public void setAcceleration(int acceleration) {
		mAcceleration = acceleration;
	}

	public HomingDirection getHomingDirection() {
		return mHomingDirection;
	}

	public void setHomingDirection(HomingDirection homingDirection) {
		mHomingDirection = homingDirection;
	}

	public int getChannel() {
		return mChannel;
	}

	public void setChannel(int channel) {
		mChannel = channel;
	}

	public String getSerialCommand() {
		StringBuffer command = new StringBuffer();

		command.append(mTotalNumberOfSteps);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mMaximumSpeed);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mAcceleration);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mHomingDirection.ordinal());
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mChannel);

		return command.toString();
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof StepperSettings)) {
			return false;
		}

		StepperSettings stepperSettings = (StepperSettings) other;
		return stepperSettings.getTotalNumberOfSteps() == getTotalNumberOfSteps() && stepperSettings.getMaximumSpeed() == getMaximumSpeed() && stepperSettings.getAcceleration() == getAcceleration() && stepperSettings.getHomingDirection() == getHomingDirection()
				&& stepperSettings.getChannel() == getChannel();
	}

	@Override
	public String toString() {
		return "StepperSettings [mTotalNumberOfSteps=" + mTotalNumberOfSteps + ", mMaximumSpeed=" + mMaximumSpeed + ", mAcceleration=" + mAcceleration + ", mHomingDirection=" + mHomingDirection + ", mChannel=" + mChannel + "]";
	}
}
