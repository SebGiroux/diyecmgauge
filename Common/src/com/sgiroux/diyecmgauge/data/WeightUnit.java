package com.sgiroux.diyecmgauge.data;

import java.util.ArrayList;
import java.util.List;

public enum WeightUnit {
	LBS("Lbs"), KG("Kg");

	private String mTitle;

	private WeightUnit(String title) {
		mTitle = title;
	}

	public String getTitle() {
		return mTitle;
	}

	public static List<String> getTitleList() {
		List<String> vehicleWeightUnits = new ArrayList<String>();

		for (WeightUnit weightUnit : WeightUnit.values()) {
			vehicleWeightUnits.add(weightUnit.getTitle());
		}

		return vehicleWeightUnits;
	}
}
