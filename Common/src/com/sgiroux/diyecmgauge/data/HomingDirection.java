package com.sgiroux.diyecmgauge.data;

import java.util.ArrayList;
import java.util.List;

public enum HomingDirection {
	NORMAL("Normal"), INVERTED("Inverted");

	private String mDirection;

	private HomingDirection(String direction) {
		mDirection = direction;
	}

	public String getDirection() {
		return mDirection;
	}

	public static List<String> getTitleList() {
		List<String> directions = new ArrayList<String>();

		for (HomingDirection direction : HomingDirection.values()) {
			directions.add(direction.getDirection());
		}

		return directions;
	}
}