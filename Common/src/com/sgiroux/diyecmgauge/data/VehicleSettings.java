package com.sgiroux.diyecmgauge.data;

import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Class representing all the possible vehicle settings of the gauge
 * 
 * @author Seb
 * 
 */
public class VehicleSettings {
	private int mVehicleWeight;
	private double mCoefficientOfDrag;
	private double mCoefficientOfRollingResistance;
	private double mFrontalArea;

	public int getVehicleWeight() {
		return mVehicleWeight;
	}

	public void setVehicleWeight(int vehicleWeight) {
		mVehicleWeight = vehicleWeight;
	}

	public double getCoefficientOfDrag() {
		return mCoefficientOfDrag;
	}

	public void setCoefficientOfDrag(double coefficientOfDrag) {
		mCoefficientOfDrag = coefficientOfDrag;
	}

	public double getCoefficientOfRollingResistance() {
		return mCoefficientOfRollingResistance;
	}

	public void setCoefficientOfRollingResistance(double coefficientOfRollingResistance) {
		mCoefficientOfRollingResistance = coefficientOfRollingResistance;
	}

	public double getFrontalArea() {
		return mFrontalArea;
	}

	public void setFrontalArea(double frontalArea) {
		mFrontalArea = frontalArea;
	}

	public String getSerialCommand() {
		StringBuffer command = new StringBuffer();

		command.append(mVehicleWeight);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mCoefficientOfDrag);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mCoefficientOfRollingResistance);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mFrontalArea);

		return command.toString();
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof VehicleSettings)) {
			return false;
		}

		VehicleSettings vehicleSettings = (VehicleSettings) other;
		return vehicleSettings.getVehicleWeight() == getVehicleWeight() && vehicleSettings.getCoefficientOfDrag() == getCoefficientOfDrag() && vehicleSettings.getCoefficientOfRollingResistance() == getCoefficientOfRollingResistance() && vehicleSettings.getFrontalArea() == getFrontalArea();
	}

	@Override
	public String toString() {
		return "VehicleSettings [mVehicleWeight=" + mVehicleWeight + ", mCoefficientOfDrag=" + mCoefficientOfDrag + ", mCoefficientOfRollingResistance=" + mCoefficientOfRollingResistance + ", mFrontalArea=" + mFrontalArea + "]";
	}
}
