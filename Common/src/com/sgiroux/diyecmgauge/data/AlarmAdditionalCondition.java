package com.sgiroux.diyecmgauge.data;

public enum AlarmAdditionalCondition {
	NONE("None"), AND("And"), OR("Or");

	private String mTitle;

	private AlarmAdditionalCondition(String title) {
		mTitle = title;
	}

	public String getTitle() {
		return mTitle;
	}

	public static AlarmAdditionalCondition fromTitle(String title) {
		for (AlarmAdditionalCondition aac : AlarmAdditionalCondition.values()) {
			if (aac.getTitle().equals(title)) {
				return aac;
			}
		}

		return null;
	}
}