package com.sgiroux.diyecmgauge.data;

import java.util.ArrayList;
import java.util.List;

public enum TimeZone {
	// @formatter:off
	UTC_MINUS_TWELVE(-12, "UTC -12:00"),
	UTC_MINUS_ELEVEN(-11, "UTC -11:00"),
	UTC_MINUS_TEN(-10, "UTC -10:00"),
	UTC_MINUS_NINE(-9, "UTC -9:00"),
	UTC_MINUS_EIGHT(-8, "UTC -8:00"),
	UTC_MINUS_SEVEN(-7, "UTC -7:00"),
	UTC_MINUS_SIX(-6, "UTC -6:00"),
	UTC_MINUS_FIVE(-5, "UTC -5:00"),
	UTC_MINUS_FOUR(-4, "UTC -4:00"),
	UTC_MINUS_THREE(-3, "UTC -3:00"),
	UTC_MINUS_TWO(-2, "UTC -2:00"),
	UTC_MINUS_ONE(-1, "UTC -1:00"),
	UTC(0, "UTC"),
	UTC_PLUS_ONE(1, "UTC +1:00"),
	UTC_PLUS_TWO(2, "UTC +2:00"),
	UTC_PLUS_THREE(3, "UTC +3:00"),
	UTC_PLUS_FOUR(4, "UTC +4:00"),
	UTC_PLUS_FIVE(5, "UTC +5:00"),
	UTC_PLUS_SIX(6, "UTC +6:00"),
	UTC_PLUS_SEVEN(7, "UTC +7:00"),
	UTC_PLUS_EIGHT(8, "UTC +8:00"),
	UTC_PLUS_NINE(9, "UTC +9:00"),
	UTC_PLUS_TEN(10, "UTC +10:00"),
	UTC_PLUS_ELEVEN(11, "UTC +11:00"),
	UTC_PLUS_TWELVE(12, "UTC +12:00"),
	UTC_PLUS_THIRTHEEN(13, "UTC +13:00");
	// @formatter:on

	private int mUtcOffset;
	private String mTitle;

	private TimeZone(int utcOffset, String title) {
		mUtcOffset = utcOffset;
		mTitle = title;
	}

	public static TimeZone fromOffset(int utcOffset) {
		TimeZone timeZone = null;

		for (TimeZone tz : TimeZone.values()) {
			if (tz.getUtcOffset() == utcOffset) {
				timeZone = tz;
				break;
			}
		}

		return timeZone;
	}

	public int getUtcOffset() {
		return mUtcOffset;
	}

	public String getTitle() {
		return mTitle;
	}

	public static List<String> getTitleList() {
		List<String> timeZones = new ArrayList<String>();

		for (TimeZone timeZone : TimeZone.values()) {
			timeZones.add(timeZone.getTitle());
		}

		return timeZones;
	}
}
