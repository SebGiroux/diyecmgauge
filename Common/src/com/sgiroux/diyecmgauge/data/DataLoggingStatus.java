package com.sgiroux.diyecmgauge.data;

public enum DataLoggingStatus {
	NO_SD_CARD("No SD card found"), NOT_DATA_LOGGING("Currently not data logging"), DATA_LOGGING("Currently data logging");
	
	private String mMessage;
	
	private DataLoggingStatus(String message) {
		mMessage = message;
	}
	
	public String getMessage() {
		return mMessage;
	}
}