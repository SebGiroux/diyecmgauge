package com.sgiroux.diyecmgauge.data;

import java.util.ArrayList;
import java.util.List;

public enum DimensionUnit {
	FEET_SQUARE("Feet�"), METER_SQUARE("Meter�");

	private String mTitle;

	private DimensionUnit(String title) {
		mTitle = title;
	}

	public String getTitle() {
		return mTitle;
	}

	public static List<String> getTitleList() {
		List<String> frontalAreaUnits = new ArrayList<String>();

		for (DimensionUnit dimensionUnit : DimensionUnit.values()) {
			frontalAreaUnits.add(dimensionUnit.getTitle());
		}

		return frontalAreaUnits;
	}
}
