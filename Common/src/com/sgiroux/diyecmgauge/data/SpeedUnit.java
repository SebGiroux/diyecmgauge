package com.sgiroux.diyecmgauge.data;

import java.util.ArrayList;
import java.util.List;

public enum SpeedUnit {
	KMH("Kilometers per hour"), MPH("Miles per hour");

	private String mTitle;

	private SpeedUnit(String title) {
		mTitle = title;
	}

	public String getTitle() {
		return mTitle;
	}

	public static List<String> getTitleList() {
		List<String> speedUnits = new ArrayList<String>();

		for (SpeedUnit speedUnit : SpeedUnit.values()) {
			speedUnits.add(speedUnit.getTitle());
		}

		return speedUnits;
	}
}