package com.sgiroux.diyecmgauge.data;

import com.sgiroux.diyecmgauge.list.IndicatorsList;
import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Alarm object representing one alarm from the gauge
 * 
 * @author Seb
 * 
 */
public class Alarm implements Cloneable {
	private static final String UNUSED_NAME = "Unused";

	private boolean mEnabled;

	private String mName = "";

	private String mChannel1 = "";
	private AlarmConditionOperator mOperator1;
	private float mThreshold1;
	private float mHysteresis1;

	private AlarmAdditionalCondition mAdditionalCondition;

	private String mChannel2 = "";
	private AlarmConditionOperator mOperator2;
	private float mThreshold2;
	private float mHysteresis2;

	private boolean mShowWarningOnScreen;
	private boolean mTriggerBuzzer;
	private boolean mStartDataLogging;
	private boolean mTurnOnLeftLED;
	private boolean mTurnOnRightLED;

	public Alarm() {
		String indicators[] = IndicatorsList.getInstance().getList();

		// Default values
		mChannel1 = indicators[0];
		mChannel2 = indicators[1];
		mOperator1 = AlarmConditionOperator.GREATHER_THAN;
		mOperator2 = AlarmConditionOperator.GREATHER_THAN;
		mAdditionalCondition = AlarmAdditionalCondition.NONE;
	}

	public Alarm(String name) {
		this();

		mName = name;
	}

	public boolean isEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean enabled) {
		mEnabled = enabled;
	}

	public String getName() {
		String name = mName;

		if (name.equals("0") && !mEnabled) {
			name = UNUSED_NAME;
		}

		return name;
	}

	public void setName(String name) {
		mName = name;
	}

	public String getChannel1() {
		return mChannel1;
	}

	public void setChannel1(String channel1) {
		mChannel1 = channel1;
	}

	public AlarmConditionOperator getOperator1() {
		return mOperator1;
	}

	public void setOperator1(AlarmConditionOperator operator1) {
		mOperator1 = operator1;
	}

	public float getThreshold1() {
		return mThreshold1;
	}

	public void setThreshold1(float threshold1) {
		mThreshold1 = threshold1;
	}

	public float getHysteresis1() {
		return mHysteresis1;
	}

	public void setHysteresis1(float hysteresis1) {
		mHysteresis1 = hysteresis1;
	}

	public AlarmAdditionalCondition getAdditionalCondition() {
		return mAdditionalCondition;
	}

	public void setAdditionalCondition(AlarmAdditionalCondition alarmAdditionalCondition) {
		mAdditionalCondition = alarmAdditionalCondition;
	}

	public String getChannel2() {
		return mChannel2;
	}

	public void setChannel2(String channel2) {
		mChannel2 = channel2;
	}

	public AlarmConditionOperator getOperator2() {
		return mOperator2;
	}

	public void setOperator2(AlarmConditionOperator operator2) {
		mOperator2 = operator2;
	}

	public float getThreshold2() {
		return mThreshold2;
	}

	public void setThreshold2(float threshold2) {
		mThreshold2 = threshold2;
	}

	public float getHysteresis2() {
		return mHysteresis2;
	}

	public void setHysteresis2(float hysteresis2) {
		mHysteresis2 = hysteresis2;
	}

	public boolean isShowWarningOnScreen() {
		return mShowWarningOnScreen;
	}

	public void setShowWarningOnScreen(boolean showWarningOnScreen) {
		mShowWarningOnScreen = showWarningOnScreen;
	}

	public boolean isTriggerBuzzer() {
		return mTriggerBuzzer;
	}

	public void setTriggerBuzzer(boolean triggerBuzzer) {
		mTriggerBuzzer = triggerBuzzer;
	}

	public boolean isStartDataLogging() {
		return mStartDataLogging;
	}

	public void setStartDataLogging(boolean startDataLogging) {
		mStartDataLogging = startDataLogging;
	}

	public boolean isTurnOnLeftLED() {
		return mTurnOnLeftLED;
	}

	public void setTurnOnLeftLED(boolean turnOnLeftLED) {
		mTurnOnLeftLED = turnOnLeftLED;
	}

	public boolean isTurnOnRightLED() {
		return mTurnOnRightLED;
	}

	public void setTurnOnRightLED(boolean turnOnRightLED) {
		mTurnOnRightLED = turnOnRightLED;
	}

	public String getSerialCommand(int alarmIndex) {
		StringBuffer command = new StringBuffer();

		command.append(alarmIndex);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mEnabled ? 1 : 0);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mName);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);

		// First condition
		command.append(IndicatorsList.getInstance().getIndexForIndicator(mChannel1));
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mOperator1.ordinal());
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mThreshold1);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mHysteresis1);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mAdditionalCondition.ordinal());
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);

		// Second condition
		command.append(IndicatorsList.getInstance().getIndexForIndicator(mChannel2));
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mOperator2.ordinal());
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mThreshold2);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mHysteresis2);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);

		command.append(mShowWarningOnScreen ? 1 : 0);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mTriggerBuzzer ? 1 : 0);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mStartDataLogging ? 1 : 0);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mTurnOnLeftLED ? 1 : 0);
		command.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		command.append(mTurnOnRightLED ? 1 : 0);

		return command.toString();
	}

	public Alarm clone() {
		try {
			return (Alarm) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Alarm)) {
			return false;
		}

		Alarm alarm = (Alarm) other;
		return alarm.isEnabled() == isEnabled() && alarm.getName().equals(getName()) && alarm.getChannel1().equals(getChannel1()) && alarm.getOperator1() == getOperator1() && alarm.getThreshold1() == getThreshold1() && alarm.getAdditionalCondition() == getAdditionalCondition()
				&& alarm.getChannel2().equals(getChannel2()) && alarm.getOperator2() == getOperator2() && alarm.getThreshold2() == getThreshold2() && alarm.getHysteresis2() == getHysteresis2() && alarm.isShowWarningOnScreen() == isShowWarningOnScreen()
				&& alarm.isTriggerBuzzer() == isTriggerBuzzer() && alarm.isStartDataLogging() == isStartDataLogging() && alarm.isTurnOnLeftLED() == isTurnOnLeftLED() && alarm.isTurnOnRightLED() == isTurnOnRightLED();
	}

	@Override
	public String toString() {
		return "Alarm [mEnabled=" + mEnabled + ", mName=" + mName + ", mChannel1=" + mChannel1 + ", mOperator1=" + mOperator1 + ", mThreshold1=" + mThreshold1 + ", mHysteresis1=" + mHysteresis1 + ", mAdditionalCondition=" + mAdditionalCondition + ", mChannel2=" + mChannel2 + ", mOperator2="
				+ mOperator2 + ", mThreshold2=" + mThreshold2 + ", mHysteresis2=" + mHysteresis2 + ", mShowWarningOnScreen=" + mShowWarningOnScreen + ", mTriggerBuzzer=" + mTriggerBuzzer + ", mStartDataLogging=" + mStartDataLogging + ", mTurnOnLeftLED=" + mTurnOnLeftLED + ", mTurnOnRightLED="
				+ mTurnOnRightLED + "]";
	}
}
