package com.sgiroux.diyecmgauge.data;

/**
 * Class that define the components of a data log row
 * 
 * @author Seb
 * 
 */
public class DataLogRow {
	private String mDataLogName = "";
	private String mDataLogDate = "";
	private String mDataLogStats = "";
	private long mDataLogSize = 0; // in bytes

	public String getDataLogName() {
		return mDataLogName;
	}

	public void setDataLogName(String dataLogName) {
		mDataLogName = dataLogName;
	}

	public String getDataLogDate() {
		return mDataLogDate;
	}

	public void setDataLogDate(String dataLogDate) {
		mDataLogDate = dataLogDate;
	}

	public String getDataLogStats() {
		return mDataLogStats;
	}

	public void setDataLogStats(String dataLogStats) {
		mDataLogStats = dataLogStats;
	}

	public long getDataLogSize() {
		return mDataLogSize;
	}

	public void setDataLogSize(long dataLogSize) {
		mDataLogSize = dataLogSize;
	}
}