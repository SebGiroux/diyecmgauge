package com.sgiroux.diyecmgauge.list;

import java.util.ArrayList;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.data.Page;
import com.sgiroux.diyecmgauge.data.PageContent;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;
import com.sgiroux.diyecmgauge.utils.NumberUtils;

/**
 * 
 * @author Seb
 * 
 */
public class PagesList implements SerialResponseReceivedListener {
	private ArrayList<Page> mPagesList = new ArrayList<Page>();
	private static PagesList mInstance;

	public static PagesList getInstance() {
		if (mInstance == null) {
			mInstance = new PagesList();
		}

		return mInstance;
	}

	public void initialize() {
		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
		refresh();
	}

	public ArrayList<Page> getList() {
		return mPagesList;
	}

	public void refresh() {
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_PAGES_LIST));
	}

	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		if (serialCommand == SerialCommand.GET_PAGES_LIST) {
			mPagesList.clear();

			String textResponse = SerialUtils.getTextResponse(response);
			String values[] = SerialUtils.splitByValues(textResponse);

			for (String value : values) {
				String[] pageText = SerialUtils.splitBySubValues(value);

				Page page = new Page();
				int pageContent = NumberUtils.getIntBelowMaxOrZero(pageText[0], PageContent.values().length);
				page.setContent(PageContent.values()[pageContent]);

				ArrayList<Integer> indicatorsChannels = new ArrayList<Integer>();
				for (int i = 1; i < pageText.length; i++) {
					int indicatorIndex = NumberUtils.getIntOrZero(pageText[i]);
					indicatorsChannels.add(indicatorIndex);
				}

				page.setIndicatorsChannels(indicatorsChannels);

				mPagesList.add(page);
			}
		}
	}
}
