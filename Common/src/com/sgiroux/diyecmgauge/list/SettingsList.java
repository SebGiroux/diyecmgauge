package com.sgiroux.diyecmgauge.list;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.data.GeneralSettings;
import com.sgiroux.diyecmgauge.data.HomingDirection;
import com.sgiroux.diyecmgauge.data.SpeedSourceDynamometer;
import com.sgiroux.diyecmgauge.data.SpeedUnit;
import com.sgiroux.diyecmgauge.data.StepperSettings;
import com.sgiroux.diyecmgauge.data.TemperatureUnit;
import com.sgiroux.diyecmgauge.data.TimeZone;
import com.sgiroux.diyecmgauge.data.VehicleSettings;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;
import com.sgiroux.diyecmgauge.utils.NumberUtils;

public class SettingsList implements SerialResponseReceivedListener {
	private GeneralSettings mGeneralSettings = new GeneralSettings();
	private VehicleSettings mVehicleSettings = new VehicleSettings();
	private StepperSettings mStepperSettings = new StepperSettings();

	private static SettingsList mInstance;

	public static SettingsList getInstance() {
		if (mInstance == null) {
			mInstance = new SettingsList();
		}

		return mInstance;
	}

	public void initialize() {
		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
		refresh();
	}

	public void setGeneralSettings(GeneralSettings generalSettings) {
		mGeneralSettings = generalSettings;
	}

	public GeneralSettings getGeneralSettings() {
		return mGeneralSettings;
	}

	public void setVehicleSettings(VehicleSettings vehicleSettings) {
		mVehicleSettings = vehicleSettings;
	}

	public VehicleSettings getVehicleSettings() {
		return mVehicleSettings;
	}

	public void setStepperSettings(StepperSettings stepperSettings) {
		mStepperSettings = stepperSettings;
	}

	public StepperSettings getStepperSettings() {
		return mStepperSettings;
	}

	public void refresh() {
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_GENERAL_SETTINGS));
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_VEHICLE_SETTINGS));
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_STEPPER_SETTINGS));
	}

	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		String textResponse = SerialUtils.getTextResponse(response);
		String values[] = SerialUtils.splitByValues(textResponse);

		switch (serialCommand) {
			case GET_GENERAL_SETTINGS:
				int nbSpeedUnit = SpeedUnit.values().length;
				int speedUnit = NumberUtils.getIntOrZero(values[0]);
				if (speedUnit < nbSpeedUnit) {
					mGeneralSettings.setSpeedUnit(SpeedUnit.values()[speedUnit]);
				}

				int nbTemperatureUnit = TemperatureUnit.values().length;
				int temperatureUnit = NumberUtils.getIntOrZero(values[1]);
				if (temperatureUnit < nbTemperatureUnit) {
					mGeneralSettings.setTemperatureUnit(TemperatureUnit.values()[temperatureUnit]);
				}

				int nbSpeedSourceDynamometer = SpeedSourceDynamometer.values().length;
				int speedSourceDynamometer = NumberUtils.getIntOrZero(values[2]);
				if (speedSourceDynamometer < nbSpeedSourceDynamometer) {
					mGeneralSettings.setSpeedSourceDynamometer(SpeedSourceDynamometer.values()[speedSourceDynamometer]);
				}

				int nbTimeZone = TimeZone.values().length;
				int timeZone = NumberUtils.getIntOrZero(values[3]);
				if (timeZone < nbTimeZone) {
					mGeneralSettings.setTimeZone(timeZone);
				}

				mGeneralSettings.setRtcSyncWithGps(values[4].equals("1"));
				mGeneralSettings.setRtcCompensate(NumberUtils.getIntOrZero(values[5]));
				mGeneralSettings.setConfigurationSerialPortOnMainConnector(values[6].equals("1"));
				break;

			case GET_VEHICLE_SETTINGS:
				mVehicleSettings.setVehicleWeight(NumberUtils.getIntOrZero(values[0]));
				mVehicleSettings.setCoefficientOfDrag(NumberUtils.getDoubleOrZero(values[1]));
				mVehicleSettings.setCoefficientOfRollingResistance(NumberUtils.getDoubleOrZero(values[2]));
				mVehicleSettings.setFrontalArea(NumberUtils.getDoubleOrZero(values[3]));
				break;

			case GET_STEPPER_SETTINGS:
				mStepperSettings.setTotalNumberOfSteps(NumberUtils.getIntOrZero(values[0]));
				mStepperSettings.setMaximumSpeed(NumberUtils.getIntOrZero(values[1]));
				mStepperSettings.setAcceleration(NumberUtils.getIntOrZero(values[2]));

				int nbHomingDirection = HomingDirection.values().length;
				int homingDirection = NumberUtils.getIntOrZero(values[3]);
				if (homingDirection < nbHomingDirection) {
					mStepperSettings.setHomingDirection(HomingDirection.values()[homingDirection]);
				}

				mStepperSettings.setChannel(NumberUtils.getIntOrZero(values[4]));
				break;

			default:
				break;
		}
	}
}
