package com.sgiroux.diyecmgauge.list;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Fetch the list of available indicators from the gauge and keep a local version for performance reason
 * 
 * @author Seb
 */
public class IndicatorsList implements SerialResponseReceivedListener {
	private String[] mIndicatorsList = null;
	private static IndicatorsList mInstance;

	public static final int UNUSED = -1;

	public static IndicatorsList getInstance() {
		if (mInstance == null) {
			mInstance = new IndicatorsList();
		}

		return mInstance;
	}

	public void initialize() {
		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
		refresh();
	}

	public String[] getList() {
		return mIndicatorsList;
	}

	public void refresh() {
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_INDICATORS_LIST));
	}

	public int getIndexForIndicator(String title) {
		int index = -1;

		for (String indicator : mIndicatorsList) {
			index++;

			if (indicator.equals(title)) {
				break;
			}
		}

		return index;
	}

	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		if (serialCommand == SerialCommand.GET_INDICATORS_LIST) {
			String textResponse = SerialUtils.getTextResponse(response);

			mIndicatorsList = SerialUtils.splitByValues(textResponse);
		}
	}
}
