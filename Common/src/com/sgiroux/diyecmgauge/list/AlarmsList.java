package com.sgiroux.diyecmgauge.list;

import java.util.ArrayList;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.data.Alarm;
import com.sgiroux.diyecmgauge.data.AlarmAdditionalCondition;
import com.sgiroux.diyecmgauge.data.AlarmConditionOperator;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;
import com.sgiroux.diyecmgauge.utils.NumberUtils;

/**
 * 
 * @author Seb
 * 
 */
public class AlarmsList implements SerialResponseReceivedListener {
	private static int MAX_NB_ALARMS = 0;

	private ArrayList<Alarm> mAlarmsList = new ArrayList<Alarm>();
	private static AlarmsList mInstance;

	public static AlarmsList getInstance() {
		if (mInstance == null) {
			mInstance = new AlarmsList();
		}

		return mInstance;
	}

	public void initialize() {
		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
		refresh();
	}

	public ArrayList<Alarm> getList() {
		return mAlarmsList;
	}

	public int getMax() {
		return MAX_NB_ALARMS;
	}

	public void refresh() {
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_ALARMS_LIST));
	}

	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		if (serialCommand == SerialCommand.GET_ALARMS_LIST) {
			mAlarmsList.clear();
			MAX_NB_ALARMS = 0;

			String textResponse = SerialUtils.getTextResponse(response);

			int nbIndicators = IndicatorsList.getInstance().getList().length;
			int nbConditionOperators = AlarmConditionOperator.values().length;

			for (String alarmValue : SerialUtils.splitByValues(textResponse)) {
				String[] alarmSubValues = SerialUtils.splitBySubValues(alarmValue);

				Alarm alarm = new Alarm();
				alarm.setEnabled(alarmSubValues[0].equals("1"));
				alarm.setName(alarmSubValues[1]);

				// First condition
				int channel1 = NumberUtils.getIntBelowMaxOrZero(alarmSubValues[2], nbIndicators);
				int operator1 = NumberUtils.getIntBelowMaxOrZero(alarmSubValues[3], nbConditionOperators);
				alarm.setChannel1(IndicatorsList.getInstance().getList()[channel1]);
				if (operator1 < AlarmConditionOperator.values().length) {
					alarm.setOperator1(AlarmConditionOperator.values()[operator1]);
				}
				alarm.setThreshold1(NumberUtils.getFloatOrZero(alarmSubValues[4]));
				alarm.setHysteresis1(NumberUtils.getFloatOrZero(alarmSubValues[5]));

				// Additional condition
				int additionalCondition = NumberUtils.getIntOrZero(alarmSubValues[6]);
				if (additionalCondition < AlarmAdditionalCondition.values().length) {
					alarm.setAdditionalCondition(AlarmAdditionalCondition.values()[additionalCondition]);
				}

				// Second condition
				int channel2 = NumberUtils.getIntBelowMaxOrZero(alarmSubValues[7], nbIndicators);
				int operator2 = NumberUtils.getIntBelowMaxOrZero(alarmSubValues[8], nbConditionOperators);
				alarm.setChannel2(IndicatorsList.getInstance().getList()[channel2]);
				if (operator2 < AlarmConditionOperator.values().length) {
					alarm.setOperator2(AlarmConditionOperator.values()[operator2]);
				}
				alarm.setThreshold2(NumberUtils.getFloatOrZero(alarmSubValues[9]));
				alarm.setHysteresis2(NumberUtils.getFloatOrZero(alarmSubValues[10]));

				// Reactions
				alarm.setShowWarningOnScreen(alarmSubValues[12].equals("1"));
				alarm.setTriggerBuzzer(alarmSubValues[13].equals("1"));
				alarm.setStartDataLogging(alarmSubValues[14].equals("1"));
				alarm.setTurnOnLeftLED(alarmSubValues[15].equals("1"));
				alarm.setTurnOnRightLED(alarmSubValues[16].equals("1"));

				mAlarmsList.add(alarm);

				MAX_NB_ALARMS++;
			}
		}
	}
}
