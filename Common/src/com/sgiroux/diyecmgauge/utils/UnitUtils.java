package com.sgiroux.diyecmgauge.utils;

/**
 * Class containing units conversion utilities methods
 * 
 * @author Seb
 * 
 */
public class UnitUtils {
	private static final double LB_PER_KG = 2.20462262;
	private static final double SQFT_PER_SQ_MT = 0.092903;

	/**
	 * Get the of lbs from the number of kilogrammes
	 * 
	 * @param kg The number of kilogrammes to be converted
	 * @return The number of lbs
	 */
	public static double getLbsFromKg(double kg) {
		return kg * LB_PER_KG;
	}

	/**
	 * Get the number of square feet from the number of square meters
	 * 
	 * @param squareMeter The number of square meters to be converted
	 * 
	 * @return The number of square feet
	 */
	public static double getSqFtFromSqMt(double squareMeter) {
		return squareMeter * SQFT_PER_SQ_MT;
	}
}
