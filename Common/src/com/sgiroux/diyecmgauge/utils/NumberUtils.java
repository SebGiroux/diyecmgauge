package com.sgiroux.diyecmgauge.utils;

/**
 * Class containing number utilities methods
 * 
 * @author Seb
 * 
 */
public class NumberUtils {
	/**
	 * Take a string and try to make a integer out of it. If impossible, returns 0
	 * 
	 * @param value The string value to be parsed into a integer
	 * 
	 * @return The number represented by the string if able to parse, zero otherwise
	 */
	public static int getIntOrZero(String value) {
		int intValue = 0;
		try {
			intValue = Integer.parseInt(value);
		} catch (NumberFormatException e) {
		}

		return intValue;
	}

	/**
	 * Take a string and try to make a float out of it. If impossible, returns 0
	 * 
	 * @param value The string value to be parsed into a float
	 * 
	 * @return The number represented by the string if able to parse, zero otherwise
	 */
	public static float getFloatOrZero(String value) {
		float floatValue = 0;
		try {
			floatValue = Float.parseFloat(value);
		} catch (NumberFormatException e) {
		}

		return floatValue;
	}

	/**
	 * Take a string and try to make a double out of it. If impossible, returns 0
	 * 
	 * @param value The string value to be parsed into a double
	 * 
	 * @return The number represented by the string if able to parse, zero otherwise
	 */
	public static double getDoubleOrZero(String value) {
		double doubleValue = 0;
		try {
			doubleValue = Double.parseDouble(value);
		} catch (NumberFormatException e) {
		}

		return doubleValue;
	}

	/**
	 * Take a string and try to make a integer out of it. If impossible or if maximum reached, returns 0
	 * 
	 * @param value The string value to be parsed into a integer
	 * @param max The maximum value this integer can be parsed to
	 * 
	 * @return The number represented by the string if able to parse and if lower than maximum, zero otherwise
	 */
	public static int getIntBelowMaxOrZero(String value, int max) {
		int intValue = getIntOrZero(value);

		if (intValue >= max) {
			intValue = 0;
		}

		return intValue;
	}
}
