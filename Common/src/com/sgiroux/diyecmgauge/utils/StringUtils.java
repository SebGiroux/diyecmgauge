package com.sgiroux.diyecmgauge.utils;

/**
 * Class containing string utilities methods
 * 
 * @author Seb
 * 
 */
public class StringUtils {
	/**
	 * Take a file size in bytes and returns a human readable representation of it
	 * 
	 * @param bytes The number of bytes to be represented
	 * @return A human readable file size
	 */
	public static String humanReadableByteCount(long bytes) {
		int unit = 1024;
		if (bytes < unit) {
			return bytes + " B";
		}

		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = "KMGTPE".charAt(exp - 1) + "";

		return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	/**
	 * Helper method to translate a byte[] array into a combo of hex digits and ASCII
	 * 
	 * @param buffer The buffer to be translated
	 * @param len The length to be used in the buffer
	 * @param offset The offset where to start using the data in the buffer
	 * 
	 * @return The truncated string in the new format
	 */
	public static String bytesToHex(byte[] buffer, int len, int offset) {
		int newLength = len - offset;
		byte[] truncated = new byte[newLength];

		System.arraycopy(buffer, offset, truncated, 0, newLength);

		return bytesToHex(truncated, newLength);
	}

	/**
	 * Helper method to translate a byte[] array into a combo of hex digits and ASCII
	 * 
	 * @param buffer The buffer to be translated
	 * @param len The length to be used in the buffer
	 * 
	 * @return The string in the new format
	 */
	public static String bytesToHex(byte[] buffer, int len) {
		final StringBuffer b = new StringBuffer();
		StringBuffer text = new StringBuffer();
		StringBuffer hex = new StringBuffer();

		for (int i = 0; i < len && buffer.length > i; i++) {
			hex.append(String.format(" %02x", buffer[i]));

			final char c = (char) buffer[i];
			if ((c >= 32) && (c <= 127)) {
				text.append(c);
			}
			else {
				text.append('.');
			}

			if (((i + 1) % 40) == 0) {
				b.append(hex).append(" ").append(text).append("\n");
				text = new StringBuffer();
				hex = new StringBuffer();
			}
		}

		if (text.length() > 0) {
			b.append(String.format("%1$-120s %2$s%n", hex.toString(), text.toString()));
		}

		return b.toString();
	}
}
