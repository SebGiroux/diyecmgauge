package com.sgiroux.diyecmgauge;

import java.util.ArrayList;

import com.sgiroux.diyecmgauge.serial.SerialCommThreadBase;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialResponseTimeoutListener;

/**
 * Singleton class
 * 
 * @author Seb
 * 
 */
public class DIYECMGauge {
	private volatile static DIYECMGauge mInstance;
	private ArrayList<SerialResponseReceivedListener> mSerialResponseListener = new ArrayList<SerialResponseReceivedListener>();
	private ArrayList<SerialResponseTimeoutListener> mSerialResponseTimeoutListener = new ArrayList<SerialResponseTimeoutListener>();

	private SerialCommThreadBase mWriterThread;

	/**
	 * 
	 * @return
	 */
	public static DIYECMGauge getInstance() {
		if (mInstance == null) {
			mInstance = new DIYECMGauge();
		}

		return mInstance;
	}

	/**
	 * Check if the listener already exists in the array list
	 * 
	 * @param serialResponseReceivedListener The listener to be checked
	 * 
	 * @return true if the listener already exists in the array list, false otherwise
	 */
	public boolean existsSerialResponseReceivedListener(SerialResponseReceivedListener serialResponseReceivedListener) {
		return mSerialResponseListener.contains(serialResponseReceivedListener);
	}

	/**
	 * Add a serial response received listener
	 * 
	 * @param serialResponseReceivedListener The instance of serial response listener to be added
	 */
	public void addSerialResponseReceivedListener(SerialResponseReceivedListener serialResponseReceivedListener) {
		mSerialResponseListener.add(serialResponseReceivedListener);
	}

	/**
	 * Remove a serial response received listener
	 * 
	 * @param serialResponseReceivedListener The instance of serial response listener to be removed
	 */
	public void removeSerialResponseReceivedListener(SerialResponseReceivedListener serialResponseReceivedListener) {
		mSerialResponseListener.remove(serialResponseReceivedListener);
	}

	/**
	 * @return The array list of serial response received listeners
	 */
	public ArrayList<SerialResponseReceivedListener> getSerialResponseReceivedListeners() {
		return mSerialResponseListener;
	}

	/**
	 * Add a serial response time out listener
	 * 
	 * @param serialResponseTimeoutListener The instance of serial response listener to be added
	 */
	public void addSerialResponseTimeoutListener(SerialResponseTimeoutListener serialResponseTimeoutListener) {
		mSerialResponseTimeoutListener.add(serialResponseTimeoutListener);
	}

	/**
	 * Remove a serial response time out listener
	 * 
	 * @param serialResponseTimeoutListener The instance of serial response listener to be removed
	 */
	public void removeSerialResponseTimeoutListener(SerialResponseTimeoutListener serialResponseTimeoutListener) {
		mSerialResponseTimeoutListener.remove(serialResponseTimeoutListener);
	}

	/**
	 * @return The array list of serial response time out listeners
	 */
	public ArrayList<SerialResponseTimeoutListener> getSerialResponseTimeoutListeners() {
		return mSerialResponseTimeoutListener;
	}

	/**
	 * Send a command to the gauge over the serial port
	 * 
	 * @param serialCommandQueue
	 */
	public void sendSerialCommand(SerialCommandQueue serialCommandQueue) {
		mWriterThread.addCommandToQueue(serialCommandQueue);

		synchronized (mWriterThread.getThreadLock()) {
			mWriterThread.getThreadLock().notify();
		}
	}

	/**
	 * 
	 * @param serialWriterThread
	 * @return
	 */
	public void setWriterThread(SerialCommThreadBase serialWriterThread) {
		mWriterThread = serialWriterThread;
	}

	/**
	 * @return
	 */
	public SerialCommThreadBase getWriterThread() {
		return mWriterThread;
	}
}
