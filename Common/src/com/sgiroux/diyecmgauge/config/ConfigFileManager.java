package com.sgiroux.diyecmgauge.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.data.Alarm;
import com.sgiroux.diyecmgauge.data.AlarmAdditionalCondition;
import com.sgiroux.diyecmgauge.data.AlarmConditionOperator;
import com.sgiroux.diyecmgauge.data.GeneralSettings;
import com.sgiroux.diyecmgauge.data.HomingDirection;
import com.sgiroux.diyecmgauge.data.Page;
import com.sgiroux.diyecmgauge.data.PageContent;
import com.sgiroux.diyecmgauge.data.SpeedSourceDynamometer;
import com.sgiroux.diyecmgauge.data.SpeedUnit;
import com.sgiroux.diyecmgauge.data.StepperSettings;
import com.sgiroux.diyecmgauge.data.TemperatureUnit;
import com.sgiroux.diyecmgauge.data.VehicleSettings;
import com.sgiroux.diyecmgauge.list.AlarmsList;
import com.sgiroux.diyecmgauge.list.IndicatorsList;
import com.sgiroux.diyecmgauge.list.PagesList;
import com.sgiroux.diyecmgauge.list.SettingsList;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorBase;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorCompletedListener;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;

/**
 * Manager to load and save configuration file. Configuration files includes pages and alarms.
 * 
 * @author Seb
 * 
 */
public class ConfigFileManager implements SerialResponseReceivedListener {
	private static final String ENCODING = "ISO-8859-1";

	public static final String CONFIG_FILE_EXTENSION = ".xml";
	public static final String CONFIG_FILE_DEFAULT_NAME = "config" + CONFIG_FILE_EXTENSION;

	private Document mDocument;

	private static ProgressMonitorBase mProgressMonitor;

	private int mNbPagesSent;
	private int mNbAlarmsSent;
	private int mNbTotalPages;
	private int mNbTotalAlarms;

	private boolean mGeneralSettingsSent;
	private boolean mVehicleSettingsSent;
	private boolean mStepperSettingsSent;

	/**
	 * Open an XML configuration file
	 * 
	 * @param progressMonitor The progress monitor object that will be shown to the user
	 * @param file The file object to be opened
	 */
	public void openConfigFile(ProgressMonitorBase progressMonitor, File file) {
		if (!DIYECMGauge.getInstance().existsSerialResponseReceivedListener(this)) {
			DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
		}

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;

		mProgressMonitor = progressMonitor;
		mProgressMonitor.setDescription("Loading configuration file");
		mProgressMonitor.setVisible(true);
		mProgressMonitor.addCompletedListener(new ProgressMonitorCompletedListener() {
			@Override
			public void onProgressMonitorCompleted() {
				// Refresh local pages, alarms list and settings once we're done sending the configuration to the gauge
				PagesList.getInstance().refresh();
				AlarmsList.getInstance().refresh();
				SettingsList.getInstance().refresh();

				DIYECMGauge.getInstance().removeSerialResponseReceivedListener(ConfigFileManager.this);
			}
		});

		mNbPagesSent = 0;
		mNbAlarmsSent = 0;

		mNbTotalPages = 0;
		mNbTotalAlarms = 0;

		mGeneralSettingsSent = false;
		mVehicleSettingsSent = false;
		mStepperSettingsSent = false;

		try {
			docBuilder = docFactory.newDocumentBuilder();

			mDocument = docBuilder.parse(new InputSource(new InputStreamReader(new FileInputStream(file.getAbsolutePath()), ENCODING)));

			readPagesNode();
			readAlarmsNode();
			readGeneralSettingsNode();
			readVehicleSettingsNode();
			readStepperSettingsNode();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read the "pages" node from the XML configuration file
	 */
	private void readPagesNode() {
		NodeList pagesNodes = mDocument.getElementsByTagName(FieldsName.PAGES.getName());

		if (pagesNodes.getLength() > 0) {
			Node pagesNode = pagesNodes.item(0);

			// For each page node
			for (int i = 0; i < pagesNode.getChildNodes().getLength(); i++) {
				Node pageNode = pagesNode.getChildNodes().item(i);

				if (pageNode.getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}

				mNbTotalPages++;

				Page page = new Page();

				// For each nodes within the page node
				for (int j = 0; j < pageNode.getChildNodes().getLength(); j++) {
					Node pageChild = pageNode.getChildNodes().item(j);

					String nodeName = pageChild.getNodeName();

					if (nodeName.equals(FieldsName.TYPE.getName())) {
						page.setContent(PageContent.valueOf(pageChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.INDICATORS.getName())) {
						for (int k = 0; k < pageChild.getChildNodes().getLength(); k++) {
							Node indicatorNode = pageChild.getChildNodes().item(k);

							if (indicatorNode.getNodeType() != Node.ELEMENT_NODE) {
								continue;
							}

							page.addIndicatorChannel(IndicatorsList.getInstance().getIndexForIndicator(indicatorNode.getTextContent()));
						}
					}
				}

				int pageIndex = Integer.parseInt(pageNode.getAttributes().getNamedItem(FieldsName.INDEX.getName()).getNodeValue());
				DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SEND_PAGE, page.getSerialCommand(pageIndex)));
			}
		}
	}

	/**
	 * Read the "alarms" node from the XML configuration file
	 */
	private void readAlarmsNode() {
		NodeList alarmsNodes = mDocument.getElementsByTagName(FieldsName.ALARMS.getName());

		if (alarmsNodes.getLength() > 0) {
			Node alarmsNode = alarmsNodes.item(0);

			// For each alarm node
			for (int i = 0; i < alarmsNode.getChildNodes().getLength(); i++) {
				Node alarmNode = alarmsNode.getChildNodes().item(i);

				if (alarmNode.getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}

				mNbTotalAlarms++;

				Alarm alarm = new Alarm();

				// For each nodes within the alarm node
				for (int j = 0; j < alarmNode.getChildNodes().getLength(); j++) {
					Node alarmChild = alarmNode.getChildNodes().item(j);

					String nodeName = alarmChild.getNodeName();

					if (nodeName.equals(FieldsName.NAME.getName())) {
						alarm.setName(alarmChild.getTextContent());
					}
					else if (nodeName.equals(FieldsName.CHANNEL1.getName())) {
						alarm.setChannel1(alarmChild.getTextContent());
					}
					else if (nodeName.equals(FieldsName.OPERATOR1.getName())) {
						alarm.setOperator1(AlarmConditionOperator.valueOf(alarmChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.THRESHOLD1.getName())) {
						alarm.setThreshold1(Float.parseFloat(alarmChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.HYSTERESIS1.getName())) {
						alarm.setHysteresis1(Float.parseFloat(alarmChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.ADDITIONAL_CONDITION.getName())) {
						alarm.setAdditionalCondition(AlarmAdditionalCondition.valueOf(alarmChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.CHANNEL2.getName())) {
						alarm.setChannel2(alarmChild.getTextContent());
					}
					else if (nodeName.equals(FieldsName.OPERATOR2.getName())) {
						alarm.setOperator2(AlarmConditionOperator.valueOf(alarmChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.THRESHOLD2.getName())) {
						alarm.setThreshold2(Float.parseFloat(alarmChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.HYSTERESIS2.getName())) {
						alarm.setHysteresis2(Float.parseFloat(alarmChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.SHOW_WARNING_ON_SCREEN.getName())) {
						alarm.setShowWarningOnScreen(Boolean.valueOf(alarmChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.TRIGGER_BUZZER.getName())) {
						alarm.setTriggerBuzzer(Boolean.valueOf(alarmChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.START_DATA_LOGGING.getName())) {
						alarm.setStartDataLogging(Boolean.valueOf(alarmChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.TURN_ON_LEFT_LED.getName())) {
						alarm.setTurnOnLeftLED(Boolean.valueOf(alarmChild.getTextContent()));
					}
					else if (nodeName.equals(FieldsName.TURN_ON_RIGHT_LED.getName())) {
						alarm.setTurnOnRightLED(Boolean.valueOf(alarmChild.getTextContent()));
					}
				}

				int alarmIndex = Integer.parseInt(alarmNode.getAttributes().getNamedItem(FieldsName.INDEX.getName()).getNodeValue());
				DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SEND_ALARM, alarm.getSerialCommand(alarmIndex)));
			}
		}
	}

	/**
	 * Read the "general settings" node from the XML configuration file
	 */
	private void readGeneralSettingsNode() {
		NodeList generalSettingsNodes = mDocument.getElementsByTagName(FieldsName.GENERAL_SETTINGS.getName());

		if (generalSettingsNodes.getLength() > 0) {
			Node generalSettingNode = generalSettingsNodes.item(0);

			GeneralSettings generalSettings = new GeneralSettings();

			for (int i = 0; i < generalSettingNode.getChildNodes().getLength(); i++) {
				Node generalSettingsChild = generalSettingNode.getChildNodes().item(i);

				String nodeName = generalSettingsChild.getNodeName();

				if (nodeName.equals(FieldsName.SPEED_UNIT.getName())) {
					generalSettings.setSpeedUnit(SpeedUnit.valueOf(generalSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.TEMPERATURE_UNIT.getName())) {
					generalSettings.setTemperatureUnit(TemperatureUnit.valueOf(generalSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.SPEED_SOURCE_FOR_DYNAMOMETER.getName())) {
					generalSettings.setSpeedSourceDynamometer(SpeedSourceDynamometer.valueOf(generalSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.REAL_TIME_CLOCK_SYNC.getName())) {
					generalSettings.setRtcSyncWithGps(Boolean.valueOf(generalSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.REAL_TIME_CLOCK_COMPENSATE.getName())) {
					generalSettings.setRtcCompensate(Integer.parseInt(generalSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.CONFIGURATION_SERIAL_PORT_ON_MAIN_CONNECTOR.getName())) {
					generalSettings.setConfigurationSerialPortOnMainConnector(Boolean.valueOf(generalSettingsChild.getTextContent()));
				}
			}

			mGeneralSettingsSent = true;
			DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SEND_GENERAL_SETTINGS, generalSettings.getSerialCommand()));
		}
	}

	/**
	 * Read the "vehicle settings" node from the XML configuration file
	 */
	private void readVehicleSettingsNode() {
		NodeList vehicleSettingsNodes = mDocument.getElementsByTagName(FieldsName.VEHICLE_SETTINGS.getName());

		if (vehicleSettingsNodes.getLength() > 0) {
			Node vehicleSettingsNode = vehicleSettingsNodes.item(0);

			VehicleSettings vehicleSettings = new VehicleSettings();

			for (int i = 0; i < vehicleSettingsNode.getChildNodes().getLength(); i++) {
				Node vehicleSettingsChild = vehicleSettingsNode.getChildNodes().item(i);

				String nodeName = vehicleSettingsChild.getNodeName();

				if (nodeName.equals(FieldsName.VEHICLE_WEIGHT.getName())) {
					vehicleSettings.setVehicleWeight(Integer.parseInt(vehicleSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.COEFFICIENT_OF_DRAG.getName())) {
					vehicleSettings.setCoefficientOfDrag(Double.parseDouble(vehicleSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.COEFFICIENT_OF_ROLLING_RESISTANCE.getName())) {
					vehicleSettings.setCoefficientOfRollingResistance(Double.parseDouble(vehicleSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.FRONTAL_AREA.getName())) {
					vehicleSettings.setFrontalArea(Double.parseDouble(vehicleSettingsChild.getTextContent()));
				}
			}

			mVehicleSettingsSent = true;
			DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SEND_VEHICLE_SETTINGS, vehicleSettings.getSerialCommand()));
		}
	}

	/**
	 * Read the "stepper settings" node from the XML configuration file
	 */
	private void readStepperSettingsNode() {
		NodeList stepperSettingsNodes = mDocument.getElementsByTagName(FieldsName.STEPPER_SETTINGS.getName());

		if (stepperSettingsNodes.getLength() > 0) {
			Node stepperSettingsNode = stepperSettingsNodes.item(0);

			StepperSettings stepperSettings = new StepperSettings();

			for (int i = 0; i < stepperSettingsNode.getChildNodes().getLength(); i++) {
				Node stepperSettingsChild = stepperSettingsNode.getChildNodes().item(i);

				String nodeName = stepperSettingsChild.getNodeName();

				if (nodeName.equals(FieldsName.TOTAL_NUMBER_OF_STEPS.getName())) {
					stepperSettings.setTotalNumberOfSteps(Integer.parseInt(stepperSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.MAXIMUM_SPEED.getName())) {
					stepperSettings.setMaximumSpeed(Integer.parseInt(stepperSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.ACCELERATION.getName())) {
					stepperSettings.setAcceleration(Integer.parseInt(stepperSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.HOMING_DIRECTION.getName())) {
					stepperSettings.setHomingDirection(HomingDirection.valueOf(stepperSettingsChild.getTextContent()));
				}
				else if (nodeName.equals(FieldsName.CHANNEL.getName())) {
					stepperSettings.setChannel(Integer.parseInt(stepperSettingsChild.getTextContent()));
				}
			}

			mStepperSettingsSent = true;
			DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SEND_STEPPER_SETTINGS, stepperSettings.getSerialCommand()));
		}
	}

	/**
	 * Save the configuration as an XML file
	 * 
	 * @param file The file object pointing where the file will be saved
	 */
	public void saveConfigFile(File file) {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;

		try {
			docBuilder = docFactory.newDocumentBuilder();

			mDocument = docBuilder.newDocument();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Element rootElement = mDocument.createElement(FieldsName.CONFIG.getName());
		mDocument.appendChild(rootElement);

		writePagesNode(rootElement);
		writeAlarmsNode(rootElement);
		writeGeneralSettingsNode(rootElement);
		writeVehicleSettingsNode(rootElement);
		writeStepperSettingsNode(rootElement);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;

		try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, ENCODING);
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			DOMSource source = new DOMSource(mDocument);

			Writer out = new OutputStreamWriter(new FileOutputStream(file.getAbsolutePath()), ENCODING);

			StreamResult result = new StreamResult(out);

			transformer.transform(source, result);

			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write the "pages" node in the XML configuration file
	 * 
	 * @param rootElement
	 */
	private void writePagesNode(Element rootElement) {
		Element pagesElement = mDocument.createElement(FieldsName.PAGES.getName());
		rootElement.appendChild(pagesElement);

		for (int i = 0; i < PagesList.getInstance().getList().size(); i++) {
			Page page = PagesList.getInstance().getList().get(i);

			Element pageElement = mDocument.createElement(FieldsName.PAGE.getName());

			// Index attribute
			pageElement.setAttribute(FieldsName.INDEX.getName(), String.valueOf(i));

			// Type node
			Element typeElement = mDocument.createElement(FieldsName.TYPE.getName());
			typeElement.setTextContent(page.getContent().name());
			pageElement.appendChild(typeElement);

			// Indicators node
			Element indicatorsElement = mDocument.createElement(FieldsName.INDICATORS.getName());

			for (int j = 0; j < page.getIndicatorsChannels().size(); j++) {
				Element indicatorElement = mDocument.createElement(FieldsName.INDICATOR.getName());
				int indicatorIndex = page.getIndicatorsChannels().get(j);

				if (indicatorIndex > -1) {
					indicatorElement.setTextContent(IndicatorsList.getInstance().getList()[indicatorIndex]);
					indicatorsElement.appendChild(indicatorElement);
				}
			}
			pageElement.appendChild(indicatorsElement);

			pagesElement.appendChild(pageElement);
		}
	}

	/**
	 * Write the "alarms" node in the XML configuration file
	 * 
	 * @param rootElement
	 */
	private void writeAlarmsNode(Element rootElement) {
		Element alarmsElement = mDocument.createElement(FieldsName.ALARMS.getName());
		rootElement.appendChild(alarmsElement);

		for (int i = 0; i < AlarmsList.getInstance().getMax(); i++) {
			Alarm alarm = null;
			if (i < AlarmsList.getInstance().getList().size()) {
				alarm = AlarmsList.getInstance().getList().get(i);
			}

			Element alarmElement = mDocument.createElement(FieldsName.ALARM.getName());

			alarmElement.setAttribute("index", String.valueOf(i));

			if (alarm != null) {
				Element nameElement = mDocument.createElement(FieldsName.NAME.getName());
				nameElement.setTextContent(alarm.getName());
				alarmElement.appendChild(nameElement);

				// First condition
				Element channel1Element = mDocument.createElement(FieldsName.CHANNEL1.getName());
				channel1Element.setTextContent(alarm.getChannel1());
				alarmElement.appendChild(channel1Element);

				Element operator1Element = mDocument.createElement(FieldsName.OPERATOR1.getName());
				operator1Element.setTextContent(alarm.getOperator1().name());
				alarmElement.appendChild(operator1Element);

				Element thresold1Element = mDocument.createElement(FieldsName.THRESHOLD1.getName());
				thresold1Element.setTextContent(String.valueOf(alarm.getThreshold1()));
				alarmElement.appendChild(thresold1Element);

				Element hysteresis1Element = mDocument.createElement(FieldsName.HYSTERESIS1.getName());
				hysteresis1Element.setTextContent(String.valueOf(alarm.getHysteresis1()));
				alarmElement.appendChild(hysteresis1Element);

				Element additionalConditionElement = mDocument.createElement(FieldsName.ADDITIONAL_CONDITION.getName());
				additionalConditionElement.setTextContent(alarm.getAdditionalCondition().name());
				alarmElement.appendChild(additionalConditionElement);

				// Second condition
				Element channel2Element = mDocument.createElement(FieldsName.CHANNEL2.getName());
				channel2Element.setTextContent(alarm.getChannel2());
				alarmElement.appendChild(channel2Element);

				Element operator2Element = mDocument.createElement(FieldsName.OPERATOR2.getName());
				operator2Element.setTextContent(alarm.getOperator2().name());
				alarmElement.appendChild(operator2Element);

				Element thresold2Element = mDocument.createElement(FieldsName.THRESHOLD2.getName());
				thresold2Element.setTextContent(String.valueOf(alarm.getThreshold2()));
				alarmElement.appendChild(thresold2Element);

				Element hysteresis2Element = mDocument.createElement(FieldsName.HYSTERESIS2.getName());
				hysteresis2Element.setTextContent(String.valueOf(alarm.getHysteresis2()));
				alarmElement.appendChild(hysteresis2Element);

				// Reaction
				Element showWarningOnScreenElement = mDocument.createElement(FieldsName.SHOW_WARNING_ON_SCREEN.getName());
				showWarningOnScreenElement.setTextContent(String.valueOf(alarm.isShowWarningOnScreen()));
				alarmElement.appendChild(showWarningOnScreenElement);

				Element triggerBuzzerElement = mDocument.createElement(FieldsName.TRIGGER_BUZZER.getName());
				triggerBuzzerElement.setTextContent(String.valueOf(alarm.isTriggerBuzzer()));
				alarmElement.appendChild(triggerBuzzerElement);

				Element startDataLoggingElement = mDocument.createElement(FieldsName.START_DATA_LOGGING.getName());
				startDataLoggingElement.setTextContent(String.valueOf(alarm.isStartDataLogging()));
				alarmElement.appendChild(startDataLoggingElement);

				Element turnOnLeftLEDElement = mDocument.createElement(FieldsName.TURN_ON_LEFT_LED.getName());
				turnOnLeftLEDElement.setTextContent(String.valueOf(alarm.isTurnOnLeftLED()));
				alarmElement.appendChild(turnOnLeftLEDElement);

				Element turnOnRightLEDElement = mDocument.createElement(FieldsName.TURN_ON_RIGHT_LED.getName());
				turnOnRightLEDElement.setTextContent(String.valueOf(alarm.isTurnOnRightLED()));
				alarmElement.appendChild(turnOnRightLEDElement);
			}

			alarmsElement.appendChild(alarmElement);
		}
	}

	/**
	 * Write the "generalSettings" node in the XML configuration file
	 * 
	 * @param rootElement
	 */
	private void writeGeneralSettingsNode(Element rootElement) {
		GeneralSettings generalSettings = SettingsList.getInstance().getGeneralSettings();

		Element generalSettingsElement = mDocument.createElement(FieldsName.GENERAL_SETTINGS.getName());

		Element speedUnitElement = mDocument.createElement(FieldsName.SPEED_UNIT.getName());
		speedUnitElement.setTextContent(String.valueOf(generalSettings.getSpeedUnit()));
		generalSettingsElement.appendChild(speedUnitElement);

		Element temperatureUnitElement = mDocument.createElement(FieldsName.TEMPERATURE_UNIT.getName());
		temperatureUnitElement.setTextContent(String.valueOf(generalSettings.getTemperatureUnit()));
		generalSettingsElement.appendChild(temperatureUnitElement);

		Element speedSourceForDynamometerElement = mDocument.createElement(FieldsName.SPEED_SOURCE_FOR_DYNAMOMETER.getName());
		speedSourceForDynamometerElement.setTextContent(String.valueOf(generalSettings.getSpeedSourceDynamometer()));
		generalSettingsElement.appendChild(speedSourceForDynamometerElement);

		Element realTimeClockSyncElement = mDocument.createElement(FieldsName.REAL_TIME_CLOCK_SYNC.getName());
		realTimeClockSyncElement.setTextContent(String.valueOf(generalSettings.isRtcSyncWithGps()));
		generalSettingsElement.appendChild(realTimeClockSyncElement);

		Element realTimeClockCompensateElement = mDocument.createElement(FieldsName.REAL_TIME_CLOCK_COMPENSATE.getName());
		realTimeClockCompensateElement.setTextContent(String.valueOf(generalSettings.getRtcCompensate()));
		generalSettingsElement.appendChild(realTimeClockCompensateElement);

		Element configurationSerialPortOnMainConnectorElement = mDocument.createElement(FieldsName.CONFIGURATION_SERIAL_PORT_ON_MAIN_CONNECTOR.getName());
		configurationSerialPortOnMainConnectorElement.setTextContent(String.valueOf(generalSettings.isConfigurationSerialPortOnMainConnector()));
		generalSettingsElement.appendChild(configurationSerialPortOnMainConnectorElement);

		rootElement.appendChild(generalSettingsElement);
	}

	/**
	 * Write the "vehicleSettings" node in the XML configuration file
	 * 
	 * @param rootElement
	 */
	private void writeVehicleSettingsNode(Element rootElement) {
		VehicleSettings vehicleSettings = SettingsList.getInstance().getVehicleSettings();

		Element vehicleSettingsElement = mDocument.createElement(FieldsName.VEHICLE_SETTINGS.getName());

		Element vehicleWeightElement = mDocument.createElement(FieldsName.VEHICLE_WEIGHT.getName());
		vehicleWeightElement.setTextContent(String.valueOf(vehicleSettings.getVehicleWeight()));
		vehicleSettingsElement.appendChild(vehicleWeightElement);

		Element coefficientOfDragElement = mDocument.createElement(FieldsName.COEFFICIENT_OF_DRAG.getName());
		coefficientOfDragElement.setTextContent(String.valueOf(vehicleSettings.getCoefficientOfDrag()));
		vehicleSettingsElement.appendChild(coefficientOfDragElement);

		Element coefficientOfRollingResistanceElement = mDocument.createElement(FieldsName.COEFFICIENT_OF_ROLLING_RESISTANCE.getName());
		coefficientOfRollingResistanceElement.setTextContent(String.valueOf(vehicleSettings.getCoefficientOfRollingResistance()));
		vehicleSettingsElement.appendChild(coefficientOfRollingResistanceElement);

		Element frontalAreaElement = mDocument.createElement(FieldsName.FRONTAL_AREA.getName());
		frontalAreaElement.setTextContent(String.valueOf(vehicleSettings.getFrontalArea()));
		vehicleSettingsElement.appendChild(frontalAreaElement);

		rootElement.appendChild(vehicleSettingsElement);
	}

	/**
	 * Write the "stepperSettings" node in the XML configuration file
	 * 
	 * @param rootElement
	 */
	private void writeStepperSettingsNode(Element rootElement) {
		StepperSettings stepperSettings = SettingsList.getInstance().getStepperSettings();

		Element stepperSettingsElement = mDocument.createElement(FieldsName.STEPPER_SETTINGS.getName());

		Element totalNumberOfStepsElement = mDocument.createElement(FieldsName.TOTAL_NUMBER_OF_STEPS.getName());
		totalNumberOfStepsElement.setTextContent(String.valueOf(stepperSettings.getTotalNumberOfSteps()));
		stepperSettingsElement.appendChild(totalNumberOfStepsElement);

		Element maximumSpeedElement = mDocument.createElement(FieldsName.MAXIMUM_SPEED.getName());
		maximumSpeedElement.setTextContent(String.valueOf(stepperSettings.getMaximumSpeed()));
		stepperSettingsElement.appendChild(maximumSpeedElement);

		Element accelerationElement = mDocument.createElement(FieldsName.ACCELERATION.getName());
		accelerationElement.setTextContent(String.valueOf(stepperSettings.getAcceleration()));
		stepperSettingsElement.appendChild(accelerationElement);

		Element homingDirectionElement = mDocument.createElement(FieldsName.HOMING_DIRECTION.getName());
		homingDirectionElement.setTextContent(String.valueOf(stepperSettings.getHomingDirection()));
		stepperSettingsElement.appendChild(homingDirectionElement);

		Element channelElement = mDocument.createElement(FieldsName.CHANNEL.getName());
		channelElement.setTextContent(String.valueOf(stepperSettings.getChannel()));
		stepperSettingsElement.appendChild(channelElement);

		rootElement.appendChild(stepperSettingsElement);
	}

	/**
	 * Check if the serial command is one of the one we sent to send everything to the gauge
	 * 
	 * @param serialCommand The serial command that was sent
	 * @return true if the serial command is one we sent, false otherwise
	 */
	private boolean isConfigCommand(SerialCommand serialCommand) {
		return serialCommand == SerialCommand.SEND_PAGE || serialCommand == SerialCommand.SEND_ALARM || serialCommand == SerialCommand.SEND_GENERAL_SETTINGS || serialCommand == SerialCommand.SEND_VEHICLE_SETTINGS;
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		if (serialCommand == SerialCommand.SEND_PAGE) {
			mNbPagesSent++;

			mProgressMonitor.setProgressMessage("Loading page " + mNbPagesSent + " of " + mNbTotalPages);
		}
		else if (serialCommand == SerialCommand.SEND_ALARM) {
			mNbAlarmsSent++;

			mProgressMonitor.setProgressMessage("Loading alarm " + mNbAlarmsSent + " of " + mNbTotalAlarms);
		}
		else if (serialCommand == SerialCommand.SEND_GENERAL_SETTINGS) {
			mGeneralSettingsSent = true;

			mProgressMonitor.setProgressMessage("Loading general settings");
		}
		else if (serialCommand == SerialCommand.SEND_VEHICLE_SETTINGS) {
			mVehicleSettingsSent = true;

			mProgressMonitor.setProgressMessage("Loading vehicle settings");
		}
		else if (serialCommand == SerialCommand.SEND_STEPPER_SETTINGS) {
			mStepperSettingsSent = true;

			mProgressMonitor.setProgressMessage("Loading stepper settings");
		}

		if (isConfigCommand(serialCommand)) {
			int done = (mNbPagesSent + mNbAlarmsSent) + (mGeneralSettingsSent ? 1 : 0) + (mVehicleSettingsSent ? 1 : 0) + (mStepperSettingsSent ? 1 : 0);
			int total = (mNbTotalPages + mNbTotalAlarms) + 2;

			mProgressMonitor.setProgress((int) (done / (float) total * 100));
		}
	}
}
