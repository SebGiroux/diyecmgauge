package com.sgiroux.diyecmgauge.config;

public enum FieldsName {
	//@formatter:off
	CONFIG("config"),
	PAGES("pages"),
	PAGE("page"),
	INDEX("index"),
	TYPE("type"),
	INDICATORS("indicators"),
	INDICATOR("indicator"),

	ALARMS("alarms"),
	ALARM("alarm"),
	NAME("name"),
	CHANNEL1("channel1"),
	OPERATOR1("operator1"),
	THRESHOLD1("threshold1"),
	HYSTERESIS1("hysteresis1"),
	ADDITIONAL_CONDITION("additionalCondition"),
	CHANNEL2("channel2"),
	OPERATOR2("operator2"),
	THRESHOLD2("threshold2"),
	HYSTERESIS2("hysteresis2"),
	SHOW_WARNING_ON_SCREEN("showWarningOnScreen"),
	TRIGGER_BUZZER("triggerBuzzer"),
	START_DATA_LOGGING("startDataLogging"),
	TURN_ON_LEFT_LED("turnOnLeftLED"),
	TURN_ON_RIGHT_LED("turnOnRightLED"),

	GENERAL_SETTINGS("generalSettings"),
	SPEED_UNIT("speedUnit"),
	TEMPERATURE_UNIT("temperatureUnit"),
	SPEED_SOURCE_FOR_DYNAMOMETER("speedSourceForDynamometer"),
	REAL_TIME_CLOCK_SYNC("realTimeClockSync"),
	REAL_TIME_CLOCK_COMPENSATE("realTimeClockCompensate"),
	CONFIGURATION_SERIAL_PORT_ON_MAIN_CONNECTOR("configurationSerialPortOnMainConnector"),

	VEHICLE_SETTINGS("vehicleSettings"),
	VEHICLE_WEIGHT("vehicleWeight"),
	COEFFICIENT_OF_DRAG("coefficientOfDrag"),
	COEFFICIENT_OF_ROLLING_RESISTANCE("coefficientOfRollingResistance"),
	FRONTAL_AREA("frontalArea"),

	STEPPER_SETTINGS("stepperSettings"),
	TOTAL_NUMBER_OF_STEPS("totalNumberOfSteps"),
	MAXIMUM_SPEED("maximumSpeed"),
	ACCELERATION("acceleration"),
	HOMING_DIRECTION("homingDirection"),
	CHANNEL("channel");
	//@formatter:on

	private String mName;

	private FieldsName(String name) {
		mName = name;
	}

	public String getName() {
		return mName;
	}
}