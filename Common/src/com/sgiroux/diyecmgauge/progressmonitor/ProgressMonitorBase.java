package com.sgiroux.diyecmgauge.progressmonitor;

/**
 * Progress monitor base class that both the Android and desktop applications implementations will extends
 * 
 * @author Seb
 * 
 */
public abstract class ProgressMonitorBase {
	protected ProgressMonitorCancelListener mCancelListener;
	protected ProgressMonitorCompletedListener mCompletedListener;

	public void addCompletedListener(ProgressMonitorCompletedListener completedListener) {
		mCompletedListener = completedListener;
	}

	public void addCancelListener(ProgressMonitorCancelListener cancelListener) {
		mCancelListener = cancelListener;
	}

	public abstract void setParent(Object parent);

	public abstract void setProgressMessage(String progressMessage);

	public abstract void setProgress(int progress);

	public abstract void setDescription(String description);

	public abstract void setVisible(boolean visible);
}
