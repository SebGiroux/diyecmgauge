package com.sgiroux.diyecmgauge.progressmonitor;

/**
 * Interface defining the completion listener for a progress monitor
 * 
 * @author Seb
 *
 */
public interface ProgressMonitorCompletedListener {
	public void onProgressMonitorCompleted();
}
