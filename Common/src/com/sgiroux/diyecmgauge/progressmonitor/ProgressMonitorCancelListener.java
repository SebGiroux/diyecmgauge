package com.sgiroux.diyecmgauge.progressmonitor;

/**
 * Interface defining the cancel listener for a progress monitor
 * 
 * @author Seb
 *
 */
public interface ProgressMonitorCancelListener {
	public void onProgressMonitorCancel();
}