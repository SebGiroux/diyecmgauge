package com.sgiroux.diyecmgauge.controller;

import java.util.ArrayList;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.data.Page;
import com.sgiroux.diyecmgauge.data.PageContent;
import com.sgiroux.diyecmgauge.list.PagesList;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;

public class PagesController {
	public enum PageDirection {
		PREVIOUS("0"), NEXT("1");

		private String mText;

		private PageDirection(String text) {
			mText = text;
		}

		public String getText() {
			return mText;
		}
	}

	/**
	 * @return The list of display pages configured in the gauge
	 */
	public ArrayList<Page> getPagesList() {
		return PagesList.getInstance().getList();
	}

	/**
	 * @return The index of the first unused display page
	 */
	public int getIndexOfFirstUnusedPage() {
		int i;

		for (i = 0; i < getPagesList().size(); i++) {
			if (getPagesList().get(i).getContent() == PageContent.UNUSED) {
				break;
			}
		}

		return i;
	}

	/**
	 * Send a display page configuration serial command to the gauge
	 *
	 * @param pageIndex The page index of the page to send
	 */
	public void sendCurrentPageConfigurationToGauge(int pageIndex) {
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SEND_PAGE, getPagesList().get(pageIndex).getSerialCommand(pageIndex)));
	}

	/**
	 * Send a change display page serial command to the gauge
	 * 
	 * @param direction The direction to go, previous or next
	 */
	public void sendChangePage(PageDirection direction) {
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.CHANGE_DISPLAY_PAGE, direction.getText()));
	}

}
