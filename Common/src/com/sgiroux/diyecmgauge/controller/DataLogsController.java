package com.sgiroux.diyecmgauge.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.data.DataLogRow;
import com.sgiroux.diyecmgauge.data.DataLoggingStatus;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorBase;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialUtils;
import com.sgiroux.diyecmgauge.utils.StringUtils;

/**
 * Controller used by the browse data logs activity (Android) and the browse data logs dialog (desktop)
 * 
 * @author Seb
 * 
 */
public class DataLogsController {
	private static final int DATA_LOG_CHUNK_SIZE = 8192; // in bytes

	private FileOutputStream mDatalogDownloadFileStream;
	private long mCurrentDataLogDownloadedSize;
	private String mCurrentDataLogFileName;
	private long mCurrentDataLogFileSize;
	private long mCurrentDataLogDownloadStartTime;
	private long mCurrentDatalogDownloadLastRemainingUpdate;

	private int mTotalDatalogs = 0;
	private long mTotalDataLogsFileSize = 0; // in bytes

	private int mChunkSize = DATA_LOG_CHUNK_SIZE;

	/**
	 * Take various data logs statistics information and format it in a way to display it to the user
	 * 
	 * @param response The response we got from the gauge for the data logs statistics command
	 * @return The data logs statistics string to be displayed
	 */
	public String getDataLogsStats(String textResponse) {
		final String values[] = SerialUtils.splitByValues(textResponse);

		final long blocksPerCluster = Long.parseLong(values[0]);
		final long clusterCount = Long.parseLong(values[1]);

		final String dataLogsFileSize = StringUtils.humanReadableByteCount(mTotalDataLogsFileSize);
		final String totalCardSize = StringUtils.humanReadableByteCount(blocksPerCluster * clusterCount * 512);

		// Make sure we don't divide by zero
		int percent = 0;
		if (blocksPerCluster + clusterCount > 0) {
			percent = (int) (mTotalDataLogsFileSize / (blocksPerCluster * clusterCount * 512) * 100);
		}

		return String.format("%s datalogs (%s on %s used - %s%%)", mTotalDatalogs, dataLogsFileSize, totalCardSize, percent);
	}

	/**
	 * Toggle the data logging. If we're currently logging, logging will be stopped. If we're not logging, logging will be started.
	 * 
	 * @param dataLoggingStatus The current data logging status
	 */
	public void toggleDataLogging(DataLoggingStatus dataLoggingStatus) {
		switch (dataLoggingStatus) {
			case DATA_LOGGING:
				DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.DATA_LOGGING_STOP));
				break;

			case NOT_DATA_LOGGING:
				DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.DATA_LOGGING_START));
				break;

			default:
				break;
		}
	}

	/**
	 * Set the chunk size to be used when downloading a data log
	 * 
	 * @param chunkSize The chunk size of each data log download request
	 */
	public void setChunkSize(int chunkSize) {
		mChunkSize = chunkSize;
	}

	/**
	 * Send a request to get a chunk of a data log data for the specified offset and size
	 * 
	 * @param fileName The file name of the data log
	 * @param offset The offset of the data log file to start from
	 * @param size The size of the chunk to get
	 */
	public void sendGetDataLogChunk(String fileName, long offset, int size) {
		StringBuffer arguments = new StringBuffer();

		arguments.append(fileName);
		arguments.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		arguments.append(offset);
		arguments.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		arguments.append(size);

		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.DATA_LOGS_GET_FILE, arguments.toString()));
	}

	/**
	 * Called when we receive a chunk of bytes from a data log
	 * 
	 * @param data The data log data from the gauge
	 * @param progressMonitor The progress monitor to be updated
	 */
	public void getDownloadDataLogChunkResponse(byte[] data, ProgressMonitorBase progressMonitor) {
		mCurrentDataLogDownloadedSize += data.length;

		if (mDatalogDownloadFileStream != null) {
			try {
				mDatalogDownloadFileStream.write(data);
				mDatalogDownloadFileStream.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}

			int downloadPercent = (int) ((float) mCurrentDataLogDownloadedSize / mCurrentDataLogFileSize * 100);

			long currentTime = System.currentTimeMillis();
			long elapsedMillis = (currentTime - mCurrentDataLogDownloadStartTime);
			int elapsedSeconds = (int) elapsedMillis / 1000;

			long totalMillis = (long) (elapsedMillis / (((double) downloadPercent) / 100.0));
			long remainingMillis = totalMillis - elapsedMillis;
			int remainingSeconds = (int) remainingMillis / 1000;

			/*
			 * Update the status string. If task is less than 1% complete or started less then 1 seconds ago, assume that the estimate is inaccurate.
			 * Also, don't update more often then every second
			 */
			if (downloadPercent >= 1 && elapsedMillis > 1000 && currentTime - mCurrentDatalogDownloadLastRemainingUpdate > 1000) {
				long bytesSecond = 0;
				if (elapsedSeconds > 0) {
					bytesSecond = mCurrentDataLogDownloadedSize / elapsedSeconds;
				}

				progressMonitor.setProgressMessage(String.format("%s / second - About %s second(s) remaining", StringUtils.humanReadableByteCount(bytesSecond), remainingSeconds));

				mCurrentDatalogDownloadLastRemainingUpdate = System.currentTimeMillis();
			}

			progressMonitor.setProgress(downloadPercent);

			// Send the request for the next chunk of data
			int chunkSize = getNextDataLogChunkSize();
			if (chunkSize > 0) {
				sendGetDataLogChunk(mCurrentDataLogFileName, mCurrentDataLogDownloadedSize, chunkSize);
			}
		}
	}

	/**
	 * @return Return what the next chunk size should be for the next get data log chunk request
	 */
	public int getNextDataLogChunkSize() {
		return Math.min((int) (mCurrentDataLogFileSize - mCurrentDataLogDownloadedSize), mChunkSize);
	}

	/**
	 * Get the list of data logs currently on the SD card of the gauge
	 * 
	 * @param textResponse The response we got from the gauge
	 * @return An array list of data log objects
	 */
	public ArrayList<DataLogRow> getDataLogListing(String textResponse) {
		ArrayList<DataLogRow> dataLogRows = new ArrayList<DataLogRow>();

		mTotalDataLogsFileSize = 0;
		mTotalDatalogs = 0;

		if (!textResponse.equals("")) {
			String rows[] = SerialUtils.splitByLines(textResponse);

			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			for (String row : rows) {
				String columns[] = SerialUtils.splitByValues(row);

				String fileName = parseFileName(columns[0]);
				Date date = null;
				try {
					date = dt.parse(columns[1] + " " + columns[2]);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				long fileBytes = Long.parseLong(columns[3]);
				String fileSize = StringUtils.humanReadableByteCount(fileBytes);
				mTotalDataLogsFileSize += fileBytes;
				mTotalDatalogs++;

				DataLogRow dataLogRow = new DataLogRow();
				dataLogRow.setDataLogName(fileName);
				dataLogRow.setDataLogSize(fileBytes);
				dataLogRow.setDataLogDate(dt.format(date));
				dataLogRow.setDataLogStats(String.format("Size: %s / Date: %s", fileSize, date));
				dataLogRows.add(dataLogRow);
			}
		}

		return dataLogRows;
	}

	/**
	 * Parse a file name we got from the gauge. The file name and extension are separated by a space. We change that for a dot.
	 * 
	 * @param fileName The name of the file to be parsed
	 * @return The new properly formatted file name
	 */
	private String parseFileName(String fileName) {
		return fileName.replace(" ", ".");
	}

	/**
	 * Verify the response we got from a delete data log command to the gauge
	 * 
	 * @param textResponse The response we got from the gauge
	 */
	public void deleteDataLog(String textResponse) {
		if (SerialUtils.isOKResponse(textResponse)) {
			// Successfully deleted the file, refresh the data logs list
			DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.DATA_LOGS_DIRECTORY_LISTING));
		}
		else {
			// TODO handle error
		}
	}

	/**
	 * Start the download of a data log file from the SD card on the gauge
	 * 
	 * @param file The file object where the data log will be downloaded
	 * @param fileSize The size of the file (in bytes) to be downloaded
	 */
	public void startDataLogDownload(File file, long fileSize) {
		try {
			mDatalogDownloadFileStream = new FileOutputStream(file);
			if (!file.exists()) {
				boolean success = file.createNewFile();
				if (!success) {
					// TODO handle error
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		String fileName = file.getName();

		mCurrentDataLogDownloadStartTime = System.currentTimeMillis();
		mCurrentDataLogDownloadedSize = 0;
		mCurrentDataLogFileName = fileName;
		mCurrentDataLogFileSize = fileSize;

		sendGetDataLogChunk(fileName, 0, getNextDataLogChunkSize());
	}

	public String getCurrentDataLogFileName() {
		return mCurrentDataLogFileName;
	}

	/**
	 * Close the data log file stream (after a download completion or a cancellation of download)
	 */
	public void closeDataLog() {
		try {
			mDatalogDownloadFileStream.close();
			mDatalogDownloadFileStream = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
