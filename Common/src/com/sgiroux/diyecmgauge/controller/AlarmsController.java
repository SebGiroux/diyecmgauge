package com.sgiroux.diyecmgauge.controller;

import java.util.ArrayList;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.data.Alarm;
import com.sgiroux.diyecmgauge.list.AlarmsList;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;

/**
 * Controller used by the alarms activity (Android) and the alarms dialog (desktop)
 * 
 * @author Seb
 * 
 */
public class AlarmsController {
	public static final int ALARM_NAME_MAX_CHARS = 15;

	/**
	 * @return An array list of all the alarms object currently configured in the gauge
	 */
	public ArrayList<Alarm> getAlarmsList() {
		return AlarmsList.getInstance().getList();
	}

	/**
	 * 
	 * @param alarmIndex
	 */
	public void sendCurrentAlarmConfigurationToGauge(int alarmIndex) {
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SEND_ALARM, getAlarmsList().get(alarmIndex).getSerialCommand(alarmIndex)));
	}
}
