package com.sgiroux.diyecmgauge.controller;

import java.util.Calendar;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Controller used by the RTC activity (Android) and the RTC dialog (desktop)
 * 
 * @author Seb
 * 
 */
public class RTCController {
	/**
	 * Send the new date / time for the gauge's RTC
	 */
	public void sendRTCDateTimeToGauge() {
		Calendar cal = Calendar.getInstance();
		cal.get(Calendar.YEAR);

		StringBuffer arguments = new StringBuffer();

		arguments.append(cal.get(Calendar.YEAR));
		arguments.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		arguments.append(cal.get(Calendar.MONTH) + 1);
		arguments.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		arguments.append(cal.get(Calendar.DAY_OF_MONTH));
		arguments.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		arguments.append(cal.get(Calendar.HOUR_OF_DAY));
		arguments.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		arguments.append(cal.get(Calendar.MINUTE));
		arguments.append(SerialUtils.SERIAL_RESPONSE_SUB_VALUES_SEPARATOR);
		arguments.append(cal.get(Calendar.SECOND));

		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SEND_RTC_DATE_TIME, arguments.toString()));
	}

	/**
	 * Parse the RTC response from the gauge and build a calendar object out of it
	 * 
	 * @param response The array of bytes received from the gauge
	 * @return A calendar instance with the date / time from the gauge
	 */
	public Calendar parseRTCDataFromGauge(byte[] response) {
		final String textResponse = SerialUtils.getTextResponse(response);
		final String values[] = SerialUtils.splitByValues(textResponse);

		int hour = Integer.parseInt(values[0]);
		int minute = Integer.parseInt(values[1]);
		int second = Integer.parseInt(values[2]);

		int day = Integer.parseInt(values[3]);
		int month = Integer.parseInt(values[4]);
		int year = Integer.parseInt(values[5]);

		Calendar cal = Calendar.getInstance();

		// Date
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.DAY_OF_MONTH, day);

		// Time
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, second);

		return cal;
	}
}
