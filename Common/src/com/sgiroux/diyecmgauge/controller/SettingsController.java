package com.sgiroux.diyecmgauge.controller;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.data.DimensionUnit;
import com.sgiroux.diyecmgauge.data.GeneralSettings;
import com.sgiroux.diyecmgauge.data.HomingDirection;
import com.sgiroux.diyecmgauge.data.StepperSettings;
import com.sgiroux.diyecmgauge.data.VehicleSettings;
import com.sgiroux.diyecmgauge.data.WeightUnit;
import com.sgiroux.diyecmgauge.list.SettingsList;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.utils.UnitUtils;

/**
 * Controller used by the Settings activity (Android) and the Settings dialog (desktop)
 * 
 * @author Seb
 * 
 */
public class SettingsController {
	/**
	 * Build a VehicleSettings object from various form fields and returns it
	 * 
	 * @param fVehicleWeight The vehicle weight from a form field
	 * @param fCoefficientOfDrag The coefficient of drag from a form field
	 * @param fCoefficientOfRollingResistance The coefficient of rolling resistance from a form field
	 * @param fFrontalArea The frontal area from a form field
	 * @param fVehicleWeightUnit The vehicle weight unit from a form field
	 * @param fFrontalAreaUnit The frontal area unit from a form field
	 * 
	 * @return A VehicleSettings object build from the fields passed in parameter
	 */
	public VehicleSettings getVehicleSettingsFromFields(String fVehicleWeight, String fCoefficientOfDrag, String fCoefficientOfRollingResistance, String fFrontalArea, String fVehicleWeightUnit, String fFrontalAreaUnit) {
		VehicleSettings vehicleSettings = new VehicleSettings();

		int vehicleWeight = 0;
		try {
			vehicleWeight = Integer.parseInt(fVehicleWeight);

			if (fVehicleWeightUnit.equals(WeightUnit.KG.getTitle())) {
				vehicleWeight = (int) UnitUtils.getLbsFromKg(vehicleWeight);
			}
		} catch (NumberFormatException e) {
		}

		double coefficientOfDrag = 0;
		try {
			coefficientOfDrag = Double.parseDouble(fCoefficientOfDrag);
		} catch (NumberFormatException e) {
		}

		double coefficientOfRollingResistance = 0;
		try {
			coefficientOfRollingResistance = Double.parseDouble(fCoefficientOfRollingResistance);
		} catch (NumberFormatException e) {
		}

		double frontalArea = 0;
		try {
			frontalArea = Double.parseDouble(fFrontalArea);

			if (fFrontalAreaUnit.equals(DimensionUnit.METER_SQUARE.getTitle())) {
				frontalArea = UnitUtils.getSqFtFromSqMt(frontalArea);
			}
		} catch (NumberFormatException e) {
		}

		vehicleSettings.setVehicleWeight(vehicleWeight);
		vehicleSettings.setCoefficientOfDrag(coefficientOfDrag);
		vehicleSettings.setCoefficientOfRollingResistance(coefficientOfRollingResistance);
		vehicleSettings.setFrontalArea(frontalArea);

		return vehicleSettings;
	}

	/**
	 * Build a StepperSettings object from various form fields and returns it
	 * 
	 * @param fTotalNumberOfSteps The total number of steps from a form field
	 * @param fMaximumSpeed The maximum speed in steps per second from a form field
	 * @param fAcceleration The acceleration in steps per second� from a form field
	 * @param homingDirectionIndex The index of homing direction from a form field
	 * @param channel The index of indicator channel from a form field
	 * 
	 * @return A StepperSettings object build from the fields passed in parameter
	 */
	public StepperSettings getStepperSettingsFromFields(String fTotalNumberOfSteps, String fMaximumSpeed, String fAcceleration, int homingDirectionIndex, int channel) {
		StepperSettings stepperSettings = new StepperSettings();

		int totalNumberOfSteps = 0;
		try {
			totalNumberOfSteps = Integer.parseInt(fTotalNumberOfSteps);
		} catch (NumberFormatException e) {
		}
		stepperSettings.setTotalNumberOfSteps(totalNumberOfSteps);

		int maximumSpeed = 0;
		try {
			maximumSpeed = Integer.parseInt(fMaximumSpeed);
		} catch (NumberFormatException e) {
		}
		stepperSettings.setMaximumSpeed(maximumSpeed);

		int acceleration = 0;
		try {
			acceleration = Integer.parseInt(fAcceleration);
		} catch (NumberFormatException e) {
		}
		stepperSettings.setAcceleration(acceleration);

		stepperSettings.setHomingDirection(HomingDirection.values()[homingDirectionIndex]);
		stepperSettings.setChannel(channel);

		return stepperSettings;
	}

	/**
	 * @return The current general settings object we got from the gauge
	 */
	public GeneralSettings getGeneralSettings() {
		return SettingsList.getInstance().getGeneralSettings();
	}

	/**
	 * Send the specified general settings to the gauge
	 * 
	 * @param generalSettings The general settings object to be sent to the gauge
	 */
	public void sendGeneralSettingsConfigurationToGauge(GeneralSettings generalSettings) {
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SEND_GENERAL_SETTINGS, generalSettings.getSerialCommand()));
	}

	/**
	 * @return The current vehicle settings object we got from the gauge
	 */
	public VehicleSettings getVehicleSettings() {
		return SettingsList.getInstance().getVehicleSettings();
	}

	/**
	 * Send the specified vehicle settings to the gauge
	 * 
	 * @param vehicleSettings The vehicle settings object to be sent to the gauge
	 */
	public void sendVehicleSettingsConfigurationToGauge(VehicleSettings vehicleSettings) {
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SEND_VEHICLE_SETTINGS, vehicleSettings.getSerialCommand()));
	}

	/**
	 * @return The current stepper settings object we got from the gauge
	 */
	public StepperSettings getStepperSettings() {
		return SettingsList.getInstance().getStepperSettings();
	}

	/**
	 * Send the specified stepper settings to the gauge
	 * 
	 * @param stepperSettings The vehicle settings object to be sent to the gauge
	 */
	public void sendStepperSettingsConfigurationToGauge(StepperSettings stepperSettings) {
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SEND_STEPPER_SETTINGS, stepperSettings.getSerialCommand()));
	}

	/**
	 * Save the current general, vehicle settings and stepper settings based on the fields content of the dialog
	 * 
	 * @param generalSettings The new general settings object to be saved
	 * @param vehicleSettings The new vehicle settings object to be saved
	 * @param stepperSettings The new stepper settings object to be saved
	 */
	public void saveCurrentSettings(GeneralSettings generalSettings, VehicleSettings vehicleSettings, StepperSettings stepperSettings) {
		// If general settings changed, send them to the gauge
		if (!getGeneralSettings().equals(generalSettings)) {
			SettingsList.getInstance().setGeneralSettings(generalSettings);
			sendGeneralSettingsConfigurationToGauge(generalSettings);
		}

		// If vehicle settings changed, send them to the gauge
		if (!getVehicleSettings().equals(vehicleSettings)) {
			SettingsList.getInstance().setVehicleSettings(vehicleSettings);
			sendVehicleSettingsConfigurationToGauge(vehicleSettings);
		}

		// If stepper settings changed, send them to the gauge
		if (!getStepperSettings().equals(stepperSettings)) {
			SettingsList.getInstance().setStepperSettings(stepperSettings);
			sendStepperSettingsConfigurationToGauge(stepperSettings);
		}
	}
}
