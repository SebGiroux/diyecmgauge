package com.sgiroux.diyecmgauge.progressmonitor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;

/**
 * Wrapper for progress dialog to allow for code sharing between the Android application and the desktop application
 * 
 * @author Seb
 * 
 */
public class ProgressMonitorDialog extends ProgressMonitorBase {
	private ProgressDialog mDialog;

	@Override
	public void setParent(Object parent) {
		mDialog = new ProgressDialog((Activity) parent);
		mDialog.setCancelable(true);
		mDialog.setIndeterminate(false);
		mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		// A message need to be shown before showing the dialog otherwise setMessage() will never work
		// See http://code.google.com/p/android/issues/detail?id=3366
		mDialog.setMessage("");
		mDialog.setProgress(0);
		mDialog.setMax(100);
		mDialog.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				if (mCancelListener != null) {
					mCancelListener.onProgressMonitorCancel();
				}
			}
		});
	}

	@Override
	public void setProgressMessage(final String progressMessage) {
		mDialog.setMessage(progressMessage);
	}

	@Override
	public void setProgress(int progress) {
		if (!mDialog.isShowing()) {
			mDialog.show();
		}

		mDialog.setProgress(progress);

		// Check if we're done
		if (progress == mDialog.getMax()) {
			if (mCompletedListener != null) {
				mCompletedListener.onProgressMonitorCompleted();
			}

			mDialog.dismiss();
		}
	}

	@Override
	public void setDescription(String description) {
		mDialog.setTitle(description);
	}

	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			mDialog.show();
		}
		else {
			mDialog.dismiss();
		}
	}
}
