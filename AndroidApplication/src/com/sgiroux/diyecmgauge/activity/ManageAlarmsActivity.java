package com.sgiroux.diyecmgauge.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.R;
import com.sgiroux.diyecmgauge.controller.AlarmsController;
import com.sgiroux.diyecmgauge.data.Alarm;
import com.sgiroux.diyecmgauge.data.AlarmAdditionalCondition;
import com.sgiroux.diyecmgauge.data.AlarmConditionOperator;
import com.sgiroux.diyecmgauge.dialog.AlarmConditionDialog;
import com.sgiroux.diyecmgauge.dialog.AlarmConditionDialog.OnAlarmConditionResult;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Activity to manage the alarms of the gauge
 * 
 * @author Seb
 */
public class ManageAlarmsActivity extends Activity implements SerialResponseReceivedListener {
	private Spinner mAlarms;
	private EditText mName;

	private CheckBox mEnabled;

	private Spinner mAdditionalCondition;
	private ArrayAdapter<String> mAdditionalConditionAdapter;

	private CheckBox mShowWarningOnScreen;
	private CheckBox mTriggerBuzzer;
	private CheckBox mStartDataLogging;
	private CheckBox mTurnOnLeftLED;
	private CheckBox mTurnOnRightLED;

	private int mOldAlarmIndex;
	private Alarm mNewAlarm;

	private AlarmsController mController;

	/**
	 * Called when the activity is first created
	 * 
	 * @param savedInstanceState If the activity is being re-initialized after previously being shut down then this Bundle contains the data it most
	 * recently supplied in onSaveInstanceState(Bundle). Note: Otherwise it is null.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mController = new AlarmsController();

		setContentView(R.layout.activity_manage_alarms);

		mAlarms = (Spinner) findViewById(R.id.alarms);
		populateAlarmsSpinner();

		mNewAlarm = getCurrentAlarm();

		mName = (EditText) findViewById(R.id.alarm_name);
		mName.setFilters(new InputFilter[] { new InputFilter.LengthFilter(AlarmsController.ALARM_NAME_MAX_CHARS) });
		mName.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				mNewAlarm.setName(mName.getText().toString());
			}
		});
		mEnabled = (CheckBox) findViewById(R.id.alarm_enabled);

		mAdditionalCondition = (Spinner) findViewById(R.id.alarm_additional_conditon);
		populateAdditionalConditionSpinner();

		mShowWarningOnScreen = (CheckBox) findViewById(R.id.alarm_show_warning_on_screen);
		mTriggerBuzzer = (CheckBox) findViewById(R.id.alarm_trigger_buzzer);
		mStartDataLogging = (CheckBox) findViewById(R.id.alarm_start_data_logging);
		mTurnOnLeftLED = (CheckBox) findViewById(R.id.alarm_turn_on_left_led);
		mTurnOnRightLED = (CheckBox) findViewById(R.id.alarm_turn_on_right_led);
		bindCheckBoxesEvents();

		bindAlarmConditionsButtonsEvents();

		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();

		DIYECMGauge.getInstance().removeSerialResponseReceivedListener(this);
	}

	/**
	 * Inflate the menu; this adds items to the action bar if it is present.
	 * 
	 * @param menu The menu that is created
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.manage_alarms, menu);
		return true;
	}

	/**
	 * When a menu item is selected, this get triggered and we figure out which item has been selected and trigger the associated action.
	 * 
	 * @param item The menu item that has been selected
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_save_alarm:
				saveAlarm();
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Save the alarm at the specified index
	 * 
	 * @param alarmIndex The index of the alarm to save
	 * @param alarm The alarm object to be saved
	 */
	private void saveAlarm(int alarmIndex, Alarm alarm) {
		mController.getAlarmsList().set(alarmIndex, alarm);
		mController.sendCurrentAlarmConfigurationToGauge(alarmIndex);
	}

	/**
	 * Save the alarm locally and send to the gauge
	 */
	private void saveAlarm() {
		saveAlarm(mAlarms.getSelectedItemPosition(), mNewAlarm);
	}

	/**
	 * Populate the alarms spinner with all the current alarm names
	 */
	private void populateAlarmsSpinner() {
		int nbAlarms = mController.getAlarmsList().size();
		String alarmNames[] = new String[nbAlarms];

		for (int i = 0; i < nbAlarms; i++) {
			alarmNames[i] = mController.getAlarmsList().get(i).getName();
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, alarmNames);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mAlarms.setAdapter(adapter);

		if (adapter.getCount() > 0) {
			mAlarms.setSelection(0);
		}

		mAlarms.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				populateActivtyWithSelectedAlarmSettings();
				mOldAlarmIndex = position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {

			}
		});
	}

	/**
	 * Populate the additional condition spinner (None, And, Or) for when a second condition is used
	 */
	private void populateAdditionalConditionSpinner() {
		int nbAdditionalCondition = AlarmAdditionalCondition.values().length;

		String additionalConditions[] = new String[nbAdditionalCondition];

		for (int i = 0; i < nbAdditionalCondition; i++) {
			additionalConditions[i] = AlarmAdditionalCondition.values()[i].getTitle();
		}

		mAdditionalConditionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, additionalConditions);
		mAdditionalConditionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mAdditionalCondition.setAdapter(mAdditionalConditionAdapter);
		mAdditionalCondition.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				if (mNewAlarm != null) {
					mNewAlarm.setAdditionalCondition(AlarmAdditionalCondition.values()[position]);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {

			}
		});
	}

	/**
	 * @return The alarm object for the currently selected alarm
	 */
	private Alarm getCurrentAlarm() {
		int selectedIndex = mAlarms.getSelectedItemPosition();
		if (selectedIndex > -1) {
			return mController.getAlarmsList().get(selectedIndex).clone();
		}

		return null;
	}

	/**
	 * Populate the activity with the settings of the currently selected alarm
	 */
	private void populateActivtyWithSelectedAlarmSettings() {
		if (!mController.getAlarmsList().get(mOldAlarmIndex).equals(mNewAlarm)) {
			final int oldAlarmIndex = mOldAlarmIndex;
			final Alarm alarm = mNewAlarm;

			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setTitle(R.string.alarm_modified);
			builder.setMessage(R.string.send_alarm_config_to_gauge);
			builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					saveAlarm(oldAlarmIndex, alarm);
				}
			});
			builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			builder.show();
		}

		mNewAlarm = getCurrentAlarm();

		mName.setText(mNewAlarm.getName());
		mEnabled.setChecked(mNewAlarm.isEnabled());

		mAdditionalCondition.setSelection(mAdditionalConditionAdapter.getPosition(mNewAlarm.getAdditionalCondition().getTitle()));
		mShowWarningOnScreen.setChecked(mNewAlarm.isShowWarningOnScreen());
		mTriggerBuzzer.setChecked(mNewAlarm.isTriggerBuzzer());
		mStartDataLogging.setChecked(mNewAlarm.isStartDataLogging());
		mTurnOnLeftLED.setChecked(mNewAlarm.isTurnOnLeftLED());
		mTurnOnRightLED.setChecked(mNewAlarm.isTurnOnRightLED());
	}

	/**
	 * Prepare a dialog to be used to configure an alarm condition
	 * 
	 * @param channel The channel to be displayed
	 * @param operator the condition operator to be displayed
	 * @param threshold The threshold to be displayed
	 * @param hysteresis The hysteresis to be displayed
	 * 
	 * @return An instance of alarm condition dialog
	 */
	private AlarmConditionDialog getAlarmConditionDialog(String channel, AlarmConditionOperator operator, float threshold, float hysteresis) {
		AlarmConditionDialog alarmCondition = new AlarmConditionDialog(ManageAlarmsActivity.this, channel, operator, threshold, hysteresis);
		return alarmCondition;
	}

	/**
	 * Bind the on click events for all the check boxes of the activity
	 */
	private void bindCheckBoxesEvents() {
		mEnabled.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mNewAlarm.setEnabled(((CheckBox) v).isChecked());
			}
		});

		mShowWarningOnScreen.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mNewAlarm.setShowWarningOnScreen(((CheckBox) v).isChecked());
			}
		});

		mTriggerBuzzer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mNewAlarm.setTriggerBuzzer(((CheckBox) v).isChecked());
			}
		});

		mStartDataLogging.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mNewAlarm.setStartDataLogging(((CheckBox) v).isChecked());
			}
		});

		mTurnOnLeftLED.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mNewAlarm.setTurnOnLeftLED(((CheckBox) v).isChecked());
			}
		});

		mTurnOnRightLED.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mNewAlarm.setTurnOnRightLED(((CheckBox) v).isChecked());
			}
		});
	}

	/**
	 * Bind the click events for the alarm condition. Open the dialog to let the user fill up the condition fields.
	 */
	private void bindAlarmConditionsButtonsEvents() {
		Button firstCondition = (Button) findViewById(R.id.alarm_first_condition);
		firstCondition.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Alarm alarm = getCurrentAlarm();

				if (alarm != null) {
					AlarmConditionDialog alarmCondition = getAlarmConditionDialog(alarm.getChannel1(), alarm.getOperator1(), alarm.getThreshold1(), alarm.getHysteresis1());
					alarmCondition.setDialogResult(new OnAlarmConditionResult() {
						@Override
						public void finish(String channel, AlarmConditionOperator operator, float threshold, float hysteresis) {
							if (mNewAlarm != null) {
								mNewAlarm.setChannel1(channel);
								mNewAlarm.setOperator1(operator);
								mNewAlarm.setThreshold1(threshold);
								mNewAlarm.setHysteresis1(hysteresis);
							}
						}
					});
					alarmCondition.show();
				}
			}
		});

		Button secondCondition = (Button) findViewById(R.id.alarm_second_condition);
		secondCondition.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Alarm alarm = getCurrentAlarm();
				AlarmConditionDialog alarmCondition = getAlarmConditionDialog(alarm.getChannel2(), alarm.getOperator2(), alarm.getThreshold2(), alarm.getHysteresis2());
				alarmCondition.setDialogResult(new OnAlarmConditionResult() {
					@Override
					public void finish(String channel, AlarmConditionOperator operator, float threshold, float hysteresis) {
						mNewAlarm.setChannel2(channel);
						mNewAlarm.setOperator2(operator);
						mNewAlarm.setThreshold2(threshold);
						mNewAlarm.setHysteresis2(hysteresis);
					}
				});
				alarmCondition.show();
			}
		});
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		String textResponse = SerialUtils.getTextResponse(response);

		if (serialCommand == SerialCommand.SEND_ALARM) {
			String message;
			if (SerialUtils.isOKResponse(textResponse)) {
				message = getString(R.string.alarm_sent_successfully);
			}
			else {
				message = getString(R.string.failed_to_send_alarm);
			}

			Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
			toast.show();
		}
	}
}
