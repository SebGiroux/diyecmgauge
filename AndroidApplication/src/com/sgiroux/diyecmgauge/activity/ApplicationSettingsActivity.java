package com.sgiroux.diyecmgauge.activity;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.sgiroux.diyecmgauge.R;

public class ApplicationSettingsActivity extends PreferenceActivity {
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTitle(R.string.application_settings);

		addPreferencesFromResource(R.xml.application_settings);
	}
}
