package com.sgiroux.diyecmgauge.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.R;
import com.sgiroux.diyecmgauge.controller.SettingsController;
import com.sgiroux.diyecmgauge.data.DimensionUnit;
import com.sgiroux.diyecmgauge.data.GeneralSettings;
import com.sgiroux.diyecmgauge.data.HomingDirection;
import com.sgiroux.diyecmgauge.data.SpeedSourceDynamometer;
import com.sgiroux.diyecmgauge.data.SpeedUnit;
import com.sgiroux.diyecmgauge.data.StepperSettings;
import com.sgiroux.diyecmgauge.data.TemperatureUnit;
import com.sgiroux.diyecmgauge.data.TimeZone;
import com.sgiroux.diyecmgauge.data.VehicleSettings;
import com.sgiroux.diyecmgauge.data.WeightUnit;
import com.sgiroux.diyecmgauge.list.IndicatorsList;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Activity to manage the settings (general and vehicle) of the gauge
 * 
 * @author Seb
 *
 */
public class GaugeSettingsActivity extends Activity implements SerialResponseReceivedListener {
	// General settings
	private Spinner mSpeedUnit;
	private Spinner mTemperatureUnit;
	private Spinner mSpeedSourceDynamometer;
	private Spinner mTimeZone;
	private CheckBox mSyncWithGps;
	private EditText mRealTimeClockCompensate;
	private CheckBox mConfigurationSerialPortOnMainConnector;

	// Vehicle settings
	private EditText mVehicleWeight;
	private Spinner mVehicleWeightUnit;
	private EditText mCoefficientOfDrag;
	private EditText mCoefficientOfRollingResistance;
	private EditText mFrontalArea;
	private Spinner mFrontalAreaUnit;

	// Stepper settings
	private EditText mTotalNumberOfSteps;
	private EditText mMaximumSpeed;
	private EditText mAcceleration;
	private Spinner mHomingDirection;
	private Spinner mChannel;

	private SettingsController mController;

	/**
	 * Called when the activity is first created
	 * 
	 * @param savedInstanceState If the activity is being re-initialized after previously being shut down then this Bundle contains the data it most
	 * recently supplied in onSaveInstanceState(Bundle). Note: Otherwise it is null.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mController = new SettingsController();

		setContentView(R.layout.activity_gauge_settings);

		mSpeedUnit = (Spinner) findViewById(R.id.speed_unit);
		mTemperatureUnit = (Spinner) findViewById(R.id.temperature_unit);
		mSpeedSourceDynamometer = (Spinner) findViewById(R.id.speed_source_dynamometer);
		mTimeZone = (Spinner) findViewById(R.id.time_zone);
		mSyncWithGps = (CheckBox) findViewById(R.id.sync_with_gps);
		mRealTimeClockCompensate = (EditText) findViewById(R.id.real_time_clock_compensation);
		mConfigurationSerialPortOnMainConnector = (CheckBox) findViewById(R.id.configuration_serial_port_on_main_connector);

		mVehicleWeight = (EditText) findViewById(R.id.vehicle_weight);
		mVehicleWeightUnit = (Spinner) findViewById(R.id.vehicle_weight_unit);
		mCoefficientOfDrag = (EditText) findViewById(R.id.coefficient_of_drag);
		mCoefficientOfRollingResistance = (EditText) findViewById(R.id.coefficient_of_rolling_resistance);
		mFrontalArea = (EditText) findViewById(R.id.frontal_area);
		mFrontalAreaUnit = (Spinner) findViewById(R.id.frontal_area_unit);

		mTotalNumberOfSteps = (EditText) findViewById(R.id.total_number_of_steps);
		mMaximumSpeed = (EditText) findViewById(R.id.maximum_speed);
		mAcceleration = (EditText) findViewById(R.id.acceleration);
		mHomingDirection = (Spinner) findViewById(R.id.homing_direction);
		mChannel = (Spinner) findViewById(R.id.channel);

		prepareTabHost();

		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();

		DIYECMGauge.getInstance().removeSerialResponseReceivedListener(this);
	}

	/**
	 * Prepare the tab host that contains the general & vehicle settings tabs
	 */
	private void prepareTabHost() {
		TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
		tabHost.setup();

		TabSpec generalSettings = tabHost.newTabSpec(getString(R.string.general_settings));
		generalSettings.setIndicator(getString(R.string.general_settings));
		generalSettings.setContent(R.id.general_settings);

		TabSpec vehicleSettings = tabHost.newTabSpec(getString(R.string.vehicle_settings));
		vehicleSettings.setIndicator(getString(R.string.vehicle_settings));
		vehicleSettings.setContent(R.id.vehicle_settings);

		TabSpec stepperSettings = tabHost.newTabSpec(getString(R.string.stepper_settings));
		stepperSettings.setIndicator(getString(R.string.stepper_settings));
		stepperSettings.setContent(R.id.stepper_settings);

		tabHost.addTab(generalSettings);
		tabHost.addTab(vehicleSettings);
		tabHost.addTab(stepperSettings);

		centerTabIndicators(tabHost);

		populateSpinners();
		populateCurrentSettings();
	}

	/**
	 * Center the indicator text of each tab
	 * 
	 * @param tabHost The tab host we want to center the indicators for
	 */
	private void centerTabIndicators(TabHost tabHost) {
		int tabCount = tabHost.getTabWidget().getTabCount();

		for (int i = 0; i < tabCount; i++) {
			final View view = tabHost.getTabWidget().getChildTabViewAt(i);
			if (view != null) {
				// Get title text view
				final View textView = view.findViewById(android.R.id.title);

				// Just in case, check the type
				if (textView instanceof TextView) {
					TextView title = (TextView) textView;

					// Center text
					title.setGravity(Gravity.CENTER);
					// Wrap text
					title.setSingleLine(false);

					// Explicitly set layout parameters
					textView.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
					textView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
				}
			}
		}
	}

	/**
	 * Populate the activity with the current general & vehicle settings
	 */
	private void populateCurrentSettings() {
		// General settings
		GeneralSettings generalSettings = mController.getGeneralSettings();

		mSpeedUnit.setSelection(generalSettings.getSpeedUnit().ordinal());
		mTemperatureUnit.setSelection(generalSettings.getTemperatureUnit().ordinal());
		mSpeedSourceDynamometer.setSelection(generalSettings.getSpeedSourceDynamometer().ordinal());
		mTimeZone.setSelection(TimeZone.fromOffset(generalSettings.getTimeZone()).ordinal());
		mSyncWithGps.setChecked(generalSettings.isRtcSyncWithGps());
		mRealTimeClockCompensate.setText(String.valueOf(generalSettings.getRtcCompensate()));
		mConfigurationSerialPortOnMainConnector.setChecked(generalSettings.isConfigurationSerialPortOnMainConnector());

		// Vehicle settings
		VehicleSettings vehicleSettings = mController.getVehicleSettings();

		mVehicleWeight.setText(String.valueOf(vehicleSettings.getVehicleWeight()));
		mCoefficientOfDrag.setText(String.valueOf(vehicleSettings.getCoefficientOfDrag()));
		mCoefficientOfRollingResistance.setText(String.valueOf(vehicleSettings.getCoefficientOfRollingResistance()));
		mFrontalArea.setText(String.valueOf(vehicleSettings.getFrontalArea()));

		// Stepper settings
		StepperSettings stepperSettings = mController.getStepperSettings();

		mTotalNumberOfSteps.setText(String.valueOf(stepperSettings.getTotalNumberOfSteps()));
		mMaximumSpeed.setText(String.valueOf(stepperSettings.getMaximumSpeed()));
		mAcceleration.setText(String.valueOf(stepperSettings.getAcceleration()));
		mHomingDirection.setSelection(stepperSettings.getHomingDirection().ordinal());
		mChannel.setSelection(stepperSettings.getChannel());
	}

	/**
	 * Populate all the spinners of the activity with all the possible values for each
	 */
	private void populateSpinners() {
		// Speed unit
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SpeedUnit.getTitleList());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mSpeedUnit.setAdapter(adapter);

		// Temperature unit
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TemperatureUnit.getTitleList());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mTemperatureUnit.setAdapter(adapter);

		// Speed source dynamometer
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SpeedSourceDynamometer.getTitleList());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mSpeedSourceDynamometer.setAdapter(adapter);

		// Time zone
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TimeZone.getTitleList());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mTimeZone.setAdapter(adapter);

		// Vehicle weight unit
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, WeightUnit.getTitleList());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mVehicleWeightUnit.setAdapter(adapter);

		// Frontal area unit
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, DimensionUnit.getTitleList());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mFrontalAreaUnit.setAdapter(adapter);

		// Homing direction
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, HomingDirection.getTitleList());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mHomingDirection.setAdapter(adapter);

		// Channel
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, IndicatorsList.getInstance().getList());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mChannel.setAdapter(adapter);
	}

	/**
	 * Inflate the menu; this adds items to the action bar if it is present.
	 * 
	 * @param menu The menu that is created
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	/**
	 * When a menu item is selected, this get triggered and we figure out which item has been selected and trigger the associated action.
	 * 
	 * @param item The menu item that has been selected
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_save_settings:
				saveCurrentSettings();
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Save the current general, vehicle settings and stepper settings based on the current fields content of this activity
	 */
	private void saveCurrentSettings() {
		GeneralSettings currentGeneralSettings = getGeneralSettingsFromCurrentSettings();
		VehicleSettings currentVehicleSettings = getVehicleSettingsFromCurrentSettings();
		StepperSettings currentStepperSettings = getStepperSettingsFromCurrentSettings();

		mController.saveCurrentSettings(currentGeneralSettings, currentVehicleSettings, currentStepperSettings);
	}

	/**
	 * @return A GeneralSettings object build from the fields of this activity
	 */
	private GeneralSettings getGeneralSettingsFromCurrentSettings() {
		GeneralSettings generalSettings = new GeneralSettings();

		generalSettings.setSpeedUnit(SpeedUnit.values()[mSpeedUnit.getSelectedItemPosition()]);
		generalSettings.setTemperatureUnit(TemperatureUnit.values()[mTemperatureUnit.getSelectedItemPosition()]);
		generalSettings.setSpeedSourceDynamometer(SpeedSourceDynamometer.values()[mSpeedSourceDynamometer.getSelectedItemPosition()]);
		generalSettings.setTimeZone(TimeZone.values()[mTimeZone.getSelectedItemPosition()].getUtcOffset());
		generalSettings.setRtcSyncWithGps(mSyncWithGps.isSelected());
		generalSettings.setConfigurationSerialPortOnMainConnector(mConfigurationSerialPortOnMainConnector.isSelected());
		int ppm = 0;
		try {
			ppm = Integer.parseInt(mRealTimeClockCompensate.getText().toString());
		} catch (NumberFormatException e) {
		}
		generalSettings.setRtcCompensate(ppm);

		return generalSettings;
	}

	/**
	 * @return A VehicleSettings object build from the fields of this activity
	 */
	private VehicleSettings getVehicleSettingsFromCurrentSettings() {
		return mController.getVehicleSettingsFromFields(mVehicleWeight.getText().toString(), mCoefficientOfDrag.getText().toString(), mCoefficientOfRollingResistance.getText().toString(), mFrontalArea.getText().toString(), mVehicleWeightUnit.getSelectedItem().toString(), mFrontalAreaUnit
				.getSelectedItem().toString());
	}

	/**
	 * @return A StepperSettings object build from the fields of this activity
	 */
	private StepperSettings getStepperSettingsFromCurrentSettings() {
		return mController.getStepperSettingsFromFields(mTotalNumberOfSteps.getText().toString(), mMaximumSpeed.getText().toString(), mAcceleration.getText().toString(), mHomingDirection.getSelectedItemPosition(), mChannel.getSelectedItemPosition());
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		String textResponse = SerialUtils.getTextResponse(response);

		if (serialCommand == SerialCommand.SEND_GENERAL_SETTINGS) {
			displayToastForResponse(textResponse, R.string.general_settings_sent_successfully, R.string.failed_to_general_settings);
		}
		else if (serialCommand == SerialCommand.SEND_VEHICLE_SETTINGS) {
			displayToastForResponse(textResponse, R.string.vehicle_settings_sent_successfully, R.string.failed_to_vehicle_settings);
		}
	}

	/***
	 * Display a toast message based on the serial response
	 * 
	 * @param textResponse The text of the serial response
	 * @param stringResSuccess The resource ID for the string message in case of success
	 * @param stringResError The resource ID for the string message in case of failure
	 */
	private void displayToastForResponse(String textResponse, int stringResSuccess, int stringResError) {
		String message;
		if (SerialUtils.isOKResponse(textResponse)) {
			message = getString(stringResSuccess);
		}
		else {
			message = getString(stringResError);
		}

		Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
		toast.show();
	}
}
