package com.sgiroux.diyecmgauge.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.R;
import com.sgiroux.diyecmgauge.adapter.IndicatorRow;
import com.sgiroux.diyecmgauge.adapter.IndicatorRowAdapter;
import com.sgiroux.diyecmgauge.adapter.IndicatorRowAdapterCallback;
import com.sgiroux.diyecmgauge.controller.PagesController;
import com.sgiroux.diyecmgauge.data.Page;
import com.sgiroux.diyecmgauge.data.PageContent;
import com.sgiroux.diyecmgauge.list.IndicatorsList;
import com.sgiroux.diyecmgauge.list.PagesList;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Activity to manage the pages of the gauge
 * 
 * @author Seb
 */
public class ManagePagesActivity extends Activity implements SerialResponseReceivedListener {
	private PagesController mController;

	private Spinner mPageSpinner;

	private Spinner mPageContent;
	private ArrayAdapter<String> mPageContentAdapter;

	private ListView mIndicatorsCurrentlyInPage;
	private IndicatorRowAdapter mIndicatorsCurrentlyInPageAdapter;
	private Button mRemove;

	private ListView mIndicatorsAvailable;
	private IndicatorRowAdapter mIndicatorsAvailableAdapter;
	private Button mAdd;

	private int mOldPageIndex;
	private Page mNewPage;

	/**
	 * Called when the activity is first created
	 * 
	 * @param savedInstanceState If the activity is being re-initialized after previously being shut down then this Bundle contains the data it most
	 * recently supplied in onSaveInstanceState(Bundle). Note: Otherwise it is null.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_manage_pages);

		mController = new PagesController();

		mPageSpinner = (Spinner) findViewById(R.id.page);
		populatePagesSpinner();

		mNewPage = getCurrentPage();

		mPageContent = (Spinner) findViewById(R.id.page_content);
		populatePageContentSpinner();

		mIndicatorsCurrentlyInPage = (ListView) findViewById(R.id.indicators_currently_in_page);
		populateIndicatorsCurrentlyInPage();

		mIndicatorsAvailable = (ListView) findViewById(R.id.indicators_available);
		populateIndicatorsAvailables();

		mAdd = (Button) findViewById(R.id.btn_add_to_current_page);
		mRemove = (Button) findViewById(R.id.btn_remove);
		bindAddRemoveButtonsEvents();

		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();

		DIYECMGauge.getInstance().removeSerialResponseReceivedListener(this);
	}

	/**
	 * Inflate the menu; this adds items to the action bar if it is present.
	 * 
	 * @param menu The menu that is created
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.manage_pages, menu);
		return true;
	}

	/**
	 * When a menu item is selected, this get triggered and we figure out which item has been selected and trigger the associated action.
	 * 
	 * @param item The menu item that has been selected
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_save_page:
				savePage();
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Populate the pages spinner
	 */
	private void populatePagesSpinner() {
		ArrayList<Page> pagesArray = PagesList.getInstance().getList();
		String pages[] = new String[pagesArray.size()];
		for (int i = 1; i <= pages.length; i++) {
			pages[i - 1] = String.valueOf(i);
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pages);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mPageSpinner.setAdapter(adapter);
		mPageSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				populateActivtyWithSelectedPageSettings();
				mOldPageIndex = position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {

			}
		});
	}

	/**
	 * Populate the page content spinner
	 */
	private void populatePageContentSpinner() {
		int pageContentLength = PageContent.values().length;
		String pageContent[] = new String[pageContentLength - 1];

		int index = 0;
		for (int i = 0; i < pageContentLength; i++) {
			PageContent currentPageContent = PageContent.values()[i];
			if (currentPageContent != PageContent.UNUSED) {
				pageContent[index++] = currentPageContent.getTitle();
			}
		}

		mPageContentAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pageContent);
		mPageContentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mPageContent.setAdapter(mPageContentAdapter);
		mPageContent.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				if (mNewPage != null) {
					mNewPage.setContent(PageContent.values()[position + 1]);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {

			}
		});
	}

	/**
	 * Populate the indicators currently in page list view
	 */
	private void populateIndicatorsCurrentlyInPage() {
		mIndicatorsCurrentlyInPageAdapter = new IndicatorRowAdapter(this);
		mIndicatorsCurrentlyInPageAdapter.setCallback(new IndicatorRowAdapterCallback() {
			@Override
			public void onIndicatorsSelectedStateChanged() {
				refreshRemoveButtonState();
			}
		});
		mIndicatorsCurrentlyInPage.setAdapter(mIndicatorsCurrentlyInPageAdapter);
	}

	/**
	 * Populate the indicators available list view
	 */
	private void populateIndicatorsAvailables() {
		String[] indicators = IndicatorsList.getInstance().getList();

		if (indicators != null) {
			mIndicatorsAvailableAdapter = new IndicatorRowAdapter(this, indicators);
			mIndicatorsAvailableAdapter.setCallback(new IndicatorRowAdapterCallback() {
				@Override
				public void onIndicatorsSelectedStateChanged() {
					if (getNbIndicatorsAvailableSelected() == 0) {
						mAdd.setEnabled(false);
					}
					else {
						mAdd.setEnabled(true);
					}
				}
			});
			mIndicatorsAvailable.setAdapter(mIndicatorsAvailableAdapter);
		}
	}

	/**
	 * Populate the activity with the settings of the currently selected page
	 */
	private void populateActivtyWithSelectedPageSettings() {
		if (!mController.getPagesList().get(mOldPageIndex).equals(mNewPage)) {
			final int oldPageIndex = mOldPageIndex;
			final Page page = mNewPage;

			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setTitle(R.string.page_modified);
			builder.setMessage(R.string.send_page_config_to_gauge);
			builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					savePage(oldPageIndex, page);
				}
			});
			builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			builder.show();
		}

		mNewPage = getCurrentPage();

		mIndicatorsCurrentlyInPageAdapter.clear();

		for (int indicatorChannel : mNewPage.getIndicatorsChannels()) {
			if (indicatorChannel != IndicatorsList.UNUSED) {
				mIndicatorsCurrentlyInPageAdapter.add(IndicatorsList.getInstance().getList()[indicatorChannel]);
			}
		}

		mPageContent.setSelection(mPageContentAdapter.getPosition(mNewPage.getContent().getTitle()));
	}

	/**
	 * Save the page at the specified index
	 * 
	 * @param pageIndex The index of the page to save
	 * @param page The page object to be saved
	 */
	private void savePage(int pageIndex, Page page) {
		mController.getPagesList().set(pageIndex, page);
		mController.sendCurrentPageConfigurationToGauge(pageIndex);
	}

	/**
	 * Save the page locally and send to the gauge
	 */
	private void savePage() {
		savePage(mPageSpinner.getSelectedItemPosition(), mNewPage);
	}

	/**
	 * @return The page object for the currently selected page
	 */
	private Page getCurrentPage() {
		int selectedIndex = mPageSpinner.getSelectedItemPosition();
		if (selectedIndex > -1) {
			return mController.getPagesList().get(selectedIndex).clone();
		}

		return null;
	}

	/**
	 * @return The number of indicators selected in the indicators currently on page list
	 */
	private int getNbIndicatorsInPageSelected() {
		return mIndicatorsCurrentlyInPageAdapter.getSelectedItems().size();
	}

	/**
	 * @return The number of indicators selected in the indicators currently available list
	 */
	private int getNbIndicatorsAvailableSelected() {
		return mIndicatorsAvailableAdapter.getSelectedItems().size();
	}

	/**
	 * Bind the click events for the two buttons: "Add indicator to page" and "Remove indicator from page"
	 */
	private void bindAddRemoveButtonsEvents() {
		mRemove.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				List<IndicatorRow> pageRows = mIndicatorsCurrentlyInPageAdapter.getSelectedItems();

				for (IndicatorRow pageRow : pageRows) {
					mIndicatorsCurrentlyInPageAdapter.remove(pageRow.getTitle());
				}

				refreshIndicatorsInPage();
				refreshRemoveButtonState();
			}
		});

		mAdd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				List<IndicatorRow> pageRows = mIndicatorsAvailableAdapter.getSelectedItems();

				int nbMaxIndicatorForPage = mNewPage.getContent().getNbIndicators();
				int nbIndicatorCurrentlyInPage = mIndicatorsCurrentlyInPageAdapter.getCount();

				for (IndicatorRow pageRow : pageRows) {
					if (nbIndicatorCurrentlyInPage == nbMaxIndicatorForPage) {
						String message = String.format(getString(R.string.maximum_number_of_indicators), mNewPage.getContent().getTitle(), nbMaxIndicatorForPage);

						AlertDialog.Builder builder = new AlertDialog.Builder(ManagePagesActivity.this);
						builder.setTitle(R.string.maximum_number_of_indicators_title);
						builder.setMessage(message);
						builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {

							}
						});
						builder.setIcon(android.R.drawable.ic_dialog_alert);
						builder.show();

						break;
					}

					mIndicatorsCurrentlyInPageAdapter.add(pageRow.getTitle());
					nbIndicatorCurrentlyInPage++;
				}

				refreshIndicatorsInPage();
			}
		});
	}

	/**
	 * Refresh the remove button state based on if any indicators are currently selected or not
	 */
	private void refreshRemoveButtonState() {
		if (getNbIndicatorsInPageSelected() == 0) {
			mRemove.setEnabled(false);
		}
		else {
			mRemove.setEnabled(true);
		}
	}

	/**
	 * Refresh the page object with the indicators listed in the list view
	 */
	private void refreshIndicatorsInPage() {
		String indicators[] = IndicatorsList.getInstance().getList();
		List<IndicatorRow> indicatorRows = mIndicatorsCurrentlyInPageAdapter.getAll();

		ArrayList<Integer> indicatorsChannels = new ArrayList<Integer>();

		for (IndicatorRow indicatorRow : indicatorRows) {
			String titleToLookFor = indicatorRow.getTitle();

			for (int i = 0; i < indicators.length; i++) {
				if (indicators[i].equals(titleToLookFor)) {
					indicatorsChannels.add(i);
				}
			}
		}

		mNewPage.setIndicatorsChannels(indicatorsChannels);
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		String textResponse = SerialUtils.getTextResponse(response);

		if (serialCommand == SerialCommand.SEND_PAGE) {
			String message;
			if (SerialUtils.isOKResponse(textResponse)) {
				message = getString(R.string.page_sent_successfully);
			}
			else {
				message = getString(R.string.failed_to_send_page);
			}

			Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
			toast.show();
		}
	}
}
