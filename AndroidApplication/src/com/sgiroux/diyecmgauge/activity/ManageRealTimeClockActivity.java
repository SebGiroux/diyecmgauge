package com.sgiroux.diyecmgauge.activity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.R;
import com.sgiroux.diyecmgauge.controller.RTCController;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;

/**
 * Activity to manage the real-time clock of the gauge
 * 
 * @author Seb
 */
public class ManageRealTimeClockActivity extends Activity implements SerialResponseReceivedListener {
	private static final String DATE_FORMAT = "MMMM dd yyyy - HH:mm:ss";

	private Timer mTimeRefreshTimer = new Timer();
	private long mGaugeAndLocalTimeOffset; // in milliseconds
	private RTCController mController;

	private TextView mCurrentLocalTime;
	private TextView mCurrentGaugeTime;

	/**
	 * Called when the activity is first created
	 * 
	 * @param savedInstanceState If the activity is being re-initialized after previously being shut down then this Bundle contains the data it most
	 * recently supplied in onSaveInstanceState(Bundle). Note: Otherwise it is null.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_manage_real_time_clock);

		mController = new RTCController();

		mCurrentLocalTime = (TextView) findViewById(R.id.current_local_time);
		mCurrentGaugeTime = (TextView) findViewById(R.id.current_gauge_time);

		mTimeRefreshTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// Called every second to update current local time every second
						SimpleDateFormat date = new SimpleDateFormat(DATE_FORMAT, Locale.US);
						mCurrentLocalTime.setText(date.format(Calendar.getInstance().getTime()));
						mCurrentGaugeTime.setText(date.format(Calendar.getInstance().getTimeInMillis() + mGaugeAndLocalTimeOffset));
					}
				});
			}
		}, 0, 1000);

		final Button btnSetGaugeTimeToLocal = (Button) findViewById(R.id.set_gauge_time_to_current_local_time);
		btnSetGaugeTimeToLocal.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mController.sendRTCDateTimeToGauge();
			}
		});

		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_RTC_DATE_TIME));
	}

	@Override
	public void onPause() {
		super.onPause();

		DIYECMGauge.getInstance().removeSerialResponseReceivedListener(this);
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		if (serialCommand == SerialCommand.GET_RTC_DATE_TIME) {
			final SimpleDateFormat date = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
			final Calendar cal = mController.parseRTCDataFromGauge(response);

			mGaugeAndLocalTimeOffset = cal.getTimeInMillis() - System.currentTimeMillis();
			mCurrentGaugeTime.setText(date.format(cal.getTime()));

			final TextView currentGaugeTime = (TextView) findViewById(R.id.current_gauge_time);
			currentGaugeTime.setText(date.format(cal.getTime()));
		}
		else if (serialCommand == SerialCommand.SEND_RTC_DATE_TIME) {
			// We got a response from the send command, lets get the date / time again
			// to make sure it was updated with the value we sent
			DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_RTC_DATE_TIME));
		}
	}
}
