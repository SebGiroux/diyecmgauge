package com.sgiroux.diyecmgauge.activity;

import java.io.File;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.DIYECMGaugeApplication;
import com.sgiroux.diyecmgauge.DIYECMGaugeSettings;
import com.sgiroux.diyecmgauge.R;
import com.sgiroux.diyecmgauge.config.ConfigFileManager;
import com.sgiroux.diyecmgauge.dialog.FileChooserDialog;
import com.sgiroux.diyecmgauge.dialog.FileChooserDialog.FileChooserType;
import com.sgiroux.diyecmgauge.dialog.FileChooserDialog.OnFileChoosenResult;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorBase;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorDialog;
import com.sgiroux.diyecmgauge.serial.IntentPacket;
import com.sgiroux.diyecmgauge.serial.SerialCommConnection;
import com.sgiroux.diyecmgauge.serial.SerialCommConnectionFactory;
import com.sgiroux.diyecmgauge.serial.SerialCommConnectionState;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialResponseTimeoutListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Main activity
 * 
 * @author Seb
 */
public class DashboardActivity extends Activity implements SerialResponseReceivedListener, SerialResponseTimeoutListener {
	private static final String LOG_TAG = "DashboardActivity";

	public static final int REQUEST_ENABLE_BLUETOOTH = 1;
	public static final int REQUEST_CONNECT_TO_GAUGE = 2;

	private String mGaugeFirmwareVersion = "";

	private TextView mStatusText;

	/**
	 * Called when the activity is first created
	 * 
	 * @param savedInstanceState If the activity is being re-initialized after previously being shut down then this Bundle contains the data it most
	 * recently supplied in onSaveInstanceState(Bundle). Note: Otherwise it is null.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);

		mStatusText = (TextView) findViewById(R.id.status_text);

		setupSettings();
		setupSDCard();

		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
		DIYECMGauge.getInstance().addSerialResponseTimeoutListener(this);

		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				DIYECMGaugeApplication.getInstance().toggleGaugeConnection(DashboardActivity.this);
			}
		}, 1000);
	}

	@Override
	protected void onResume() {
		super.onResume();

		final IntentFilter dataFilterSetupBluetooth = new IntentFilter(DIYECMGaugeApplication.BROADCAST_SETUP_BLUETOOTH);
		registerReceiver(mReceiver, dataFilterSetupBluetooth);

		final IntentFilter dataFilterGaugeConnectionStateChange = new IntentFilter(DIYECMGaugeApplication.BROADCAST_GAUGE_CONNECTION_STATE_CHANGE);
		registerReceiver(mReceiver, dataFilterGaugeConnectionStateChange);
	}

	@Override
	public void onPause() {
		super.onPause();

		unregisterReceiver(mReceiver);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		DIYECMGauge.getInstance().removeSerialResponseReceivedListener(this);
		DIYECMGauge.getInstance().removeSerialResponseTimeoutListener(this);
	}

	/**
	 * Create the settings object
	 */
	private void setupSettings() {
		DIYECMGaugeApplication.getInstance().setSettings(new DIYECMGaugeSettings(this));
	}

	/**
	 * Setup the external storage by creating the configuration and data logs directories if they don't already exists
	 */
	private void setupSDCard() {
		// Make sure SD card is mounted
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			// If configuration directory doesn't exists, create it
			if (!DIYECMGaugeSettings.getConfigDirectory().exists()) {
				File configDirectory = DIYECMGaugeSettings.getConfigDirectory();
				if (configDirectory.mkdirs()) {
					Log.i(LOG_TAG, "Created config directory");
				}
				else {
					Log.e(LOG_TAG, "Error creating config directory");
				}
			}

			// If data logs directory doesn't exists, create it
			if (!DIYECMGaugeSettings.getDataLogsDirectory().exists()) {
				File dataLogsDirectory = DIYECMGaugeSettings.getDataLogsDirectory();
				if (dataLogsDirectory.mkdirs()) {
					Log.i(LOG_TAG, "Created datalogs directory");
				}
				else {
					Log.e(LOG_TAG, "Error creating datalogs directory");
				}
			}
		}
		else {
			Log.e(LOG_TAG, "SD card isn't mounted");
			Toast.makeText(this, R.string.no_sd_card_found, Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Inflate the menu; this adds items to the action bar if it is present.
	 * 
	 * @param menu The menu that is created
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.dashboard, menu);
		return true;
	}

	/**
	 * When a menu item is selected, this get triggered and we figure out which item has been selected and trigger the associated action.
	 * 
	 * @param item The menu item that has been selected
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		FileChooserDialog fileChooserUpload;

		switch (item.getItemId()) {
			case R.id.action_open_config_file:
				fileChooserUpload = new FileChooserDialog(this, DIYECMGaugeSettings.getConfigDirectory(), ConfigFileManager.CONFIG_FILE_EXTENSION, FileChooserType.OPEN);
				fileChooserUpload.show();
				fileChooserUpload.setFileName(ConfigFileManager.CONFIG_FILE_DEFAULT_NAME);
				fileChooserUpload.setDialogResult(new OnFileChoosenResult() {
					@Override
					public void finish(File file) {
						ConfigFileManager configFileManager = new ConfigFileManager();
						ProgressMonitorBase progressMonitor = new ProgressMonitorDialog();

						progressMonitor.setParent(DashboardActivity.this);
						configFileManager.openConfigFile(progressMonitor, file);
					}
				});

				break;

			case R.id.action_save_config_file:
				fileChooserUpload = new FileChooserDialog(this, DIYECMGaugeSettings.getConfigDirectory(), ConfigFileManager.CONFIG_FILE_EXTENSION, FileChooserType.SAVE);
				fileChooserUpload.show();
				fileChooserUpload.setFileName(ConfigFileManager.CONFIG_FILE_DEFAULT_NAME);
				fileChooserUpload.setDialogResult(new OnFileChoosenResult() {
					@Override
					public void finish(File file) {
						ConfigFileManager configFileManager = new ConfigFileManager();
						configFileManager.saveConfigFile(file);
					}
				});

				break;

			case R.id.action_manage_pages:
				intent = new Intent(this, ManagePagesActivity.class);
				startActivity(intent);
				break;

			case R.id.action_browse_data_logs:
				intent = new Intent(this, BrowseDataLogsActivity.class);
				startActivity(intent);
				break;

			case R.id.action_manage_alarms:
				intent = new Intent(this, ManageAlarmsActivity.class);
				startActivity(intent);
				break;

			case R.id.action_manage_real_time_clock:
				intent = new Intent(this, ManageRealTimeClockActivity.class);
				startActivity(intent);
				break;

			case R.id.action_gauge_settings:
				intent = new Intent(this, GaugeSettingsActivity.class);
				startActivity(intent);
				break;

			case R.id.action_application_settings:
				intent = new Intent(this, ApplicationSettingsActivity.class);
				startActivity(intent);
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		if (serialCommand == SerialCommand.GET_VERSION) {
			mGaugeFirmwareVersion = SerialUtils.getTextResponse(response);

			mStatusText.setText(String.format(getString(R.string.connected_to), mGaugeFirmwareVersion));
		}
		else if (serialCommand == SerialCommand.GET_DEBUG) {
			String message = SerialUtils.getTextResponse(response);
			Log.d(LOG_TAG, "DEBUG: " + message);
		}
	}

	@Override
	public void onResponseTimeout(SerialCommand serialCommand) {
		if (serialCommand == SerialCommand.GET_VERSION) {
			mStatusText.setText(R.string.no_gauge_found);
		}
	}

	/**
	 * When starting an activity for result within this activity, when we get back here, this method will be triggered with the proper request code
	 * and result code and associated data.
	 * 
	 * @param requestCode The request code that was used within startActivityForResult()
	 * @param resultCode The result code
	 * @param data The data associated with the result
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
				case REQUEST_ENABLE_BLUETOOTH:
					DIYECMGaugeApplication.getInstance().toggleGaugeConnection(this);
					break;
			}
		}
	}

	/**
	 * Broadcast receiver that will receive data from the gauge comms thread about gauge connection state that changed.
	 */
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(final Context context, final Intent intent) {
			final String action = intent.getAction();

			if (action.equals(DIYECMGaugeApplication.BROADCAST_SETUP_BLUETOOTH)) {
				SerialCommConnection connection = SerialCommConnectionFactory.getInstance().getConnection();
				if (connection != null) {
					IntentPacket requestUserDataIntent = connection.getUserDataIntent(DashboardActivity.this);
					startActivityForResult(requestUserDataIntent.getIntent(), requestUserDataIntent.getRequestCode());
				}
			}
			else if (action.equals(DIYECMGaugeApplication.BROADCAST_GAUGE_CONNECTION_STATE_CHANGE)) {
				SerialCommConnectionState connectionState = DIYECMGaugeApplication.getInstance().getConnectionState();

				switch (connectionState) {
					case CONNECTING:
						mStatusText.setText(R.string.connecting);
						break;

					case DISCONNECTED:
						mStatusText.setText(R.string.disconnected);
						break;

					default:
						break;
				}
			}
		}
	};
}
