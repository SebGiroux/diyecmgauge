package com.sgiroux.diyecmgauge.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.DIYECMGaugeSettings;
import com.sgiroux.diyecmgauge.R;
import com.sgiroux.diyecmgauge.adapter.DataLogRowAdapter;
import com.sgiroux.diyecmgauge.adapter.DataLogRowAdapterCallback;
import com.sgiroux.diyecmgauge.controller.DataLogsController;
import com.sgiroux.diyecmgauge.data.DataLogRow;
import com.sgiroux.diyecmgauge.data.DataLoggingStatus;
import com.sgiroux.diyecmgauge.dialog.FileChooserDialog;
import com.sgiroux.diyecmgauge.dialog.FileChooserDialog.FileChooserType;
import com.sgiroux.diyecmgauge.dialog.FileChooserDialog.OnFileChoosenResult;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorBase;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorCancelListener;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorCompletedListener;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorDialog;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;
import com.sgiroux.diyecmgauge.utils.CompressUtils;
import com.sgiroux.diyecmgauge.utils.EmailUtils;

/**
 * Activity to browse the data logs on the gauge. Also allow to download and delete them.
 * 
 * @author Seb
 */
public class BrowseDataLogsActivity extends Activity implements SerialResponseReceivedListener {
	private DataLogsController mController;

	private DataLoggingStatus mDataLoggingStatusFlag = DataLoggingStatus.NO_SD_CARD;

	private TextView mDataLogsStats;
	private TextView mDataLoggingStatus;
	private DataLogRowAdapter mDataLogsArrayAdapter;

	private ListView mDataLogsList;

	private ProgressMonitorBase mProgressMonitor = new ProgressMonitorDialog();
	private boolean mEmailAfterDownload = false;

	/**
	 * Called when the activity is first created
	 * 
	 * @param savedInstanceState If the activity is being re-initialized after previously being shut down then this Bundle contains the data it most
	 * recently supplied in onSaveInstanceState(Bundle). Note: Otherwise it is null.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_browse_data_logs);

		mController = new DataLogsController();

		mDataLogsStats = (TextView) findViewById(R.id.data_logs_stats);

		mDataLogsList = (ListView) findViewById(android.R.id.list);
		mDataLogsList.setItemsCanFocus(true);

		mDataLoggingStatus = (TextView) findViewById(R.id.data_logging_status);

		prepareProgressMonitor();

		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);

		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.DATA_LOGS_DIRECTORY_LISTING));
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SD_CARD_GET_STATS));
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.DATA_LOGGING_STATUS));

		mDataLogsArrayAdapter = new DataLogRowAdapter(this);
		mDataLogsArrayAdapter.setCallback(new DataLogRowAdapterCallback(this));

		mDataLogsList.setAdapter(mDataLogsArrayAdapter);
	}

	@Override
	public void onPause() {
		super.onPause();

		DIYECMGauge.getInstance().removeSerialResponseReceivedListener(this);
	}

	/**
	 * Show the dialog to let the user select a file name for the data log to be downloaded
	 * 
	 * @param selectedRow The data log row associated with the data log to download
	 * @param emailAfterDownload true if the file should be emailed after download, false otherwise
	 */
	public void downloadDataLog(final DataLogRow selectedRow, final boolean emailAfterDownload) {
		FileChooserDialog fileChooserUpload = new FileChooserDialog(this, DIYECMGaugeSettings.getDataLogsDirectory(), ".msl", FileChooserType.SAVE);
		fileChooserUpload.show();
		fileChooserUpload.setFileName(selectedRow.getDataLogName());
		fileChooserUpload.setDialogResult(new OnFileChoosenResult() {
			@Override
			public void finish(File file) {
				mEmailAfterDownload = emailAfterDownload;
				mController.startDataLogDownload(file, selectedRow.getDataLogSize());
			}
		});
	}

	/**
	 * Inflate the menu; this adds items to the action bar if it is present.
	 * 
	 * @param menu The menu that is created
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.browse_data_logs, menu);
		return true;
	}

	/**
	 * Called when the menu should be prepared. We update the menu based on data logging state.
	 * 
	 * @param menu The menu to prepare
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem toggleDataLogging = menu.findItem(R.id.action_toggle_data_logging);

		switch (mDataLoggingStatusFlag) {
			case DATA_LOGGING:
				toggleDataLogging.setTitle(R.string.stop_data_logging);
				toggleDataLogging.setEnabled(true);
				break;

			case NOT_DATA_LOGGING:
				toggleDataLogging.setTitle(R.string.start_data_logging);
				toggleDataLogging.setEnabled(true);
				break;

			case NO_SD_CARD:
				toggleDataLogging.setTitle(R.string.start_data_logging);
				toggleDataLogging.setEnabled(false);
				break;
		}

		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * When a menu item is selected, this get triggered and we figure out which item has been selected and trigger the associated action.
	 * 
	 * @param item The menu item that has been selected
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_toggle_data_logging:
				mController.toggleDataLogging(mDataLoggingStatusFlag);
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Prepare the progress monitor shown when downloading data logs
	 */
	private void prepareProgressMonitor() {
		mProgressMonitor = new ProgressMonitorDialog();
		mProgressMonitor.setParent(this);
		mProgressMonitor.setDescription(getString(R.string.downloading_file));
		mProgressMonitor.addCompletedListener(new ProgressMonitorCompletedListener() {
			@Override
			public void onProgressMonitorCompleted() {
				dataLogDownloadDone();
			}
		});
		mProgressMonitor.addCancelListener(new ProgressMonitorCancelListener() {
			@Override
			public void onProgressMonitorCancel() {
				dataLogDownloadDone();
			}
		});
	}

	/**
	 * Should be called once done downloading a data log (or cancelling). Trigger the email manager if the user wanted the data log to be sent by
	 * email.
	 */
	private void dataLogDownloadDone() {
		mController.closeDataLog();

		if (mEmailAfterDownload) {
			String dataLogPath = DIYECMGaugeSettings.getDataLogsDirectory().getAbsolutePath() + File.separator + mController.getCurrentDataLogFileName();

			List<String> paths = new ArrayList<String>();
			paths.add(dataLogPath);

			String emailText = String.format(getString(R.string.email_body), getString(R.string.app_name));
			String subject = String.format(getString(R.string.email_subject), getString(R.string.app_name), System.currentTimeMillis());
			String zipFileName = dataLogPath + CompressUtils.ZIP_EXTENSION;

			EmailUtils.email(BrowseDataLogsActivity.this, "", null, subject, emailText, paths, zipFileName);

			mEmailAfterDownload = false;
		}
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(final SerialCommand serialCommand, final byte[] response) {
		String textResponse;

		switch (serialCommand) {
			case SD_CARD_GET_STATS:
				textResponse = SerialUtils.getTextResponse(response);
				final String stats = mController.getDataLogsStats(textResponse);
				mDataLogsStats.setText(stats);

				break;

			case DATA_LOGGING_STATUS:
				textResponse = SerialUtils.getTextResponse(response);
				mDataLoggingStatusFlag = DataLoggingStatus.values()[Integer.parseInt(textResponse)];
				mDataLoggingStatus.setText(mDataLoggingStatusFlag.getMessage());

				break;

			case DATA_LOGS_DIRECTORY_LISTING:
				textResponse = SerialUtils.getTextResponse(response);
				ArrayList<DataLogRow> dataLogRows = mController.getDataLogListing(textResponse);
				mDataLogsArrayAdapter.setResults(dataLogRows);
				break;

			case DATA_LOGS_DELETE_FILE:
				textResponse = SerialUtils.getTextResponse(response);
				mController.deleteDataLog(textResponse);
				break;

			case DATA_LOGS_GET_FILE:
				mController.getDownloadDataLogChunkResponse(SerialUtils.getBytesResponse(response), mProgressMonitor);
				break;

			default:
				break;
		}
	}
}
