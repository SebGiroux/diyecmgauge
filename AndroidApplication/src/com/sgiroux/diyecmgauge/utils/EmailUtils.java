package com.sgiroux.diyecmgauge.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.sgiroux.diyecmgauge.R;

/**
 * Helper class that is used to send an email
 */
public class EmailUtils {
	private static final String ZIP_CONTENT_TYPE = "application/zip";
	private static final String PLAIN_CONTENT_TYPE = "plain/text";

	/**
	 * Function used to send an email. Can take multiple files in parameter, they will be compressed (zipped) and added as an attachment.
	 * 
	 * @param context Current context where the email will be send
	 * @param emailTo Email address to send the email to
	 * @param emailCC Email address to send carbon copy
	 * @param subject Subject of the email
	 * @param emailText Text of the email
	 * @param filePaths Files to send
	 * @param zipFileName The name for the zip file
	 */
	public static void email(Context context, String emailTo, String emailCC, String subject, String emailText, List<String> filePaths, String zipFileName) {
		List<String> actualFiles = new ArrayList<String>();

		if (filePaths != null) {
			for (String name : filePaths) {
				if (name != null) {
					File file = new File(name);

					if (file.exists() && file.canRead()) {
						actualFiles.add(name);
					}
				}
			}
		}

		// Need to "send multiple" to get more than one attachment
		final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] { emailTo });
		if (emailCC != null) {
			emailIntent.putExtra(android.content.Intent.EXTRA_CC, new String[] { emailCC });
		}

		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, emailText);

		if (actualFiles.size() > 0) {
			emailIntent.setType(ZIP_CONTENT_TYPE);
			File zipFile = new File(zipFileName);
			CompressUtils compressFiles = new CompressUtils(actualFiles, zipFile.getAbsolutePath());

			compressFiles.zip();

			emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(zipFile));
		}
		else {
			emailIntent.setType(PLAIN_CONTENT_TYPE);
		}

		context.startActivity(Intent.createChooser(emailIntent, context.getString(R.string.send_email)));
	}
}