package com.sgiroux.diyecmgauge.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import android.util.Log;

/**
 * Utility class that compress a list of data log files into a ZIP format file
 */
public class CompressUtils {
	private static final String LOG_TAG = "CompressUtils";
	public static final String ZIP_EXTENSION = ".zip";
	private static final int BUFFER = 2048;

	private List<String> mFiles;
	private String mZipFile;

	/**
	 * Constructor
	 * 
	 * @param files The array of file names to compress
	 * @param zipFile The resulting ZIP file name
	 */
	public CompressUtils(List<String> files, String zipFile) {
		mFiles = files;
		mZipFile = zipFile;
	}

	/**
	 * Main method that compress the data log files into a single ZIP file. It will create the ZIP file at the specified destination. Data log files
	 * will stay intact
	 */
	public void zip() {
		try {
			BufferedInputStream origin = null;
			FileOutputStream dest = new FileOutputStream(mZipFile);

			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));

			byte data[] = new byte[BUFFER];

			for (String fileName : mFiles) {
				Log.v(LOG_TAG, String.format("Adding: %s", fileName));
				FileInputStream fi = new FileInputStream(fileName);
				origin = new BufferedInputStream(fi, BUFFER);
				ZipEntry entry = new ZipEntry(fileName.substring(fileName.lastIndexOf("/") + 1));
				out.putNextEntry(entry);
				int count;
				while ((count = origin.read(data, 0, BUFFER)) != -1) {
					out.write(data, 0, count);
				}
				origin.close();
			}

			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
