/*
 * Copyright (C) 2013 Keisuke SUZUKI
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * Distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sgiroux.diyecmgauge.usb;

public class UsbVidPid {
	private int mVid;
	private int mPid;

	public UsbVidPid(int vid, int pid) {
		mVid = vid;
		mPid = pid;
	}

	public void setVid(int vid) {
		mVid = vid;
	}

	public void setPid(int pid) {
		mPid = pid;
	}

	public int getVid() {
		return mVid;
	}

	public int getPid() {
		return mPid;
	}
}
