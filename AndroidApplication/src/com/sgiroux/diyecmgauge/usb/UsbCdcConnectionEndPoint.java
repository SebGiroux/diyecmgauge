package com.sgiroux.diyecmgauge.usb;

import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;

class UsbCdcConnectionEndPoint {
	private UsbDeviceConnection mConnection;
	private UsbEndpoint mEndpointIn;
	private UsbEndpoint mEndpointOut;

	public UsbCdcConnectionEndPoint(UsbDeviceConnection connection, UsbEndpoint endpointIn, UsbEndpoint endpointOut) {
		mConnection = connection;
		mEndpointIn = endpointIn;
		mEndpointOut = endpointOut;
	}

	public UsbDeviceConnection getConnection() {
		return mConnection;
	}

	public UsbEndpoint getEndpointIn() {
		return mEndpointIn;
	}

	public UsbEndpoint getEndpointOut() {
		return mEndpointOut;
	}
}