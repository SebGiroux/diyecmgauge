/*
 * Copyright (C) 2013 Keisuke SUZUKI
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * Distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sgiroux.diyecmgauge.usb;

import android.content.Context;
import android.hardware.usb.UsbDevice;

public class Physicaloid {
	private Context mContext;

	protected SerialCommunicator mSerial;

	private static final Object LOCK = new Object();
	protected static final Object LOCK_WRITE = new Object();
	protected static final Object LOCK_READ = new Object();

	public static final int TEENSY_VID = 0x16C0;

	public Physicaloid(Context context) {
		mContext = context;
	}

	/**
	 * Opens a device and communicate USB UART by default settings
	 * 
	 * @return true : successful , false : fail
	 * @throws RuntimeException
	 */
	public boolean open() throws RuntimeException {
		return open(new UartConfig());
	}

	/**
	 * Opens a device and communicate USB UART
	 * 
	 * @param uart UART configuration
	 * @return true : successful , false : fail
	 * @throws RuntimeException
	 */
	public boolean open(UartConfig uart) throws RuntimeException {
		synchronized (LOCK) {
			if (mSerial == null) {
				mSerial = getSerialCommunicator(mContext);
				if (mSerial == null)
					return false;
			}
			if (mSerial.open()) {
				mSerial.setUartConfig(uart);
				return true;
			}
			else {
				return false;
			}
		}
	}

	public SerialCommunicator getSerialCommunicator(Context context) {
		UsbAccessor usbAccess = UsbAccessor.INSTANCE;
		usbAccess.init(context);

		for (UsbDevice device : usbAccess.manager().getDeviceList().values()) {
			int vid = device.getVendorId();
			if (vid == Physicaloid.TEENSY_VID) {
				return new UartCdcAcm(context);
			}
		}

		return null;
	}

	/**
	 * Closes a device.
	 * 
	 * @return true : successful , false : fail
	 * @throws RuntimeException
	 */
	public boolean close() throws RuntimeException {
		synchronized (LOCK) {
			if (mSerial == null)
				return true;
			if (mSerial.close()) {
				mSerial = null;
				return true;
			}
			else {
				return false;
			}
		}
	}

	/**
	 * Reads from a device
	 * 
	 * @param millis timeout in milliseconds
	 * @return read byte size
	 * @throws RuntimeException
	 */
	public byte[] read(int millis) throws RuntimeException {
		synchronized (LOCK_READ) {
			if (mSerial == null)
				return new byte[] {};
			return mSerial.read(millis);
		}
	}

	/**
	 * Writes to a device.
	 * 
	 * @param buf
	 * @param size
	 * @return written byte size
	 * @throws RuntimeException
	 */
	public int write(byte[] buf, int size) throws RuntimeException {
		synchronized (LOCK_WRITE) {
			if (mSerial == null)
				return 0;
			return mSerial.write(buf, size);
		}
	}

	/**
	 * Gets opened or closed status
	 * 
	 * @return true : opened, false : closed
	 * @throws RuntimeException
	 */
	public boolean isOpened() throws RuntimeException {
		synchronized (LOCK) {
			if (mSerial == null)
				return false;
			return mSerial.isOpened();
		}
	}

	/**
	 * Sets Serial Configuration
	 * 
	 * @param settings
	 */
	public void setConfig(UartConfig settings) throws RuntimeException {
		synchronized (LOCK) {
			if (mSerial == null)
				return;
			mSerial.setUartConfig(settings);
		}
	}

	/**
	 * Sets Baud Rate
	 * 
	 * @param baudrate any baud-rate e.g. 9600
	 * @return true : successful, false : fail
	 */
	public boolean setBaudrate(int baudrate) throws RuntimeException {
		synchronized (LOCK) {
			if (mSerial == null)
				return false;
			return mSerial.setBaudrate(baudrate);
		}
	}

	/**
	 * Sets Data Bits
	 * 
	 * @param dataBits data bits e.g. UartConfig.DATA_BITS8
	 * @return true : successful, false : fail
	 */
	public boolean setDataBits(int dataBits) throws RuntimeException {
		synchronized (LOCK) {
			if (mSerial == null)
				return false;
			return mSerial.setDataBits(dataBits);
		}
	}

	/**
	 * Sets Parity Bits
	 * 
	 * @param parity parity bits e.g. UartConfig.PARITY_NONE
	 * @return true : successful, false : fail
	 */
	public boolean setParity(int parity) throws RuntimeException {
		synchronized (LOCK) {
			if (mSerial == null)
				return false;
			return mSerial.setParity(parity);
		}
	}

	/**
	 * Sets Stop bits
	 * 
	 * @param stopBits stop bits e.g. UartConfig.STOP_BITS1
	 * @return true : successful, false : fail
	 */
	public boolean setStopBits(int stopBits) throws RuntimeException {
		synchronized (LOCK) {
			if (mSerial == null)
				return false;
			return mSerial.setStopBits(stopBits);
		}
	}

	/**
	 * Sets flow control DTR/RTS
	 * 
	 * @param dtrOn true then DTR on
	 * @param rtsOn true then RTS on
	 * @return true : successful, false : fail
	 */
	public boolean setDtrRts(boolean dtrOn, boolean rtsOn) throws RuntimeException {
		synchronized (LOCK) {
			if (mSerial == null)
				return false;
			return mSerial.setDtrRts(dtrOn, rtsOn);
		}
	}
}
