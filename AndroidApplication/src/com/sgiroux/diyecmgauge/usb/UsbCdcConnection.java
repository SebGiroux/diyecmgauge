/*
 * Copyright (C) 2013 Keisuke SUZUKI
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * Distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sgiroux.diyecmgauge.usb;

import android.content.Context;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.util.Log;
import android.util.SparseArray;

public class UsbCdcConnection {
	private static final String LOG_TAG = UsbCdcConnection.class.getSimpleName();

	private UsbAccessor mUsbAccess;

	private int mCdcAcmInterfaceNumber;

	private SparseArray<UsbCdcConnectionEndPoint> mUsbConnectionEndPoints;

	public UsbCdcConnection(Context context) {
		mUsbAccess = UsbAccessor.INSTANCE;
		mUsbAccess.init(context);
		mCdcAcmInterfaceNumber = 0;
		mUsbConnectionEndPoints = new SparseArray<UsbCdcConnectionEndPoint>();
	}

	/**
	 * Open first device with VID and PID
	 * 
	 * @param ids vid and pid
	 * @return true : open successful, false : open fail
	 */
	public boolean open(UsbVidPid ids) {
		return open(ids, false, 0);
	}

	/**
	 * Open first CDC-ACM device with VID and PID
	 * 
	 * @param ids vid and pid
	 * @param isCdcAcm true then search only cdc-acm
	 * @return true : open successful, false : open fail
	 */
	public boolean open(UsbVidPid ids, boolean isCdcAcm) {
		return open(ids, isCdcAcm, 0);
	}

	/**
	 * Open channel-th device with VID and PID
	 * 
	 * @param ids vid and pid
	 * @param channel channel
	 * @return true : open successful, false : open fail
	 */
	public boolean open(UsbVidPid ids, boolean isCdcAcm, int channel) {
		if (ids == null) {
			return false;
		}

		int deviceNumber = 0;
		int channelNumber = 0;

		for (UsbDevice usbdev : mUsbAccess.manager().getDeviceList().values()) {
			if (usbdev.getVendorId() == ids.getVid() && (ids.getPid() == 0 || ids.getPid() == usbdev.getProductId())) {
				for (int intfNum = 0; intfNum < usbdev.getInterfaceCount(); intfNum++) {
					if ((isCdcAcm && (usbdev.getInterface(intfNum).getInterfaceClass() == UsbConstants.USB_CLASS_CDC_DATA)) || !isCdcAcm) {
						if (channel == channelNumber) {
							if (!mUsbAccess.deviceIsConnected(deviceNumber) && mUsbAccess.openDevice(deviceNumber, intfNum, channel)) {
								Log.d(LOG_TAG, "Find VID:" + Integer.toHexString(usbdev.getVendorId()) + ", PID:" + Integer.toHexString(usbdev.getProductId()) + ", DevNum:" + deviceNumber + ", IntfNum:" + intfNum);

								mUsbConnectionEndPoints.put(channel, new UsbCdcConnectionEndPoint(mUsbAccess.connection(channel), getEndpoint(deviceNumber, intfNum, UsbConstants.USB_DIR_IN), getEndpoint(deviceNumber, intfNum, UsbConstants.USB_DIR_OUT)));
								mCdcAcmInterfaceNumber = intfNum;
								return true;
							}
							channelNumber++;
						}
					}
				}
			}
			deviceNumber++;
		}

		Log.d(LOG_TAG, "Cannot find VID:" + ids.getVid() + ", PID:" + ids.getPid());

		return false;
	}

	private UsbEndpoint getEndpoint(int deviceNumber, int interfaceNumber, int usbDir) {
		UsbInterface usbInterface = mUsbAccess.intface(deviceNumber, interfaceNumber);
		if (usbInterface == null) {
			return null;
		}

		for (int i = 0; i < usbInterface.getEndpointCount(); i++) {
			UsbEndpoint usbEndPoint = mUsbAccess.endpoint(deviceNumber, interfaceNumber, i);
			if (usbEndPoint == null) {
				return null;
			}
			if (usbEndPoint.getDirection() == usbDir) {
				return usbEndPoint;
			}
		}
		return null;
	}

	/**
	 * Closes devices
	 */
	public boolean close() {
		mUsbConnectionEndPoints.clear();
		return mUsbAccess.closeAll();
	}

	/**
	 * Gets the CDC-ACM interface's number
	 * 
	 * @return interface number
	 */
	public int getCdcAcmInterfaceNum() {
		return mCdcAcmInterfaceNumber;
	}

	/**
	 * Gets UsbDeviceConnection for CDC
	 * 
	 * @return UsbDeviceConnection or null
	 */
	public UsbDeviceConnection getConnection() {
		return getConnection(0);
	}

	/**
	 * Gets UsbDeviceConnection for CDC
	 * 
	 * @param channel channel
	 * @return UsbDeviceConnection or null
	 */
	public UsbDeviceConnection getConnection(int channel) {
		UsbCdcConnectionEndPoint connnectionEndPoint = mUsbConnectionEndPoints.get(channel);
		if (connnectionEndPoint == null) {
			return null;
		}
		return connnectionEndPoint.getConnection();
	}

	/**
	 * Gets IN UsbEndpoint for CDC
	 * 
	 * @return UsbEndpoint or null
	 */
	public UsbEndpoint getEndpointIn() {
		return getEndpointIn(0);
	}

	/**
	 * Gets IN UsbEndpoint for CDC
	 * 
	 * @param ch channel
	 * @return UsbEndpoint or null
	 */
	public UsbEndpoint getEndpointIn(int ch) {
		UsbCdcConnectionEndPoint con = mUsbConnectionEndPoints.get(ch);
		if (con == null)
			return null;
		return con.getEndpointIn();
	}

	/**
	 * Gets OUT UsbEndpoint for CDC
	 * 
	 * @return UsbEndpoint or null
	 */
	public UsbEndpoint getEndpointOut() {
		return getEndpointOut(0);
	}

	/**
	 * Gets OUT UsbEndpoint for CDC
	 * 
	 * @param ch channel
	 * @return UsbEndpoint or null
	 */
	public UsbEndpoint getEndpointOut(int ch) {
		UsbCdcConnectionEndPoint con = mUsbConnectionEndPoints.get(ch);
		if (con == null)
			return null;
		return con.getEndpointOut();
	}
}
