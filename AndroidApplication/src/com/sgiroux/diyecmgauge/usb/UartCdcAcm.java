/*
 * Copyright (C) 2013 Keisuke SUZUKI
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * Distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sgiroux.diyecmgauge.usb;

import android.content.Context;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.util.Log;

public class UartCdcAcm extends SerialCommunicator {
	private static final String TAG = UartCdcAcm.class.getSimpleName();

	private static final int DEFAULT_BAUDRATE = 9600;

	private UsbCdcConnection mUsbConnetionManager;

	private UartConfig mUartConfig;
	private static final int USB_READ_BUFFER_SIZE = 16384;
	private static final int USB_WRITE_BUFFER_SIZE = 256;

	private UsbDeviceConnection mConnection;
	private UsbEndpoint mEndpointIn;
	private UsbEndpoint mEndpointOut;
	private int mInterfaceNum;

	private boolean mIsOpened;

	public UartCdcAcm(Context context) {
		super(context);

		mUsbConnetionManager = new UsbCdcConnection(context);
		mUartConfig = new UartConfig();
		mIsOpened = false;
	}

	@Override
	public boolean open() {
		if (open(new UsbVidPid(Physicaloid.TEENSY_VID, 0))) {
			return true;
		}

		return false;
	}

	public boolean open(UsbVidPid ids) {
		if (mUsbConnetionManager.open(ids, true)) {
			mConnection = mUsbConnetionManager.getConnection();
			mEndpointIn = mUsbConnetionManager.getEndpointIn();
			mEndpointOut = mUsbConnetionManager.getEndpointOut();
			mInterfaceNum = mUsbConnetionManager.getCdcAcmInterfaceNum();

			if (!init()) {
				return false;
			}

			if (!setBaudrate(DEFAULT_BAUDRATE)) {
				return false;
			}

			mIsOpened = true;
			return true;
		}
		return false;
	}

	@Override
	public boolean close() {
		mIsOpened = false;
		return mUsbConnetionManager.close();
	}

	@Override
	public byte[] read(int millis) {
		int len = 0;
		byte[] rbuf = new byte[16384];

		try {
			len = mConnection.bulkTransfer(mEndpointIn, rbuf, rbuf.length, millis);
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}

		if (len == USB_READ_BUFFER_SIZE) {
			Log.w(TAG, "USB read buffer is maxing out");
		}

		byte[] data = new byte[len];
		System.arraycopy(rbuf, 0, data, 0, len);

		return data;
	}

	@Override
	public int write(byte[] buf, int size) {
		if (buf == null) {
			return 0;
		}
		int offset = 0;
		int write_size;
		int written_size;
		byte[] wbuf = new byte[USB_WRITE_BUFFER_SIZE];

		while (offset < size) {
			write_size = USB_WRITE_BUFFER_SIZE;

			if (offset + write_size > size) {
				write_size = size - offset;
			}
			System.arraycopy(buf, offset, wbuf, 0, write_size);

			written_size = mConnection.bulkTransfer(mEndpointOut, wbuf, write_size, 100);

			if (written_size < 0) {
				return -1;
			}
			offset += written_size;
		}

		return offset;
	}

	/**
	 * Sets Uart configurations
	 * 
	 * @param config configurations
	 * @return true : successful, false : fail
	 */
	public boolean setUartConfig(UartConfig config) {
		boolean res = true;
		boolean ret = true;
		
		if (mUartConfig.mBaudrate != config.mBaudrate) {
			res = setBaudrate(config.mBaudrate);
			ret = ret && res;
		}

		if (mUartConfig.mDataBits != config.mDataBits) {
			res = setDataBits(config.mDataBits);
			ret = ret && res;
		}

		if (mUartConfig.mParity != config.mParity) {
			res = setParity(config.mParity);
			ret = ret && res;
		}

		if (mUartConfig.mStopBits != config.mStopBits) {
			res = setStopBits(config.mStopBits);
			ret = ret && res;
		}

		if (mUartConfig.mDtrOn != config.mDtrOn || mUartConfig.mRtsOn != config.mRtsOn) {
			res = setDtrRts(config.mDtrOn, config.mRtsOn);
			ret = ret && res;
		}

		return ret;
	}

	/**
	 * Initializes CDC communication
	 * 
	 * @return true : successful, false : fail
	 */
	private boolean init() {
		if (mConnection == null) {
			return false;
		}

		int ret = mConnection.controlTransfer(0x21, 0x22, 0x00, mInterfaceNum, null, 0, 0); // init CDC
		if (ret < 0) {
			return false;
		}

		return true;
	}

	@Override
	public boolean isOpened() {
		return mIsOpened;
	}

	/**
	 * Sets baudrate
	 * 
	 * @param baudrate baudrate e.g. 9600
	 * @return true : successful, false : fail
	 */
	public boolean setBaudrate(int baudrate) {
		byte[] baudByte = new byte[4];

		baudByte[0] = (byte) (baudrate & 0x000000FF);
		baudByte[1] = (byte) ((baudrate & 0x0000FF00) >> 8);
		baudByte[2] = (byte) ((baudrate & 0x00FF0000) >> 16);
		baudByte[3] = (byte) ((baudrate & 0xFF000000) >> 24);

		int ret = mConnection.controlTransfer(0x21, 0x20, 0, mInterfaceNum, new byte[] { baudByte[0], baudByte[1], baudByte[2], baudByte[3], 0x00, 0x00, 0x08 }, 7, 100);
		if (ret < 0) {
			Log.d(TAG, "Fail to setBaudrate");
			return false;
		}

		mUartConfig.mBaudrate = baudrate;

		return true;
	}

	/**
	 * Sets Data bits
	 * 
	 * @param dataBits data bits e.g. UartConfig.DATA_BITS8
	 * @return true : successful, false : fail
	 */
	public boolean setDataBits(int dataBits) {
		Log.d(TAG, "Fail to setDataBits");

		mUartConfig.mDataBits = dataBits;

		return false;
	}

	/**
	 * Sets Parity bit
	 * 
	 * @param parity parity bits e.g. UartConfig.PARITY_NONE
	 * @return true : successful, false : fail
	 */
	public boolean setParity(int parity) {
		Log.d(TAG, "Fail to setParity");

		mUartConfig.mParity = parity;

		return false;
	}

	/**
	 * Sets Stop bits
	 * 
	 * @param stopBits stop bits e.g. UartConfig.STOP_BITS1
	 * @return true : successful, false : fail
	 */
	public boolean setStopBits(int stopBits) {
		Log.d(TAG, "Fail to setStopBits");

		mUartConfig.mStopBits = stopBits;

		return false;
	}

	@Override
	public boolean setDtrRts(boolean dtrOn, boolean rtsOn) {
		int ctrlValue = 0x0000;

		if (dtrOn) {
			ctrlValue |= 0x0001;
		}

		if (rtsOn) {
			ctrlValue |= 0x0002;
		}

		int ret = mConnection.controlTransfer(0x21, 0x22, ctrlValue, mInterfaceNum, null, 0, 100);
		if (ret < 0) {
			Log.d(TAG, "Fail to setDtrRts");

			return false;
		}

		mUartConfig.mDtrOn = dtrOn;
		mUartConfig.mRtsOn = rtsOn;

		return true;
	}

	@Override
	public UartConfig getUartConfig() {
		return mUartConfig;
	}

	@Override
	public int getBaudrate() {
		return mUartConfig.mBaudrate;
	}

	@Override
	public int getDataBits() {
		return mUartConfig.mDataBits;
	}

	@Override
	public int getParity() {
		return mUartConfig.mParity;
	}

	@Override
	public int getStopBits() {
		return mUartConfig.mStopBits;
	}

	@Override
	public boolean getDtr() {
		return mUartConfig.mDtrOn;
	}

	@Override
	public boolean getRts() {
		return mUartConfig.mRtsOn;
	}
}
