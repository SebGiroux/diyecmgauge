package com.sgiroux.diyecmgauge.serial;

import android.content.Intent;

public class IntentPacket {
	private int mRequestCode;
	private Intent mIntent;

	public IntentPacket(Intent intent, int requestCode) {
		mIntent = intent;
		mRequestCode = requestCode;
	}

	public int getRequestCode() {
		return mRequestCode;
	}

	public Intent getIntent() {
		return mIntent;
	}
}