package com.sgiroux.diyecmgauge.serial;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;

import com.sgiroux.diyecmgauge.DIYECMGauge;

public class SerialCommThread extends SerialCommThreadBase {
	private Context mContext;

	/**
	 * Initialize the thread
	 * 
	 * @param context
	 */
	public SerialCommThread(Context context) {
		mContext = context;
	}

	/**
	 * Write an array of byte to the serial line for the specified serial command and arguments
	 * 
	 * @param serialCommand The serial command to be written
	 * @param arguments The arguments of the serial command to be written
	 */
	@Override
	public void write(SerialCommand serialCommand, String arguments) {
		byte[] command = SerialUtils.prepareCommand(serialCommand, arguments);

		SerialCommConnection connection = SerialCommConnectionFactory.getInstance().getConnection();

		connection.write(SerialCommConnection.UNEXPECTED_RESPONSE_LENGTH, command, 0, null);

		int timeout = SERIAL_COMMAND_REGULAR_TIME_OUT;
		if (!isSerialCommandRegularTimeout(serialCommand)) {
			timeout = SERIAL_COMMAND_SLOW_COMMAND_TIME_OUT;
		}

		byte[] data = connection.readBytes(timeout, SerialUtils.SERIAL_RESPONSE_TERMINATION_CHARACTER);
		if (data != null) {
			for (byte d : data) {
				addByteToBuffer(d);
			}
		}
	}

	/**
	 * Trigger all the listeners to tell them we got the response for a serial command that we sent
	 * 
	 * @param buffer The buffer of the response that will be sent to the listeners
	 */
	@Override
	protected void triggerAllSerialResponseReceivedListeners(final ArrayList<Byte> buffer) {
		final SerialCommand serialCommand = getSerialCommandInBuffer();

		if (serialCommand != null) {
			// Create a copy of the buffer so that when the UI thread process the serial response,
			// the content of the array list is still the same
			int bufferLength = buffer.size();
			final byte[] bufferCopy = new byte[bufferLength];
			for (int i = 0; i < bufferLength; i++) {
				bufferCopy[i] = buffer.get(i);
			}

			for (final SerialResponseReceivedListener responseReceivedListener : DIYECMGauge.getInstance().getSerialResponseReceivedListeners()) {
				((Activity) mContext).runOnUiThread(new Runnable() {
					@Override
					public void run() {
						responseReceivedListener.onResponseReceived(serialCommand, bufferCopy);
					}
				});
			}
		}
	}

	@Override
	protected void triggerAllSerialResponseTimeoutListeners(final SerialCommand serialCommand) {
		for (final SerialResponseTimeoutListener timeoutListener : DIYECMGauge.getInstance().getSerialResponseTimeoutListeners()) {
			((Activity) mContext).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					timeoutListener.onResponseTimeout(serialCommand);
				}
			});
		}
	}
}
