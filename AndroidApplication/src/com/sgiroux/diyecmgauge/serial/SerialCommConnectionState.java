package com.sgiroux.diyecmgauge.serial;

public enum SerialCommConnectionState {
	DISCONNECTED("Disconnected"), CONNECTING("Connecting"), CONNECTED("Connected");

	private String mText;

	private SerialCommConnectionState(String text) {
		mText = text;
	}

	public String getText() {
		return mText;
	}
}