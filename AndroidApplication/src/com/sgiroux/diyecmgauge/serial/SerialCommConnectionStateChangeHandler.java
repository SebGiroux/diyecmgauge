package com.sgiroux.diyecmgauge.serial;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.DIYECMGaugeApplication;
import com.sgiroux.diyecmgauge.list.AlarmsList;
import com.sgiroux.diyecmgauge.list.IndicatorsList;
import com.sgiroux.diyecmgauge.list.PagesList;
import com.sgiroux.diyecmgauge.list.SettingsList;

public class SerialCommConnectionStateChangeHandler extends Handler {
	private static final String LOGTAG = "CommsConnectionStateChangeHandler";

	public static final int MSG_CONNECTING = 1;
	public static final int MSG_CONNECTED = 2;
	public static final int MSG_DISCONNECTED = 3;
	public static final int MSG_CONNECTION_LOST = 4;
	public static final int MSG_CONNECTION_FAIL = 5;

	@Override
	public void handleMessage(final Message message) {
		Log.d(LOGTAG, "Connection state handler message: " + message.what);

		switch (message.what) {
			case MSG_CONNECTING:
				DIYECMGaugeApplication.getInstance().setConnectionState(SerialCommConnectionState.CONNECTING);
				break;

			case MSG_CONNECTED:
				DIYECMGaugeApplication.getInstance().setConnectionState(SerialCommConnectionState.CONNECTED);

				DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_VERSION));

				// Initialize the indicators list
				IndicatorsList.getInstance().initialize();

				// Initialize the pages list
				PagesList.getInstance().initialize();

				// Initialize the alarms list
				AlarmsList.getInstance().initialize();

				// Initialize the settings list
				SettingsList.getInstance().initialize();
				break;

			case MSG_DISCONNECTED:
			case MSG_CONNECTION_LOST:
			case MSG_CONNECTION_FAIL:
				DIYECMGaugeApplication.getInstance().setConnectionState(SerialCommConnectionState.DISCONNECTED);
				break;

			default:
				Log.e(LOGTAG, String.format("Invalid connection state %s", message.what));
				break;
		}
	}
}