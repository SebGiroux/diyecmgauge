package com.sgiroux.diyecmgauge.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;

import com.sgiroux.diyecmgauge.DIYECMGaugeApplication;
import com.sgiroux.diyecmgauge.activity.BluetoothDeviceListActivity;
import com.sgiroux.diyecmgauge.activity.DashboardActivity;
import com.sgiroux.diyecmgauge.utils.StringUtils;

/**
 * Class that deal with Bluetooth connection to the gauge. This class was taken from "MSDroid".
 * 
 * @author Trevor Page
 * 
 */
class BluetoothConnection extends SerialCommConnection {
	private static final String LOGTAG = "BluetoothConnection";

	/** UUID for Serial Port Profile */
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothDevice mBluetoothDevice;

	private ConnectRunnable mConnectRunnableObj;
	private Thread mConnectThread;

	private ConnectedRunnable mConnectedRunnableObj;
	private Thread mConnectedThread;

	private static final int READ_BUFFER_SIZE = 16384; // Make sure this is always at least double of DataLogsController.DATA_LOG_CHUNK_SIZE
	private CircularByteBuffer mCircularBuffer;

	private String mMacAddressStr;

	/**
	 * 
	 * @param instanceType
	 */
	public BluetoothConnection() {
		mCircularBuffer = new CircularByteBuffer(READ_BUFFER_SIZE);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			return;
		}

		if (!mBluetoothAdapter.isEnabled()) {
			return;
		}
	}

	@Override
	public boolean isUsable() {
		if (!mBluetoothAdapter.isEnabled()) {
			return false;
		}

		final String macAddressStr = DIYECMGaugeApplication.getInstance().getSettings().getBluetoothMACAddress();
		if (macAddressStr != null) {
			setMACAddr(macAddressStr);
			return true;
		}
		return false;
	}

	private synchronized void setMACAddr(String MAC) {
		mMacAddressStr = MAC;
	}

	@Override
	public synchronized boolean connect() {
		if (isUsable()) {
			mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(mMacAddressStr);
			mConnectRunnableObj = new ConnectRunnable(mBluetoothDevice);
			mConnectThread = new Thread(mConnectRunnableObj);
			mConnectThread.setName(ConnectRunnable.INNER_LOGTAG);
			mConnectThread.start();

			return true;
		}

		return false;
	}

	private synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
		mConnectRunnableObj = null;
		mConnectThread = null;
		mConnectedRunnableObj = new ConnectedRunnable(socket);
		mConnectedThread = new Thread(mConnectedRunnableObj);
		mConnectedThread.setName(ConnectedRunnable.INNER_LOGTAG);
		mConnectedThread.start();
	}

	public synchronized void disconnect() {
		if (mConnectThread != null) {
			mConnectRunnableObj.cancel();
		}

		if (mConnectedThread != null) {
			mConnectedRunnableObj.cancel();
		}

		sendMessage(SerialCommConnectionStateChangeHandler.MSG_DISCONNECTED);
	}

	/**
	 * Write to the ConnectedThread in an unsynchronized manner
	 * 
	 * @param out The bytes to write
	 * @see ConnectedThread#write(byte[])
	 */
	@Override
	public synchronized void writeImpl(byte[] out, int byteInterDelay, int[] waits) {
		if (mConnectedRunnableObj != null) {
			mConnectedRunnableObj.write(out, byteInterDelay, waits);
		}
	}

	@Override
	public byte[] readBytesWithWaitImpl(int millis) {
		synchronized (mCircularBuffer.getBuffer()) {
			if (mExpectedResponseLength > mCircularBuffer.avail() || mExpectedResponseLength == UNEXPECTED_RESPONSE_LENGTH) {
				try {
					mCircularBuffer.getBuffer().wait(millis);
				} catch (InterruptedException e) {

				}
			}

			return mCircularBuffer.popFront();
		}
	}

	private class ConnectRunnable implements Runnable {
		private static final String INNER_LOGTAG = "ConnectRunnable";
		private BluetoothSocket mmSocket;
		private final BluetoothDevice mmDevice;
		private boolean mCancelConnect = false;

		public ConnectRunnable(BluetoothDevice device) {
			mmDevice = device;
		}

		@Override
		public void run() {
			Log.i(INNER_LOGTAG, "Begin ConnectRunnable");

			// Always cancel discovery because it will slow down a connection
			mBluetoothAdapter.cancelDiscovery();

			mmSocket = getSocket();

			if (mmSocket == null) {
				sendMessage(SerialCommConnectionStateChangeHandler.MSG_CONNECTION_FAIL);
			}
			else {
				if (mCancelConnect) {
					try {
						mmSocket.close();
					} catch (IOException e) {
						Log.e(LOGTAG, "close() of connect socket failed", e);
					}
				}
				else {
					synchronized (mCircularBuffer.getBuffer()) {
						mCircularBuffer.reset();
					}

					connected(mmSocket, mmDevice);
				}
			}
		}

		private BluetoothSocket getSocket() {
			try {
				return getSocketByReflection();
			} catch (Exception e) {
				try {
					return getSocketByAPI();
				} catch (Exception e2) {
					return null;
				}
			}
		}

		private BluetoothSocket getSocketByReflection() throws Exception {
			try {
				Method m = null;
				m = mmDevice.getClass().getMethod("createRfcommSocket", new Class[] { int.class });
				BluetoothSocket socket = (BluetoothSocket) m.invoke(mmDevice, Integer.valueOf(1));
				socket.connect();
				return socket;
			} catch (Exception e) {
				Log.e(INNER_LOGTAG, "Reflection socket create or connect exception", e);
				throw e;
			}
		}

		private BluetoothSocket getSocketByAPI() throws Exception {
			try {
				BluetoothSocket socket = mmDevice.createRfcommSocketToServiceRecord(MY_UUID);
				socket.connect();
				return socket;
			} catch (Exception e) {
				Log.e(INNER_LOGTAG, "Standard API socket create or connect exception", e);
				throw e;
			}
		}

		/**
		 * Cancel the connection
		 */
		public void cancel() {
			mCancelConnect = true;
		}
	}

	/**
	 * 
	 *
	 */
	private class ConnectedRunnable implements Runnable {
		private volatile boolean mRun = true;
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;
		private static final String INNER_LOGTAG = "ConnectedRunnable";

		/**
		 * 
		 * @param socket
		 */
		public ConnectedRunnable(BluetoothSocket socket) {
			Log.d(LOGTAG, "Create ConnectedThread");

			mmSocket = socket;

			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(LOGTAG, "Temp sockets creation failed", e);
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;
		}

		/**
		 * 
		 */
		public void cancel() {
			mRun = false;

			if (mmInStream != null) {
				try {
					mmInStream.close();
				} catch (IOException e1) {

				}
			}

			if (mmOutStream != null) {
				try {
					mmOutStream.close();
				} catch (IOException e1) {

				}
			}

			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(LOGTAG, "close() of connect socket failed", e);
			}
		}

		/**
		 * 
		 */
		@Override
		public void run() {
			Log.i(LOGTAG, "Begin connection thread");

			sendMessage(SerialCommConnectionStateChangeHandler.MSG_CONNECTED);

			while (mRun) {
				try {
					mCircularBuffer.pushBack(mmInStream.read(mCircularBuffer.getBuffer(), mCircularBuffer.getHead(), mCircularBuffer.contigousRoom()));

					boolean expectedLengthReached = (mExpectedResponseLength <= mCircularBuffer.avail() && mExpectedResponseLength != UNEXPECTED_RESPONSE_LENGTH);
					boolean expectedTerminationCharacterFound = (mExpectedTerminationCharacter != null && mCircularBuffer.isByteInBuffer((byte) SerialUtils.SERIAL_RESPONSE_TERMINATION_CHARACTER));

					// If we have an expected response length and we've reached/exceeded it
					// or if a termination character was specified and is in the buffer
					if (expectedLengthReached || expectedTerminationCharacterFound) {
						synchronized (mCircularBuffer.getBuffer()) {
							mCircularBuffer.getBuffer().notify();
						}
					}
				} catch (IOException e) {
					if (mRun) {
						sendMessage(SerialCommConnectionStateChangeHandler.MSG_CONNECTION_LOST);
					}
					break;
				}
			}
		}

		/**
		 * Write to the connected OutStream.
		 * 
		 * @param buffer The bytes to write
		 */
		private void write(byte[] buffer) {
			// Don't attempt the write if we have been flagged as not running.
			// The socket is probably already closed and the write will throw.
			if (mRun) {
				Log.d(LOGTAG, "Writing " + StringUtils.bytesToHex(buffer, buffer.length));
				try {
					mmOutStream.write(buffer);
				} catch (IOException e) {
					Log.e(LOGTAG, "Exception during write", e);
				}
			}
		}

		/**
		 * Write to the connected OutStream.
		 * 
		 * @param buffer The bytes to write
		 * @param byteInterDelay
		 * @param specialInterDelays
		 */
		public void write(byte[] buffer, int byteInterDelay, int[] specialInterDelays) {
			flush();

			if (byteInterDelay == 0 && specialInterDelays == null) {
				write(buffer);
			}
			else {
				try {
					for (int i = 0; i < buffer.length; i++) {
						if (i > 0) {
							try {
								Thread.sleep(byteInterDelay);
							} catch (InterruptedException e) {

							}
						}
						mmOutStream.write((int) buffer[i]);
						if (specialInterDelays != null && specialInterDelays.length > i) {
							try {
								Thread.sleep(specialInterDelays[i]);
							} catch (InterruptedException e) {

							}
						}
					}
				} catch (IOException e) {
					Log.e(LOGTAG, "Exception during write", e);
				}
			}
		}

		/**
		 * 
		 */
		public void flush() {
			synchronized (mCircularBuffer.getBuffer()) {
				try {
					mmOutStream.flush();

					while (mmInStream.available() > 0) {
						mmInStream.read();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		public boolean isOpen() {
			return mRun;
		}
	}

	@Override
	public SerialCommConnectionType getType() {
		return SerialCommConnectionType.BLUETOOTH;
	}

	@Override
	public boolean isRequireDataIntent() {
		if (!mBluetoothAdapter.isEnabled() || DIYECMGaugeApplication.getInstance().getSettings().getBluetoothMACAddress() == null) {
			return true;
		}

		return false;
	}

	@Override
	public IntentPacket getUserDataIntent(Activity parentActivity) {
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			return new IntentPacket(enableBtIntent, DashboardActivity.REQUEST_ENABLE_BLUETOOTH);
		}

		String macAddress = DIYECMGaugeApplication.getInstance().getSettings().getBluetoothMACAddress();
		if (macAddress == null) {
			// Launch the DeviceListActivity to see devices and do scan
			Intent serverIntent = new Intent(parentActivity, BluetoothDeviceListActivity.class);
			return new IntentPacket(serverIntent, DashboardActivity.REQUEST_CONNECT_TO_GAUGE);
		}
		return null;
	}

	@Override
	public synchronized void flush() {
		if (mConnectedRunnableObj != null) {
			mConnectedRunnableObj.flush();
		}
	}

	@Override
	public void setBaudRate(int baud) {

	}

	@Override
	public synchronized boolean isOpen() {
		return mConnectedRunnableObj != null && mConnectedRunnableObj.isOpen();
	}
}
