package com.sgiroux.diyecmgauge.serial;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;

import com.sgiroux.diyecmgauge.utils.StringUtils;

/**
 * Communications connection class that all the connection methods such as Bluetooth and USB should extends
 * 
 */
public abstract class SerialCommConnection {
	private static final String LOGTAG = "CommConnection";
	public static final int UNEXPECTED_RESPONSE_LENGTH = -1;
	private SparseArray<byte[]> mBuffers = new SparseArray<byte[]>();
	protected int mExpectedResponseLength;
	protected Character mExpectedTerminationCharacter;
	protected byte[] mSingleBuffer = new byte[1];
	private Handler mHandler;

	public abstract SerialCommConnectionType getType();

	private static volatile boolean mLogComms = true;
	private static final int MAX_LOG_BYTES = 500;

	/**
	 * A connection is responsible for checking if it has all the information to hand to be ready to connect
	 */
	public abstract boolean isUsable();

	public final void connect(Handler receiveHandler) {
		mHandler = receiveHandler;
		connect();
	}

	public abstract void setBaudRate(int baud);

	protected abstract boolean connect();

	public abstract void disconnect();

	protected abstract byte[] readBytesWithWaitImpl(int timeout);

	protected abstract void writeImpl(byte[] txPayload, int chosenInterWriteDelay, int[] txWaits);

	public abstract boolean isRequireDataIntent();

	public abstract IntentPacket getUserDataIntent(Activity parentActivity);

	public abstract void flush();

	public abstract boolean isOpen();

	protected void sendMessage(int msg) {
		if (mHandler != null) {
			mHandler.sendMessage(mHandler.obtainMessage(msg));
		}
	}

	public final byte[] readBytes(int timeout, Character terminationCharacter) {
		mExpectedTerminationCharacter = terminationCharacter;

		byte[] result = null;

		try {
			result = readBytesWithWaitImpl(timeout);
			if (mLogComms) {
				logBytes(true, result, mExpectedResponseLength);
			}
		} catch (RuntimeException e) {
			Log.e(LOGTAG, "Connection lost while writing to the gauge", e);

			sendMessage(SerialCommConnectionStateChangeHandler.MSG_DISCONNECTED);
		}
		return result;
	}

	public final byte[] readBytes(int timeout) {
		return readBytes(timeout, null);
	}

	public final void write(int expectedResponseLength, byte[] txPayload, int chosenInterWriteDelay, int[] txWaits) {
		mExpectedResponseLength = expectedResponseLength;

		try {
			write(txPayload, chosenInterWriteDelay, txWaits);
		} catch (RuntimeException e) {
			Log.e(LOGTAG, "Connection lost while writing to the gauge", e);

			sendMessage(SerialCommConnectionStateChangeHandler.MSG_DISCONNECTED);
		}

	}

	public final void write(byte[] txPayload, int chosenInterWriteDelay, int[] txWaits) {
		mExpectedResponseLength = UNEXPECTED_RESPONSE_LENGTH;
		if (mLogComms) {
			logBytes(false, txPayload, txPayload.length);
		}
		try {
			writeImpl(txPayload, chosenInterWriteDelay, txWaits);
		} catch (RuntimeException e) {
			Log.e(LOGTAG, "Connection lost while writing to the gauge", e);

			sendMessage(SerialCommConnectionStateChangeHandler.MSG_DISCONNECTED);
		}

	}

	private void logBytes(boolean b, byte[] payload, int length) {
		if (length == -1) {
			length = MAX_LOG_BYTES;
			Log.d(LOGTAG, String.format("Limited to first %s bytes", MAX_LOG_BYTES));
		}
		Log.d(LOGTAG, String.format("%s: %s %s", getClass().getName(), b ? "READ" : "WRITE", StringUtils.bytesToHex(payload, length)));
	}

	protected byte[] getBuffer(int length) {
		byte[] buffer;
		synchronized (mBuffers) {
			// Pull out the buffer of the right size
			buffer = mBuffers.get(length);
			if (buffer == null) {
				buffer = new byte[length];
				mBuffers.put(length, buffer);
			}

		}
		return buffer;
	}
}