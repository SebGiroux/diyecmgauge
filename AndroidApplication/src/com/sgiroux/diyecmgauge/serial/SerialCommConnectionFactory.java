package com.sgiroux.diyecmgauge.serial;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Parcelable;
import android.util.Log;

import com.sgiroux.diyecmgauge.DIYECMGaugeApplication;

/**
 *
 */
public class SerialCommConnectionFactory {
	public static final String ACTION_USB_DEVICE_ATTACHED = "android.hardware.usb.action.USB_DEVICE_ATTACHED";
	private static final String ACTION_USB_DEVICE_DETACHED = "android.hardware.usb.action.USB_DEVICE_DETACHED";
	public static final String ACTION_USB_PERMISSION = "com.sgiroux.diyecmgauge.usb_permissions";
	private static final String EXTRA_DEVICE = "device";
	private static final String EXTRA_PERMISSION_GRANTED = "permission";

	private static final String USB_SYSTEM_FEATURE = "android.hardware.usb.host";

	private static final String LOGTAG = "CommsConnectionFactory";

	private final SerialCommConnectionType[] CONNECTION_TYPES_ORDER = new SerialCommConnectionType[] { SerialCommConnectionType.USB_CDC_ACM, SerialCommConnectionType.BLUETOOTH };

	private boolean mUSBSupported = false;
	private boolean mBluetoothSupported = false;
	private boolean mUSBCdcAcmAttached = false;

	private SerialCommConnection mConnection;

	private static volatile SerialCommConnectionFactory mInstance;

	/**
	 * Get an instance of this class based on the type we're interested in
	 * 
	 * @param instanceType The type of instance we're interested in
	 * @return An instance of CommsConnectionFactory
	 */
	public static SerialCommConnectionFactory getInstance() {
		if (mInstance == null) {
			mInstance = new SerialCommConnectionFactory();
		}

		return mInstance;
	}

	private final BroadcastReceiver mPermissionReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Parcelable device = intent.getParcelableExtra(EXTRA_DEVICE);

			if (action.equals(ACTION_USB_DEVICE_ATTACHED)) {
				if (mUSBSupported) {
					Log.d(LOGTAG, "New USB device has been attached");
					testPermissions();
				}
			}
			else if (action.equals(ACTION_USB_DEVICE_DETACHED)) {
				if (mUSBSupported) {
					Log.d(LOGTAG, "New USB device has been detached");
					testPermissions();
				}
			}
			else if (action.equals(ACTION_USB_PERMISSION)) {
				if (intent.getBooleanExtra(EXTRA_PERMISSION_GRANTED, false) && device != null) {
					Log.d(LOGTAG, "User accepted USB permission");
					testPermissions();
				}
			}
		}
	};

	public SerialCommConnectionFactory() {
		Context context = DIYECMGaugeApplication.getInstance().getContext();

		mBluetoothSupported = BluetoothAdapter.getDefaultAdapter() != null;
		mUSBSupported = context.getPackageManager().hasSystemFeature(USB_SYSTEM_FEATURE);

		if (mUSBSupported) {
			IntentFilter filter = new IntentFilter();

			filter.addAction(ACTION_USB_DEVICE_ATTACHED);
			filter.addAction(ACTION_USB_DEVICE_DETACHED);
			filter.addAction(ACTION_USB_PERMISSION);

			context.getApplicationContext().registerReceiver(mPermissionReceiver, filter);
			testPermissions();
		}
	}

	/**
	 * This method works out what USB capabilities are available.
	 */
	public void testPermissions() {
		if (!mUSBSupported) {
			Log.w(LOGTAG, "No USB support on this device");
			return;
		}

		USBCapabilitiesTester tester = new USBCapabilitiesTester();

		if (!tester.hasManager()) {
			mUSBSupported = false;
			Log.w(LOGTAG, "No USB support on this device");
			return;
		}

		if (tester.getNumberOfDevices(false) == 0) {
			mUSBCdcAcmAttached = false;
		}
		else {
			tester.scanDevices();
			mUSBCdcAcmAttached = tester.foundCdcAcm();
		}

		if (mUSBCdcAcmAttached) {
			Log.i(LOGTAG, "Found an attached USB device");
		}
		else {
			Log.i(LOGTAG, "No USB device found");
		}
	}

	/**
	 * @return true if USB is available and a Cdc Acm device is currently attached, false otherwise
	 */
	public boolean isUSBAvailableAndAttached() {
		boolean isUSBSupportedAndAttached = isUSBSupported() && isCdcAcmAttached();
		Log.d(LOGTAG, "USB (CdcAcm) is " + isUSBSupportedAndAttached);
		return isUSBSupportedAndAttached;
	}

	/**
	 * @return true if the current Android device support Bluetooth, false otherwise
	 */
	public boolean isBluetoothAvailable() {
		boolean bluetoothAvailable = isBluetoothSupported() && BluetoothAdapter.getDefaultAdapter().isEnabled();
		Log.d(LOGTAG, "Bluetooth is " + bluetoothAvailable);
		return bluetoothAvailable;
	}

	/**
	 * @return true if the device supports USB, false otherwise
	 */
	public boolean isUSBSupported() {
		return mUSBSupported;
	}

	/**
	 * @return true if a Cdc Acm usb device is currently attached to the device, false otherwise
	 */
	private boolean isCdcAcmAttached() {
		if (!mUSBSupported) {
			return false;
		}

		return mUSBCdcAcmAttached;
	}

	/**
	 * @return true if Bluetooth is supported on the device, false otherwise
	 */
	private boolean isBluetoothSupported() {
		return mBluetoothSupported;
	}

	/**
	 * Get a connection
	 * 
	 * @return A connection if available, null otherwise
	 */
	public SerialCommConnection getConnection() {
		SerialCommConnection conn = null;

		for (SerialCommConnectionType type : CONNECTION_TYPES_ORDER) {
			conn = getConnectionObject(type);

			if (conn != null) {
				return conn;
			}
		}
		return null;
	}

	/**
	 * Get a connection, preferably from the cache, or cache a new one
	 * 
	 * @param connectionType The type of connection (Bluetooth / USB)
	 * 
	 * @return The object, or null if it is not possible
	 */
	private synchronized SerialCommConnection getConnectionObject(SerialCommConnectionType connectionType) {
		if (mConnection == null) {
			mConnection = getNewConnection(connectionType);
		}

		return mConnection;
	}

	/**
	 * Get a new connection object of the requested type.
	 * 
	 * @param connectionType The type of connection (Bluetooth / USB)
	 * 
	 * @return The connection, or null if not possible.
	 */
	private SerialCommConnection getNewConnection(SerialCommConnectionType connectionType) {
		Log.d(LOGTAG, String.format("getNewConnection: %s", connectionType.name()));

		SerialCommConnection connection = null;

		if (connectionType == SerialCommConnectionType.BLUETOOTH && isBluetoothSupported()) {
			connection = new BluetoothConnection();
			Log.i(LOGTAG, "Creating new bluetooth connection");
		}
		if (connectionType == SerialCommConnectionType.USB_CDC_ACM && isCdcAcmAttached()) {
			connection = new USBCdcAcmConnection();
			Log.i(LOGTAG, "Creating new USB CDC ACM connection");
		}

		return connection;
	}

	/**
	 * Is the default connection ready to be used?
	 * 
	 * @return true if the default connection is ready to be used, false otherwise
	 */
	public boolean isReady() {
		SerialCommConnection connection = getConnection();
		boolean usable = (connection != null && connection.isUsable());
		Log.d(LOGTAG, "Connection is usable : " + usable);
		return usable;
	}
}