package com.sgiroux.diyecmgauge.serial;

/**
 * Enumeration of the currently supported communications connection type
 * 
 * @author Seb
 * 
 */
public enum SerialCommConnectionType {
	BLUETOOTH, USB_CDC_ACM
}