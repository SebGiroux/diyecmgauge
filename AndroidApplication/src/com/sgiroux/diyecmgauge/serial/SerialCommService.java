package com.sgiroux.diyecmgauge.serial;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.DIYECMGaugeApplication;

/**
 * Communications service that holds the connections to the gauge
 * 
 * @author Seb
 * 
 */
public class SerialCommService extends Service {
	private SerialCommConnectionStateChangeHandler mConnectionStateChangeHandler;

	/*
	 * This is the object that receives interactions from clients. See RemoteService for a more complete example.
	 */
	private final IBinder mBinder = new LocalBinder();

	public class LocalBinder extends Binder {
		public SerialCommService getService() {
			return SerialCommService.this;
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();

		mConnectionStateChangeHandler = new SerialCommConnectionStateChangeHandler();

		DIYECMGaugeApplication.getInstance().setCommsService(this);
	}

	@Override
	public int onStartCommand(final Intent intent, final int flags, final int startId) {
		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		return START_STICKY;
	}

	@Override
	public IBinder onBind(final Intent intent) {
		return mBinder;
	}

	public SerialCommConnectionStateChangeHandler getConnectionStateChangeHandler() {
		return mConnectionStateChangeHandler;
	}

	/**
	 * Connect to the gauge
	 * 
	 * @param context
	 */
	public void connectToGauge(Context context) {
		DIYECMGauge.getInstance().setWriterThread(new SerialCommThread(context));
		DIYECMGauge.getInstance().getWriterThread().start();

		DIYECMGaugeApplication.getInstance().setConnectionState(SerialCommConnectionState.CONNECTING);
		SerialCommConnectionFactory.getInstance().getConnection().connect(mConnectionStateChangeHandler);
	}

	/**
	 * Disconnect from the gauge
	 */
	public void disconnectFromGauge() {
		DIYECMGaugeApplication.getInstance().setConnectionState(SerialCommConnectionState.DISCONNECTED);

		SerialCommConnectionFactory.getInstance().getConnection().disconnect();
	}
}
