package com.sgiroux.diyecmgauge.serial;

/**
 * Circular buffer implementation. This class was taken from "MSDroid".
 * 
 * @author Trevor Page
 * 
 */
public class CircularByteBuffer {
	private final byte[] mBuf;
	private final int mCapacity;
	private int mHead;
	private int mTail;
	private byte[] mIbuf;

	public CircularByteBuffer(int capacity) {
		mBuf = new byte[capacity];
		mCapacity = capacity;
		mIbuf = new byte[0];

		reset();
	}

	public synchronized void reset() {
		mHead = 0;
		mTail = 0;
	}

	public synchronized int contigousRoom() {
		return mHead >= mTail ? mCapacity - mHead : mTail - mHead;
	}

	private int room() {
		return mHead >= mTail ? mCapacity - (mHead - mTail) : mTail - mHead;
	}

	public synchronized int avail() {
		return mCapacity - room();
	}

	public byte[] getBuffer() {
		return mBuf;
	}

	public int getHead() {
		return mHead;
	}

	public synchronized void pushBack(int qty) {
		if (qty > 0) {
			mHead += qty;
			if (mHead >= mCapacity) {
				mHead -= mCapacity;
			}
		}
	}

	/**
	 * Check if a byte is currently in the buffer
	 * 
	 * @param data The byte to look for
	 * 
	 * @return true if the byte is currently in the buffer, false otherwise
	 */
	public synchronized boolean isByteInBuffer(byte data) {
		if (mHead > mTail) {
			for (int i = mTail; i < mHead; i++) {
				if (mBuf[i] == data) {
					return true;
				}
			}
		}
		else if (mTail > mHead) {
			for (int i = mTail; i < mCapacity; i++) {
				if (mBuf[i] == data) {
					return true;
				}
			}
			for (int i = 0; i < mHead; i++) {
				if (mBuf[i] == data) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Obtain a byte[] array of all bytes available from the circular buffer.
	 */
	public synchronized byte[] popFront() {
		if (mIbuf.length != avail()) {
			mIbuf = new byte[avail()];
		}

		if (mHead > mTail) {
			System.arraycopy(mBuf, mTail, mIbuf, 0, mHead - mTail);
		}
		else if (mTail > mHead) {
			System.arraycopy(mBuf, mTail, mIbuf, 0, mCapacity - mTail);
			System.arraycopy(mBuf, 0, mIbuf, mCapacity - mTail, mHead);
		}

		mTail = mHead;

		return mIbuf;
	}

}
