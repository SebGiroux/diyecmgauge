package com.sgiroux.diyecmgauge.serial;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.util.Log;

import com.sgiroux.diyecmgauge.DIYECMGaugeApplication;

/**
 * This class hides the USB details from Gingerbread and older Android versions. It will only get loaded after we've already detected USB host
 * capabilities for support on older Android devices.
 * 
 * @author Dave G. Smith
 * 
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
public class USBCapabilitiesTester {
	private static final String LOGTAG = "USBCapabilitiesTester";
	private static final String UNKOWN = "UNKNOWN";
	private static final String USB_SYSTEM_SERVICE = "usb";

	private UsbManager mUsbManager;
	private HashMap<String, UsbDevice> mDevicesList;

	// List of VendorId:ProductId for Cdc Acm devices.
	// @formatter:off
	private Set<String> mSupportedCdcAcmDevices = new HashSet<String>(Arrays.asList(new String[] { 
		"16C0:0483"
	}));
	// @formatter:on

	private boolean mCdcAcmSupported;
	private boolean mUSBSupported;

	public USBCapabilitiesTester() {
		Context context = DIYECMGaugeApplication.getInstance().getContext();

		mUsbManager = (UsbManager) context.getSystemService(USB_SYSTEM_SERVICE);
		mUSBSupported = (mUsbManager != null);

		if (mUSBSupported) {
			generateDevicesList();
		}
	}

	public boolean hasManager() {
		return mUsbManager != null;
	}

	public void generateDevicesList() {
		if (mUsbManager != null) {
			mDevicesList = mUsbManager.getDeviceList();
		}
	}

	/**
	 * @return The current devices list
	 */
	public String getDeviceNameWithPermissions(int index) {
		int nbDevicesWithPermissions = 0;
		String deviceName = "";

		for (Entry<String, UsbDevice> entry : mUsbManager.getDeviceList().entrySet()) {
			if (mUsbManager.hasPermission(entry.getValue())) {
				if (index == nbDevicesWithPermissions) {
					deviceName = entry.getKey();
					break;
				}

				nbDevicesWithPermissions++;
			}
		}

		return deviceName;
	}

	/**
	 * Returns the number of USB devices
	 * 
	 * @param permissions true if we want the number of USB devices that have permissions accepted already, false otherwise
	 * @return Number of USB devices
	 */
	public int getNumberOfDevices(boolean permissions) {
		if (mDevicesList == null || !hasManager()) {
			return 0;
		}

		generateDevicesList();

		if (permissions) {
			int nbDevicesWithPermissions = 0;

			for (Entry<String, UsbDevice> entry : mDevicesList.entrySet()) {
				if (mUsbManager.hasPermission(entry.getValue())) {
					nbDevicesWithPermissions++;
				}
			}

			return nbDevicesWithPermissions;
		}
		else {
			return mDevicesList.size();
		}
	}

	public void scanDevices() {
		if (getNumberOfDevices(false) > 0) {
			for (String deviceName : mDevicesList.keySet()) {
				UsbDevice device = mDevicesList.get(deviceName);
				Log.i(LOGTAG, String.format("Found USB device %s, key %s", deviceName, getDeviceKey(device)));
				if (isSupportedUSBDevice(device)) {
					if (mUsbManager.hasPermission(device)) {
						Log.i(LOGTAG, "User has already accepted USB permission for this device");

						String key = getDeviceKey(device);

						mCdcAcmSupported |= mSupportedCdcAcmDevices.contains(key);
					}
					else {
						Log.i(LOGTAG, "Requesting permission for device");

						PendingIntent permissionIntent = PendingIntent.getBroadcast(DIYECMGaugeApplication.getInstance().getContext(), 0, new Intent(SerialCommConnectionFactory.ACTION_USB_PERMISSION), 0);
						mUsbManager.requestPermission(device, permissionIntent);
					}
				}
				else {
					Log.i(LOGTAG, "Got an unsupported USB device");
				}
			}
		}
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	private boolean isSupportedUSBDevice(UsbDevice d) {
		if (!mUSBSupported) {
			return false;
		}
		String key = getDeviceKey(d);

		boolean supported = mSupportedCdcAcmDevices.contains(key);
		return supported;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	private String getDeviceKey(UsbDevice device) {
		if (!mUSBSupported) {
			return UNKOWN;
		}

		int vid = device.getVendorId();
		int pid = device.getProductId();

		String key = String.format("%04X:%04X", vid, pid);

		return key;
	}

	public boolean foundCdcAcm() {
		return mCdcAcmSupported;
	}
}