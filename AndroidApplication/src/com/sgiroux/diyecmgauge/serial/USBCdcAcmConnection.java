package com.sgiroux.diyecmgauge.serial;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.sgiroux.diyecmgauge.DIYECMGaugeApplication;
import com.sgiroux.diyecmgauge.usb.Physicaloid;
import com.sgiroux.diyecmgauge.usb.UartConfig;
import com.sgiroux.diyecmgauge.utils.StringUtils;

/**
 * This class wrap the implementation of the CDC ACM USB drivers.
 */
public class USBCdcAcmConnection extends SerialCommConnection {
	private static final String LOGTAG = "USBCdcAcmConnection";

	private Physicaloid mPhysicaloid;
	private static final int GAUGE_BAUD_RATE = 9600;

	public USBCdcAcmConnection() {
		Context context = DIYECMGaugeApplication.getInstance().getContext();

		mPhysicaloid = new Physicaloid(context);
		mPhysicaloid.setBaudrate(GAUGE_BAUD_RATE);
	}

	@Override
	public SerialCommConnectionType getType() {
		return SerialCommConnectionType.USB_CDC_ACM;
	}

	@Override
	public synchronized void disconnect() {
		mPhysicaloid.close();
		Log.i(LOGTAG, "Disconnecting USB device");
	}

	@Override
	public synchronized boolean isUsable() {
		return true;
	}

	@Override
	public synchronized boolean connect() {
		boolean success = false;

		if (isUsable()) {
			Log.i(LOGTAG, "Attempting to open USB device");

			if (isOpen()) {
				Log.w(LOGTAG, "Device was already open");
			}
			else {
				UartConfig uartConfig = new UartConfig(GAUGE_BAUD_RATE, UartConfig.DATA_BITS8, UartConfig.STOP_BITS1, UartConfig.PARITY_NONE, false, false);
				if (mPhysicaloid.open(uartConfig)) {
					sendMessage(SerialCommConnectionStateChangeHandler.MSG_CONNECTED);
				}
				else {
					sendMessage(SerialCommConnectionStateChangeHandler.MSG_DISCONNECTED);
					Log.e(LOGTAG, "Could not open device (maybe it's already open?)");
				}
			}
		}
		else {
			Log.e(LOGTAG, "Connection is not usable");
		}

		flush();

		return success;
	}

	@Override
	protected byte[] readBytesWithWaitImpl(int millis) {
		return mPhysicaloid.read(millis);
	}

	@Override
	protected void writeImpl(byte[] buffer, int byteInterDelay, int[] specialInterDelays) {
		synchronized (this) {
			if (!isOpen()) {
				sendMessage(SerialCommConnectionStateChangeHandler.MSG_DISCONNECTED);
				return;
			}

			// First ensure there are no floaters in the pipes
			flush();
		}

		if (byteInterDelay == 0 && specialInterDelays == null) {
			write(buffer);
		}
		else {
			for (int i = 0; i < buffer.length; i++) {
				if (i > 0) {
					try {
						Thread.sleep(byteInterDelay);
					} catch (InterruptedException e) {

					}
				}
				write(buffer[i]);
				if (specialInterDelays != null && specialInterDelays.length > i) {
					try {
						Thread.sleep(specialInterDelays[i]);
					} catch (InterruptedException e) {

					}
				}
			}
		}
	}

	public synchronized void flush() {
		//mPhysicaloid.clearReadBuffer();
	}

	private synchronized void write(byte b) {
		write(new byte[] { b });
	}

	private synchronized void write(byte[] buffer) {
		Log.d(LOGTAG, String.format("Writing %s", StringUtils.bytesToHex(buffer, buffer.length)));

		mPhysicaloid.write(buffer, buffer.length);
	}

	/**
	 * Data intent not used for USB
	 */
	@Override
	public boolean isRequireDataIntent() {
		return false;
	}

	/**
	 * Data intent not used for USB
	 */
	@Override
	public IntentPacket getUserDataIntent(Activity parentActivity) {
		return null;
	}

	@Override
	public synchronized void setBaudRate(int baud) {
		mPhysicaloid.setBaudrate(baud);
	}

	@Override
	public synchronized boolean isOpen() {
		return mPhysicaloid != null && mPhysicaloid.isOpened();
	}
}
