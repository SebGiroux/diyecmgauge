package com.sgiroux.diyecmgauge.preference;

import java.util.Set;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.preference.ListPreference;
import android.util.AttributeSet;

/**
 * List preference used in the Settings activity to let the user choose a Bluetooth device used to connect to his gauge
 */
public class BluetoothDevicePreference extends ListPreference {
	/**
	 * Build the list of Bluetooth device
	 * 
	 * @param context The Context the list preference is running in, through which it can access the current theme, resources, etc.
	 * @param attrs The attributes of the XML tag that is inflating the list preference.
	 */
	public BluetoothDevicePreference(Context context, AttributeSet attrs) {
		super(context, attrs);

		BluetoothAdapter bta = BluetoothAdapter.getDefaultAdapter();
		if (bta != null) {
			Set<BluetoothDevice> pairedDevices = bta.getBondedDevices();
			CharSequence[] entries = new CharSequence[pairedDevices.size()];
			CharSequence[] entryValues = new CharSequence[pairedDevices.size()];
			int i = 0;
			for (BluetoothDevice dev : pairedDevices) {
				entries[i] = dev.getName();
				entryValues[i] = dev.getAddress();
				i++;
			}
			setEntries(entries);
			setEntryValues(entryValues);
		}
	}

	/**
	 * Simple constructor to use when creating a view from code.
	 * 
	 * @param context The Context the list preference is running in, through which it can access the current theme, resources, etc.
	 */
	public BluetoothDevicePreference(Context context) {
		this(context, null);
	}
}
