package com.sgiroux.diyecmgauge.adapter;

public interface IndicatorRowAdapterCallback {
	public void onIndicatorsSelectedStateChanged();
}