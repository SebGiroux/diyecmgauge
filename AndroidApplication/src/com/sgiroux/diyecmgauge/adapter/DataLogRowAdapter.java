package com.sgiroux.diyecmgauge.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sgiroux.diyecmgauge.R;
import com.sgiroux.diyecmgauge.data.DataLogRow;

/**
 * Custom adapter which has two rows (data log name and data log stats (size + date))
 * 
 * @author Seb
 * 
 */
public class DataLogRowAdapter extends BaseAdapter {
	private Context mContext;
	private DataLogRowAdapterCallback mCallback;
	private List<DataLogRow> mDataLogRows = new ArrayList<DataLogRow>();
	private LayoutInflater mInflater;

	public DataLogRowAdapter(Context context) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
	}

	public void setResults(ArrayList<DataLogRow> rows) {
		mDataLogRows = rows;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mDataLogRows.size();
	}

	@Override
	public Object getItem(int position) {
		return mDataLogRows.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.activity_browse_data_logs_row, null);

			holder = new ViewHolder();
			holder.mTxtDatalogName = (TextView) convertView.findViewById(R.id.datalog_name);
			holder.mTxtDatalogStats = (TextView) convertView.findViewById(R.id.datalog_stats);

			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mCallback.onDatalogSelected(mContext, mDataLogRows.get(position));
			}
		});

		holder.mTxtDatalogName.setText(mDataLogRows.get(position).getDataLogName());
		holder.mTxtDatalogStats.setText(mDataLogRows.get(position).getDataLogStats());

		return convertView;
	}

	public void setCallback(DataLogRowAdapterCallback callback) {
		mCallback = callback;
	}

	private static class ViewHolder {
		TextView mTxtDatalogName;
		TextView mTxtDatalogStats;
	}
}
