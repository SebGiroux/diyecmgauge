package com.sgiroux.diyecmgauge.adapter;

public class IndicatorRow {
	private String mTitle;
	private boolean mSelected;

	public IndicatorRow(String title) {
		mTitle = title;
	}

	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String title) {
		mTitle = title;
	}

	public boolean isSelected() {
		return mSelected;
	}

	public void setSelected(boolean selected) {
		mSelected = selected;
	}
}
