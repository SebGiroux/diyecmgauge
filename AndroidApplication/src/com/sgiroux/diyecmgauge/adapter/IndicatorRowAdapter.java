package com.sgiroux.diyecmgauge.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.sgiroux.diyecmgauge.R;

public class IndicatorRowAdapter extends BaseAdapter {
	private List<IndicatorRow> mPageRows = new ArrayList<IndicatorRow>();
	private LayoutInflater mInflater;
	private IndicatorRowAdapterCallback mCallback;

	public IndicatorRowAdapter(Context context) {
		mInflater = LayoutInflater.from(context);
	}

	public IndicatorRowAdapter(Context context, String[] titles) {
		this(context);

		for (String title : titles) {
			add(title);
		}
	}

	@Override
	public int getCount() {
		return mPageRows.size();
	}

	@Override
	public Object getItem(int position) {
		return mPageRows.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public List<IndicatorRow> getAll() {
		return mPageRows;
	}

	/**
	 * Check if an item at a specific position is currently selected
	 * 
	 * @param position The position to look for
	 * 
	 * @return true if the item is selected, false otherwise
	 */
	public boolean isItemSelected(int position) {
		return ((IndicatorRow) getItem(position)).isSelected();
	}

	/**
	 * Get all the selected indicators of the adapter
	 * 
	 * @return A list of all the selected IndicatorRow objects
	 */
	public List<IndicatorRow> getSelectedItems() {
		List<IndicatorRow> selectedRows = new ArrayList<IndicatorRow>();

		for (IndicatorRow datalogRow : mPageRows) {
			if (datalogRow.isSelected()) {
				selectedRows.add(datalogRow);
			}
		}

		return selectedRows;
	}

	/**
	 * Add an indicator to the adapter
	 * 
	 * @param title The title of the indicator to add
	 */
	public void add(String title) {
		mPageRows.add(new IndicatorRow(title));
		notifyDataSetChanged();
	}

	/**
	 * Remove an indicator from the adapter
	 * 
	 * @param title The title of the indicator to remove
	 */
	public void remove(String title) {
		for (IndicatorRow pageRow : mPageRows) {
			if (pageRow.getTitle().equals(title)) {
				mPageRows.remove(pageRow);
				notifyDataSetChanged();
				break;
			}
		}
	}

	/**
	 * Clear all items from the adapter
	 */
	public void clear() {
		mPageRows.clear();
		notifyDataSetChanged();
	}

	/**
	 * Get a View that displays the data at the specified position in the data set
	 * 
	 * @param position The position of the item within the adapter's data set of the item whose view we want.
	 * @param convertView The old view to reuse, if possible.
	 * @param parent The parent that this view will eventually be attached to
	 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.listview_indicator_row, null);

			holder = new ViewHolder();
			holder.txtPage = (TextView) convertView.findViewById(R.id.page);
			holder.chkSelected = (CheckBox) convertView.findViewById(R.id.selected);

			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.chkSelected.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mPageRows.get(position).setSelected(isChecked);

				mCallback.onIndicatorsSelectedStateChanged();
			}
		});

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				CheckBox selected = (CheckBox) v.findViewById(R.id.selected);

				selected.setChecked(!selected.isChecked());

				mPageRows.get(position).setSelected(selected.isChecked());

				mCallback.onIndicatorsSelectedStateChanged();
			}
		});

		holder.txtPage.setText(mPageRows.get(position).getTitle());
		holder.chkSelected.setChecked(mPageRows.get(position).isSelected());

		return convertView;
	}

	/**
	 * Set the callback to be called when an event occurs in the adapter
	 * 
	 * @param callback
	 */
	public void setCallback(IndicatorRowAdapterCallback callback) {
		mCallback = callback;
	}

	private static class ViewHolder {
		TextView txtPage;
		CheckBox chkSelected;
	}
}
