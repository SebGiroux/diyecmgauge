package com.sgiroux.diyecmgauge.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.R;
import com.sgiroux.diyecmgauge.activity.BrowseDataLogsActivity;
import com.sgiroux.diyecmgauge.data.DataLogRow;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;

/**
 * Callback used when a click is performed on a row of the list of data log files
 * 
 * @author Seb
 *
 */
public class DataLogRowAdapterCallback {
	private static final int DATA_LOG_DOWNLOAD = 0;
	private static final int DATA_LOG_EMAIL = 1;
	private static final int DATA_LOG_DELETE = 2;

	private BrowseDataLogsActivity mActivity;
	
	/**
	 * Constructor
	 * 
	 * @param activity The activity that the list of data log files belongs to
	 */
	public DataLogRowAdapterCallback(BrowseDataLogsActivity activity) {
		mActivity = activity;
	}
	
	/**
	 * Triggered when a data log file is selected (clicked)
	 * 
	 * @param context The context the event was triggred in
	 * @param selectedRow The DataLogRow object representing the selected row
	 */
	public void onDatalogSelected(final Context context, final DataLogRow selectedRow) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(R.string.manage_data_logs_menu_title);
		builder.setItems(R.array.manage_data_logs_menu_items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
					case DATA_LOG_DOWNLOAD:
						mActivity.downloadDataLog(selectedRow, false);
						break;

					case DATA_LOG_EMAIL:
						mActivity.downloadDataLog(selectedRow, true);
						break;

					case DATA_LOG_DELETE:
						String fileName = selectedRow.getDataLogName();
						DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.DATA_LOGS_DELETE_FILE, fileName));
						break;
				}
			}
		}).create().show();
	}
}
