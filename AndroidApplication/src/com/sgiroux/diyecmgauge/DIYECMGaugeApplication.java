package com.sgiroux.diyecmgauge;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.sgiroux.diyecmgauge.serial.SerialCommConnection;
import com.sgiroux.diyecmgauge.serial.SerialCommConnectionFactory;
import com.sgiroux.diyecmgauge.serial.SerialCommConnectionState;
import com.sgiroux.diyecmgauge.serial.SerialCommService;

/**
 * Main class that holds the application state.
 * 
 * @author Seb
 */
public class DIYECMGaugeApplication extends Application {
	public static final String BROADCAST_GAUGE_CONNECTION_STATE_CHANGE = "com.sgiroux.diyecmgauge.gauge_connection_state_change";
	public static final String BROADCAST_SETUP_BLUETOOTH = "com.sgiroux.diyecmgauge.setup_bluetooth";

	private static DIYECMGaugeApplication mInstance;

	private DIYECMGaugeSettings mSettings;
	private SerialCommConnectionState mCommsConnectionState = SerialCommConnectionState.DISCONNECTED;
	private SerialCommService mCommsService;

	private final ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(final ComponentName className, final IBinder service) {

		}

		@Override
		public void onServiceDisconnected(final ComponentName className) {

		}
	};

	private boolean mIsBound;

	@Override
	public void onCreate() {
		super.onCreate();

		mInstance = this;

		doBindService();
	}

	/**
	 * Establish a connection with the service. We use an explicit class name because we want a specific service implementation that we know will be
	 * running in our own process (and thus won't be supporting component replacement by other applications).
	 */
	private void doBindService() {
		final Intent serviceIntent = new Intent(this, SerialCommService.class);
		bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);

		mIsBound = true;
	}

	public void doUnbindService() {
		if (mIsBound) {
			// Detach our existing connection.
			unbindService(mConnection);
			mIsBound = false;
		}
	}

	@Override
	public void onTerminate() {
		super.onTerminate();

		doUnbindService();
	}

	public static DIYECMGaugeApplication getInstance() {
		return mInstance;
	}

	public DIYECMGaugeSettings getSettings() {
		return mSettings;
	}

	public void setSettings(DIYECMGaugeSettings settings) {
		mSettings = settings;
	}

	public Context getContext() {
		return mInstance;
	}

	/**
	 * Toggle the connection to the gauge. This function is also responsible for showing the intent related to the current connection method used
	 * (Device selection for Bluetooth for example).
	 * 
	 * @param context
	 */
	public void toggleGaugeConnection(Context context) {
		SerialCommConnection connection = SerialCommConnectionFactory.getInstance().getConnection();

		if (connection != null) {
			boolean requireDataIntent = connection.isRequireDataIntent();

			// This is used for Bluetooth to request to enable Bluetooth and/or
			// request to connect to an adapter
			if (requireDataIntent) {
				final Intent broadcast = new Intent();
				broadcast.setAction(DIYECMGaugeApplication.BROADCAST_SETUP_BLUETOOTH);
				sendBroadcast(broadcast);
			}
			else {
				final SerialCommConnectionState connectionState = getConnectionState();

				// If currently disconnected, connect
				if (connectionState == SerialCommConnectionState.DISCONNECTED) {
					mCommsService.connectToGauge(context);
				}
				// Otherwise, disconnect
				else {
					mCommsService.disconnectFromGauge();
				}
			}
		}
	}

	public SerialCommConnectionState getConnectionState() {
		return mCommsConnectionState;
	}

	public void setConnectionState(SerialCommConnectionState connectionState) {
		mCommsConnectionState = connectionState;

		String broadcastStateChange = DIYECMGaugeApplication.BROADCAST_GAUGE_CONNECTION_STATE_CHANGE;

		if (broadcastStateChange != null) {
			final Intent broadcast = new Intent();
			broadcast.setAction(DIYECMGaugeApplication.BROADCAST_GAUGE_CONNECTION_STATE_CHANGE);
			DIYECMGaugeApplication.getInstance().sendBroadcast(broadcast);
		}
	}

	public SerialCommService getCommsService() {
		return mCommsService;
	}

	public void setCommsService(SerialCommService commsService) {
		mCommsService = commsService;
	}
}
