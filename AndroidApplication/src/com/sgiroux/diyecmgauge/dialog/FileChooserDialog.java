package com.sgiroux.diyecmgauge.dialog;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Locale;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.sgiroux.diyecmgauge.R;

/**
 * Dialog to let the user select a file name. It will use an auto complete to propose file name to the user based on the files currently in a
 * directory. It will also filter the files by the specified extension.
 * 
 * @author Seb
 * 
 */
public class FileChooserDialog extends Dialog {
	private Context mContext;
	private AutoCompleteTextView mFileNameAutoComplete;
	private Button mButtonOK;
	private Button mButtonCancel;
	private File mDirectory;
	private ArrayList<String> mFileNames = new ArrayList<String>();
	private OnFileChoosenResult mDialogResult;
	private FileChooserType mChooserType;

	public enum FileChooserType {
		OPEN, SAVE
	}

	/**
	 * Initialise the dialog and create the file names array
	 * 
	 * @param context The context the dialog will be used in
	 * @param directory The directory to list the files of
	 * @param fileExtension The file extension of the files to be used
	 * @param chooserType The type of file chooser to be created, either for opening or saving
	 */
	public FileChooserDialog(final Context context, final File directory, final String fileExtension, final FileChooserType chooserType) {
		super(context);

		mContext = context;
		mDirectory = directory;
		mChooserType = chooserType;

		File[] files = directory.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase(Locale.ENGLISH).endsWith(fileExtension);
			}
		});

		for (File file : files) {
			mFileNames.add(file.getName());
		}
	}

	/**
	 * Set a file name that will be pre-filled in the auto complete field
	 * 
	 * @param fileName
	 */
	public void setFileName(String fileName) {
		mFileNameAutoComplete.setText(fileName);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTitle(R.string.choose_a_file);
		setContentView(R.layout.dialog_file_chooser);

		mFileNameAutoComplete = (AutoCompleteTextView) findViewById(R.id.file_name);
		mFileNameAutoComplete.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_dropdown_item_1line, mFileNames));

		mButtonOK = (Button) findViewById(R.id.btn_ok);
		mButtonOK.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String fileName = mFileNameAutoComplete.getText().toString();

				// Validate file exists when we're in "open" or "upload" mode
				File file = new File(mDirectory, fileName);
				if (mChooserType == FileChooserType.OPEN && (fileName.equals("") || !file.exists())) {
					mFileNameAutoComplete.setError(mContext.getString(R.string.file_name_that_exists));
				}
				else {
					// Tell the parent of this dialog that we're done
					mDialogResult.finish(file);

					// Close the dialog
					dismiss();
				}
			}
		});

		mButtonCancel = (Button) findViewById(R.id.btn_cancel);
		mButtonCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close the dialog
				dismiss();
			}
		});

		// Change button's text based on the chooser type
		switch (mChooserType) {
			case OPEN:
				mButtonOK.setText(R.string.open);
				break;

			case SAVE:
				mButtonOK.setText(R.string.save);
				break;
		}
	}

	/**
	 * Used by the parent to set a new OnFileChoosenResult to the dialog
	 * 
	 * @param dialogResult
	 */
	public void setDialogResult(OnFileChoosenResult dialogResult) {
		mDialogResult = dialogResult;
	}

	/**
	 * Interface used to send the data back to the dialog's parent
	 */
	public interface OnFileChoosenResult {
		public void finish(File file);
	}
}