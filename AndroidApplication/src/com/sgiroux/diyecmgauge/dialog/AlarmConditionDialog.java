package com.sgiroux.diyecmgauge.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.sgiroux.diyecmgauge.R;
import com.sgiroux.diyecmgauge.data.AlarmConditionOperator;
import com.sgiroux.diyecmgauge.list.IndicatorsList;

/**
 * Dialog used to edit the settings of a condition of an alarm. Those settings include channel, operator, threshold and hysteresis.
 * 
 * @author Seb
 * 
 */
public class AlarmConditionDialog extends Dialog {
	private OnAlarmConditionResult mDialogResult;

	private Context mContext;

	private Spinner mChannelSpinner;
	private Spinner mOperatorSpinner;

	private EditText mThresholdEdit;
	private EditText mHysteresisEdit;

	private String mChannel;
	private AlarmConditionOperator mOperator;
	private float mThreshold;
	private float mHysteresis;

	/**
	 * 
	 * @param context
	 * @param channel
	 * @param operator
	 * @param threshold
	 * @param hysteresis
	 * 
	 */
	public AlarmConditionDialog(Context context, String channel, AlarmConditionOperator operator, float threshold, float hysteresis) {
		super(context);

		mContext = context;

		mChannel = channel;
		mOperator = operator;
		mThreshold = threshold;
		mHysteresis = hysteresis;
	}

	/**
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTitle(R.string.edit_alarm_condition_dialog_title);
		setContentView(R.layout.dialog_alarm_condition);

		populateChannelSpinner();
		populateOperatorSpinner();
		populateThresholdAndHysteresis();

		bindButtonsEvents();
	}

	/**
	 * Populate the channels spinner with all the available channels
	 */
	private void populateChannelSpinner() {
		mChannelSpinner = (Spinner) findViewById(R.id.alarm_channel);
		String[] channels = IndicatorsList.getInstance().getList();
		ArrayAdapter<String> channelAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, channels);
		channelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mChannelSpinner.setAdapter(channelAdapter);
		mChannelSpinner.setSelection(channelAdapter.getPosition(mChannel));
	}

	/**
	 * Populate the operator spinner with all the available operators
	 */
	private void populateOperatorSpinner() {
		mOperatorSpinner = (Spinner) findViewById(R.id.alarm_operator);
		int operatorLength = AlarmConditionOperator.values().length;
		String operators[] = new String[operatorLength];
		for (int i = 0; i < operatorLength; i++) {
			operators[i] = AlarmConditionOperator.values()[i].getTitle();
		}

		ArrayAdapter<String> operatorAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, operators);
		operatorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mOperatorSpinner.setAdapter(operatorAdapter);
		mOperatorSpinner.setSelection(operatorAdapter.getPosition(mOperator.getTitle()));
	}

	/**
	 * Bind the clicks event for the OK and Cancel buttons
	 */
	private void bindButtonsEvents() {
		Button okButton = (Button) findViewById(R.id.alarm_btn_ok);
		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mChannel = mChannelSpinner.getSelectedItem().toString();
				mOperator = AlarmConditionOperator.values()[mOperatorSpinner.getSelectedItemPosition()];
				mThreshold = 0;
				try {
					mThreshold = Float.parseFloat(mThresholdEdit.getText().toString());
				} catch (NumberFormatException e) {
				}
				mHysteresis = 0;
				try {
					mHysteresis = Float.parseFloat(mHysteresisEdit.getText().toString());
				} catch (NumberFormatException e) {
				}

				mDialogResult.finish(mChannel, mOperator, mThreshold, mHysteresis);

				dismiss();
			}
		});

		Button cancelButton = (Button) findViewById(R.id.alarm_btn_cancel);
		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
	}

	/**
	 * Populate the threshold and hysteresis edit text with the current alarm configured values
	 */
	private void populateThresholdAndHysteresis() {
		mThresholdEdit = (EditText) findViewById(R.id.alarm_threshold);
		mThresholdEdit.setText(String.valueOf(mThreshold));

		mHysteresisEdit = (EditText) findViewById(R.id.alarm_hysteresis);
		mHysteresisEdit.setText(String.valueOf(mHysteresis));
	}

	/**
	 * Used by the parent to set a new OnEditIndicatorResult to the dialog
	 * 
	 * @param dialogResult
	 */
	public void setDialogResult(OnAlarmConditionResult dialogResult) {
		mDialogResult = dialogResult;
	}

	/**
	 * Interface used to send the data back to the dialog's parent
	 */
	public interface OnAlarmConditionResult {
		public void finish(String channel, AlarmConditionOperator operator, float threshold, float hysteresis);
	}
}
