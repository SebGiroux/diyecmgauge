package com.sgiroux.diyecmgauge;

import java.io.File;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

/**
 * Class used to load / save the settings of the application
 * 
 * @author Seb
 * 
 */
public class DIYECMGaugeSettings {
	// Bluetooth
	private static final String BLUETOOTH_MAC_ADDRESS = "bluetoothMACAddress";

	// Directories
	private static final String APP_DIRECTORY = "/DIYECMGauge";
	private static final String CONFIG_DIRECTORY = "/config/";
	private static final String DATA_LOGS_DIRECTORY = "/datalogs/";

	private SharedPreferences mPreferences;

	public DIYECMGaugeSettings(Context context) {
		mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public void setBluetoothMACAdress(String bluetoothMACAddress) {
		SharedPreferences.Editor editor = mPreferences.edit();

		editor.putString(BLUETOOTH_MAC_ADDRESS, bluetoothMACAddress);
		editor.commit();
	}

	public String getBluetoothMACAddress() {
		return mPreferences.getString(BLUETOOTH_MAC_ADDRESS, null);
	}

	/**
	 * @return The full path to the configuration files directory
	 */
	public static File getConfigDirectory() {
		return new File(Environment.getExternalStorageDirectory() + APP_DIRECTORY, CONFIG_DIRECTORY);
	}

	/**
	 * @return The full path to the data logs files directory
	 */
	public static File getDataLogsDirectory() {
		return new File(Environment.getExternalStorageDirectory() + APP_DIRECTORY, DATA_LOGS_DIRECTORY);
	}
}
