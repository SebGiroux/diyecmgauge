; NSIS script used to create an executable file for the JAR file (for the desktop application on Windows).
; Make sure you've build the DIYECMGauge.jar and its sitting in this directory before compiling.
; To download the NSIS compiler, go to http://nsis.sourceforge.net/Download
; By Sébastien Giroux

OutFile "DIYECMGauge.exe"

!include LogicLib.nsh

SilentInstall silent
Name "DIYECMGauge"
Icon "..\graphics\DIYECMGauge.ico"

Section "DIYECMGauge"
	; Copy the jar file in the temp folder to run it
	SetOutPath $TEMP
	File "DIYECMGauge.jar"
	
	; Execute the jar and wait
	nsExec::Exec `java -jar "$TEMP\DIYECMGauge.jar"`
	
	; We're done, clean up the file
	Delete "$TEMP\DIYECMGauge.jar"
SectionEnd