package com.sgiroux.diyecmgauge.dialogs;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.controller.PagesController;
import com.sgiroux.diyecmgauge.data.Page;
import com.sgiroux.diyecmgauge.data.PageContent;
import com.sgiroux.diyecmgauge.list.IndicatorsList;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Manage the pages of indicators displayed by the gauge
 * 
 * @author Seb
 * 
 */
public class ManagePagesDialog extends JDialog implements SerialResponseReceivedListener {
	private static final long serialVersionUID = 2032676840136285700L;

	private static final String DIALOG_TITLE = "DIYECMGauge - Manage Pages";

	private MainDialog mParent;

	private int mPageSelectedIndex;
	private JComboBox mCmbPage;

	private JList mLstIndicatorsCurrentlyInPage;
	private DefaultListModel mLstIndicatorsCurrentlyInPageModel;
	private JScrollPane mLstIndicatorsCurrentlyInPageScroll;

	private JList mLstIndicatorsAvailables;
	private DefaultListModel mLstIndicatorsAvailablesModel;
	private JScrollPane mLstIndicatorsAvailablesScroll;

	private JButton mBtnUp;
	private JButton mBtnDown;

	private JButton mBtnAddIndicator;
	private JButton mBtnRemoveIndicator;
	private int mPageContentSelectedIndex;
	private JComboBox mCmbPageContent;
	private JLabel mLblPageContent;
	private JPanel mPnlSaveCancelButtons;
	private JButton mBtnSave;
	private JButton mBtnCancel;

	// Flag to avoid the "page content changed" code to be triggered while we populate the UI while first opening the dialog
	private boolean mIsUIReady = false;

	// Flag to keep track of changes in the current page to prompt the user if something changed
	private boolean mSomethingChanged = false;

	private PagesController mController;
	
	/**
	 * 
	 */
	public ManagePagesDialog(final MainDialog parent) {
		super(parent);
		mParent = parent;

		mController = new PagesController();
		
		prepareUI();
		populatePagesContentCombo();
		populateAvailablesIndicatorsList();
		populatePagesCombo();
		prepareDialog();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				// Cleanup the listeners when the dialog is closed
				DIYECMGauge.getInstance().removeSerialResponseReceivedListener(ManagePagesDialog.this);
				removeWindowListener(this);
			}
		});

		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
	}

	/**
	 * Prepare the dialog properties
	 */
	private void prepareDialog() {
		setSize(650, 300);
		setLocationRelativeTo(null);
		setTitle(DIALOG_TITLE);
		setIconImage(new ImageIcon(getClass().getClassLoader().getResource("DIYECMGauge.png")).getImage());
		setVisible(true);
	}

	/**
	 * Prepare all the user interface of the dialog
	 */
	private void prepareUI() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, 1.0 };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE };

		JPanel pnlMain = new JPanel();
		pnlMain.setBorder(new EmptyBorder(10, 10, 10, 10));
		pnlMain.setLayout(gridBagLayout);
		getContentPane().add(pnlMain);

		JLabel lblPage = new JLabel("Page:");
		GridBagConstraints gbcLblPage = new GridBagConstraints();
		gbcLblPage.anchor = GridBagConstraints.EAST;
		gbcLblPage.insets = new Insets(0, 0, 5, 5);
		gbcLblPage.gridx = 0;
		gbcLblPage.gridy = 0;
		pnlMain.add(lblPage, gbcLblPage);

		mCmbPage = new JComboBox();
		mCmbPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				populateDialogWithSelectedPageSettings();

				mPageSelectedIndex = mCmbPage.getSelectedIndex();
			}
		});
		GridBagConstraints gbcComboBox = new GridBagConstraints();
		gbcComboBox.insets = new Insets(0, 0, 5, 5);
		gbcComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbcComboBox.gridx = 1;
		gbcComboBox.gridy = 0;
		pnlMain.add(mCmbPage, gbcComboBox);

		mLblPageContent = new JLabel("Page content:");
		GridBagConstraints gbc_lblPageContent = new GridBagConstraints();
		gbc_lblPageContent.insets = new Insets(0, 0, 5, 5);
		gbc_lblPageContent.anchor = GridBagConstraints.EAST;
		gbc_lblPageContent.gridx = 0;
		gbc_lblPageContent.gridy = 1;
		pnlMain.add(mLblPageContent, gbc_lblPageContent);

		mCmbPageContent = new JComboBox();
		mCmbPageContent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					PageContent pc = getCurrentPageContent();

					if (pc != PageContent.UNUSED && mController.getIndexOfFirstUnusedPage() < mCmbPage.getSelectedIndex()) {
						String message = "You cannot show anything on this page as there is other pages before this one that are unused. Use them first.";

						JOptionPane.showMessageDialog(null, message, "Unused page", JOptionPane.OK_OPTION);

						mCmbPageContent.setSelectedIndex(mPageContentSelectedIndex);
					}
					else {
						int nbIndicators = pc.getNbIndicators();

						// Remove the last element until we have just the number of indicators we need
						while (mLstIndicatorsCurrentlyInPageModel.getSize() > nbIndicators) {
							mLstIndicatorsCurrentlyInPageModel.remove(mLstIndicatorsCurrentlyInPageModel.getSize() - 1);
						}

						updatePageIndicatorsWithList();
					}

					mPageContentSelectedIndex = mCmbPageContent.getSelectedIndex();
					mController.getPagesList().get(getCurrentPageIndex()).setContent(PageContent.values()[mPageContentSelectedIndex]);
				}
			}
		});
		GridBagConstraints gbcPageContent = new GridBagConstraints();
		gbcPageContent.insets = new Insets(0, 0, 5, 5);
		gbcPageContent.fill = GridBagConstraints.HORIZONTAL;
		gbcPageContent.gridx = 1;
		gbcPageContent.gridy = 1;
		pnlMain.add(mCmbPageContent, gbcPageContent);

		JLabel lblIndicatorsCurrentlyInPage = new JLabel("Indicators currently in page:");
		GridBagConstraints gbcLblCurrentlyInPage = new GridBagConstraints();
		gbcLblCurrentlyInPage.insets = new Insets(0, 0, 5, 5);
		gbcLblCurrentlyInPage.gridx = 0;
		gbcLblCurrentlyInPage.gridy = 2;
		pnlMain.add(lblIndicatorsCurrentlyInPage, gbcLblCurrentlyInPage);

		JLabel lblIndicatorsAvailable = new JLabel("Indicators availables:");
		GridBagConstraints gbcLblIndicatorsAvailable = new GridBagConstraints();
		gbcLblIndicatorsAvailable.insets = new Insets(0, 0, 5, 0);
		gbcLblIndicatorsAvailable.gridx = 2;
		gbcLblIndicatorsAvailable.gridy = 2;
		pnlMain.add(lblIndicatorsAvailable, gbcLblIndicatorsAvailable);

		mLstIndicatorsCurrentlyInPageModel = new DefaultListModel();
		mLstIndicatorsCurrentlyInPage = new JList(mLstIndicatorsCurrentlyInPageModel);
		mLstIndicatorsCurrentlyInPage.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		mLstIndicatorsCurrentlyInPageScroll = new JScrollPane(mLstIndicatorsCurrentlyInPage);
		GridBagConstraints gbcList = new GridBagConstraints();
		gbcList.insets = new Insets(0, 0, 5, 5);
		gbcList.fill = GridBagConstraints.BOTH;
		gbcList.gridx = 0;
		gbcList.gridy = 3;
		pnlMain.add(mLstIndicatorsCurrentlyInPageScroll, gbcList);

		JPanel pnlMiddleButtons = new JPanel();
		GridBagConstraints gbcPanel = new GridBagConstraints();
		gbcPanel.anchor = GridBagConstraints.NORTH;
		gbcPanel.insets = new Insets(0, 0, 5, 5);
		gbcPanel.gridx = 1;
		gbcPanel.gridy = 3;
		pnlMain.add(pnlMiddleButtons, gbcPanel);
		pnlMiddleButtons.setLayout(new BoxLayout(pnlMiddleButtons, BoxLayout.Y_AXIS));

		mBtnAddIndicator = new JButton("<<");
		mBtnAddIndicator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (mLstIndicatorsCurrentlyInPageModel.getSize() < getCurrentPageContent().getNbIndicators()) {
					// Make sure an item is selected
					if (mLstIndicatorsAvailables.getSelectedIndex() > -1) {
						String addedIndicator = mLstIndicatorsAvailables.getSelectedValue().toString();

						mLstIndicatorsCurrentlyInPageModel.addElement(addedIndicator);

						updatePageIndicatorsWithList();
					}
				}
				else {
					String message = "The maximum number of indicator for \"" + getCurrentPageContent().getTitle() + "\" page is " + getCurrentPageContent().getNbIndicators() + ".";

					JOptionPane.showMessageDialog(null, message, "Maximum indicators", JOptionPane.OK_OPTION);
				}
			}
		});
		pnlMiddleButtons.add(mBtnAddIndicator);

		mBtnRemoveIndicator = new JButton(">>");
		mBtnRemoveIndicator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Make sure an item is selected
				if (mLstIndicatorsCurrentlyInPage.getSelectedIndex() > -1) {
					mLstIndicatorsCurrentlyInPageModel.remove(mLstIndicatorsCurrentlyInPage.getSelectedIndex());

					updatePageIndicatorsWithList();
				}
			}
		});
		pnlMiddleButtons.add(mBtnRemoveIndicator);

		mBtnUp = new JButton("Up");
		mBtnUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedIndex = mLstIndicatorsCurrentlyInPage.getSelectedIndex();

				// Make sure at one item is selected and its not the first one
				if (selectedIndex > 0) {
					swapListElements(mLstIndicatorsCurrentlyInPageModel, selectedIndex, selectedIndex - 1);

					// Keep the selected element the same
					mLstIndicatorsCurrentlyInPage.setSelectedIndex(selectedIndex - 1);

					updatePageIndicatorsWithList();
				}
			}
		});
		pnlMiddleButtons.add(mBtnUp);

		mBtnDown = new JButton("Down");
		mBtnDown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedIndex = mLstIndicatorsCurrentlyInPage.getSelectedIndex();

				// Make sure the selected element is not already the last one
				if (selectedIndex < (mLstIndicatorsCurrentlyInPageModel.getSize() - 1)) {
					swapListElements(mLstIndicatorsCurrentlyInPageModel, selectedIndex, selectedIndex + 1);

					// Keep the selected element the same
					mLstIndicatorsCurrentlyInPage.setSelectedIndex(selectedIndex + 1);

					updatePageIndicatorsWithList();
				}
			}
		});
		pnlMiddleButtons.add(mBtnDown);

		mLstIndicatorsAvailablesModel = new DefaultListModel();
		mLstIndicatorsAvailables = new JList(mLstIndicatorsAvailablesModel);
		mLstIndicatorsAvailables.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		mLstIndicatorsAvailablesScroll = new JScrollPane(mLstIndicatorsAvailables);
		GridBagConstraints gbcListIndicatorsAvailables = new GridBagConstraints();
		gbcListIndicatorsAvailables.insets = new Insets(0, 0, 5, 0);
		gbcListIndicatorsAvailables.fill = GridBagConstraints.BOTH;
		gbcListIndicatorsAvailables.gridx = 2;
		gbcListIndicatorsAvailables.gridy = 3;
		pnlMain.add(mLstIndicatorsAvailablesScroll, gbcListIndicatorsAvailables);

		mPnlSaveCancelButtons = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.anchor = GridBagConstraints.SOUTHEAST;
		gbc_panel.gridx = 2;
		gbc_panel.gridy = 4;
		pnlMain.add(mPnlSaveCancelButtons, gbc_panel);

		mBtnSave = new JButton("Save");
		mBtnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (mSomethingChanged) {
					sendCurrentPageConfigurationToGauge();
				}

				dispose();
			}
		});
		mPnlSaveCancelButtons.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		mPnlSaveCancelButtons.add(mBtnSave);

		mBtnCancel = new JButton("Cancel");
		mBtnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		mPnlSaveCancelButtons.add(mBtnCancel);
	}

	/**
	 * Update the object of the currently selected page with the list of indicators from the UI
	 */
	public void updatePageIndicatorsWithList() {
		ArrayList<Integer> indicators = new ArrayList<Integer>();

		for (int i = 0; i < mLstIndicatorsCurrentlyInPageModel.getSize(); i++) {
			indicators.add(IndicatorsList.getInstance().getIndexForIndicator(mLstIndicatorsCurrentlyInPageModel.get(i).toString()));
		}

		mController.getPagesList().get(getCurrentPageIndex()).setIndicatorsChannels(indicators);

		mSomethingChanged = true;
	}

	/**
	 * @return The PageContent object that is currently selected
	 */
	private PageContent getCurrentPageContent() {
		int selectedIndex = mCmbPageContent.getSelectedIndex();

		return PageContent.values()[selectedIndex];
	}

	/**
	 * Swap the positions of two JList elements
	 * 
	 * @param listModel The list model of the JList element that the elements need swapped
	 * @param position1 The position of the first element to be swapped
	 * @param position2 The position of the second element to be swapped
	 */
	private void swapListElements(DefaultListModel listModel, int position1, int position2) {
		Object tmp = listModel.get(position1);

		listModel.set(position1, listModel.get(position2));
		listModel.set(position2, tmp);
	}

	/**
	 * Populate the dialog with the currently selected page
	 * 
	 * Should be called when a page selection change and we need to refresh the current page settings in the UI
	 */
	private void populateDialogWithSelectedPageSettings() {
		/*
		 * Before changing page, we check if the previous page changed. If that is the case, we ask the user and send the modifications to the gauge.
		 */
		if (mSomethingChanged) {
			if (JOptionPane.showConfirmDialog(null, "Current page configuration has been modified, do you want to send this page configuration to the gauge ?", "Page modified", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				mController.sendCurrentPageConfigurationToGauge(mPageSelectedIndex);
			}

			mSomethingChanged = false;
		}

		mIsUIReady = false;

		int selectedIndex = mCmbPage.getSelectedIndex();

		Page page = mController.getPagesList().get(selectedIndex);

		mCmbPageContent.setSelectedItem(page.getContent().getTitle());

		mLstIndicatorsCurrentlyInPageModel.clear();

		for (int indicatorChannel : page.getIndicatorsChannels()) {
			if (indicatorChannel != IndicatorsList.UNUSED) {
				mLstIndicatorsCurrentlyInPageModel.addElement(IndicatorsList.getInstance().getList()[indicatorChannel]);
			}
		}

		mIsUIReady = true;
	}

	/**
	 * 
	 * @return
	 */
	private int getCurrentPageIndex() {
		return mCmbPage.getSelectedIndex();
	}

	/**
	 * Send the current page configuration to the gauge
	 */
	private void sendCurrentPageConfigurationToGauge() {
		mController.sendCurrentPageConfigurationToGauge(getCurrentPageIndex());
	}
	
	/**
	 * Populate the pages combo (From 1 to the number of pages in the gauge)
	 */
	private void populatePagesCombo() {
		for (int i = 1; i <= mController.getPagesList().size(); i++) {
			mCmbPage.addItem(i);
		}
	}

	/**
	 * Populate the available page contents combo
	 */
	private void populatePagesContentCombo() {
		for (PageContent pc : PageContent.values()) {
			mCmbPageContent.addItem(pc.getTitle());
		}
	}

	/**
	 * Populate the list with the available indicators list
	 */
	private void populateAvailablesIndicatorsList() {
		String[] indicators = IndicatorsList.getInstance().getList();

		if (indicators != null) {
			int indicatorsLength = indicators.length;

			for (int i = 0; i < indicatorsLength; i++) {
				mLstIndicatorsAvailablesModel.addElement(indicators[i]);
			}
		}
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		if (serialCommand == SerialCommand.SEND_PAGE) {
			String textResponse = SerialUtils.getTextResponse(response);

			mParent.printDebugIntoList("Response to writing page command: " + textResponse);
		}
	}
}
