package com.sgiroux.diyecmgauge.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.controller.DataLogsController;
import com.sgiroux.diyecmgauge.data.DataLogRow;
import com.sgiroux.diyecmgauge.data.DataLoggingStatus;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorCancelListener;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorCompletedListener;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorDialog;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;
import com.sgiroux.diyecmgauge.utils.StringUtils;

/**
 * Data logs dialog that allow to fetch, download or delete the data logs on the SD card of the gauge
 * 
 * @author Seb
 * 
 */
public class BrowseDataLogsDialog extends JDialog implements SerialResponseReceivedListener {
	private static final long serialVersionUID = -4523644476398233771L;

	private static final String DIALOG_TITLE = "DIYECMGauge - Browse Data Logs";

	private JTable mTblDatalogs;
	private JScrollPane mTblDatalogsScroll;
	private DefaultTableModel mTblDatalogsModel;

	private static final String MENU_ITEM_DOWNLOAD = "Download";
	private static final String MENU_ITEM_DELETE = "Delete";

	private JPopupMenu mPopup;
	private JLabel mLblDataLogsStats;
	private JLabel mLblDataLoggingStatus;

	private JPanel mPnlBottomButtons;
	private JButton mBtnDataLogging;
	private JButton mBtnOk;

	private DataLogsController mController;

	private ProgressMonitorDialog mProgressMonitor;
	private DataLoggingStatus mDataLoggingStatus;

	private ArrayList<Long> mDataLogsSize = new ArrayList<Long>();

	public BrowseDataLogsDialog(final MainDialog parent) {
		super(parent);

		prepareUI();
		prepareDialog();
		prepareProgressMonitor(parent);

		mController = new DataLogsController();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				// Cleanup the listeners when the dialog is closed
				DIYECMGauge.getInstance().removeSerialResponseReceivedListener(BrowseDataLogsDialog.this);
				removeWindowListener(this);
			}
		});

		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);

		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.DATA_LOGS_DIRECTORY_LISTING));
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.SD_CARD_GET_STATS));
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.DATA_LOGGING_STATUS));
	}

	/**
	 * Prepare the progress monitor shown when downloading data logs
	 * 
	 * @param parent The parent of this dialog
	 */
	private void prepareProgressMonitor(final MainDialog parent) {
		mProgressMonitor = new ProgressMonitorDialog();
		mProgressMonitor.setParent(parent);
		mProgressMonitor.setDescription("Downloading file");
		mProgressMonitor.addCompletedListener(new ProgressMonitorCompletedListener() {
			@Override
			public void onProgressMonitorCompleted() {
				mController.closeDataLog();
			}
		});
		mProgressMonitor.addCancelListener(new ProgressMonitorCancelListener() {
			@Override
			public void onProgressMonitorCancel() {
				mController.closeDataLog();
			}
		});
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		String textResponse;

		switch (serialCommand) {
			case SD_CARD_GET_STATS:
				textResponse = SerialUtils.getTextResponse(response);
				final String stats = mController.getDataLogsStats(textResponse);
				mLblDataLogsStats.setText(stats);
				break;

			case DATA_LOGGING_STATUS:
				textResponse = SerialUtils.getTextResponse(response);
				mDataLoggingStatus = DataLoggingStatus.values()[Integer.parseInt(textResponse)];
				mLblDataLoggingStatus.setText(mDataLoggingStatus.getMessage());

				refreshDataLoggingButton();

				break;

			case DATA_LOGS_DIRECTORY_LISTING:
				textResponse = SerialUtils.getTextResponse(response);
				ArrayList<DataLogRow> dataLogRows = mController.getDataLogListing(textResponse);

				// Clear the table
				mTblDatalogsModel.setRowCount(0);
				mDataLogsSize.clear();

				for (DataLogRow dataLogRow : dataLogRows) {
					String fileSize = StringUtils.humanReadableByteCount(dataLogRow.getDataLogSize());
					mTblDatalogsModel.addRow(new String[] { dataLogRow.getDataLogName(), dataLogRow.getDataLogDate(), fileSize });
					mDataLogsSize.add(dataLogRow.getDataLogSize());
				}

				break;

			case DATA_LOGS_DELETE_FILE:
				textResponse = SerialUtils.getTextResponse(response);
				mController.deleteDataLog(textResponse);
				break;

			case DATA_LOGS_GET_FILE:
				mController.getDownloadDataLogChunkResponse(SerialUtils.getBytesResponse(response), mProgressMonitor);
				break;

			default:
				break;
		}
	}

	/**
	 * Create the context menu for when a user right click on a data log in the list
	 * 
	 * @return The context menu created (or reused)
	 */
	private JPopupMenu createContextMenu() {
		if (mPopup == null) {
			mPopup = new JPopupMenu();

			ActionListener menuListener = new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					int selectedRow = mTblDatalogs.getSelectedRow();
					String fileName = mTblDatalogs.getValueAt(selectedRow, 0).toString();

					if (event.getActionCommand().equals(MENU_ITEM_DOWNLOAD)) {
						final JFileChooser fc = new JFileChooser();
						fc.setDialogTitle("Save data Log");
						fc.setSelectedFile(new File(fileName));

						int returnVal = fc.showSaveDialog(BrowseDataLogsDialog.this);
						if (returnVal == JFileChooser.APPROVE_OPTION) {
							File file = fc.getSelectedFile();

							mController.startDataLogDownload(file, mDataLogsSize.get(selectedRow));
						}
					}
					else if (event.getActionCommand().equals(MENU_ITEM_DELETE)) {
						DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.DATA_LOGS_DELETE_FILE, fileName));
					}
				}
			};

			JMenuItem item;
			mPopup.add(item = new JMenuItem(MENU_ITEM_DOWNLOAD));
			item.setHorizontalTextPosition(JMenuItem.RIGHT);
			item.addActionListener(menuListener);

			mPopup.addSeparator();

			mPopup.add(item = new JMenuItem(MENU_ITEM_DELETE));
			item.setHorizontalTextPosition(JMenuItem.RIGHT);
			item.addActionListener(menuListener);

			addMouseListener(new MousePopupListener());
		}

		return mPopup;
	}

	// An inner class to check whether mouse events are the popup trigger
	class MousePopupListener extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			checkPopup(e);
		}

		public void mouseClicked(MouseEvent e) {
			checkPopup(e);
		}

		public void mouseReleased(MouseEvent e) {
			checkPopup(e);
		}

		private void checkPopup(MouseEvent e) {
			if (e.isPopupTrigger()) {
				mPopup.show(BrowseDataLogsDialog.this, e.getX(), e.getY());
			}
		}
	}

	private void prepareDialog() {
		setSize(550, 400);
		setLocationRelativeTo(null);
		setTitle(DIALOG_TITLE);
		setIconImage(new ImageIcon(getClass().getClassLoader().getResource("DIYECMGauge.png")).getImage());
		setVisible(true);
	}

	private void prepareUI() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0 };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0 };

		JPanel pnlMain = new JPanel();
		pnlMain.setBorder(new EmptyBorder(10, 10, 5, 10));
		pnlMain.setLayout(gridBagLayout);
		getContentPane().add(pnlMain);

		mLblDataLogsStats = new JLabel();
		GridBagConstraints gbc_lblCurrentlyDatalogs = new GridBagConstraints();
		gbc_lblCurrentlyDatalogs.insets = new Insets(0, 0, 5, 0);
		gbc_lblCurrentlyDatalogs.gridx = 0;
		gbc_lblCurrentlyDatalogs.gridy = 0;
		pnlMain.add(mLblDataLogsStats, gbc_lblCurrentlyDatalogs);
		gbc_lblCurrentlyDatalogs.insets = new Insets(0, 0, 5, 0);
		gbc_lblCurrentlyDatalogs.gridx = 2;
		gbc_lblCurrentlyDatalogs.gridy = 1;

		mTblDatalogs = new JTable();
		mTblDatalogs.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				int r = mTblDatalogs.rowAtPoint(e.getPoint());
				if (r >= 0 && r < mTblDatalogs.getRowCount()) {
					mTblDatalogs.setRowSelectionInterval(r, r);
				}
				else {
					mTblDatalogs.clearSelection();
				}

				int rowindex = mTblDatalogs.getSelectedRow();
				if (rowindex < 0) {
					return;
				}
				if (e.isPopupTrigger() && e.getComponent() instanceof JTable) {
					JPopupMenu popup = createContextMenu();
					popup.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});

		mTblDatalogsModel = new DefaultTableModel() {
			private static final long serialVersionUID = 2197507523874645578L;

			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(mTblDatalogsModel);
		mTblDatalogs.setRowSorter(sorter);

		mTblDatalogsModel.addColumn("File Name");
		mTblDatalogsModel.addColumn("Date");
		mTblDatalogsModel.addColumn("Size");

		mLblDataLoggingStatus = new JLabel();
		GridBagConstraints gbc_lblDataLoggingStatus = new GridBagConstraints();
		gbc_lblDataLoggingStatus.insets = new Insets(0, 0, 5, 0);
		gbc_lblDataLoggingStatus.gridx = 0;
		gbc_lblDataLoggingStatus.gridy = 1;
		pnlMain.add(mLblDataLoggingStatus, gbc_lblDataLoggingStatus);

		mTblDatalogsScroll = new JScrollPane(mTblDatalogs);
		mTblDatalogs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		mTblDatalogs.setModel(mTblDatalogsModel);

		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.insets = new Insets(0, 0, 5, 0);
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 2;
		pnlMain.add(mTblDatalogsScroll, gbc_table);

		mPnlBottomButtons = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 3;
		pnlMain.add(mPnlBottomButtons, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 257, 257, 0 };
		gbl_panel.rowHeights = new int[] { 23, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		mPnlBottomButtons.setLayout(gbl_panel);

		mBtnDataLogging = new JButton("Start data logging");
		GridBagConstraints gbc_btnDataLogging = new GridBagConstraints();
		gbc_btnDataLogging.anchor = GridBagConstraints.WEST;
		gbc_btnDataLogging.fill = GridBagConstraints.VERTICAL;
		gbc_btnDataLogging.insets = new Insets(0, 0, 0, 5);
		gbc_btnDataLogging.gridx = 0;
		gbc_btnDataLogging.gridy = 0;
		mPnlBottomButtons.add(mBtnDataLogging, gbc_btnDataLogging);
		mBtnDataLogging.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mController.toggleDataLogging(mDataLoggingStatus);
			}
		});

		mBtnOk = new JButton("OK");
		GridBagConstraints gbc_btnOK = new GridBagConstraints();
		gbc_btnOK.anchor = GridBagConstraints.EAST;
		gbc_btnOK.fill = GridBagConstraints.VERTICAL;
		gbc_btnOK.gridx = 1;
		gbc_btnOK.gridy = 0;
		mPnlBottomButtons.add(mBtnOk, gbc_btnOK);
		mBtnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	/**
	 * Refresh the data logging button with the proper state based on the data logging status
	 */
	private void refreshDataLoggingButton() {
		switch (mDataLoggingStatus) {
			case NO_SD_CARD:
				mBtnDataLogging.setVisible(false);
				break;

			case NOT_DATA_LOGGING:
				mBtnDataLogging.setVisible(true);
				mBtnDataLogging.setText("Start data logging");
				break;

			case DATA_LOGGING:
				mBtnDataLogging.setVisible(true);
				mBtnDataLogging.setText("Stop data logging");
				break;
		}
	}
}
