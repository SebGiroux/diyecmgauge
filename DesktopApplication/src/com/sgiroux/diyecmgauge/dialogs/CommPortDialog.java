package com.sgiroux.diyecmgauge.dialogs;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import jssc.SerialPortList;

import com.sgiroux.diyecmgauge.DIYECMGaugeSettings;

/**
 * Communications port dialog used to change the communication port used to talk to the gauge
 * 
 * @author Seb
 * 
 */
public class CommPortDialog extends JDialog {
	private static final long serialVersionUID = -5716514281767010873L;

	private static final String DIALOG_TITLE = "DIYECMGauge - Comm port";

	private final JComboBox mComboBox = new JComboBox();

	/**
	 * 
	 * @param parent
	 */
	public CommPortDialog(final MainDialog parent) {
		super(parent);

		prepareUI(parent);
		populateComboBox();
		prepareDialog();
	}

	/**
	 * Prepare the dialog properties
	 */
	private void prepareDialog() {
		setSize(250, 100);
		setLocationRelativeTo(null);
		setTitle(DIALOG_TITLE);
		setIconImage(new ImageIcon(getClass().getClassLoader().getResource("DIYECMGauge.png")).getImage());
		setVisible(true);
	}

	/**
	 * Prepare all the user interface of the dialog
	 * 
	 * @param parent
	 */
	private void prepareUI(final MainDialog parent) {
		JPanel pnlMain = new JPanel();
		pnlMain.setBorder(new EmptyBorder(10, 10, 10, 10));
		getContentPane().add(pnlMain);

		final JLabel lblPort = new JLabel("Port:");
		pnlMain.add(lblPort, BorderLayout.CENTER);

		pnlMain.add(mComboBox, BorderLayout.EAST);

		JButton btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String portName = mComboBox.getSelectedItem().toString();

				DIYECMGaugeSettings.getInstance().set(DIYECMGaugeSettings.COMM_PORT, portName);
				parent.changeCommPort(portName);

				dispose();
			}
		});
		pnlMain.add(btnSelect, BorderLayout.SOUTH);
	}

	/**
	 * Populate the combo box with all the serial port available on the computer
	 */
	private void populateComboBox() {
		String[] portNames = SerialPortList.getPortNames();
		for (int i = 0; i < portNames.length; i++) {
			mComboBox.addItem(portNames[i]);
		}

		// Select the currently selected communication port in settings
		mComboBox.setSelectedItem(DIYECMGaugeSettings.getInstance().get(DIYECMGaugeSettings.COMM_PORT));
	}
}
