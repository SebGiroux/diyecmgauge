package com.sgiroux.diyecmgauge.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.controller.AlarmsController;
import com.sgiroux.diyecmgauge.data.Alarm;
import com.sgiroux.diyecmgauge.data.AlarmAdditionalCondition;
import com.sgiroux.diyecmgauge.data.AlarmConditionOperator;
import com.sgiroux.diyecmgauge.list.IndicatorsList;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * Manage the alarms that will be triggered by the gauge
 * 
 * @author Seb
 * 
 */
public class AlarmsDialog extends JDialog implements SerialResponseReceivedListener {
	private static final long serialVersionUID = -8093984338778764535L;

	private static final String DIALOG_TITLE = "DIYECMGauge - Manage Alarms";

	private MainDialog mParent;

	private JTextField mTxtAlarmName;
	private JCheckBox mChkEnabled;

	private JList mLstListOfAlarms;
	private DefaultListModel mLstListOfAlarmsModel;

	private JComboBox mCmbChannel1;
	private JComboBox mCmbCondition1;
	private JFormattedTextField mTxtThreshold1;
	private JFormattedTextField mTxtHysteresis1;

	private JComboBox mCmbAdditionalCondition;

	private JComboBox mCmbChannel2;
	private JComboBox mCmbCondition2;
	private JFormattedTextField mTxtThreshold2;
	private JFormattedTextField mTxtHysteresis2;

	private JCheckBox mChkWarningOnScreen;
	private JCheckBox mChkTriggerBuzzer;
	private JCheckBox mChkStartDataLogging;
	private JCheckBox mChkTurnOnLeftLED;
	private JCheckBox mChkTurnOnRightLED;

	// Flag to avoid the "alarm content changed" code to be triggered while we populate the UI while first opening the dialog
	private boolean mIsUIReady = false;

	// Flag to keep track of changes in the current alarm to prompt the user if something changed
	private boolean mSomethingChanged = false;

	private AlarmsController mController;

	/**
	 * 
	 * @param parent
	 */
	public AlarmsDialog(final MainDialog parent) {
		super(parent);
		mParent = parent;

		mController = new AlarmsController();

		prepareUI();
		prepareDialog();
		populateChannelCombos();
		popuplateConditionOperatorCombos();
		populateAdditionalConditionCombo();
		populateAlarmsList();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				// Cleanup the listeners when the dialog is closed
				DIYECMGauge.getInstance().removeSerialResponseReceivedListener(AlarmsDialog.this);
				removeWindowListener(this);
			}
		});

		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
	}

	/**
	 * Prepare the dialog properties
	 */
	private void prepareDialog() {
		setSize(720, 380);
		setLocationRelativeTo(null);
		setTitle(DIALOG_TITLE);
		setIconImage(new ImageIcon(getClass().getClassLoader().getResource("DIYECMGauge.png")).getImage());
		setVisible(true);
	}

	/**
	 * Prepare all the UI elements of the dialog
	 */
	private void prepareUI() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 1.0, Double.MIN_VALUE };

		JPanel pnlMain = new JPanel();
		pnlMain.setBorder(new EmptyBorder(10, 10, 10, 10));
		pnlMain.setLayout(gridBagLayout);
		getContentPane().add(pnlMain);

		JLabel lblListOfAlarms = new JLabel("List of alarms:");
		lblListOfAlarms.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbcLblListOfAlarms = new GridBagConstraints();
		gbcLblListOfAlarms.insets = new Insets(0, 0, 5, 5);
		gbcLblListOfAlarms.gridx = 0;
		gbcLblListOfAlarms.gridy = 0;
		pnlMain.add(lblListOfAlarms, gbcLblListOfAlarms);

		JLabel lblAlarmName = new JLabel("Alarm name:");
		GridBagConstraints gbcLblAlarmName = new GridBagConstraints();
		gbcLblAlarmName.anchor = GridBagConstraints.EAST;
		gbcLblAlarmName.insets = new Insets(0, 0, 5, 5);
		gbcLblAlarmName.gridx = 1;
		gbcLblAlarmName.gridy = 1;
		pnlMain.add(lblAlarmName, gbcLblAlarmName);

		mTxtAlarmName = new JTextField();
		GridBagConstraints gbcTxtAlarmName = new GridBagConstraints();
		gbcTxtAlarmName.insets = new Insets(0, 0, 5, 5);
		gbcTxtAlarmName.fill = GridBagConstraints.HORIZONTAL;
		gbcTxtAlarmName.gridx = 2;
		gbcTxtAlarmName.gridy = 1;
		pnlMain.add(mTxtAlarmName, gbcTxtAlarmName);
		mTxtAlarmName.setColumns(AlarmsController.ALARM_NAME_MAX_CHARS);
		mTxtAlarmName.setDocument(new JTextFieldLimit(AlarmsController.ALARM_NAME_MAX_CHARS));

		mTxtAlarmName.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent keyEvent) {
			}

			public void keyReleased(KeyEvent keyEvent) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mLstListOfAlarmsModel.set(selectedIndex, mTxtAlarmName.getText());
						mController.getAlarmsList().get(selectedIndex).setName(mTxtAlarmName.getText());

						mSomethingChanged = true;
					}
				}
			}

			public void keyTyped(KeyEvent keyEvent) {
			}
		});

		mChkEnabled = new JCheckBox("Enabled");
		mChkEnabled.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mController.getAlarmsList().get(selectedIndex).setEnabled(mChkEnabled.isSelected());

						mSomethingChanged = true;
					}
				}
			}
		});
		GridBagConstraints gbc_mChkEnabled = new GridBagConstraints();
		gbc_mChkEnabled.anchor = GridBagConstraints.WEST;
		gbc_mChkEnabled.insets = new Insets(0, 0, 5, 5);
		gbc_mChkEnabled.gridx = 3;
		gbc_mChkEnabled.gridy = 1;
		pnlMain.add(mChkEnabled, gbc_mChkEnabled);

		JLabel lblChannel = new JLabel("Channel");
		GridBagConstraints gbcLblChannel = new GridBagConstraints();
		gbcLblChannel.insets = new Insets(0, 0, 5, 5);
		gbcLblChannel.gridx = 1;
		gbcLblChannel.gridy = 2;
		pnlMain.add(lblChannel, gbcLblChannel);

		JLabel lblCondition = new JLabel("Condition");
		GridBagConstraints gbcLblCondition = new GridBagConstraints();
		gbcLblCondition.insets = new Insets(0, 0, 5, 5);
		gbcLblCondition.gridx = 2;
		gbcLblCondition.gridy = 2;
		pnlMain.add(lblCondition, gbcLblCondition);

		JLabel lblThreshold = new JLabel("Threshold");
		GridBagConstraints gbcLblThreshold = new GridBagConstraints();
		gbcLblThreshold.insets = new Insets(0, 0, 5, 5);
		gbcLblThreshold.gridx = 3;
		gbcLblThreshold.gridy = 2;
		pnlMain.add(lblThreshold, gbcLblThreshold);

		JLabel lblHysteresis = new JLabel("Hysteresis");
		GridBagConstraints gbcLblHysteresis = new GridBagConstraints();
		gbcLblHysteresis.insets = new Insets(0, 0, 5, 0);
		gbcLblHysteresis.gridx = 4;
		gbcLblHysteresis.gridy = 2;
		pnlMain.add(lblHysteresis, gbcLblHysteresis);

		mCmbChannel1 = new JComboBox();
		mCmbChannel1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mController.getAlarmsList().get(selectedIndex).setChannel1(mCmbChannel1.getSelectedItem().toString());

						mSomethingChanged = true;
					}
				}
			}
		});
		GridBagConstraints gbcComboBox_1 = new GridBagConstraints();
		gbcComboBox_1.insets = new Insets(0, 0, 5, 5);
		gbcComboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbcComboBox_1.gridx = 1;
		gbcComboBox_1.gridy = 3;
		pnlMain.add(mCmbChannel1, gbcComboBox_1);

		mCmbCondition1 = new JComboBox();
		mCmbCondition1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mController.getAlarmsList().get(selectedIndex).setOperator1(AlarmConditionOperator.fromTitle(mCmbCondition1.getSelectedItem().toString()));

						mSomethingChanged = true;
					}
				}
			}
		});
		GridBagConstraints gbcComboBox = new GridBagConstraints();
		gbcComboBox.insets = new Insets(0, 0, 5, 5);
		gbcComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbcComboBox.gridx = 2;
		gbcComboBox.gridy = 3;
		pnlMain.add(mCmbCondition1, gbcComboBox);

		mTxtThreshold1 = new JFormattedTextField();
		mTxtThreshold1.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent keyEvent) {
			}

			public void keyReleased(KeyEvent keyEvent) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						float value = 0;
						try {
							value = Float.parseFloat(mTxtThreshold1.getText());
						} catch (NumberFormatException e) {
						}

						mController.getAlarmsList().get(selectedIndex).setThreshold1(value);

						mSomethingChanged = true;
					}
				}
			}

			public void keyTyped(KeyEvent keyEvent) {
			}
		});
		GridBagConstraints gbcTxtThreshold1 = new GridBagConstraints();
		gbcTxtThreshold1.insets = new Insets(0, 0, 5, 5);
		gbcTxtThreshold1.fill = GridBagConstraints.HORIZONTAL;
		gbcTxtThreshold1.gridx = 3;
		gbcTxtThreshold1.gridy = 3;
		pnlMain.add(mTxtThreshold1, gbcTxtThreshold1);

		mTxtHysteresis1 = new JFormattedTextField();
		mTxtHysteresis1.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent keyEvent) {
			}

			public void keyReleased(KeyEvent keyEvent) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						float value = 0;
						try {
							value = Float.parseFloat(mTxtHysteresis1.getText());
						} catch (NumberFormatException e) {
						}

						mController.getAlarmsList().get(selectedIndex).setHysteresis1(value);

						mSomethingChanged = true;
					}
				}
			}

			public void keyTyped(KeyEvent keyEvent) {
			}
		});
		GridBagConstraints gbcTxtHysteresis1 = new GridBagConstraints();
		gbcTxtHysteresis1.insets = new Insets(0, 0, 5, 0);
		gbcTxtHysteresis1.fill = GridBagConstraints.HORIZONTAL;
		gbcTxtHysteresis1.gridx = 4;
		gbcTxtHysteresis1.gridy = 3;
		pnlMain.add(mTxtHysteresis1, gbcTxtHysteresis1);

		mCmbAdditionalCondition = new JComboBox();
		mCmbAdditionalCondition.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mController.getAlarmsList().get(selectedIndex).setAdditionalCondition(AlarmAdditionalCondition.fromTitle(mCmbAdditionalCondition.getSelectedItem().toString()));

						mSomethingChanged = true;
					}
				}
			}
		});
		GridBagConstraints gbcCmbAdditionalCondition = new GridBagConstraints();
		gbcCmbAdditionalCondition.insets = new Insets(0, 0, 5, 5);
		gbcCmbAdditionalCondition.fill = GridBagConstraints.HORIZONTAL;
		gbcCmbAdditionalCondition.gridx = 1;
		gbcCmbAdditionalCondition.gridy = 4;
		pnlMain.add(mCmbAdditionalCondition, gbcCmbAdditionalCondition);

		mCmbChannel2 = new JComboBox();
		mCmbChannel2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mController.getAlarmsList().get(selectedIndex).setChannel2(mCmbChannel2.getSelectedItem().toString());

						mSomethingChanged = true;
					}
				}
			}
		});
		GridBagConstraints gbcCmbChannel2 = new GridBagConstraints();
		gbcCmbChannel2.insets = new Insets(0, 0, 5, 5);
		gbcCmbChannel2.fill = GridBagConstraints.HORIZONTAL;
		gbcCmbChannel2.gridx = 1;
		gbcCmbChannel2.gridy = 5;
		pnlMain.add(mCmbChannel2, gbcCmbChannel2);

		mCmbCondition2 = new JComboBox();
		mCmbCondition2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mController.getAlarmsList().get(selectedIndex).setOperator2(AlarmConditionOperator.fromTitle(mCmbCondition2.getSelectedItem().toString()));

						mSomethingChanged = true;
					}
				}
			}
		});
		GridBagConstraints gbcCmbCondition2 = new GridBagConstraints();
		gbcCmbCondition2.insets = new Insets(0, 0, 5, 5);
		gbcCmbCondition2.fill = GridBagConstraints.HORIZONTAL;
		gbcCmbCondition2.gridx = 2;
		gbcCmbCondition2.gridy = 5;
		pnlMain.add(mCmbCondition2, gbcCmbCondition2);

		mTxtThreshold2 = new JFormattedTextField();
		mTxtThreshold2.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent keyEvent) {
			}

			public void keyReleased(KeyEvent keyEvent) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						float value = 0;
						try {
							value = Float.parseFloat(mTxtThreshold2.getText());
						} catch (NumberFormatException e) {
						}

						mController.getAlarmsList().get(selectedIndex).setThreshold2(value);

						mSomethingChanged = true;
					}
				}
			}

			public void keyTyped(KeyEvent keyEvent) {
			}
		});
		GridBagConstraints gbcTxtThreshold2 = new GridBagConstraints();
		gbcTxtThreshold2.insets = new Insets(0, 0, 5, 5);
		gbcTxtThreshold2.fill = GridBagConstraints.HORIZONTAL;
		gbcTxtThreshold2.gridx = 3;
		gbcTxtThreshold2.gridy = 5;
		pnlMain.add(mTxtThreshold2, gbcTxtThreshold2);

		mTxtHysteresis2 = new JFormattedTextField();
		mTxtHysteresis2.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent keyEvent) {
			}

			public void keyReleased(KeyEvent keyEvent) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						float value = 0;
						try {
							value = Float.parseFloat(mTxtHysteresis2.getText());
						} catch (NumberFormatException e) {
						}

						mController.getAlarmsList().get(selectedIndex).setHysteresis2(value);

						mSomethingChanged = true;
					}
				}
			}

			public void keyTyped(KeyEvent keyEvent) {
			}
		});
		GridBagConstraints gbcTxtHysteresis2 = new GridBagConstraints();
		gbcTxtHysteresis2.insets = new Insets(0, 0, 5, 0);
		gbcTxtHysteresis2.fill = GridBagConstraints.HORIZONTAL;
		gbcTxtHysteresis2.gridx = 4;
		gbcTxtHysteresis2.gridy = 5;
		pnlMain.add(mTxtHysteresis2, gbcTxtHysteresis2);

		final JLabel lblWhenAlarmValue = new JLabel("When alarm conditions are met:");
		GridBagConstraints gbcLblWhenAlarmValue = new GridBagConstraints();
		gbcLblWhenAlarmValue.anchor = GridBagConstraints.NORTH;
		gbcLblWhenAlarmValue.insets = new Insets(0, 0, 5, 5);
		gbcLblWhenAlarmValue.gridx = 2;
		gbcLblWhenAlarmValue.gridy = 6;
		pnlMain.add(lblWhenAlarmValue, gbcLblWhenAlarmValue);

		final JPanel pnlConditionsMet = new JPanel();
		GridBagConstraints gbcPanelConditionsMet = new GridBagConstraints();
		gbcPanelConditionsMet.gridwidth = 2;
		gbcPanelConditionsMet.insets = new Insets(0, 0, 5, 0);
		gbcPanelConditionsMet.fill = GridBagConstraints.BOTH;
		gbcPanelConditionsMet.gridx = 3;
		gbcPanelConditionsMet.gridy = 6;
		pnlMain.add(pnlConditionsMet, gbcPanelConditionsMet);
		pnlConditionsMet.setLayout(new BoxLayout(pnlConditionsMet, BoxLayout.Y_AXIS));

		mChkWarningOnScreen = new JCheckBox("Show warning on screen");
		mChkWarningOnScreen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mController.getAlarmsList().get(selectedIndex).setShowWarningOnScreen(mChkWarningOnScreen.isSelected());

						mSomethingChanged = true;
					}
				}
			}
		});
		pnlConditionsMet.add(mChkWarningOnScreen);

		mChkTriggerBuzzer = new JCheckBox("Trigger buzzer");
		mChkTriggerBuzzer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mController.getAlarmsList().get(selectedIndex).setTriggerBuzzer(mChkTriggerBuzzer.isSelected());

						mSomethingChanged = true;
					}
				}
			}
		});
		pnlConditionsMet.add(mChkTriggerBuzzer);

		mChkStartDataLogging = new JCheckBox("Start data logging");
		mChkStartDataLogging.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mController.getAlarmsList().get(selectedIndex).setStartDataLogging(mChkStartDataLogging.isSelected());

						mSomethingChanged = true;
					}
				}
			}
		});
		pnlConditionsMet.add(mChkStartDataLogging);

		mChkTurnOnLeftLED = new JCheckBox("Turn on left LED");
		mChkTurnOnLeftLED.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mController.getAlarmsList().get(selectedIndex).setTurnOnLeftLED(mChkTurnOnLeftLED.isSelected());

						mSomethingChanged = true;
					}
				}
			}
		});
		pnlConditionsMet.add(mChkTurnOnLeftLED);

		mChkTurnOnRightLED = new JCheckBox("Turn on right LED");
		mChkTurnOnRightLED.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (mIsUIReady) {
					int selectedIndex = mLstListOfAlarms.getSelectedIndex();
					if (selectedIndex > -1) {
						mController.getAlarmsList().get(selectedIndex).setTurnOnRightLED(mChkTurnOnRightLED.isSelected());

						mSomethingChanged = true;
					}
				}
			}
		});
		pnlConditionsMet.add(mChkTurnOnRightLED);

		JPanel pnlBottomButtons = new JPanel();
		GridBagConstraints gbcBottomButtons = new GridBagConstraints();
		gbcBottomButtons.anchor = GridBagConstraints.SOUTHEAST;
		gbcBottomButtons.gridwidth = 2;
		gbcBottomButtons.gridx = 3;
		gbcBottomButtons.gridy = 7;
		pnlMain.add(pnlBottomButtons, gbcBottomButtons);

		final JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (mSomethingChanged) {
					sendCurrentAlarmConfigurationToGauge();
				}

				dispose();
			}
		});
		pnlBottomButtons.add(btnSave);
		pnlBottomButtons.add(btnCancel);

		mLstListOfAlarmsModel = new DefaultListModel();
		mLstListOfAlarms = new JList(mLstListOfAlarmsModel);
		mLstListOfAlarms.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				populateDialogWithSelectedAlarmSettings();
			}
		});
		GridBagConstraints gbcList = new GridBagConstraints();
		gbcList.gridheight = 6;
		gbcList.insets = new Insets(0, 0, 5, 5);
		gbcList.fill = GridBagConstraints.BOTH;
		gbcList.gridx = 0;
		gbcList.gridy = 1;
		pnlMain.add(mLstListOfAlarms, gbcList);
	}

	/**
	 * Populate the dialog with the selected alarm settings
	 */
	private void populateDialogWithSelectedAlarmSettings() {
		// Before changing page, we check if the previous page changed. If that is the case, we ask the user and send the modifications to the gauge.
		if (mSomethingChanged) {
			if (JOptionPane.showConfirmDialog(null, "Current alarm configuration has been modified, do you want to send this alarm configuration to the gauge ?", "Alarm modified", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				sendCurrentAlarmConfigurationToGauge();
			}

			mSomethingChanged = false;
		}

		int selectedIndex = mLstListOfAlarms.getSelectedIndex();
		if (selectedIndex > -1) {
			mIsUIReady = false;

			Alarm alarm = mController.getAlarmsList().get(selectedIndex);

			mChkEnabled.setSelected(alarm.isEnabled());
			mTxtAlarmName.setText(alarm.getName());

			mCmbChannel1.setSelectedItem(alarm.getChannel1());
			mCmbCondition1.setSelectedItem(alarm.getOperator1().getTitle());
			mTxtThreshold1.setText(String.valueOf(alarm.getThreshold1()));
			mTxtHysteresis1.setText(String.valueOf(alarm.getHysteresis1()));

			mCmbAdditionalCondition.setSelectedItem(alarm.getAdditionalCondition().getTitle());

			mCmbChannel2.setSelectedItem(alarm.getChannel2());
			mCmbCondition2.setSelectedItem(alarm.getOperator2().getTitle());
			mTxtThreshold2.setText(String.valueOf(alarm.getThreshold2()));
			mTxtHysteresis2.setText(String.valueOf(alarm.getHysteresis2()));

			mChkWarningOnScreen.setSelected(alarm.isShowWarningOnScreen());
			mChkTriggerBuzzer.setSelected(alarm.isTriggerBuzzer());
			mChkStartDataLogging.setSelected(alarm.isStartDataLogging());
			mChkTurnOnLeftLED.setSelected(alarm.isTurnOnLeftLED());
			mChkTurnOnRightLED.setSelected(alarm.isTurnOnRightLED());

			mIsUIReady = true;
		}
	}

	/**
	 * Populate the alarms list with the current alarms
	 */
	private void populateAlarmsList() {
		mLstListOfAlarmsModel.clear();

		for (Alarm alarm : mController.getAlarmsList()) {
			mLstListOfAlarmsModel.addElement(alarm.getName());
		}

		if (mLstListOfAlarmsModel.size() > 0) {
			// Select the first alarm if there is one
			mLstListOfAlarms.setSelectedIndex(0);
			populateDialogWithSelectedAlarmSettings();
		}
	}

	/**
	 * Populate the channel combos with the list of available indicators
	 */
	private void populateChannelCombos() {
		String[] indicators = IndicatorsList.getInstance().getList();

		if (indicators != null) {
			int indicatorsLength = indicators.length;

			for (int i = 0; i < indicatorsLength; i++) {
				mCmbChannel1.addItem(indicators[i]);
				mCmbChannel2.addItem(indicators[i]);
			}
		}
	}

	/**
	 * Populate the condition operator combos with the list of available alarm condition operators
	 */
	private void popuplateConditionOperatorCombos() {
		for (AlarmConditionOperator aco : AlarmConditionOperator.values()) {
			mCmbCondition1.addItem(aco.getTitle());
			mCmbCondition2.addItem(aco.getTitle());
		}
	}

	/**
	 * Populate the additional condition combo with the list of available alarm additional conditions
	 */
	private void populateAdditionalConditionCombo() {
		for (AlarmAdditionalCondition aac : AlarmAdditionalCondition.values()) {
			mCmbAdditionalCondition.addItem(aac.getTitle());
		}
	}

	/**
	 * Send the current alarm configuration to the gauge
	 */
	private void sendCurrentAlarmConfigurationToGauge() {
		mController.sendCurrentAlarmConfigurationToGauge(mLstListOfAlarms.getSelectedIndex());
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		if (serialCommand == SerialCommand.SEND_ALARM) {
			String textResponse = SerialUtils.getTextResponse(response);

			mParent.printDebugIntoList("Response to writing alarm command: " + textResponse);
		}
	}

	/**
	 * Used to limit a text field to a specific amount of characters
	 * 
	 * @author Seb
	 * 
	 */
	private static class JTextFieldLimit extends PlainDocument {
		private static final long serialVersionUID = -7777820678106409639L;
		private int mLimit;

		private JTextFieldLimit(int limit) {
			super();
			mLimit = limit;
		}

		public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
			if (str == null) {
				return;
			}

			if ((getLength() + str.length()) <= mLimit) {
				super.insertString(offset, str, attr);
			}
		}
	}
}
