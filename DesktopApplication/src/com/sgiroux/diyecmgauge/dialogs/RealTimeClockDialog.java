package com.sgiroux.diyecmgauge.dialogs;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.controller.RTCController;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;

public class RealTimeClockDialog extends JDialog implements SerialResponseReceivedListener {
	private static final long serialVersionUID = -2395499936342463140L;
	private static final String DIALOG_TITLE = "DIYECMGauge - Real-Time Clock";
	private static final String DATE_FORMAT = "MMMM dd yyyy - HH:mm:ss";

	private JLabel mLblCurrentGaugeTime = new JLabel();
	private JLabel mLblCurrentLocalTime = new JLabel();

	private long mGaugeAndLocalTimeOffset; // in milliseconds
	private Timer mTimeRefreshTimer = new Timer();
	private RTCController mController;

	public RealTimeClockDialog(final MainDialog parent) {
		super(parent);

		prepareUI();
		prepareDialog();

		mController = new RTCController();

		mTimeRefreshTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				// Called every second to update current PC time every second
				SimpleDateFormat date = new SimpleDateFormat(DATE_FORMAT, Locale.US);

				mLblCurrentLocalTime.setText(date.format(Calendar.getInstance().getTime()));
				mLblCurrentGaugeTime.setText(date.format(Calendar.getInstance().getTimeInMillis() + mGaugeAndLocalTimeOffset));
			}
		}, 0, 1000);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				// Cleanup the listeners when the dialog is closed
				DIYECMGauge.getInstance().removeSerialResponseReceivedListener(RealTimeClockDialog.this);
				removeWindowListener(this);
			}
		});

		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);
		DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_RTC_DATE_TIME));
	}

	/**
	 * Prepare the dialog properties
	 */
	private void prepareDialog() {
		setSize(400, 250);
		setLocationRelativeTo(null);
		setTitle(DIALOG_TITLE);
		setIconImage(new ImageIcon(getClass().getClassLoader().getResource("DIYECMGauge.png")).getImage());
		setVisible(true);
	}

	/**
	 * Prepare all the user interface of the dialog
	 */
	private void prepareUI() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0 };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0, 0.0 };

		JPanel pnlMain = new JPanel();
		pnlMain.setBorder(new EmptyBorder(10, 10, 5, 10));
		pnlMain.setLayout(gridBagLayout);
		getContentPane().add(pnlMain);

		JLabel lblCurrentGaugeTimeLabel = new JLabel("Current gauge time:");
		GridBagConstraints gbcLblCurrentGaugeTimeLabel = new GridBagConstraints();
		gbcLblCurrentGaugeTimeLabel.insets = new Insets(0, 0, 5, 0);
		gbcLblCurrentGaugeTimeLabel.gridx = 0;
		gbcLblCurrentGaugeTimeLabel.gridy = 0;
		pnlMain.add(lblCurrentGaugeTimeLabel, gbcLblCurrentGaugeTimeLabel);
		mLblCurrentGaugeTime.setText("Fetching gauge time...");

		mLblCurrentGaugeTime.setFont(new Font("Tahoma", Font.BOLD, 16));
		GridBagConstraints gbcLblCurrentGaugeTime = new GridBagConstraints();
		gbcLblCurrentGaugeTime.insets = new Insets(0, 0, 5, 0);
		gbcLblCurrentGaugeTime.gridx = 0;
		gbcLblCurrentGaugeTime.gridy = 1;
		pnlMain.add(mLblCurrentGaugeTime, gbcLblCurrentGaugeTime);

		JButton btnSetGaugeTime = new JButton("Set gauge time to current local time");
		btnSetGaugeTime.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mController.sendRTCDateTimeToGauge();
			}
		});
		GridBagConstraints gbcBtnSetGaugeTime = new GridBagConstraints();
		gbcBtnSetGaugeTime.insets = new Insets(0, 0, 5, 0);
		gbcBtnSetGaugeTime.gridx = 0;
		gbcBtnSetGaugeTime.gridy = 2;
		pnlMain.add(btnSetGaugeTime, gbcBtnSetGaugeTime);

		JLabel lblCurrentLocalTimeLabel = new JLabel("Current local time:");
		GridBagConstraints gbcLblCurrentLocalTimeLabel = new GridBagConstraints();
		gbcLblCurrentLocalTimeLabel.insets = new Insets(0, 0, 5, 0);
		gbcLblCurrentLocalTimeLabel.gridx = 0;
		gbcLblCurrentLocalTimeLabel.gridy = 3;
		pnlMain.add(lblCurrentLocalTimeLabel, gbcLblCurrentLocalTimeLabel);

		mLblCurrentLocalTime.setFont(new Font("Tahoma", Font.BOLD, 16));
		GridBagConstraints gbcLblCurrentLocalTime = new GridBagConstraints();
		gbcLblCurrentLocalTime.anchor = GridBagConstraints.NORTH;
		gbcLblCurrentLocalTime.insets = new Insets(0, 0, 5, 0);
		gbcLblCurrentLocalTime.gridx = 0;
		gbcLblCurrentLocalTime.gridy = 4;
		pnlMain.add(mLblCurrentLocalTime, gbcLblCurrentLocalTime);

		JPanel buttonPannel = new JPanel();
		GridBagConstraints gbcButtonPanel = new GridBagConstraints();
		gbcButtonPanel.anchor = GridBagConstraints.EAST;
		gbcButtonPanel.fill = GridBagConstraints.VERTICAL;
		gbcButtonPanel.gridx = 0;
		gbcButtonPanel.gridy = 5;
		pnlMain.add(buttonPannel, gbcButtonPanel);
		buttonPannel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		buttonPannel.add(btnOK);
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		if (serialCommand == SerialCommand.GET_RTC_DATE_TIME) {
			final SimpleDateFormat date = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
			final Calendar cal = mController.parseRTCDataFromGauge(response);

			mLblCurrentGaugeTime.setText(date.format(cal.getTime()));
			mGaugeAndLocalTimeOffset = cal.getTimeInMillis() - System.currentTimeMillis();
		}
		else if (serialCommand == SerialCommand.SEND_RTC_DATE_TIME) {
			// We got a response from the send command, lets get the date / time again
			// to make sure it was updated with the value we sent
			DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_RTC_DATE_TIME));
		}
	}
}
