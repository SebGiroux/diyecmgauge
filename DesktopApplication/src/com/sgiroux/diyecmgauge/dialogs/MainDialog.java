package com.sgiroux.diyecmgauge.dialogs;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import jssc.SerialPort;
import jssc.SerialPortException;

import com.sgiroux.diyecmgauge.DIYECMGauge;
import com.sgiroux.diyecmgauge.DIYECMGaugeSettings;
import com.sgiroux.diyecmgauge.SerialCommThread;
import com.sgiroux.diyecmgauge.config.ConfigFileManager;
import com.sgiroux.diyecmgauge.controller.PagesController;
import com.sgiroux.diyecmgauge.controller.PagesController.PageDirection;
import com.sgiroux.diyecmgauge.dialogs.SettingsDialog.SettingsTab;
import com.sgiroux.diyecmgauge.list.AlarmsList;
import com.sgiroux.diyecmgauge.list.IndicatorsList;
import com.sgiroux.diyecmgauge.list.PagesList;
import com.sgiroux.diyecmgauge.list.SettingsList;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorBase;
import com.sgiroux.diyecmgauge.progressmonitor.ProgressMonitorDialog;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialCommandQueue;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;

/**
 * 
 * @author Seb
 * 
 */
public class MainDialog extends JFrame implements SerialResponseReceivedListener {
	private static final long serialVersionUID = -2367457087748583113L;

	private SerialPort mSerialPort;

	// Bits per second for COM port
	private static final int DATA_RATE = 115200;

	private JList mList;
	private DefaultListModel mListModel = new DefaultListModel();
	private JScrollPane mScrollPane;
	private String mGaugeFirmwareVersion = "";

	private JMenuBar mMenuBar;

	private JLabel mLblStatusBar;

	/**
	 * 
	 * @param arguments
	 */
	public static void main(String[] arguments) {
		new MainDialog();
	}

	/**
	 * Constructor
	 */
	public MainDialog() {
		setSize(600, 400);

		// Center in screen
		setLocationRelativeTo(null);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (mSerialPort != null && mSerialPort.isOpened()) {
					try {
						mSerialPort.closePort();
					} catch (SerialPortException e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		setTitle("DIYECMGauge by Sébastien Giroux");
		setIconImage(new ImageIcon(getClass().getClassLoader().getResource("DIYECMGauge.png")).getImage());
		setVisible(true);

		mList = new JList(mListModel);
		mScrollPane = new JScrollPane(mList);
		getContentPane().add(mScrollPane, BorderLayout.CENTER);

		// Prepare the menus and status bar
		prepareMenus();
		prepareStatusBar();

		// Initialize the settings
		DIYECMGaugeSettings.getInstance().initialize();

		DIYECMGauge.getInstance().setWriterThread(new SerialCommThread());
		DIYECMGauge.getInstance().addSerialResponseReceivedListener(this);

		validate();

		initializeCommPort();

		DIYECMGauge.getInstance().getWriterThread().start();
	}

	/**
	 * Prepare the main menu bar of the application
	 */
	private void prepareMenus() {
		mMenuBar = new JMenuBar();
		setJMenuBar(mMenuBar);

		JMenu menuFile = new JMenu("File");

		JMenuItem menuFileOpenConfig = new JMenuItem("Open configuration file");
		menuFileOpenConfig.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK));

		JMenuItem menuFileSaveConfig = new JMenuItem("Save configuration file");
		menuFileSaveConfig.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));

		JMenuItem menuFileQuit = new JMenuItem("Quit");
		menuFileQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_DOWN_MASK));

		JMenu menuPages = new JMenu("Pages");
		JMenuItem menuPagesManage = new JMenuItem("Manage");
		menuPagesManage.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_DOWN_MASK));

		JMenuItem menuPagesPrevious = new JMenuItem("Previous page");
		menuPagesPrevious.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT, KeyEvent.CTRL_DOWN_MASK)); // num pad

		JMenuItem menuPagesNext = new JMenuItem("Next page");
		menuPagesNext.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ADD, KeyEvent.CTRL_DOWN_MASK)); // num pad

		JMenu menuDataLogs = new JMenu("Data Logs");
		JMenuItem menuDataLogsBrowse = new JMenuItem("Browse");
		menuDataLogsBrowse.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, KeyEvent.CTRL_DOWN_MASK));

		JMenu menuAlarms = new JMenu("Alarms");
		JMenuItem menuAlarmsManage = new JMenuItem("Manage");
		menuAlarmsManage.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_DOWN_MASK));

		JMenu menuRealTimeClock = new JMenu("Real-Time Clock");
		JMenuItem menuRealTimeClockManage = new JMenuItem("Manage");
		menuRealTimeClockManage.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_DOWN_MASK));

		JMenu menuSettings = new JMenu("Settings");
		JMenuItem menuSettingsGeneral = new JMenuItem("General");
		menuSettingsGeneral.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, KeyEvent.CTRL_DOWN_MASK));

		JMenuItem menuSettingsVehicle = new JMenuItem("Vehicle");
		menuSettingsVehicle.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_DOWN_MASK));

		JMenuItem menuSettingsStepper = new JMenuItem("Stepper");
		menuSettingsStepper.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, KeyEvent.CTRL_DOWN_MASK));

		JMenu menuConfiguration = new JMenu("Configuration");
		JMenuItem menuConfigurationCommPort = new JMenuItem("Comm Port");

		JMenu menuConfigurationLookAndFeel = new JMenu("Look And Feel");

		final UIManager.LookAndFeelInfo[] inst = UIManager.getInstalledLookAndFeels();
		JMenuItem mnuEditLookAndFeelOne;

		ButtonGroup lookAndFeelGroup = new ButtonGroup();

		for (int i = 0; i < inst.length; i++) {
			mnuEditLookAndFeelOne = new JRadioButtonMenuItem(inst[i].getName());

			final LookAndFeelInfo lookAndFeelInfo = inst[i];

			if (inst[i].getName().equals(UIManager.getLookAndFeel().getName())) {
				mnuEditLookAndFeelOne.setSelected(true);
			}

			mnuEditLookAndFeelOne.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						UIManager.setLookAndFeel(lookAndFeelInfo.getClassName());
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (InstantiationException e1) {
						e1.printStackTrace();
					} catch (IllegalAccessException e1) {
						e1.printStackTrace();
					} catch (UnsupportedLookAndFeelException e1) {
						e1.printStackTrace();
					}

					SwingUtilities.updateComponentTreeUI(MainDialog.this);
				}
			});

			lookAndFeelGroup.add(mnuEditLookAndFeelOne);
			menuConfigurationLookAndFeel.add(mnuEditLookAndFeelOne);
		}

		mMenuBar.add(menuFile);
		menuFile.add(menuFileOpenConfig);
		menuFile.add(menuFileSaveConfig);
		menuFile.addSeparator();
		menuFile.add(menuFileQuit);

		mMenuBar.add(menuPages);
		menuPages.add(menuPagesManage);
		menuPages.addSeparator();
		menuPages.add(menuPagesPrevious);
		menuPages.add(menuPagesNext);

		mMenuBar.add(menuDataLogs);
		menuDataLogs.add(menuDataLogsBrowse);

		mMenuBar.add(menuAlarms);
		menuAlarms.add(menuAlarmsManage);

		mMenuBar.add(menuRealTimeClock);
		menuRealTimeClock.add(menuRealTimeClockManage);

		mMenuBar.add(menuSettings);
		menuSettings.add(menuSettingsGeneral);
		menuSettings.add(menuSettingsVehicle);
		menuSettings.add(menuSettingsStepper);

		mMenuBar.add(menuConfiguration);
		menuConfiguration.add(menuConfigurationCommPort);
		menuConfiguration.add(menuConfigurationLookAndFeel);

		menuFileOpenConfig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openConfigFile();
			}
		});

		menuFileSaveConfig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveConfigFile();
			}
		});

		menuFileQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispatchEvent(new WindowEvent(MainDialog.this, WindowEvent.WINDOW_CLOSING));
			}
		});

		menuPagesManage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ManagePagesDialog(MainDialog.this);
			}
		});

		menuPagesPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeDisplayPage(PageDirection.PREVIOUS);
			}
		});

		menuPagesNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeDisplayPage(PageDirection.NEXT);
			}
		});

		menuDataLogsBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new BrowseDataLogsDialog(MainDialog.this);
			}
		});

		menuAlarmsManage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AlarmsDialog(MainDialog.this);
			}
		});

		menuRealTimeClockManage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new RealTimeClockDialog(MainDialog.this);
			}
		});

		menuSettingsGeneral.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SettingsDialog dialog = new SettingsDialog(MainDialog.this);
				dialog.setSelectedTab(SettingsTab.GENERAL);
			}
		});

		menuSettingsVehicle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SettingsDialog dialog = new SettingsDialog(MainDialog.this);
				dialog.setSelectedTab(SettingsTab.VEHICLE);
			}
		});

		menuSettingsStepper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SettingsDialog dialog = new SettingsDialog(MainDialog.this);
				dialog.setSelectedTab(SettingsTab.STEPPER);
			}
		});

		menuConfigurationCommPort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new CommPortDialog(MainDialog.this);
			}
		});
	}

	/**
	 * Function called to open a configuration file. It will ask the user which file to open.
	 */
	private void openConfigFile() {
		final JFileChooser fc = new JFileChooser();

		String lastConfigDirectory = DIYECMGaugeSettings.getInstance().get(DIYECMGaugeSettings.LAST_CONFIG_DIRECTORY);
		if (lastConfigDirectory != null) {
			fc.setCurrentDirectory(new File(lastConfigDirectory));
		}

		UIManager.put("FileChooser.openDialogTitleText", "Open configuration file");
		SwingUtilities.updateComponentTreeUI(fc);
		fc.addChoosableFileFilter(new FileNameExtensionFilter("Configuration file (*.xml)", "xml"));

		int returnVal = fc.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			DIYECMGaugeSettings.getInstance().set(DIYECMGaugeSettings.LAST_CONFIG_DIRECTORY, file.getParent());

			ConfigFileManager configFileManager = new ConfigFileManager();
			ProgressMonitorBase progressMonitor = new ProgressMonitorDialog();

			progressMonitor.setParent(this);
			configFileManager.openConfigFile(progressMonitor, file);
		}
	}

	/**
	 * Function called to save a configuration file. It will ask the user where the file should be saved.
	 */
	private void saveConfigFile() {
		final JFileChooser fc = new JFileChooser();

		String lastConfigDirectory = DIYECMGaugeSettings.getInstance().get(DIYECMGaugeSettings.LAST_CONFIG_DIRECTORY);
		if (lastConfigDirectory != null) {
			fc.setCurrentDirectory(new File(lastConfigDirectory));
		}

		UIManager.put("FileChooser.saveDialogTitleText", "Save configuration file");
		SwingUtilities.updateComponentTreeUI(fc);
		fc.addChoosableFileFilter(new FileNameExtensionFilter("Configuration file (*.xml)", "xml"));

		int returnVal = fc.showSaveDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			DIYECMGaugeSettings.getInstance().set(DIYECMGaugeSettings.LAST_CONFIG_DIRECTORY, file.getParent());

			ConfigFileManager configFileManager = new ConfigFileManager();
			configFileManager.saveConfigFile(file);
		}
	}

	/**
	 * Prepare the status bar
	 */
	private void prepareStatusBar() {
		JPanel statusBar = new JPanel();
		statusBar.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		getContentPane().add(statusBar, BorderLayout.SOUTH);
		statusBar.setLayout(new BorderLayout(0, 0));

		mLblStatusBar = new JLabel();
		mLblStatusBar.setText("Welcome to DIYECMGauge software!");
		mLblStatusBar.setHorizontalAlignment(SwingConstants.LEFT);
		mLblStatusBar.setVerticalAlignment(SwingConstants.TOP);
		statusBar.add(mLblStatusBar);
	}

	/**
	 * Initialize the comm port
	 */
	private void initializeCommPort() {
		String portName = DIYECMGaugeSettings.getInstance().get(DIYECMGaugeSettings.COMM_PORT);
		changeCommPort(portName);
	}

	/**
	 * Change the current comm port (serial) used to communicate with the DIYECMGauge
	 * 
	 * @param portName
	 */
	public void changeCommPort(String portName) {
		try {
			if (mSerialPort != null) {
				mSerialPort.closePort();
			}

			// Open serial port, and use class name for the appName.
			mSerialPort = new SerialPort(portName);

			mSerialPort.openPort();

			// Set port parameters
			mSerialPort.setParams(DATA_RATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			// Open the streams
			SerialCommThread serialCommThread = (SerialCommThread) DIYECMGauge.getInstance().getWriterThread();
			serialCommThread.setSerialPort(mSerialPort);

			// Add event listeners
			mSerialPort.addEventListener((SerialCommThread) DIYECMGauge.getInstance().getWriterThread());

			mLblStatusBar.setText("Fetching gauge version...");

			DIYECMGauge.getInstance().sendSerialCommand(new SerialCommandQueue(SerialCommand.GET_VERSION));

			// Initialize the indicators list
			IndicatorsList.getInstance().initialize();

			// Initialize the pages list
			PagesList.getInstance().initialize();

			// Initialize the alarms list
			AlarmsList.getInstance().initialize();

			// Initialize the settings list
			SettingsList.getInstance().initialize();
		} catch (SerialPortException e) {
			mLblStatusBar.setText("Port " + portName + " not found or already in use by another application");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This should be called when you stop using the port. This will prevent port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (mSerialPort != null) {
			try {
				mSerialPort.removeEventListener();
				mSerialPort.closePort();
			} catch (SerialPortException e) {
				e.printStackTrace();
			}

			DIYECMGauge.getInstance().getWriterThread().stopWriter();
		}
	}

	/**
	 * Triggered whenever we receive a command from the gauge
	 * 
	 * @param serialCommand The serial command to be sent
	 * @param response The array of byte of the data we received
	 */
	@Override
	public void onResponseReceived(SerialCommand serialCommand, byte[] response) {
		if (serialCommand == SerialCommand.GET_VERSION) {
			mGaugeFirmwareVersion = SerialUtils.getTextResponse(response);
			mLblStatusBar.setText("Connected to " + mGaugeFirmwareVersion);
		}
		else if (serialCommand == SerialCommand.GET_DEBUG) {
			String message = SerialUtils.getTextResponse(response);
			System.out.println("DEBUG: " + message);
		}
	}

	/**
	 * Print a debug message into the debug list
	 * 
	 * @param message The debug message to be printed
	 */
	public void printDebugIntoList(String message) {
		mListModel.addElement(message);

		JScrollBar vertical = mScrollPane.getVerticalScrollBar();
		vertical.setValue(vertical.getMaximum());
	}

	/**
	 * Change the display page of the gauge
	 * 
	 * @param direction The direction to take, previous or next
	 */
	private void changeDisplayPage(PageDirection direction) {
		PagesController pageController = new PagesController();
		pageController.sendChangePage(direction);
	}
}