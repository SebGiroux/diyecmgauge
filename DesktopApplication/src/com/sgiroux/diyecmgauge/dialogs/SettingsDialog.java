package com.sgiroux.diyecmgauge.dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.sgiroux.diyecmgauge.controller.SettingsController;
import com.sgiroux.diyecmgauge.data.DimensionUnit;
import com.sgiroux.diyecmgauge.data.GeneralSettings;
import com.sgiroux.diyecmgauge.data.HomingDirection;
import com.sgiroux.diyecmgauge.data.SpeedSourceDynamometer;
import com.sgiroux.diyecmgauge.data.SpeedUnit;
import com.sgiroux.diyecmgauge.data.StepperSettings;
import com.sgiroux.diyecmgauge.data.TemperatureUnit;
import com.sgiroux.diyecmgauge.data.TimeZone;
import com.sgiroux.diyecmgauge.data.VehicleSettings;
import com.sgiroux.diyecmgauge.data.WeightUnit;
import com.sgiroux.diyecmgauge.list.IndicatorsList;

public class SettingsDialog extends JDialog {
	private static final long serialVersionUID = -898763469626365786L;
	private static final String DIALOG_TITLE = "DIYECMGauge - Settings";

	public enum SettingsTab {
		GENERAL, VEHICLE, STEPPER
	}

	private final JTabbedPane mTabs = new JTabbedPane(JTabbedPane.TOP);

	private SettingsController mController;

	// General settings
	private JComboBox mCmbSpeedUnit;
	private JComboBox mCmbTemperatureUnit;
	private JComboBox mCmbVehicleSpeedSourceForDynamometer;
	private JComboBox mCmbTimeZone;
	private JCheckBox mChkRealTimeClockSync;
	private JCheckBox mChkConfigurationSerialPortOnMainConnector;

	// Vehicle settings
	private JFormattedTextField mTxtVehicleWeight;
	private JFormattedTextField mTxtCoefficientOfDrag;
	private JFormattedTextField mTxtCoefficientOfRollingResistance;
	private JFormattedTextField mTxtFrontalArea;
	private JComboBox mCmbVehicleWeightUnit;
	private JComboBox mCmbFrontalAreaUnit;
	private JTextField mTxtRealTimeClockCompensation;

	// Stepper settings
	private JTextField mTxtTotalNumberOfSteps;
	private JTextField mTxtMaximumSpeed;
	private JTextField mTxtAcceleration;
	private JComboBox mCmbHomingDirection = new JComboBox();
	private JComboBox mCmbChannel = new JComboBox();

	public SettingsDialog(final MainDialog parent) {
		super(parent);

		mController = new SettingsController();

		prepareUI();
		populateCombos();
		populateCurrentSettings();
		prepareDialog();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				// Cleanup the listener when the dialog is closed
				removeWindowListener(this);
			}
		});
	}

	/**
	 * Populate the combo box with all the possible values for each of them
	 */
	private void populateCombos() {
		// Speed unit combo
		for (SpeedUnit speedUnit : SpeedUnit.values()) {
			mCmbSpeedUnit.addItem(speedUnit.getTitle());
		}

		// Temperature unit combo
		for (TemperatureUnit temperatureUnit : TemperatureUnit.values()) {
			mCmbTemperatureUnit.addItem(temperatureUnit.getTitle());
		}

		// Vehicle speed source for dynamometer combo
		for (SpeedSourceDynamometer speedSourceDynamometer : SpeedSourceDynamometer.values()) {
			mCmbVehicleSpeedSourceForDynamometer.addItem(speedSourceDynamometer.getTitle());
		}

		// Time Zone combo
		for (TimeZone timeZone : TimeZone.values()) {
			mCmbTimeZone.addItem(timeZone.getTitle());
		}

		// Weight unit combo
		for (WeightUnit weightUnit : WeightUnit.values()) {
			mCmbVehicleWeightUnit.addItem(weightUnit.getTitle());
		}

		// Dimension unit combo
		for (DimensionUnit dimensionUnit : DimensionUnit.values()) {
			mCmbFrontalAreaUnit.addItem(dimensionUnit.getTitle());
		}

		// Homing direction combo
		for (HomingDirection direction : HomingDirection.values()) {
			mCmbHomingDirection.addItem(direction.getDirection());
		}

		// Channel combo
		String[] indicators = IndicatorsList.getInstance().getList();

		if (indicators != null) {
			int indicatorsLength = indicators.length;

			for (int i = 0; i < indicatorsLength; i++) {
				mCmbChannel.addItem(indicators[i]);
			}
		}
	}

	/**
	 * Populate the dialog with the current general & vehicle settings
	 */
	private void populateCurrentSettings() {
		// General settings
		GeneralSettings generalSettings = mController.getGeneralSettings();

		mCmbSpeedUnit.setSelectedIndex(generalSettings.getSpeedUnit().ordinal());
		mCmbTemperatureUnit.setSelectedIndex(generalSettings.getTemperatureUnit().ordinal());
		mCmbVehicleSpeedSourceForDynamometer.setSelectedIndex(generalSettings.getSpeedSourceDynamometer().ordinal());
		mCmbTimeZone.setSelectedIndex(TimeZone.fromOffset(generalSettings.getTimeZone()).ordinal());
		mChkRealTimeClockSync.setSelected(generalSettings.isRtcSyncWithGps());
		mTxtRealTimeClockCompensation.setText(String.valueOf(generalSettings.getRtcCompensate()));
		mChkConfigurationSerialPortOnMainConnector.setSelected(generalSettings.isConfigurationSerialPortOnMainConnector());

		// Vehicle settings
		VehicleSettings vehicleSettings = mController.getVehicleSettings();

		mTxtVehicleWeight.setText(String.valueOf(vehicleSettings.getVehicleWeight()));
		mTxtCoefficientOfDrag.setText(String.valueOf(vehicleSettings.getCoefficientOfDrag()));
		mTxtCoefficientOfRollingResistance.setText(String.valueOf(vehicleSettings.getCoefficientOfRollingResistance()));
		mTxtFrontalArea.setText(String.valueOf(vehicleSettings.getFrontalArea()));

		// Stepper settings
		StepperSettings stepperSettings = mController.getStepperSettings();

		mTxtTotalNumberOfSteps.setText(String.valueOf(stepperSettings.getTotalNumberOfSteps()));
		mTxtMaximumSpeed.setText(String.valueOf(stepperSettings.getMaximumSpeed()));
		mTxtAcceleration.setText(String.valueOf(stepperSettings.getAcceleration()));

		mCmbHomingDirection.setSelectedIndex(stepperSettings.getHomingDirection().ordinal());
		if (mCmbChannel.getItemCount() > stepperSettings.getChannel()) {
			mCmbChannel.setSelectedIndex(stepperSettings.getChannel());
		}
	}

	/**
	 * Save the current general, vehicle settings and stepper settings based on the current fields content of this dialog
	 */
	private void saveCurrentSettings() {
		GeneralSettings currentGeneralSettings = getGeneralSettingsFromCurrentSettings();
		VehicleSettings currentVehicleSettings = getVehicleSettingsFromCurrentSettings();
		StepperSettings currentStepperSettings = getStepperSettingsFromCurrentSettings();

		mController.saveCurrentSettings(currentGeneralSettings, currentVehicleSettings, currentStepperSettings);
	}

	/**
	 * @return A GeneralSettings object build from the fields of this dialog
	 */
	private GeneralSettings getGeneralSettingsFromCurrentSettings() {
		GeneralSettings generalSettings = new GeneralSettings();

		generalSettings.setSpeedUnit(SpeedUnit.values()[mCmbSpeedUnit.getSelectedIndex()]);
		generalSettings.setTemperatureUnit(TemperatureUnit.values()[mCmbTemperatureUnit.getSelectedIndex()]);
		generalSettings.setSpeedSourceDynamometer(SpeedSourceDynamometer.values()[mCmbVehicleSpeedSourceForDynamometer.getSelectedIndex()]);
		generalSettings.setTimeZone(TimeZone.values()[mCmbTimeZone.getSelectedIndex()].getUtcOffset());
		generalSettings.setRtcSyncWithGps(mChkRealTimeClockSync.isSelected());
		generalSettings.setConfigurationSerialPortOnMainConnector(mChkConfigurationSerialPortOnMainConnector.isSelected());

		int ppm = 0;
		try {
			ppm = Integer.parseInt(mTxtRealTimeClockCompensation.getText());
		} catch (NumberFormatException e) {
		}
		generalSettings.setRtcCompensate(ppm);

		return generalSettings;
	}

	/**
	 * @return A VehicleSettings object build from the fields of this dialog
	 */
	private VehicleSettings getVehicleSettingsFromCurrentSettings() {
		return mController.getVehicleSettingsFromFields(mTxtVehicleWeight.getText(), mTxtCoefficientOfDrag.getText(), mTxtCoefficientOfRollingResistance.getText(), mTxtFrontalArea.getText(), mCmbVehicleWeightUnit.getSelectedItem().toString(), mCmbFrontalAreaUnit.getSelectedItem().toString());
	}

	/**
	 * @return A StepperSettings object build from the fields of this dialog
	 */
	private StepperSettings getStepperSettingsFromCurrentSettings() {
		return mController.getStepperSettingsFromFields(mTxtTotalNumberOfSteps.getText(), mTxtMaximumSpeed.getText(), mTxtAcceleration.getText(), mCmbHomingDirection.getSelectedIndex(), mCmbChannel.getSelectedIndex());
	}

	/**
	 * Prepare all the user interface of the dialog
	 */
	private void prepareUI() {
		getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));

		JButton btnSave = new JButton("Save");
		panel.add(btnSave);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveCurrentSettings();
				dispose();
			}
		});

		JButton btnCancel = new JButton("Cancel");
		panel.add(btnCancel);
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		mTabs.addTab("General", buildGeneralPanel());
		mTabs.addTab("Vehicle", buildVehiclePanel());
		mTabs.addTab("Stepper", buildStepperPanel());

		getContentPane().add(mTabs, BorderLayout.CENTER);
	}

	/**
	 * @return A panel with the general settings layout
	 */
	private JComponent buildGeneralPanel() {
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));

		GridBagLayout gblPanel = new GridBagLayout();
		gblPanel.columnWidths = new int[] { 0, 0, 0 };
		gblPanel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		gblPanel.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gblPanel.rowWeights = new double[] { 0, 0.0, 0, 0.0, 0, 0.0, 0.0, 1.0 };
		panel.setLayout(gblPanel);

		JLabel lblSpeedUnit = new JLabel("Speed unit:");
		GridBagConstraints gbcLblSpeedUnit = new GridBagConstraints();
		gbcLblSpeedUnit.anchor = GridBagConstraints.WEST;
		gbcLblSpeedUnit.insets = new Insets(0, 0, 5, 5);
		gbcLblSpeedUnit.gridx = 0;
		gbcLblSpeedUnit.gridy = 0;
		panel.add(lblSpeedUnit, gbcLblSpeedUnit);

		mCmbSpeedUnit = new JComboBox();
		GridBagConstraints gbcCmbSpeedUnit = new GridBagConstraints();
		gbcCmbSpeedUnit.insets = new Insets(0, 0, 5, 0);
		gbcCmbSpeedUnit.fill = GridBagConstraints.HORIZONTAL;
		gbcCmbSpeedUnit.gridx = 1;
		gbcCmbSpeedUnit.gridy = 0;
		panel.add(mCmbSpeedUnit, gbcCmbSpeedUnit);

		JLabel lblTemperatureUnit = new JLabel("Temperature unit:");
		GridBagConstraints gbcLblTemperatureUnit = new GridBagConstraints();
		gbcLblTemperatureUnit.anchor = GridBagConstraints.WEST;
		gbcLblTemperatureUnit.insets = new Insets(0, 0, 5, 5);
		gbcLblTemperatureUnit.gridx = 0;
		gbcLblTemperatureUnit.gridy = 1;
		panel.add(lblTemperatureUnit, gbcLblTemperatureUnit);

		mCmbTemperatureUnit = new JComboBox();
		GridBagConstraints gbcCmbTemperatureUnit = new GridBagConstraints();
		gbcCmbTemperatureUnit.insets = new Insets(0, 0, 5, 0);
		gbcCmbTemperatureUnit.fill = GridBagConstraints.HORIZONTAL;
		gbcCmbTemperatureUnit.gridx = 1;
		gbcCmbTemperatureUnit.gridy = 1;
		panel.add(mCmbTemperatureUnit, gbcCmbTemperatureUnit);

		JLabel lblVehicleSpeedSourceForDynamometer = new JLabel("Vehicle speed source for Dynamometer:");
		GridBagConstraints gbcLblVehicleSpeedSourceForDynamometer = new GridBagConstraints();
		gbcLblVehicleSpeedSourceForDynamometer.anchor = GridBagConstraints.WEST;
		gbcLblVehicleSpeedSourceForDynamometer.insets = new Insets(0, 0, 5, 5);
		gbcLblVehicleSpeedSourceForDynamometer.gridx = 0;
		gbcLblVehicleSpeedSourceForDynamometer.gridy = 2;
		panel.add(lblVehicleSpeedSourceForDynamometer, gbcLblVehicleSpeedSourceForDynamometer);

		mCmbVehicleSpeedSourceForDynamometer = new JComboBox();
		GridBagConstraints gbcCmbVehicleSpeedSourceForDynamometer = new GridBagConstraints();
		gbcCmbVehicleSpeedSourceForDynamometer.insets = new Insets(0, 0, 5, 0);
		gbcCmbVehicleSpeedSourceForDynamometer.fill = GridBagConstraints.HORIZONTAL;
		gbcCmbVehicleSpeedSourceForDynamometer.gridx = 1;
		gbcCmbVehicleSpeedSourceForDynamometer.gridy = 2;
		panel.add(mCmbVehicleSpeedSourceForDynamometer, gbcCmbVehicleSpeedSourceForDynamometer);

		JLabel lblTimeZone = new JLabel("Time Zone:");
		GridBagConstraints gbc_lblTimeZone = new GridBagConstraints();
		gbc_lblTimeZone.anchor = GridBagConstraints.WEST;
		gbc_lblTimeZone.insets = new Insets(0, 0, 5, 5);
		gbc_lblTimeZone.gridx = 0;
		gbc_lblTimeZone.gridy = 3;
		panel.add(lblTimeZone, gbc_lblTimeZone);

		mCmbTimeZone = new JComboBox();
		GridBagConstraints gbc_mCmbTimeZone = new GridBagConstraints();
		gbc_mCmbTimeZone.insets = new Insets(0, 0, 5, 0);
		gbc_mCmbTimeZone.fill = GridBagConstraints.HORIZONTAL;
		gbc_mCmbTimeZone.gridx = 1;
		gbc_mCmbTimeZone.gridy = 3;
		panel.add(mCmbTimeZone, gbc_mCmbTimeZone);

		JLabel lblRealTimeClockSync = new JLabel("Real time clock sync:");
		GridBagConstraints gbcLblRealTimeClockSync = new GridBagConstraints();
		gbcLblRealTimeClockSync.anchor = GridBagConstraints.WEST;
		gbcLblRealTimeClockSync.insets = new Insets(0, 0, 5, 5);
		gbcLblRealTimeClockSync.gridx = 0;
		gbcLblRealTimeClockSync.gridy = 4;
		panel.add(lblRealTimeClockSync, gbcLblRealTimeClockSync);

		mChkRealTimeClockSync = new JCheckBox("Sync with GPS");
		GridBagConstraints gbcChkRealTimeClockSync = new GridBagConstraints();
		gbcChkRealTimeClockSync.anchor = GridBagConstraints.WEST;
		gbcChkRealTimeClockSync.insets = new Insets(0, 0, 5, 0);
		gbcChkRealTimeClockSync.gridx = 1;
		gbcChkRealTimeClockSync.gridy = 4;
		panel.add(mChkRealTimeClockSync, gbcChkRealTimeClockSync);

		JLabel lblRealTimeClockCompensation = new JLabel("Real time clock compensation:");
		GridBagConstraints gbc_lblRealTimeClockCompensation = new GridBagConstraints();
		gbc_lblRealTimeClockCompensation.insets = new Insets(0, 0, 5, 5);
		gbc_lblRealTimeClockCompensation.anchor = GridBagConstraints.WEST;
		gbc_lblRealTimeClockCompensation.gridx = 0;
		gbc_lblRealTimeClockCompensation.gridy = 5;
		panel.add(lblRealTimeClockCompensation, gbc_lblRealTimeClockCompensation);

		mTxtRealTimeClockCompensation = new JTextField();
		GridBagConstraints gbc_mTxtRtcCompensate = new GridBagConstraints();
		gbc_mTxtRtcCompensate.insets = new Insets(0, 0, 5, 0);
		gbc_mTxtRtcCompensate.fill = GridBagConstraints.HORIZONTAL;
		gbc_mTxtRtcCompensate.gridx = 1;
		gbc_mTxtRtcCompensate.gridy = 5;
		panel.add(mTxtRealTimeClockCompensation, gbc_mTxtRtcCompensate);
		mTxtRealTimeClockCompensation.setColumns(10);

		JLabel lblConfigurationSerialPort = new JLabel("Configuration serial port on main connector RX/TX pins:");
		GridBagConstraints gbc_lblConfigurationSerialPort = new GridBagConstraints();
		gbc_lblConfigurationSerialPort.anchor = GridBagConstraints.WEST;
		gbc_lblConfigurationSerialPort.insets = new Insets(0, 0, 5, 5);
		gbc_lblConfigurationSerialPort.gridx = 0;
		gbc_lblConfigurationSerialPort.gridy = 6;
		panel.add(lblConfigurationSerialPort, gbc_lblConfigurationSerialPort);

		mChkConfigurationSerialPortOnMainConnector = new JCheckBox("Yes");
		GridBagConstraints gbc_mChkConfigurationSerialPortOnMainConnector = new GridBagConstraints();
		gbc_mChkConfigurationSerialPortOnMainConnector.anchor = GridBagConstraints.WEST;
		gbc_mChkConfigurationSerialPortOnMainConnector.insets = new Insets(0, 0, 5, 0);
		gbc_mChkConfigurationSerialPortOnMainConnector.gridx = 1;
		gbc_mChkConfigurationSerialPortOnMainConnector.gridy = 6;
		panel.add(mChkConfigurationSerialPortOnMainConnector, gbc_mChkConfigurationSerialPortOnMainConnector);

		return panel;
	}

	/**
	 * @return A panel with the vehicle settings layout
	 */
	private JComponent buildVehiclePanel() {
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));

		GridBagLayout gblPanel = new GridBagLayout();
		gblPanel.columnWidths = new int[] { 0, 0, 0, 0 };
		gblPanel.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gblPanel.columnWeights = new double[] { 0.0, 0.9, 0.1, Double.MIN_VALUE };
		gblPanel.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0 };
		panel.setLayout(gblPanel);

		JLabel lblVehicleWeight = new JLabel("Vehicle weight:");
		GridBagConstraints gbc_lblVehicleWeight = new GridBagConstraints();
		gbc_lblVehicleWeight.anchor = GridBagConstraints.WEST;
		gbc_lblVehicleWeight.insets = new Insets(0, 0, 5, 5);
		gbc_lblVehicleWeight.gridx = 0;
		gbc_lblVehicleWeight.gridy = 0;
		panel.add(lblVehicleWeight, gbc_lblVehicleWeight);

		mTxtVehicleWeight = new JFormattedTextField();
		GridBagConstraints gbcTxtVehicleWeight = new GridBagConstraints();
		gbcTxtVehicleWeight.anchor = GridBagConstraints.BELOW_BASELINE;
		gbcTxtVehicleWeight.insets = new Insets(0, 0, 5, 5);
		gbcTxtVehicleWeight.fill = GridBagConstraints.HORIZONTAL;
		gbcTxtVehicleWeight.gridx = 1;
		gbcTxtVehicleWeight.gridy = 0;
		panel.add(mTxtVehicleWeight, gbcTxtVehicleWeight);

		mCmbVehicleWeightUnit = new JComboBox();
		GridBagConstraints gbc_cmbVehicleWeightUnit = new GridBagConstraints();
		gbc_cmbVehicleWeightUnit.insets = new Insets(0, 0, 5, 0);
		gbc_cmbVehicleWeightUnit.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbVehicleWeightUnit.gridx = 2;
		gbc_cmbVehicleWeightUnit.gridy = 0;
		panel.add(mCmbVehicleWeightUnit, gbc_cmbVehicleWeightUnit);

		JLabel lblCoefficientOfDrag = new JLabel("Coefficient of drag:");
		GridBagConstraints gbcLblCoefficientOfDrag = new GridBagConstraints();
		gbcLblCoefficientOfDrag.anchor = GridBagConstraints.WEST;
		gbcLblCoefficientOfDrag.insets = new Insets(0, 0, 5, 5);
		gbcLblCoefficientOfDrag.gridx = 0;
		gbcLblCoefficientOfDrag.gridy = 1;
		panel.add(lblCoefficientOfDrag, gbcLblCoefficientOfDrag);

		mTxtCoefficientOfDrag = new JFormattedTextField();
		GridBagConstraints gbcTxtCoefficientOfDrag = new GridBagConstraints();
		gbcTxtCoefficientOfDrag.anchor = GridBagConstraints.BELOW_BASELINE;
		gbcTxtCoefficientOfDrag.insets = new Insets(0, 0, 5, 5);
		gbcTxtCoefficientOfDrag.fill = GridBagConstraints.HORIZONTAL;
		gbcTxtCoefficientOfDrag.gridx = 1;
		gbcTxtCoefficientOfDrag.gridy = 1;
		panel.add(mTxtCoefficientOfDrag, gbcTxtCoefficientOfDrag);

		JLabel lblCoefficientOfRollingResistance = new JLabel("Coefficient of rolling resistance:");
		GridBagConstraints gbcLblCoefficientOfRollingResistance = new GridBagConstraints();
		gbcLblCoefficientOfRollingResistance.anchor = GridBagConstraints.WEST;
		gbcLblCoefficientOfRollingResistance.insets = new Insets(0, 0, 5, 5);
		gbcLblCoefficientOfRollingResistance.gridx = 0;
		gbcLblCoefficientOfRollingResistance.gridy = 2;
		panel.add(lblCoefficientOfRollingResistance, gbcLblCoefficientOfRollingResistance);

		mTxtCoefficientOfRollingResistance = new JFormattedTextField();
		GridBagConstraints gbcTxtCoefficientOfRollingResistance = new GridBagConstraints();
		gbcTxtCoefficientOfRollingResistance.anchor = GridBagConstraints.BELOW_BASELINE;
		gbcTxtCoefficientOfRollingResistance.insets = new Insets(0, 0, 5, 5);
		gbcTxtCoefficientOfRollingResistance.fill = GridBagConstraints.HORIZONTAL;
		gbcTxtCoefficientOfRollingResistance.gridx = 1;
		gbcTxtCoefficientOfRollingResistance.gridy = 2;
		panel.add(mTxtCoefficientOfRollingResistance, gbcTxtCoefficientOfRollingResistance);

		JLabel lblFrontalArea = new JLabel("Frontal area:");
		GridBagConstraints gbcLblFrontalArea = new GridBagConstraints();
		gbcLblFrontalArea.anchor = GridBagConstraints.WEST;
		gbcLblFrontalArea.insets = new Insets(0, 0, 5, 5);
		gbcLblFrontalArea.gridx = 0;
		gbcLblFrontalArea.gridy = 3;
		panel.add(lblFrontalArea, gbcLblFrontalArea);

		mTxtFrontalArea = new JFormattedTextField();
		GridBagConstraints gbc_txtFrontalArea = new GridBagConstraints();
		gbc_txtFrontalArea.anchor = GridBagConstraints.BELOW_BASELINE;
		gbc_txtFrontalArea.insets = new Insets(0, 0, 5, 5);
		gbc_txtFrontalArea.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFrontalArea.gridx = 1;
		gbc_txtFrontalArea.gridy = 3;
		panel.add(mTxtFrontalArea, gbc_txtFrontalArea);

		mCmbFrontalAreaUnit = new JComboBox();
		GridBagConstraints gbc_cmbFrontalAreaUnit = new GridBagConstraints();
		gbc_cmbFrontalAreaUnit.insets = new Insets(0, 0, 5, 0);
		gbc_cmbFrontalAreaUnit.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbFrontalAreaUnit.gridx = 2;
		gbc_cmbFrontalAreaUnit.gridy = 3;
		panel.add(mCmbFrontalAreaUnit, gbc_cmbFrontalAreaUnit);

		return panel;
	}

	/**
	 * @return A panel with the stepper settings layout
	 */
	private JPanel buildStepperPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0, 0.0, 0.0, 0, 0.0, 1.0 };

		JPanel pnlMain = new JPanel();
		pnlMain.setBorder(new EmptyBorder(10, 10, 10, 10));
		pnlMain.setLayout(gridBagLayout);

		JLabel lblTotalNumberOfSteps = new JLabel("Total number of steps:");
		GridBagConstraints gbcLblTotalNumberOfSteps = new GridBagConstraints();
		gbcLblTotalNumberOfSteps.anchor = GridBagConstraints.WEST;
		gbcLblTotalNumberOfSteps.insets = new Insets(0, 0, 5, 5);
		gbcLblTotalNumberOfSteps.gridx = 0;
		gbcLblTotalNumberOfSteps.gridy = 0;
		pnlMain.add(lblTotalNumberOfSteps, gbcLblTotalNumberOfSteps);

		mTxtTotalNumberOfSteps = new JTextField();
		GridBagConstraints gbcTxtTotalNumberOfSteps = new GridBagConstraints();
		gbcTxtTotalNumberOfSteps.insets = new Insets(0, 0, 5, 0);
		gbcTxtTotalNumberOfSteps.fill = GridBagConstraints.HORIZONTAL;
		gbcTxtTotalNumberOfSteps.gridx = 1;
		gbcTxtTotalNumberOfSteps.gridy = 0;
		pnlMain.add(mTxtTotalNumberOfSteps, gbcTxtTotalNumberOfSteps);
		mTxtTotalNumberOfSteps.setColumns(10);

		JLabel lblMaximumSpeed = new JLabel("Maximum speed (in steps per second):");
		GridBagConstraints gbcLblMaximumSpeed = new GridBagConstraints();
		gbcLblMaximumSpeed.anchor = GridBagConstraints.WEST;
		gbcLblMaximumSpeed.insets = new Insets(0, 0, 5, 5);
		gbcLblMaximumSpeed.gridx = 0;
		gbcLblMaximumSpeed.gridy = 1;
		pnlMain.add(lblMaximumSpeed, gbcLblMaximumSpeed);

		mTxtMaximumSpeed = new JTextField();
		GridBagConstraints gbcTxtMaximumSpeed = new GridBagConstraints();
		gbcTxtMaximumSpeed.insets = new Insets(0, 0, 5, 0);
		gbcTxtMaximumSpeed.fill = GridBagConstraints.HORIZONTAL;
		gbcTxtMaximumSpeed.gridx = 1;
		gbcTxtMaximumSpeed.gridy = 1;
		pnlMain.add(mTxtMaximumSpeed, gbcTxtMaximumSpeed);
		mTxtMaximumSpeed.setColumns(10);

		JLabel lblAcceleration = new JLabel("Acceleration (in steps per second�):");
		GridBagConstraints gbcLblAcceleration = new GridBagConstraints();
		gbcLblAcceleration.anchor = GridBagConstraints.WEST;
		gbcLblAcceleration.insets = new Insets(0, 0, 5, 5);
		gbcLblAcceleration.gridx = 0;
		gbcLblAcceleration.gridy = 2;
		pnlMain.add(lblAcceleration, gbcLblAcceleration);

		mTxtAcceleration = new JTextField();
		GridBagConstraints gbcTxtAcceleration = new GridBagConstraints();
		gbcTxtAcceleration.insets = new Insets(0, 0, 5, 0);
		gbcTxtAcceleration.fill = GridBagConstraints.HORIZONTAL;
		gbcTxtAcceleration.gridx = 1;
		gbcTxtAcceleration.gridy = 2;
		pnlMain.add(mTxtAcceleration, gbcTxtAcceleration);
		mTxtAcceleration.setColumns(10);

		JLabel lblHomingDirection = new JLabel("Homing direction:");
		GridBagConstraints gbcLblHomingDirection = new GridBagConstraints();
		gbcLblHomingDirection.anchor = GridBagConstraints.WEST;
		gbcLblHomingDirection.insets = new Insets(0, 0, 5, 5);
		gbcLblHomingDirection.gridx = 0;
		gbcLblHomingDirection.gridy = 3;
		pnlMain.add(lblHomingDirection, gbcLblHomingDirection);

		GridBagConstraints gbcCmbHomingDirection = new GridBagConstraints();
		gbcCmbHomingDirection.insets = new Insets(0, 0, 5, 0);
		gbcCmbHomingDirection.fill = GridBagConstraints.HORIZONTAL;
		gbcCmbHomingDirection.gridx = 1;
		gbcCmbHomingDirection.gridy = 3;
		pnlMain.add(mCmbHomingDirection, gbcCmbHomingDirection);

		JLabel lblChannel = new JLabel("Channel:");
		GridBagConstraints gbcLblChannel = new GridBagConstraints();
		gbcLblChannel.anchor = GridBagConstraints.WEST;
		gbcLblChannel.insets = new Insets(0, 0, 5, 5);
		gbcLblChannel.gridx = 0;
		gbcLblChannel.gridy = 4;
		pnlMain.add(lblChannel, gbcLblChannel);

		GridBagConstraints gbcCmbChannel = new GridBagConstraints();
		gbcCmbChannel.insets = new Insets(0, 0, 5, 0);
		gbcCmbChannel.fill = GridBagConstraints.HORIZONTAL;
		gbcCmbChannel.gridx = 1;
		gbcCmbChannel.gridy = 4;
		pnlMain.add(mCmbChannel, gbcCmbChannel);

		return pnlMain;
	}

	/**
	 * Prepare the dialog properties
	 */
	private void prepareDialog() {
		setSize(540, 350);
		setLocationRelativeTo(null);
		setTitle(DIALOG_TITLE);
		setIconImage(new ImageIcon(getClass().getClassLoader().getResource("DIYECMGauge.png")).getImage());
		setVisible(true);
	}

	/**
	 * Set the currently selected tab
	 * 
	 * @param tab The tab to be selected
	 */
	public void setSelectedTab(SettingsTab tab) {
		mTabs.setSelectedIndex(tab.ordinal());
	}
}
