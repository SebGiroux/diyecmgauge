package com.sgiroux.diyecmgauge.progressmonitor;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Wrapper for progress dialog to allow for code sharing between the Android application and the desktop application
 * 
 * @author Seb
 * 
 */
public class ProgressMonitorDialog extends ProgressMonitorBase {
	private JDialog mDialog;
	private JLabel mLblDescription;
	private JLabel mLblProgressMessage;
	private JProgressBar mProgressBar;

	@Override
	public void setParent(Object parent) {
		mDialog = new JDialog((JFrame) parent);

		prepareUI();
		prepareDialog();
	}

	private void prepareDialog() {
		mDialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mDialog.setUndecorated(true);
		mDialog.setAlwaysOnTop(true);
		mDialog.setSize(500, 120);
		mDialog.setLocationRelativeTo(null);
	}

	private void prepareUI() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0, 0, 0, 0 };

		JPanel pnlMain = new JPanel();
		pnlMain.setBorder(new EmptyBorder(10, 10, 10, 10));
		pnlMain.setLayout(gridBagLayout);
		mDialog.getContentPane().add(pnlMain);

		mLblDescription = new JLabel();
		GridBagConstraints gbcLblDescription = new GridBagConstraints();
		gbcLblDescription.anchor = GridBagConstraints.WEST;
		gbcLblDescription.insets = new Insets(0, 0, 5, 0);
		gbcLblDescription.gridx = 0;
		gbcLblDescription.gridy = 0;
		pnlMain.add(mLblDescription, gbcLblDescription);

		mLblProgressMessage = new JLabel();
		GridBagConstraints gbcLblProgressMessage = new GridBagConstraints();
		gbcLblProgressMessage.anchor = GridBagConstraints.WEST;
		gbcLblProgressMessage.insets = new Insets(0, 0, 5, 0);
		gbcLblProgressMessage.gridx = 0;
		gbcLblProgressMessage.gridy = 1;
		pnlMain.add(mLblProgressMessage, gbcLblProgressMessage);

		mProgressBar = new JProgressBar();
		mProgressBar.setStringPainted(true);
		mProgressBar.setMinimum(0);
		mProgressBar.setMaximum(100);
		GridBagConstraints gbcProgressBar = new GridBagConstraints();
		gbcProgressBar.fill = GridBagConstraints.HORIZONTAL;
		gbcProgressBar.insets = new Insets(0, 0, 5, 0);
		gbcProgressBar.gridx = 0;
		gbcProgressBar.gridy = 2;
		pnlMain.add(mProgressBar, gbcProgressBar);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (mCancelListener != null) {
					mCancelListener.onProgressMonitorCancel();
				}

				mDialog.dispose();
			}
		});
		GridBagConstraints gbcBtnCancel = new GridBagConstraints();
		gbcBtnCancel.gridx = 0;
		gbcBtnCancel.gridy = 3;
		pnlMain.add(btnCancel, gbcBtnCancel);

		mDialog.getRootPane().setBorder(BorderFactory.createLineBorder(Color.GRAY));
	}

	@Override
	public void setDescription(String description) {
		mLblDescription.setText(description);
	}

	@Override
	public void setProgressMessage(String progressMessage) {
		mLblProgressMessage.setText(progressMessage);
	}

	@Override
	public void setProgress(int value) {
		if (!mDialog.isVisible()) {
			mDialog.setVisible(true);
		}

		mProgressBar.setValue(value);

		// Check if we're done
		if (value == mProgressBar.getMaximum()) {
			if (mCompletedListener != null) {
				mCompletedListener.onProgressMonitorCompleted();
			}

			mDialog.dispose();
		}
	}

	@Override
	public void setVisible(boolean visible) {
		mDialog.setVisible(visible);
	}
}
