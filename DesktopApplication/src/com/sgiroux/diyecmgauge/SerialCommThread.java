package com.sgiroux.diyecmgauge;

import java.util.ArrayList;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import com.sgiroux.diyecmgauge.serial.SerialCommThreadBase;
import com.sgiroux.diyecmgauge.serial.SerialCommand;
import com.sgiroux.diyecmgauge.serial.SerialResponseReceivedListener;
import com.sgiroux.diyecmgauge.serial.SerialResponseTimeoutListener;
import com.sgiroux.diyecmgauge.serial.SerialUtils;

public class SerialCommThread extends SerialCommThreadBase implements SerialPortEventListener {
	private SerialPort mSerialPort;

	/**
	 * Write an array of byte to the serial line for the specified serial command and arguments
	 * 
	 * @param serialCommand The serial command to be written
	 * @param arguments The arguments of the serial command to be written
	 */
	@Override
	public void write(SerialCommand serialCommand, String arguments) {
		byte[] command = SerialUtils.prepareCommand(serialCommand, arguments);

		try {
			mSerialPort.writeBytes(command);
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set the serial port used that will be used by this thread
	 * 
	 * @param serialPort The serial port to read / write to
	 */
	public void setSerialPort(SerialPort serialPort) {
		mSerialPort = serialPort;
	}

	/**
	 * Handle an event on the serial port. Read the data and add it to the buffer.
	 * 
	 * @param oEvent
	 */
	@Override
	public void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.isRXCHAR()) {
			try {
				byte data[] = mSerialPort.readBytes();
				for (int i = 0; i < data.length; i++) {
					addByteToBuffer(data[i]);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Trigger all the listeners to tell them we got the response for a serial command that we sent
	 * 
	 * @param buffer The buffer of the response that will be sent to the listeners
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void triggerAllSerialResponseReceivedListeners(final ArrayList<Byte> buffer) {
		SerialCommand serialCommand = getSerialCommandInBuffer();

		if (serialCommand != null) {
			ArrayList<SerialResponseReceivedListener> responseReceivedListeners = (ArrayList<SerialResponseReceivedListener>) DIYECMGauge.getInstance().getSerialResponseReceivedListeners().clone();
			for (SerialResponseReceivedListener responseReceivedListener : responseReceivedListeners) {
				responseReceivedListener.onResponseReceived(serialCommand, SerialUtils.convertArrayListToByteArray(buffer));
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void triggerAllSerialResponseTimeoutListeners(final SerialCommand serialCommand) {
		ArrayList<SerialResponseTimeoutListener> timeoutListeners = (ArrayList<SerialResponseTimeoutListener>) DIYECMGauge.getInstance().getSerialResponseTimeoutListeners().clone();
		for (SerialResponseTimeoutListener timeoutListener : timeoutListeners) {
			timeoutListener.onResponseTimeout(serialCommand);
		}
	}
}
