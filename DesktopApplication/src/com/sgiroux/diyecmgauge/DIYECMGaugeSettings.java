package com.sgiroux.diyecmgauge;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/**
 * Singleton class for the application settings
 * 
 * @author Seb
 * 
 */
public class DIYECMGaugeSettings {
	private static final String SETTINGS_FILE_NAME = "settings.xml";
	private Properties mSettingsProps = new Properties();

	public static final String COMM_PORT = "comm_port";
	public static final String LAST_CONFIG_DIRECTORY = "last_config_directory";

	private static DIYECMGaugeSettings mInstance;

	/**
	 * Singleton method to get a single instance of this class
	 * 
	 * @return An instance of this class
	 */
	public static DIYECMGaugeSettings getInstance() {
		if (mInstance == null) {
			mInstance = new DIYECMGaugeSettings();
		}

		return mInstance;
	}

	/**
	 * Called to initialize the settings file
	 */
	public void initialize() {
		mInstance.load();
	}

	/**
	 * Load the settings files into the settings properties object
	 */
	private void load() {
		File settingsFile = new File(SETTINGS_FILE_NAME);
		if (!settingsFile.exists()) {
			try {
				if (settingsFile.createNewFile()) {
					System.out.println("Successfully created " + SETTINGS_FILE_NAME);
				}
				else {
					System.out.println("Failed to create " + SETTINGS_FILE_NAME);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			FileInputStream fis = new FileInputStream(SETTINGS_FILE_NAME);
			mSettingsProps.loadFromXML(fis);
			fis.close();
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the value of a key from the settings file
	 * 
	 * @param key
	 * @return
	 */
	public String get(String key) {
		return mSettingsProps.getProperty(key);
	}

	/**
	 * Set a value to a key in the settings file
	 * 
	 * @param key
	 * @param value
	 */
	public void set(String key, String value) {
		mSettingsProps.setProperty(key, value);
		save();
	}

	/**
	 * Save the settings file with the current keys / values
	 */
	private void save() {
		try {
			FileOutputStream fos = new FileOutputStream(SETTINGS_FILE_NAME);
			mSettingsProps.storeToXML(fos, "");
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
